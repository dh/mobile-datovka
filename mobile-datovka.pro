QT += qml quick quickcontrols2 network xml sql widgets svg printsupport

TEMPLATE = app
APP_NAME = Datovka
GOOGLEPLAY_CODE_VERSION = 1

include(pri/version.pri)

DESCRIPTION = "Mobile client for ISDS"

CONFIG += object_parallel_to_source

DEFINES += \
    DEBUG=1 \
    VERSION=\\\"$${VERSION}\\\" \
    APP_NAME=\\\"$${APP_NAME}\\\"

QMAKE_CXXFLAGS += \
    -g -O0 -std=c++11 \
    -Wall -Wextra -pedantic \
    -Wdate-time -Wformat -Werror=format-security
SOURCES += \
    src/crypto/crypto.c \
    src/datovka_shared/crypto/crypto_trusted_certs.c \
    src/datovka_shared/crypto/crypto_pin.c \
    src/datovka_shared/crypto/crypto_pwd.c \
    src/datovka_shared/crypto/crypto_version.cpp \
    src/datovka_shared/crypto/crypto_wrapped.cpp
HEADERS += \
    src/crypto/crypto.h \
    src/datovka_shared/crypto/crypto_trusted_certs.h \
    src/datovka_shared/crypto/crypto_pin.h \
    src/datovka_shared/crypto/crypto_pwd.h \
    src/datovka_shared/crypto/crypto_version.h \
    src/datovka_shared/crypto/crypto_wrapped.h
LIBS += \
    -lcrypto

# Additional import path used to resolve
# QML modules in code model of Qt Creator
QML_IMPORT_PATH =

# Supported languages
LANGUAGES = en cs

TRANSLATIONS += res/locale/datovka_en.ts \
    res/locale/datovka_cs.ts

lupdate_only {
SOURCES += qml/*.qml \
    qml/components/*.qml \
    qml/dialogues/*.qml \
    qml/pages/*.qml \
    qml/wizards/*.qml
}

# Run lrelease to generate the qm files.
qtPrepareTool(LRELEASE, lrelease)
contains(QMAKE_HOST.os, Windows) {
    # Remove shell quotes and replace them with system quotes.
    LRELEASE=$$replace(LRELEASE, "'", "")
    LRELEASE=$$system_quote($$LRELEASE)
}
command = $$LRELEASE mobile-datovka.pro
system($$command)|error("Failed to run: $$command")

# Copy translation files at qmake invocation.
message(Copying Qt translation from $$system_path($$[QT_INSTALL_DATA]).)
system($$QMAKE_COPY $$system_path($$[QT_INSTALL_DATA]/translations/qtbase_cs.qm) $$system_path(res/locale/qtbase_cs.qm))
system($$QMAKE_COPY $$system_path($$[QT_INSTALL_DATA]/translations/qtbase_en.qm) $$system_path(res/locale/qtbase_en.qm))

TRANSLATIONS_FILES += \
    res/locale/qtbase_cs.qm \
    res/locale/qtbase_en.qm \
    res/locale/datovka_cs.qm \
    res/locale/datovka_en.qm

SOURCES += \
    src/auxiliaries/email_helper.cpp \
    src/auxiliaries/ios_helper.cpp \
    src/backup.cpp \
    src/datovka_shared/compat_qt/random.cpp \
    src/datovka_shared/gov_services/helper.cpp \
    src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp \
    src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp \
    src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp \
    src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp \
    src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp \
    src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp \
    src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp \
    src/datovka_shared/gov_services/service/gov_service_form_field.cpp \
    src/datovka_shared/gov_services/service/gov_service.cpp \
    src/datovka_shared/gov_services/service/gov_services_all.cpp \
    src/datovka_shared/gov_services/service/gov_szr_rob_vu.cpp \
    src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp \
    src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp \
    src/datovka_shared/graphics/graphics.cpp \
    src/datovka_shared/identifiers/account_id.cpp \
    src/datovka_shared/io/prefs_db.cpp \
    src/datovka_shared/io/prefs_db_tables.cpp \
    src/datovka_shared/io/records_management_db.cpp \
    src/datovka_shared/io/records_management_db_tables.cpp \
    src/datovka_shared/io/sqlite/db.cpp \
    src/datovka_shared/io/sqlite/db_single.cpp \
    src/datovka_shared/io/sqlite/table.cpp \
    src/datovka_shared/isds/account_interface.cpp \
    src/datovka_shared/isds/box_interface.cpp \
    src/datovka_shared/isds/box_interface2.cpp \
    src/datovka_shared/isds/error.cpp \
    src/datovka_shared/isds/generic_interface.cpp \
    src/datovka_shared/isds/internal_conversion.cpp \
    src/datovka_shared/isds/message_interface.cpp \
    src/datovka_shared/isds/type_conversion.cpp \
    src/datovka_shared/json/helper.cpp \
    src/datovka_shared/localisation/countries.cpp \
    src/datovka_shared/localisation/localisation.cpp \
    src/datovka_shared/log/global.cpp \
    src/datovka_shared/log/log.cpp \
    src/datovka_shared/log/log_c.cpp \
    src/datovka_shared/log/log_device.cpp \
    src/datovka_shared/log/memory_log.cpp \
    src/datovka_shared/records_management/conversion.cpp \
    src/datovka_shared/records_management/io/records_management_connection.cpp \
    src/datovka_shared/records_management/json/entry_error.cpp \
    src/datovka_shared/records_management/json/service_info.cpp \
    src/datovka_shared/records_management/json/stored_files.cpp \
    src/datovka_shared/records_management/json/upload_account_status.cpp \
    src/datovka_shared/records_management/json/upload_file.cpp \
    src/datovka_shared/records_management/json/upload_hierarchy.cpp \
    src/datovka_shared/records_management/models/upload_hierarchy_proxy_model.cpp \
    src/datovka_shared/settings/account.cpp \
    src/datovka_shared/settings/account_container.cpp \
    src/datovka_shared/settings/pin.cpp \
    src/datovka_shared/settings/prefs.cpp \
    src/datovka_shared/settings/prefs_helper.cpp \
    src/datovka_shared/settings/records_management.cpp \
    src/datovka_shared/utility/pdf_printer.cpp \
    src/datovka_shared/utility/strings.cpp \
    src/datovka_shared/worker/pool.cpp \
    src/dialogues/dialogues.cpp \
    src/dialogues/qml_dialogue_helper.cpp \
    src/dialogues/qml_input_dialogue.cpp \
    src/dialogues/qml_message_dialogue.cpp \
    src/dialogues/widget_input_dialogue.cpp \
    src/dialogues/widget_message_dialogue.cpp \
    src/files.cpp \
    src/font/font.cpp \
    src/global.cpp \
    src/gov_services/models/gov_form_list_model.cpp \
    src/gov_services/models/gov_service_list_model.cpp \
    src/gov_wrapper.cpp \
    src/initialisation.cpp \
    src/io/filesystem.cpp \
    src/isds/conversion/isds_conversion.cpp \
    src/isds/conversion/isds_time_conversion.cpp \
    src/isds/conversion/isds_type_conversion.cpp \
    src/isds/interface/response_status.cpp \
    src/isds/io/connection.cpp \
    src/isds/isds_login.cpp \
    src/isds/isds_tasks.cpp \
    src/isds/isds_wrapper.cpp \
    src/isds/services/box_interface.cpp \
    src/isds/services/helper.cpp \
    src/isds/services/login_interface.cpp \
    src/isds/services/message_interface.cpp \
    src/isds/services/message_interface_offline.cpp \
    src/isds/session/isds_context.cpp \
    src/isds/session/isds_session.cpp \
    src/isds/session/isds_sessions.cpp \
    src/isds/xml/box_interface.cpp \
    src/isds/xml/cms.cpp \
    src/isds/xml/helper.cpp \
    src/isds/xml/login_interface.cpp \
    src/isds/xml/message_interface.cpp \
    src/isds/xml/response_status.cpp \
    src/isds/xml/services_message.cpp \
    src/isds/xml/soap.cpp \
    src/json/backup.cpp \
    src/locker.cpp \
    src/log.cpp \
    src/main.cpp \
    src/messages.cpp \
    src/models/accountmodel.cpp \
    src/models/backup_selection_model.cpp \
    src/models/databoxmodel.cpp \
    src/models/filemodel.cpp \
    src/models/list_sort_filter_proxy_model.cpp \
    src/models/messagemodel.cpp \
    src/net/db_wrapper.cpp \
    src/qml_identifiers/qml_account_id.cpp \
    src/qml_interaction/image_provider.cpp \
    src/qml_interaction/interaction_filesystem.cpp \
    src/qml_interaction/interaction_zfo_file.cpp \
    src/qml_interaction/message_info.cpp \
    src/qml_interaction/string_manipulation.cpp \
    src/qml_isds/message_interface.cpp \
    src/records_management/models/upload_hierarchy_list_model.cpp \
    src/records_management/models/upload_hierarchy_qml_proxy_model.cpp \
    src/records_management.cpp \
    src/settings.cpp \
    src/settings/account.cpp \
    src/settings/accounts.cpp \
    src/settings/convert_for_compatibility.cpp \
    src/settings/convert_to_prefs.cpp \
    src/settings/prefs_defaults.cpp \
    src/settings/prefs_specific.cpp \
    src/setwrapper.cpp \
    src/sqlite/account_db.cpp \
    src/sqlite/account_db_tables.cpp \
    src/sqlite/dbs.cpp \
    src/sqlite/db_tables.cpp \
    src/sqlite/file_db_container.cpp \
    src/sqlite/file_db.cpp \
    src/sqlite/file_db_tables.cpp \
    src/sqlite/message_db_container.cpp \
    src/sqlite/message_db.cpp \
    src/sqlite/message_db_tables.cpp \
    src/sqlite/zfo_db.cpp \
    src/sqlite/zfo_db_tables.cpp \
    src/worker/task_change_password.cpp \
    src/worker/task_download_account_info.cpp \
    src/worker/task_download_delivery_info.cpp \
    src/worker/task_download_dt_info.cpp \
    src/worker/task_download_message.cpp \
    src/worker/task_download_message_list.cpp \
    src/worker/task_find_databox.cpp \
    src/worker/task_find_databox_fulltext.cpp \
    src/worker/task_import_zfo.cpp \
    src/worker/task_keep_alive.cpp \
    src/worker/task_records_management_stored_messages.cpp \
    src/worker/task_send_message.cpp \
    src/worker/task_send_sms.cpp \
    src/wrap_accounts.cpp \
    src/zfo.cpp

HEADERS += \
    src/auxiliaries/email_helper.h \
    src/auxiliaries/ios_helper.h \
    src/backup.h \
    src/common.h \
    src/datovka_shared/compat_qt/misc.h \
    src/datovka_shared/compat_qt/random.h \
    src/datovka_shared/gov_services/helper.h \
    src/datovka_shared/gov_services/service/gov_mv_crr_vbh.h \
    src/datovka_shared/gov_services/service/gov_mv_ir_vp.h \
    src/datovka_shared/gov_services/service/gov_mv_rt_vt.h \
    src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.h \
    src/datovka_shared/gov_services/service/gov_mv_skd_vp.h \
    src/datovka_shared/gov_services/service/gov_mv_vr_vp.h \
    src/datovka_shared/gov_services/service/gov_mv_zr_vp.h \
    src/datovka_shared/gov_services/service/gov_service_form_field.h \
    src/datovka_shared/gov_services/service/gov_service.h \
    src/datovka_shared/gov_services/service/gov_services_all.h \
    src/datovka_shared/gov_services/service/gov_szr_rob_vu.h \
    src/datovka_shared/gov_services/service/gov_szr_rob_vvu.h \
    src/datovka_shared/gov_services/service/gov_szr_ros_vv.h \
    src/datovka_shared/graphics/graphics.h \
    src/datovka_shared/identifiers/account_id.h \
    src/datovka_shared/identifiers/account_id_p.h \
    src/datovka_shared/io/prefs_db.h \
    src/datovka_shared/io/prefs_db_tables.h \
    src/datovka_shared/io/records_management_db.h \
    src/datovka_shared/io/records_management_db_tables.h \
    src/datovka_shared/io/db_tables.h \
    src/datovka_shared/io/sqlite/db.h \
    src/datovka_shared/io/sqlite/db_single.h \
    src/datovka_shared/io/sqlite/table.h \
    src/datovka_shared/isds/account_interface.h \
    src/datovka_shared/isds/box_interface.h \
    src/datovka_shared/isds/box_interface2.h \
    src/datovka_shared/isds/error.h \
    src/datovka_shared/isds/generic_interface.h \
    src/datovka_shared/isds/internal_conversion.h \
    src/datovka_shared/isds/message_interface.h \
    src/datovka_shared/isds/type_conversion.h \
    src/datovka_shared/isds/types.h \
    src/datovka_shared/json/helper.h \
    src/datovka_shared/localisation/countries.h \
    src/datovka_shared/localisation/localisation.h \
    src/datovka_shared/log/global.h \
    src/datovka_shared/log/log_c.h \
    src/datovka_shared/log/log_common.h \
    src/datovka_shared/log/log_device.h \
    src/datovka_shared/log/log.h \
    src/datovka_shared/log/memory_log.h \
    src/datovka_shared/records_management/conversion.h \
    src/datovka_shared/records_management/io/records_management_connection.h \
    src/datovka_shared/records_management/json/entry_error.h \
    src/datovka_shared/records_management/json/service_info.h \
    src/datovka_shared/records_management/json/stored_files.h \
    src/datovka_shared/records_management/json/upload_account_status.h \
    src/datovka_shared/records_management/json/upload_file.h \
    src/datovka_shared/records_management/json/upload_hierarchy.h \
    src/datovka_shared/records_management/models/upload_hierarchy_proxy_model.h \
    src/datovka_shared/settings/account.h \
    src/datovka_shared/settings/account_container.h \
    src/datovka_shared/settings/account_p.h \
    src/datovka_shared/settings/pin.h \
    src/datovka_shared/settings/prefs.h \
    src/datovka_shared/settings/prefs_helper.h \
    src/datovka_shared/settings/records_management.h \
    src/datovka_shared/utility/pdf_printer.h \
    src/datovka_shared/utility/strings.h \
    src/datovka_shared/worker/pool.h \
    src/dialogues/dialogues.h \
    src/dialogues/qml_dialogue_helper.h \
    src/dialogues/qml_input_dialogue.h \
    src/dialogues/qml_message_dialogue.h \
    src/dialogues/widget_input_dialogue.h \
    src/dialogues/widget_message_dialogue.h \
    src/files.h \
    src/font/font.h \
    src/global.h \
    src/gov_services/models/gov_form_list_model.h \
    src/gov_services/models/gov_service_list_model.h \
    src/gov_wrapper.h \
    src/initialisation.h \
    src/io/filesystem.h \
    src/isds/conversion/isds_conversion.h \
    src/isds/conversion/isds_time_conversion.h \
    src/isds/conversion/isds_type_conversion.h \
    src/isds/interface/response_status.h \
    src/isds/io/connection.h \
    src/isds/isds_const.h \
    src/isds/isds_login.h \
    src/isds/isds_tasks.h \
    src/isds/isds_wrapper.h \
    src/isds/services/box_interface.h \
    src/isds/services/helper.h \
    src/isds/services/login_interface.h \
    src/isds/services/message_interface.h \
    src/isds/services/message_interface_offline.h \
    src/isds/session/isds_context.h \
    src/isds/session/isds_session.h \
    src/isds/session/isds_sessions.h \
    src/isds/xml/box_interface.h \
    src/isds/xml/cms.h \
    src/isds/xml/helper.h \
    src/isds/xml/login_interface.h \
    src/isds/xml/message_interface.h \
    src/isds/xml/response_status.h \
    src/isds/xml/services_message.h \
    src/isds/xml/soap.h \
    src/json/backup.h \
    src/locker.h \
    src/log.h \
    src/messages.h \
    src/models/accountmodel.h \
    src/models/backup_selection_model.h \
    src/models/databoxmodel.h \
    src/models/filemodel.h \
    src/models/list_sort_filter_proxy_model.h \
    src/models/messagemodel.h \
    src/net/db_wrapper.h \
    src/qml_identifiers/qml_account_id.h \
    src/qml_interaction/image_provider.h \
    src/qml_interaction/interaction_filesystem.h \
    src/qml_interaction/interaction_zfo_file.h \
    src/qml_interaction/message_info.h \
    src/qml_interaction/string_manipulation.h \
    src/qml_isds/message_interface.h \
    src/records_management/models/upload_hierarchy_list_model.h \
    src/records_management/models/upload_hierarchy_qml_proxy_model.h \
    src/records_management.h \
    src/settings.h \
    src/settings/account.h \
    src/settings/accounts.h \
    src/settings/convert_for_compatibility.h \
    src/settings/convert_to_prefs.h \
    src/settings/prefs_defaults.h \
    src/settings/prefs_specific.h \
    src/setwrapper.h \
    src/sqlite/account_db.h \
    src/sqlite/account_db_tables.h \
    src/sqlite/dbs.h \
    src/sqlite/file_db_container.h \
    src/sqlite/file_db.h \
    src/sqlite/file_db_tables.h \
    src/sqlite/message_db_container.h \
    src/sqlite/message_db.h \
    src/sqlite/message_db_tables.h \
    src/sqlite/zfo_db.h \
    src/sqlite/zfo_db_tables.h \
    src/worker/emitter.h \
    src/worker/task.h \
    src/worker/task_change_password.h \
    src/worker/task_download_account_info.h \
    src/worker/task_download_delivery_info.h \
    src/worker/task_download_dt_info.h \
    src/worker/task_download_message.h \
    src/worker/task_download_message_list.h \
    src/worker/task_find_databox.h \
    src/worker/task_find_databox_fulltext.h \
    src/worker/task_import_zfo.h \
    src/worker/task_keep_alive.h \
    src/worker/task_records_management_stored_messages.h \
    src/worker/task_send_message.h \
    src/worker/task_send_sms.h \
    src/wrap_accounts.h \
    src/zfo.h

RESOURCES += \
    res/qml.qrc \
    res/translations.qrc

# Linux target.
linux {
    isEmpty(PREFIX) {
        PREFIX = "/usr/local"
    }

    BINDIR="$${PREFIX}/bin"
    DATADIR="$${PREFIX}/share"

    LOCALE_INST_DIR = "$${DATADIR}/$${APP_NAME}/localisations"

    target.path = "$${BINDIR}"

    desktop.path = "$${DATADIR}/applications"
    desktop.files += "linux/nic.cz.mobile-datovka.desktop"

    metainfo.path = "$${DATADIR}/metainfo"
    metainfo.files += "linux/cz.nic.datovka.mobile.metainfo.xml"

    icon64.path = "$${DATADIR}/icons/hicolor/64x64/apps"
    icon64.files += "res/datovka.png"

    localisation.path = "$${LOCALE_INST_DIR}"
    localisation.files += res/locale/datovka_cs.qm \
		res/locale/datovka_en.qm

    INSTALLS += target \
		desktop \
		metainfo \
		icon64
}

# iOS target.
ios {
    # Newer Qt for iOS does not support package printsupport
    QT -= printsupport
    system(sed -e "s/@VERSION@/$${VERSION}/g" "ios/Info.tmp" > "ios/Info.plist")
    include(ios/ios.pri)
}

# Android target.
android {

    exists(android/codeversion.pri) {
        include(android/codeversion.pri)
    }

    QT += androidextras

    SOURCES += \
        android/src/android_io.cpp \
        src/os_android.cpp

    HEADERS += \
        android/src/android_io.h \
        src/os_android.h

    versionAtLeast(QT_VERSION, 5.14.0) {
        # Newer Qt (since 5.14.0) requires different type of android manifest structure and higher minimum sdk version.
        contains(QMAKE_HOST.os, Windows) {
            system(sed.exe -e "s/@VERSION@/$${VERSION}/g" -e "s/@GOOGLEPLAY_CODE_VERSION@/$${GOOGLEPLAY_CODE_VERSION}/g" "android/AndroidManifestNew.tmp" > "android/AndroidManifest.xml")
        }
        contains(QMAKE_HOST.os, Linux) {
            system(sed -e "s/@VERSION@/$${VERSION}/g" -e "s/@GOOGLEPLAY_CODE_VERSION@/$${GOOGLEPLAY_CODE_VERSION}/g" "android/AndroidManifestNew.tmp" > "android/AndroidManifest.xml")
        }
        contains(QMAKE_HOST.os, Darwin) {
            system(sed -e "s/@VERSION@/$${VERSION}/g" -e "s/@GOOGLEPLAY_CODE_VERSION@/$${GOOGLEPLAY_CODE_VERSION}/g" "android/AndroidManifestNew.tmp" > "android/AndroidManifest.xml")
        }
    } else {

        # Android NDK r17c does not need the '-nostdlib++' option. Starting
        # from NDK r18 the tool chain uses Clang which needs this parameter.
        QMAKE_LINK += -nostdlib++

        # Older Qt use original manifest structure and lower minimum sdk version.
        contains(QMAKE_HOST.os, Windows) {
            system(sed.exe -e "s/@VERSION@/$${VERSION}/g" -e "s/@GOOGLEPLAY_CODE_VERSION@/$${GOOGLEPLAY_CODE_VERSION}/g" "android/AndroidManifest.tmp" > "android/AndroidManifest.xml")
        }
        contains(QMAKE_HOST.os, Linux) {
            system(sed -e "s/@VERSION@/$${VERSION}/g" -e "s/@GOOGLEPLAY_CODE_VERSION@/$${GOOGLEPLAY_CODE_VERSION}/g" "android/AndroidManifest.tmp" > "android/AndroidManifest.xml")
        }
        contains(QMAKE_HOST.os, Darwin) {
            system(sed -e "s/@VERSION@/$${VERSION}/g" -e "s/@GOOGLEPLAY_CODE_VERSION@/$${GOOGLEPLAY_CODE_VERSION}/g" "android/AndroidManifest.tmp" > "android/AndroidManifest.xml")
        }
    }

    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
    include(android/android.pri)

    OTHER_FILES += android/src/cz/nic/mobiledatovka/java/QFileProvider.java
}

OTHER_FILES +=
