#!/bin/bash

# OpenSSL build script for Android. Use OpenSSL 1.1.x version only.
# Usages: Set path to NDK and openssl version bellow and run script.
# Set path to your NDK location/directory
export ANDROID_NDK_HOME=~/android_tools/android-ndk-r20
# Use openssl version 1.1.x only. Older version 1.0.x does not supported by script!
OPENSSL_VERSION=1.1.1d


# Do not change these variables and other code!
ANDROID_API=21
ANDROID_LIB_ROOT=libs_openssl_1.1.x

rm -rf ${ANDROID_LIB_ROOT}

mkdir -p "${ANDROID_LIB_ROOT}/include/openssl"

if [ ! -f "openssl-$OPENSSL_VERSION.tar.gz" ]; then
    wget https://www.openssl.org/source/openssl-$OPENSSL_VERSION.tar.gz
fi

for arch in "arm64" "arm" "x86" "x86_64" 
do
    mkdir $ANDROID_LIB_ROOT/$arch
    rm -fr openssl-$OPENSSL_VERSION
    tar xfa openssl-$OPENSSL_VERSION.tar.gz
    cd openssl-$OPENSSL_VERSION

    case $arch in
        arm)
            ANDROID_API=16
            ;;
        x86)
            ANDROID_API=16
            ;;
        arm64)
            ANDROID_API=21
            ;;
        x86_64)
            ANDROID_API=21
            ;;
    esac
    ANDROID_TOOLCHAIN=""
    for host in "linux-x86_64" "linux-x86" "darwin-x86_64" "darwin-x86"
    do
        if [ -d "$ANDROID_NDK_HOME/toolchains/llvm/prebuilt/$host/bin" ]; then
            ANDROID_TOOLCHAIN="$ANDROID_NDK_HOME/toolchains/llvm/prebuilt/$host/bin"
            break
        fi
    done

    export PATH="$ANDROID_TOOLCHAIN":"$PATH"

    ./Configure shared android-${arch} -D__ANDROID_API__=${ANDROID_API} || exit 1
    make depend
    make -j$(nproc) SHLIB_VERSION_NUMBER= SHLIB_EXT=_1_1.so build_libs || exit 1
    llvm-strip -strip-all libcrypto_1_1.so
    llvm-strip -strip-all libssl_1_1.so

    cp libcrypto_1_1.so ../$ANDROID_LIB_ROOT/$arch
    cp libcrypto.a ../$ANDROID_LIB_ROOT/$arch

    cp libssl_1_1.so ../$ANDROID_LIB_ROOT/$arch
    cp libssl.a ../$ANDROID_LIB_ROOT/$arch

	# copy openssl header to libs
	cp -r -L include/openssl ../${ANDROID_LIB_ROOT}/include/

    cd ..

done

# delete openssl package and build folder
rm -rf openssl-$OPENSSL_VERSION
rm openssl-$OPENSSL_VERSION.tar.gz

echo "BUILD DONE!"
