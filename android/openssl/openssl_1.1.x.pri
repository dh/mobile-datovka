INCLUDEPATH += \
    $$PWD \
    src \
    android/openssl/libs_openssl_1.1.x/include/

versionAtLeast(QT_VERSION, "5.14.0") {

    equals(ANDROID_TARGET_ARCH, armeabi) {
        LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm/libcrypto.a \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm/libssl.a
    }
    equals(ANDROID_TARGET_ARCH, armeabi-v7a) {
        LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm/libcrypto.a \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm/libssl.a
    }
    equals(ANDROID_TARGET_ARCH, arm64-v8a) {
        LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm64/libcrypto.a \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm64/libssl.a
    }
    equals(ANDROID_TARGET_ARCH, x86) {
        LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86/libcrypto.a \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86/libssl.a
    }
    equals(ANDROID_TARGET_ARCH, x86_64) {
        LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86_64/libcrypto.a \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86_64/libssl.a
    }

    ANDROID_EXTRA_LIBS = \
    $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm/libcrypto_1_1.so \
    $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm/libssl_1_1.so \
    $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm64/libcrypto_1_1.so \
    $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm64/libssl_1_1.so \
    $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86/libcrypto_1_1.so \
    $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86/libssl_1_1.so \
    $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86_64/libcrypto_1_1.so \
    $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86_64/libssl_1_1.so

} else {

    equals(ANDROID_TARGET_ARCH, armeabi) {
        LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm/libcrypto.a \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm/libssl.a

        ANDROID_EXTRA_LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm/libcrypto_1_1.so \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm/libssl_1_1.so
    }
    equals(ANDROID_TARGET_ARCH, armeabi-v7a) {
        LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm/libcrypto.a \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm/libssl.a

        ANDROID_EXTRA_LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm/libcrypto_1_1.so \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm/libssl_1_1.so
    }
    equals(ANDROID_TARGET_ARCH, arm64-v8a) {
        LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm64/libcrypto.a \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm64/libssl.a

        ANDROID_EXTRA_LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm64/libcrypto_1_1.so \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/arm64/libssl_1_1.so \
    }
    equals(ANDROID_TARGET_ARCH, x86) {
        LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86/libcrypto.a \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86/libssl.a

        ANDROID_EXTRA_LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86/libcrypto_1_1.so \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86/libssl_1_1.so \
    }
    equals(ANDROID_TARGET_ARCH, x86_64) {
        LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86_64/libcrypto.a \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86_64/libssl.a

        ANDROID_EXTRA_LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86_64/libcrypto_1_1.so \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.1.x/x86_64/libssl_1_1.so
    }
}
