/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

package cz.nic.mobiledatovka.java;

import org.qtproject.qt5.android.QtNative;

import java.lang.String;
import android.content.Intent;
import java.io.File;
import android.net.Uri;
import android.util.Log;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.MediaStore;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileOutputStream;

import java.util.List;
import android.content.pm.ResolveInfo;
import java.util.ArrayList;
import android.content.pm.PackageManager;
import java.util.Comparator;
import java.util.Collections;
import android.content.Context;
import android.os.Parcelable;

import android.os.Build;

import android.support.v4.content.FileProvider;
import android.support.v4.app.ShareCompat;

import org.json.*;

public class QFileProvider
{
	// Authority defined in AndroidManifest.xml
	private static String AUTHORITY="cz.nic.mobiledatovka.fileprovider";

	protected QFileProvider() {}

	public static boolean isSDKVersion24OrNewest() {
		// 24 and newest use fileprovider only
		return (Build.VERSION.SDK_INT >= 24);
	}

	public static boolean startActivity(Intent theIntent, Uri uri) {

		final Context context = QtNative.activity();
		final PackageManager packageManager = context.getPackageManager();
		final boolean isLowerOrEqualsKitKat = Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT;

		ResolveInfo defaultAppInfo = packageManager.resolveActivity(theIntent, PackageManager.MATCH_DEFAULT_ONLY);
		if (defaultAppInfo == null) {
			return false;
		}

		List<ResolveInfo> appInfoList = packageManager.queryIntentActivities(theIntent, PackageManager.MATCH_DEFAULT_ONLY);
		if (appInfoList.isEmpty()) {
			return false;
		}

		Collections.sort(appInfoList, new Comparator<ResolveInfo>() {
		    @Override
		    public int compare(ResolveInfo first, ResolveInfo second) {
			String firstName = first.loadLabel(packageManager).toString();
			String secondName = second.loadLabel(packageManager).toString();
			return firstName.compareToIgnoreCase(secondName);
		    }
		});

		List<Intent> targetedIntents = new ArrayList<Intent>();

		for (ResolveInfo appInfo : appInfoList) {

			String targetPackageName = appInfo.activityInfo.packageName;
			if (targetPackageName.equals(context.getPackageName())) {
				continue;
			}

			Intent targetedIntent = new Intent(theIntent);
			targetedIntent.setPackage(targetPackageName);
			targetedIntents.add(targetedIntent);

			if (isLowerOrEqualsKitKat) {
				context.grantUriPermission(targetPackageName,
				   uri, Intent.FLAG_GRANT_READ_URI_PERMISSION
				       | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
			}
		}

		if (targetedIntents.isEmpty()) {
			return false;
		}

		Intent chooserIntent = Intent.createChooser(targetedIntents.remove(targetedIntents.size() - 1), "View File");
		if (targetedIntents.isEmpty()) {
		} else {
			chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedIntents.toArray(new Parcelable[] {}));
		}

		if (chooserIntent.resolveActivity(QtNative.activity().getPackageManager()) != null) {
			QtNative.activity().startActivity(chooserIntent);
			return true;
		}

		return false;
	}

	public static boolean viewFile(String filePath) {

		if (QtNative.activity() == null) {
			return false;
		}

		// Intent viewIntent = new Intent();
		Intent viewIntent = ShareCompat.IntentBuilder.from(QtNative.activity()).getIntent();
		viewIntent.setAction(Intent.ACTION_VIEW);

		File imageFileToShare = new File(filePath);

		Uri uri;
		try {
			uri = FileProvider.getUriForFile(QtNative.activity(), AUTHORITY, imageFileToShare);
		} catch (IllegalArgumentException e) {
			Log.d("Error viewFile - cannot be shared: ", filePath);
			return false;
		}

		Log.d("File path: ", filePath);
		Log.d("File content URI: ", uri.toString());

		String mimeType = QtNative.activity().getContentResolver().getType(uri);

		viewIntent.setDataAndType(uri, mimeType);

		viewIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		viewIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

		return startActivity(viewIntent, uri);
	}

	public static boolean sendEmail(String subject, String to, String body, String filePathsJson) {

		if (QtNative.activity() == null) {
			return false;
		}

		ArrayList<Uri> uris = new ArrayList<Uri>();
		try {
			JSONObject jsonObj = new JSONObject(filePathsJson);
			JSONArray attachments = jsonObj.getJSONArray("attachments");
			for (int i=0; i<attachments.length(); i++) {
				File attachment = new File(attachments.getString(i));
				if (!attachment.exists() || !attachment.canRead()) {
					System.out.println("FileProvider: Cannot read file " + attachment);
					return false;
				}
				Uri uri;
				try {
					uri = FileProvider.getUriForFile(QtNative.activity(), AUTHORITY, attachment);
					uris.add(uri);
				} catch (IllegalArgumentException ex) {
					System.out.println("FileProvider: " + ex.getMessage());
					return false;
				}
			}
		} catch (JSONException ex) {
			System.out.println("JSONException: " + ex.getMessage());
			return false;
		}

		Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
		emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		emailIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
		emailIntent.setType("plain/text");
		emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {to});
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, body);
		emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);

		try {
			Intent i = Intent.createChooser(emailIntent, "Send mail...");
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			QtNative.activity().startActivity(i);
		} catch (android.content.ActivityNotFoundException ex) {
			System.out.println("There are no email clients installed.");
			return false;
		} catch (java.lang.RuntimeException ex) {
			System.out.println("RuntimeException: " + ex.getMessage());
			return false;
		} catch (java.lang.Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
			return false;
		}

		System.out.println("Email has created.");
		return true;
	}
}
