
# Qt-5.12.4 comes with bundled OpenSSL-1.1.1, see:
# https://www.qt.io/blog/2019/06/17/qt-5-12-4-released-support-openssl-1-1-1
versionAtLeast(QT_VERSION, 5.12.4) {
    include(openssl/openssl_1.1.x.pri)
} else {
    include(openssl/openssl_1.0.x.pri)
}

DISTFILES += \
    $${_PRO_FILE_PWD_}/android/AndroidManifest.xml
