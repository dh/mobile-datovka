/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QByteArray>
#include <QFile>
#include <QObject>
#include <QString>
#include <QtTest/QtTest>

#include "src/isds/interface/response_status.h"
#include "src/isds/xml/response_status.h"
#include "tests/test_isds_xml_response_status.h"

static
QByteArray readFileData(const QString &fPath)
{
	QFile f(fPath);
	f.open(QIODevice::ReadOnly);

	QByteArray data = f.readAll();
	f.close();
	return data;
}

class TestIsdsXmlResponseStatus : public QObject {
	Q_OBJECT

public:
	TestIsdsXmlResponseStatus(void);

private slots:
	void initTestCase(void);

	void cleanupTestCase(void);

	void dbStatusFromData(void);

	void dbStatusDataOk(void);

	void dmStatusFromData(void);

	void dmStatusDataOk(void);

private:
	const QString m_soapGetOwnerInfoFromLoginResponsePath;
	const QString m_soapGetSignedDeliveryInfoResponsePath;
	const QString m_soapSignedMessageDownloadResponsePath;

	QByteArray m_ownerInfoResponse;
	QByteArray m_downloadMessageResponse;
	QByteArray m_downloadDeliveryInfoResponse;
};

TestIsdsXmlResponseStatus::TestIsdsXmlResponseStatus(void)
    : QObject(Q_NULLPTR),
    m_soapGetOwnerInfoFromLoginResponsePath("data/soap_GetOwnerInfoFromLoginResponse.xml"),
    m_soapGetSignedDeliveryInfoResponsePath("data/soap_GetSignedDeliveryInfoResponse_7534645.xml"),
    m_soapSignedMessageDownloadResponsePath("data/soap_SignedMessageDownloadResponse_7534645.xml"),
    m_ownerInfoResponse(),
    m_downloadMessageResponse(),
    m_downloadDeliveryInfoResponse()
{
}

void TestIsdsXmlResponseStatus::initTestCase(void)
{
	m_ownerInfoResponse =
	    readFileData(m_soapGetOwnerInfoFromLoginResponsePath);

	m_downloadMessageResponse =
	    readFileData(m_soapSignedMessageDownloadResponsePath);

	m_downloadDeliveryInfoResponse =
	    readFileData(m_soapGetSignedDeliveryInfoResponsePath);
}

void TestIsdsXmlResponseStatus::cleanupTestCase(void)
{
}

void TestIsdsXmlResponseStatus::dbStatusFromData(void)
{
	bool iOk = true;
	Isds::DbStatus dbStatus = Isds::Xml::toDbStatus(QByteArray(), &iOk);
	QVERIFY(iOk == false);
	QVERIFY(dbStatus.isNull());
	QVERIFY(dbStatus.code().isNull());
	QVERIFY(dbStatus.message().isNull());

	iOk = false;
	dbStatus = Isds::Xml::toDbStatus(m_ownerInfoResponse, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!dbStatus.isNull());
	QVERIFY(dbStatus.code() == QLatin1String("0000"));
	QVERIFY(dbStatus.message() == QStringLiteral("Provedeno úspěšně."));

	iOk = true;
	dbStatus = Isds::Xml::toDbStatus(m_downloadMessageResponse, &iOk);
	QVERIFY(iOk == false);
	QVERIFY(dbStatus.isNull());
	QVERIFY(dbStatus.code().isNull());
	QVERIFY(dbStatus.message().isNull());

	iOk = true;
	dbStatus = Isds::Xml::toDbStatus(m_downloadDeliveryInfoResponse, &iOk);
	QVERIFY(iOk == false);
	QVERIFY(dbStatus.isNull());
	QVERIFY(dbStatus.code().isNull());
	QVERIFY(dbStatus.message().isNull());
}

void TestIsdsXmlResponseStatus::dbStatusDataOk(void)
{
	const QLatin1String nonEmptyString("non-empty string");

	QString message = nonEmptyString;
	QVERIFY(!Isds::Xml::dbStatusOk(QByteArray(), message));
	QVERIFY(message.isNull());

	message.clear();
	QVERIFY(Isds::Xml::dbStatusOk(m_ownerInfoResponse, message));
	QVERIFY(message == QStringLiteral("Provedeno úspěšně."));

	message = nonEmptyString;
	QVERIFY(!Isds::Xml::dbStatusOk(m_downloadMessageResponse, message));
	QVERIFY(message.isNull());

	message = nonEmptyString;
	QVERIFY(!Isds::Xml::dbStatusOk(m_downloadDeliveryInfoResponse, message));
	QVERIFY(message.isNull());
}

void TestIsdsXmlResponseStatus::dmStatusFromData(void)
{
	bool iOk = true;
	Isds::DmStatus dmStatus = Isds::Xml::toDmStatus(QByteArray(), &iOk);
	QVERIFY(iOk == false);
	QVERIFY(dmStatus.isNull());
	QVERIFY(dmStatus.code().isNull());
	QVERIFY(dmStatus.message().isNull());

	iOk = true;
	dmStatus = Isds::Xml::toDmStatus(m_ownerInfoResponse, &iOk);
	QVERIFY(iOk == false);
	QVERIFY(dmStatus.isNull());
	QVERIFY(dmStatus.code().isNull());
	QVERIFY(dmStatus.message().isNull());

	iOk = false;
	dmStatus = Isds::Xml::toDmStatus(m_downloadMessageResponse, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!dmStatus.isNull());
	QVERIFY(dmStatus.code() == QLatin1String("0000"));
	QVERIFY(dmStatus.message() == QStringLiteral("Provedeno úspěšně."));

	iOk = false;
	dmStatus = Isds::Xml::toDmStatus(m_downloadDeliveryInfoResponse, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!dmStatus.isNull());
	QVERIFY(dmStatus.code() == QLatin1String("0000"));
	QVERIFY(dmStatus.message() == QStringLiteral("Provedeno úspěšně."));
}

void TestIsdsXmlResponseStatus::dmStatusDataOk(void)
{
	const QLatin1String nonEmptyString("non-empty string");

	QString message = nonEmptyString;
	QVERIFY(!Isds::Xml::dmStatusOk(QByteArray(), message));
	QVERIFY(message.isNull());

	message = nonEmptyString;
	QVERIFY(!Isds::Xml::dmStatusOk(m_ownerInfoResponse, message));
	QVERIFY(message.isNull());

	message.clear();
	QVERIFY(Isds::Xml::dmStatusOk(m_downloadMessageResponse, message));
	QVERIFY(message == QStringLiteral("Provedeno úspěšně."));

	message.clear();
	QVERIFY(Isds::Xml::dmStatusOk(m_downloadDeliveryInfoResponse, message));
	QVERIFY(message == QStringLiteral("Provedeno úspěšně."));
}

QObject *newTestIsdsXmlResponseStatus(void)
{
	return new (::std::nothrow) TestIsdsXmlResponseStatus();
}

//QTEST_MAIN(TestIsdsXmlResponseStatus)
#include "test_isds_xml_response_status.moc"
