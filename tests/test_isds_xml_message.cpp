/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QByteArray>
#include <QFile>
#include <QObject>
#include <QString>
#include <QtTest/QtTest>

#include "src/datovka_shared/isds/message_interface.h"
#include "src/isds/services/message_interface_offline.h"
#include "src/isds/xml/message_interface.h"
#include "tests/test_isds_xml_message.h"

static
QByteArray readFileData(const QString &fPath)
{
	QFile f(fPath);
	f.open(QIODevice::ReadOnly);

	QByteArray data = f.readAll();
	f.close();
	return data;
}

class TestIsdsXmlMessage : public QObject {
	Q_OBJECT

public:
	TestIsdsXmlMessage(void);

private slots:
	void initTestCase(void);

	void cleanupTestCase(void);

	void cmsZfoDd(void);

	void cmsContentDd(void);

	void cmsZfoDz(void);

	void cmsContentDz(void);

	void cmsZfoOdz(void);

	void cmsContentOdz(void);

	void authenticateMessageResponse(void);

	void getMessageAuthorResponse(void);

private:
	void dd(const QByteArray &ddContent,
	    enum Isds::Type::RawType expectedRawType);

	void dz(const QByteArray &dzContent,
	    enum Isds::Type::RawType expectedRawType);

	const QString m_ddZfoPath;
	const QString m_ddCmsContentPath;
	const QString m_dzZfoPath;
	const QString m_dzCmsContentPath;
	const QString m_odzZfoPath;
	const QString m_odzCmsContentPath;
	const QString m_authenticateMessageResponsePath;
	const QString m_getMessageAuthorResponsePath;
};

TestIsdsXmlMessage::TestIsdsXmlMessage(void)
    : QObject(Q_NULLPTR),
    m_ddZfoPath("data/DD_7534645.zfo"),
    m_ddCmsContentPath("data/cms_conten_dd_7534645.xml"),
    m_dzZfoPath("data/DDZ_7534645.zfo"),
    m_dzCmsContentPath("data/cms_conten_ddz_7534645.xml"),
    m_odzZfoPath("data/ODZ_7534645.zfo"),
    m_odzCmsContentPath("data/cms_conten_odz_7534645.xml"),
    m_authenticateMessageResponsePath("data/soap_AuthenticateMessageResponse.xml"),
    m_getMessageAuthorResponsePath("data/soap_GetMessageAuthorResponse.xml")
{
}

void TestIsdsXmlMessage::initTestCase(void)
{
}

void TestIsdsXmlMessage::cleanupTestCase(void)
{
}

void TestIsdsXmlMessage::cmsZfoDd(void)
{
	const QByteArray ddCmsData = readFileData(m_ddZfoPath);
	QVERIFY(ddCmsData.size() > 0);

	dd(ddCmsData, Isds::Type::RT_CMS_SIGNED_DELIVERYINFO);
}

void TestIsdsXmlMessage::cmsContentDd(void)
{
	QByteArray ddContent = readFileData(m_ddCmsContentPath);
	QVERIFY(ddContent.size() > 0);

	dd(ddContent, Isds::Type::RT_PLAIN_SIGNED_DELIVERYINFO);
}

void TestIsdsXmlMessage::cmsZfoDz(void)
{
	const QByteArray dzCmsData = readFileData(m_dzZfoPath);
	QVERIFY(dzCmsData.size() > 0);

	dz(dzCmsData, Isds::Type::RT_CMS_SIGNED_INCOMING_MESSAGE);
}

void TestIsdsXmlMessage::cmsContentDz(void)
{
	QByteArray dzContent = readFileData(m_dzCmsContentPath);
	QVERIFY(dzContent.size() > 0);

	dz(dzContent, Isds::Type::RT_PLAIN_SIGNED_INCOMING_MESSAGE);
}

void TestIsdsXmlMessage::cmsZfoOdz(void)
{
	const QByteArray odzCmsData = readFileData(m_odzZfoPath);
	QVERIFY(odzCmsData.size() > 0);

	dz(odzCmsData, Isds::Type::RT_CMS_SIGNED_OUTGOING_MESSAGE);
}

void TestIsdsXmlMessage::cmsContentOdz(void)
{
	QByteArray odzContent = readFileData(m_odzCmsContentPath);
	QVERIFY(odzContent.size() > 0);

	dz(odzContent, Isds::Type::RT_PLAIN_SIGNED_OUTGOING_MESSAGE);
}

void TestIsdsXmlMessage::authenticateMessageResponse(void)
{
	QByteArray response = readFileData(m_authenticateMessageResponsePath);
	QVERIFY(response.size() > 0);

	bool iOk = true;
	bool ret = Isds::Xml::readAuthenticateMessageResponse(QByteArray(), &iOk);
	QVERIFY(ret == false);
	QVERIFY(iOk == false);

	iOk = false;
	ret = Isds::Xml::readAuthenticateMessageResponse(response, &iOk);
	QVERIFY(ret == true);
	QVERIFY(iOk == true);
}

void TestIsdsXmlMessage::getMessageAuthorResponse(void)
{
	QByteArray response = readFileData(m_getMessageAuthorResponsePath);
	QVERIFY(response.size() > 0);

	enum Isds::Type::SenderType userType = Isds::Type::ST_PRIMARY;
	QString authorName("non-empty");
	bool iOk = Isds::Xml::readMessageAuthor(QByteArray(), userType, authorName);
	QVERIFY(iOk == false);
	QVERIFY(userType == Isds::Type::ST_NULL);
	QVERIFY(authorName.isNull());

	userType = Isds::Type::ST_NULL;
	authorName.clear();
	iOk = Isds::Xml::readMessageAuthor(response, userType, authorName);
	QVERIFY(iOk == true);
	QVERIFY(userType == Isds::Type::ST_PRIMARY);
	QVERIFY(authorName == QLatin1String("Biggus Dickus"));
}

void TestIsdsXmlMessage::dd(const QByteArray &ddContent,
    enum Isds::Type::RawType expectedRawType)
{
	bool iOk = false;
	Isds::Message message = Isds::toMessage(ddContent, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!message.isNull());
	QVERIFY(message.raw() == ddContent);
	QVERIFY(message.rawType() == expectedRawType);
	const Isds::Envelope &envelope = message.envelope();
	QVERIFY(!envelope.isNull());
	const QList<Isds::Document> &documents = message.documents();
	QVERIFY(documents.size() == 0);

	const QList<Isds::Event> dmEvents = envelope.dmEvents();
	QVERIFY(dmEvents.size() == 3);
}

void TestIsdsXmlMessage::dz(const QByteArray &dzContent,
    enum Isds::Type::RawType expectedRawType)
{
	bool iOk = false;
	Isds::Message message = Isds::toMessage(dzContent, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!message.isNull());
	QVERIFY(message.raw() == dzContent);
	QVERIFY(message.rawType() == expectedRawType);
	const Isds::Envelope &envelope = message.envelope();
	QVERIFY(!envelope.isNull());
	const QList<Isds::Document> &documents = message.documents();
	QVERIFY(documents.size() == 1);

	const QList<Isds::Event> dmEvents = envelope.dmEvents();
	QVERIFY(dmEvents.size() == 0);
}

QObject *newTestIsdsXmlMessage(void)
{
	return new (::std::nothrow) TestIsdsXmlMessage();
}

//QTEST_MAIN(TestIsdsXmlMessage)
#include "test_isds_xml_message.moc"
