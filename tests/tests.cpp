/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QTest>

#include "tests/test_isds_xml_box.h"
#include "tests/test_isds_xml_message.h"
#include "tests/test_isds_xml_response_status.h"

static
int testThisClassAndDelete(QObject *testClassObj, int argc, char *argv[])
{
	int status;

	if (Q_UNLIKELY(testClassObj == Q_NULLPTR)) {
		return 1;
	}

	status = QTest::qExec(testClassObj, argc, argv);

	delete testClassObj;

	return status;
}

int main(int argc, char *argv[])
{
	int status = 0;

#if defined (TEST_ISDS_XML_BOX)
	status |= testThisClassAndDelete(newTestIsdsXmlBox(), argc, argv);
#endif /* defined (TEST_ISDS_XML_BOX) */

#if defined (TEST_ISDS_XML_MESSAGE)
	status |= testThisClassAndDelete(newTestIsdsXmlMessage(), argc, argv);
#endif /* defined (TEST_ISDS_XML_MESSAGE) */

#if defined (TEST_ISDS_XML_RESPONSE_STATUS)
	status |= testThisClassAndDelete(newTestIsdsXmlResponseStatus(), argc, argv);
#endif /* defined (TEST_ISDS_XML_RESPONSE_STATUS) */

	return status;
}
