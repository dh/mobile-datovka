
QT += xml

DEFINES += \
	TEST_ISDS_XML_MESSAGE=1

LIBS += \
	-lcrypto

SOURCES += \
	$${top_srcdir}src/crypto/crypto.c \
	$${top_srcdir}src/datovka_shared/isds/internal_conversion.cpp \
	$${top_srcdir}src/datovka_shared/isds/message_interface.cpp \
	$${top_srcdir}src/datovka_shared/isds/type_conversion.cpp \
	$${top_srcdir}src/isds/conversion/isds_time_conversion.cpp \
	$${top_srcdir}src/isds/conversion/isds_type_conversion.cpp \
	$${top_srcdir}src/isds/services/message_interface_offline.cpp \
	$${top_srcdir}src/isds/xml/cms.cpp \
	$${top_srcdir}src/isds/xml/helper.cpp \
	$${top_srcdir}src/isds/xml/message_interface.cpp \
	$${top_srcdir}src/isds/xml/soap.cpp \
	$${top_srcdir}tests/test_isds_xml_message.cpp

HEADERS += \
	$${top_srcdir}src/crypto/crypto.h \
	$${top_srcdir}src/datovka_shared/isds/internal_conversion.h \
	$${top_srcdir}src/datovka_shared/isds/message_interface.h \
	$${top_srcdir}src/datovka_shared/isds/type_conversion.h \
	$${top_srcdir}src/datovka_shared/isds/types.h \
	$${top_srcdir}src/isds/conversion/isds_time_conversion.h \
	$${top_srcdir}src/isds/conversion/isds_type_conversion.h \
	$${top_srcdir}src/isds/services/message_interface_offline.h \
	$${top_srcdir}src/isds/xml/cms.h \
	$${top_srcdir}src/isds/xml/helper.h \
	$${top_srcdir}src/isds/xml/message_interface.h \
	$${top_srcdir}src/isds/xml/soap.h \
	$${top_srcdir}tests/test_isds_xml_message.h
