
QT += xml

DEFINES += \
	TEST_ISDS_XML_BOX=1

SOURCES += \
	$${top_srcdir}src/datovka_shared/isds/box_interface.cpp \
	$${top_srcdir}src/datovka_shared/isds/box_interface2.cpp \
	$${top_srcdir}src/datovka_shared/isds/internal_conversion.cpp \
	$${top_srcdir}src/datovka_shared/isds/type_conversion.cpp \
	$${top_srcdir}src/isds/conversion/isds_time_conversion.cpp \
	$${top_srcdir}src/isds/conversion/isds_type_conversion.cpp \
	$${top_srcdir}src/isds/xml/box_interface.cpp \
	$${top_srcdir}src/isds/xml/helper.cpp \
	$${top_srcdir}src/isds/xml/soap.cpp \
	$${top_srcdir}tests/test_isds_xml_box.cpp

HEADERS += \
	$${top_srcdir}src/datovka_shared/isds/box_interface.h \
	$${top_srcdir}src/datovka_shared/isds/box_interface2.h \
	$${top_srcdir}src/datovka_shared/isds/internal_conversion.h \
	$${top_srcdir}src/datovka_shared/isds/type_conversion.h \
	$${top_srcdir}src/isds/conversion/isds_time_conversion.h \
	$${top_srcdir}src/isds/conversion/isds_type_conversion.h \
	$${top_srcdir}src/isds/xml/box_interface.h \
	$${top_srcdir}src/isds/xml/helper.h \
	$${top_srcdir}src/isds/xml/soap.h \
	$${top_srcdir}tests/test_isds_xml_box.h
