INCLUDEPATH += \
    $$PWD \
    src \
    ios/openssl/include/

LIBS = \
    $${_PRO_FILE_PWD_}/ios/openssl/lib/libcrypto.a \
    $${_PRO_FILE_PWD_}/ios/openssl/lib/libssl.a

HEADERS += \
    ios/src/app_delegate.h \
    ios/src/doc_picker_controller.h \
    ios/src/doc_view_controller.h \
    ios/src/qt_app_delegate.h \
    ios/src/icloud_io.h \
    ios/src/ios_file_opener.h \
    ios/src/send_email_controller.h \
    ios/src/url_opener.h

OBJECTIVE_SOURCES += \
    ios/src/app_delegate.mm \
    ios/src/doc_picker_controller.mm \
    ios/src/doc_view_controller.mm \
    ios/src/icloud_io.mm \
    ios/src/ios_file_opener.mm \
    ios/src/send_email_controller.mm \
    ios/src/url_opener.mm

QMAKE_IOS_DEPLOYMENT_TARGET = 12.0
QMAKE_INFO_PLIST = $${_PRO_FILE_PWD_}/ios/Info.plist
QMAKE_ASSET_CATALOGS = $${_PRO_FILE_PWD_}/ios/Images.xcassets
QMAKE_ASSET_CATALOGS_APP_ICON = "AppIcon"
QMAKE_ASSET_CATALOGS_LAUNCH_IMAGE = "LaunchImage"
LIBS += -framework UIKit -framework MessageUI -framework MobileCoreServices

