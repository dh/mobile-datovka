/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QtCore> /* Include something from Qt to pull Q_OS_* definitions. */

/*
 * Code inspired by:
 * https://bugreports.qt.io/browse/QTBUG-42942
 * https://github.com/JoseExposito/ubuntuone-qt-files
 */

#ifdef Q_OS_IOS
#include "ios/src/send_email_controller.h"
#include "ios/src/doc_view_controller.h"
#include "ios/src/url_opener.h"
#endif /* Q_OS_IOS */

void UrlOpener::openFile(const QString &filePath)
{
#ifndef Q_OS_IOS

	Q_UNUSED(filePath);

#else /* Q_OS_IOS */

	NSString *url = filePath.toNSString();
	NSURL *fileURL = [NSURL fileURLWithPath:url];
	static DocViewController* mtv = nil;
	if (mtv != nil) {
		[mtv removeFromParentViewController];
		[mtv release];
	}

	UIDocumentInteractionController *documentInteractionController = nil;
	documentInteractionController = [UIDocumentInteractionController
	    interactionControllerWithURL:fileURL];

	UIViewController *rootv = [[[[UIApplication sharedApplication]windows]
	    firstObject]rootViewController];
	if (rootv != nil) {
		mtv = [[DocViewController alloc] init];
		[rootv addChildViewController:mtv];
		documentInteractionController.delegate = mtv;
		[documentInteractionController presentPreviewAnimated:NO];
	}

#endif /* !Q_OS_IOS */
}

void UrlOpener::createEmail(const QString &bodyText, const QString &to,
    const QString &subject, const QStringList &filePaths)
{
#ifndef Q_OS_IOS

	Q_UNUSED(to);
	Q_UNUSED(bodyText);
	Q_UNUSED(subject);
	Q_UNUSED(filePaths);

#else /* Q_OS_IOS */

	NSString *recipient = to.toNSString();
	NSString *body = bodyText.toNSString();
	NSString *sbjct = subject.toNSString();

	NSMutableArray *tmp = [NSMutableArray arrayWithCapacity:filePaths.size()];
	for (int i = 0; i < filePaths.size(); ++i) {
		[tmp addObject:filePaths[i].toNSString()];
	}
	NSArray *filePath = [NSArray arrayWithArray:tmp];

	static SimpleEmailSendController *email = nil;
	if (email != nil) {
		[email removeFromParentViewController];
		[email release];
	}

	UIViewController *rootv = [[[[UIApplication sharedApplication]windows]
	    firstObject]rootViewController];
	if (rootv != nil) {
		email = [[SimpleEmailSendController alloc] init];
		[rootv addChildViewController:email];
		[email createEmail:body rec:recipient sub:sbjct files:filePath];
	}
#endif /* !Q_OS_IOS */
}
