/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

/*
 * We cannot use conditional declaration by Q_OS_IOS because that would cause
 * the moc_ file not to be generated.
 */

/*!
 * @brief Used for opening URLs and create email with attachments on iOS.
 */
class UrlOpener {
private:
	/*!
	 * @brief Constructor.
	 */
	UrlOpener(void);

public:
	/*!
	 * @brief Open file on iOS.
	 *
	 * @param[in] filePath Path to file.
	 */
	static
	void openFile(const QString &filePath);

	/*!
	 * @brief Create email on iOS.
	 *
	 * @param[in] bodyText Email text.
	 * @param[in] to Recipient mail address.
	 * @param[in] subject Email subject.
	 * @param[in] filePaths Paths to attachment files.
	 */
	static
	void createEmail(const QString &bodyText, const QString &to,
	    const QString &subject, const QStringList &filePaths);
};
