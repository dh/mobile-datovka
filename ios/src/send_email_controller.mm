/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QtCore>

#include "ios/src/send_email_controller.h"

@implementation SimpleEmailSendController

- (void) showCantSendMailAlert
{
	UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Can't send email", @"Can't send email")
		message:NSLocalizedString(@"The device is not configured for sending email", @"The device is not configured for sending email")
		preferredStyle:UIAlertControllerStyleAlert];
	UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {}];
	[alert addAction:defaultAction];
	[self presentViewController:alert animated:YES completion:nil];
}

- (NSString *)guessMIMETypeFromFileName:(NSString *)fileName
{
	CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)[fileName pathExtension], NULL);
	CFStringRef MIMEType = UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType);
	CFRelease(UTI);
	if (!MIMEType) {
		return @"application/octet-stream";
	}
	return (__bridge NSString *)(MIMEType);
}

- (void)createEmail:(NSString *)body rec:(NSString *)recipient
    sub:(NSString *)subject files:(NSArray *)filePaths
{
	if (![MFMailComposeViewController canSendMail]) {
		[self showCantSendMailAlert];
		return;
	}

	MFMailComposeViewController *controller =
	    [[MFMailComposeViewController alloc] init];
	controller.mailComposeDelegate = self;
	[controller setToRecipients:@[recipient]];
	[controller setSubject:subject];
	[controller setMessageBody:body isHTML:NO];

	// attachments
	for (NSUInteger i = 0; i < [filePaths count]; i++) {
		NSData *filedata = [NSData dataWithContentsOfFile:filePaths[i]];
		NSString *filename = [filePaths[i] lastPathComponent];
		NSString *mimetype = [self guessMIMETypeFromFileName:filename];
		[controller addAttachmentData:filedata mimeType:mimetype fileName:filename];
	}

	if (controller) [self presentViewController:controller animated:YES completion:nil];
	[controller release];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
    didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
	Q_UNUSED(controller);

	if (error) {
		NSLog(@"Error : %@", error);
	}

	switch (result) {
	case MFMailComposeResultSent:
		NSLog(@"You sent the email.");
		break;
	case MFMailComposeResultSaved:
		NSLog(@"You saved a draft of this email");
		break;
	case MFMailComposeResultCancelled:
		NSLog(@"You cancelled sending this email.");
		break;
	case MFMailComposeResultFailed:
		NSLog(@"Mail failed:  An error occurred when trying to compose this email");
		break;
	default:
		NSLog(@"An error occurred when trying to compose this email");
		break;
	}

	[self dismissViewControllerAnimated:YES completion:nil];
}

@end
