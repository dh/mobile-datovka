/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QList>
#include <QUrl>

#include "ios/src/doc_picker_controller.h"
#include "src/auxiliaries/ios_helper.h"
#include "src/global.h"

@interface DocumentPickerController () <UIDocumentPickerDelegate>

@end

@implementation DocumentPickerController

- (void)viewDidLoad {
	[super viewDidLoad];
}

- (void)openExportDocumentPicker:(NSArray<NSURL *> *)exportUrls {

	if (@available(iOS 11.0, *)) {
		UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithURLs:exportUrls inMode:UIDocumentPickerModeExportToService];
		documentPicker.delegate = self;
		documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
		[self presentViewController:documentPicker animated:YES completion:nil];
	} else {
		for (NSURL *url in exportUrls) {
			UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithURL:url inMode:UIDocumentPickerModeExportToService];
			documentPicker.delegate = self;
			documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
			[self presentViewController:documentPicker animated:YES completion:nil];
		}
	}
}

- (void)openImportDocumentPicker:(NSArray<NSString *> *)allowedUtis {

	UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:allowedUtis inMode:UIDocumentPickerModeImport];
	documentPicker.delegate = self;
	documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
	[self presentViewController:documentPicker animated:NO completion:^{
		if (@available(iOS 11.0, *)) {
			documentPicker.allowsMultipleSelection = YES;
		}
	}];
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentsAtURLs:(NSArray<NSURL *> *)urls {

	if (controller.documentPickerMode == UIDocumentPickerModeImport) {

		NSLog(@"SELECTED FILE URLs: %@", urls);
		QList<QUrl> qUrls;
		// Convert NSURL on QUrl for all selected files
		for (NSURL *url in urls) {
			QUrl tmpUrl =  QUrl::fromNSURL(url);
			qUrls.append(tmpUrl);
		}
		if (Q_NULLPTR != GlobInstcs::iOSHelperPtr) {
			GlobInstcs::iOSHelperPtr->importFilesToAppInbox(qUrls);
		}

	} else if (controller.documentPickerMode == UIDocumentPickerModeExportToService) {
		NSLog(@"STORAGE FILE URLs: %@", urls);
	}
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {

	if (controller.documentPickerMode == UIDocumentPickerModeImport) {

		NSLog(@"SELECTED FILE URL: %@", url);
		QList<QUrl> qUrls;
		// Convert NSURL on QUrl
		QUrl tmpUrl =  QUrl::fromNSURL(url);
		qUrls.append(tmpUrl);
		if (Q_NULLPTR != GlobInstcs::iOSHelperPtr) {
			GlobInstcs::iOSHelperPtr->importFilesToAppInbox(qUrls);
		}

	} else if (controller.documentPickerMode == UIDocumentPickerModeExportToService) {
		NSLog(@"STORAGE FILE URL: %@", url);
	}
}

- (void)documentPickerWasCancelled:(UIDocumentPickerViewController *)controller {
	Q_UNUSED(controller);
	if (Q_NULLPTR != GlobInstcs::iOSHelperPtr) {
		GlobInstcs::iOSHelperPtr->importFilesToAppInbox(QList<QUrl>());
	}
}

@end
