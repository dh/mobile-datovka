/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "ios/src/doc_picker_controller.h"
#include "ios/src/icloud_io.h"

static
NSURL *getCloudBaseUrl(void)
{
	return [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
}

static
NSURL *getCloudDocumentsUrl(NSURL *baseURL)
{
	return [baseURL URLByAppendingPathComponent:@"Documents"];
}

bool ICloudIo::isCloudOn(void)
{
	return [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
}

ICloudIo::ICloudResult ICloudIo::moveFileToCloud(const QString &srcFilePath,
    const QString &destFilePath)
{
	NSURL *baseURL = getCloudBaseUrl();
	if (!baseURL) {
		return ICLOUD_NOT_ON;
	}

	// Create iCloud target path
	NSURL *messageURL = [getCloudDocumentsUrl(baseURL) URLByAppendingPathComponent:destFilePath.toNSString()];

	// Create subdirectorues on iCloud
	NSError *error = nil;
	if (![[NSFileManager defaultManager] createDirectoryAtURL:messageURL
	    withIntermediateDirectories:YES attributes:nil error:&error]) {
		return ICLOUD_TARGET_SAVE_DIR_ERROR;
	}

	// Create target upload path for iCloud
	NSURL *sourceFileUrl = [NSURL fileURLWithPath:srcFilePath.toNSString()];
	NSString *fileName = [srcFilePath.toNSString() lastPathComponent];
	NSURL *fileURL = [messageURL URLByAppendingPathComponent:fileName];

	// Upload file to target directory on iCloud
	if ([[NSFileManager defaultManager] setUbiquitous:YES
	    itemAtURL:sourceFileUrl destinationURL:fileURL error:&error]) {
		return ICLOUD_FILE_UPLOAD_SUCCESS;
	} else {
		// Code 516 = file exists in the iCloud. See NSFileManager error codes.
		if (error.code == NSFileWriteFileExistsError) {
			return ICLOUD_FILE_EXISTS;
		} else {
			NSLog(@"iCloud: Error code: %zd", error.code);
			NSLog(@"iCloud: %@", error);
			return ICLOUD_FILE_UPLOAD_ERROR;
		}
	}
}

bool ICloudIo::openDocumentPickerControllerForImport(const QStringList &allowedUtis)
{
	static DocumentPickerController *dpc = nil;
	if (dpc != nil) {
		[dpc removeFromParentViewController];
		[dpc release];
	}

	UIViewController *rootv = [[[[UIApplication sharedApplication]windows] firstObject]rootViewController];

	if (rootv != nil) {
		dpc = [[DocumentPickerController alloc] init];
		[rootv addChildViewController:dpc];
		if (allowedUtis.isEmpty()) {
			[dpc openImportDocumentPicker:@[@"public.data"]];
		} else {
			// exportFilesPath is not empty so export Document Picker will open
			NSMutableArray<NSString*> *nsUITs = [NSMutableArray array];
			// covert export file paths to array of nsurl
			for (int i = 0; i < allowedUtis.count(); ++i) {
				[nsUITs addObject:allowedUtis.at(i).toNSString()];
			}
			if ([nsUITs count] > 0) {
				[dpc openImportDocumentPicker:nsUITs];
			}
		}
		return true;
	}

	return false;
}

bool ICloudIo::openDocumentPickerControllerForExport(const QStringList &exportFilesPath)
{
	static DocumentPickerController *dpc = nil;
	if (dpc != nil) {
		[dpc removeFromParentViewController];
		[dpc release];
	}

	UIViewController *rootv = [[[[UIApplication sharedApplication]windows] firstObject]rootViewController];

	if (rootv != nil) {
		dpc = [[DocumentPickerController alloc] init];
		[rootv addChildViewController:dpc];

		NSMutableArray<NSURL*> *exportUrls = [NSMutableArray array];
		// covert export file paths to array of nsurl
		for (int i = 0; i < exportFilesPath.count(); ++i) {
			QUrl url(QUrl::fromLocalFile(exportFilesPath.at(i)));
			if (url.isValid()) {
				NSURL *fileUrl = url.toNSURL();
				[exportUrls addObject:fileUrl];
				//NSLog(@"ADD FILE URL to list: %@", fileUrl);
			} else {
				NSLog(@"ERROR FILE URL: %@", url.toNSURL());
			}
		}
		if ([exportUrls count] > 0) {
			[dpc openExportDocumentPicker:exportUrls];
		}
		return true;
	}

	return false;
}

QUrl ICloudIo::moveFile(const QUrl &sourceFileUrl,
    const QString &newFilePath)
{
	// Convert string path to URL
	NSURL *ofp = sourceFileUrl.toNSURL();
	NSURL *np = [NSURL fileURLWithPath:newFilePath.toNSString()];
	NSString *fileName = [ofp lastPathComponent];

	// Create subdirectorues in the sandbox local storage
	NSError *error = nil;
	if (![[NSFileManager defaultManager] createDirectoryAtURL:np
	    withIntermediateDirectories:YES attributes:nil error:&error]) {
		NSLog(@"Local storage: Create message subdirectories error: %@", error);
		return QUrl();
	}

	NSURL *nfp = [np URLByAppendingPathComponent:fileName];
	//NSLog(@"TMP url: %@", ofp);
	//NSLog(@"NP url: %@", np);
	//NSLog(@"SEND url: %@", nfp);

	// Remove file from sandbox local storage if exists
	[[NSFileManager defaultManager] removeItemAtURL:nfp error:&error];

	if ([[NSFileManager defaultManager] moveItemAtURL:ofp toURL:nfp error:&error]) {
		NSLog(@"Local storage: File has moved to target path.");
		return QUrl::fromNSURL(nfp);
	} else {
		if (error.code == NSFileWriteFileExistsError) {
			NSLog(@"Local storage: File with the same name already exists in the target path.");
		} else {
			NSLog(@"Local storage: Error code: %zd %@", error.code, error);
		}
		return QUrl();
	}
}
