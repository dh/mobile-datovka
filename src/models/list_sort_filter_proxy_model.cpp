/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QQmlEngine> /* qmlRegisterType */

#include "src/models/list_sort_filter_proxy_model.h"

void ListSortFilterProxyModel::declareQML(void)
{
	qmlRegisterType<ListSortFilterProxyModel>("cz.nic.mobileDatovka.models", 1, 0, "ListSortFilterProxyModel");
	qRegisterMetaType<ListSortFilterProxyModel>("ListSortFilterProxyModel");
}

ListSortFilterProxyModel::ListSortFilterProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent),
    m_filterRoles()
{
}

ListSortFilterProxyModel::ListSortFilterProxyModel(
    const ListSortFilterProxyModel &model, QObject *parent)
    : QSortFilterProxyModel(parent),
    m_filterRoles(model.m_filterRoles)
{
	Q_UNUSED(model);
	Q_ASSERT(0);
}

void ListSortFilterProxyModel::setFilterRole(int role)
{
	m_filterRoles.clear();
	m_filterRoles.append(role);
}

void ListSortFilterProxyModel::setFilterRoles(const QList<int> &roles)
{
	if (roles.contains(-1)) {
		m_filterRoles.clear();
		m_filterRoles.append(-1);
	} else {
		m_filterRoles = roles;
	}
}

const QList<int> &ListSortFilterProxyModel::filterRoles(void) const
{
	return m_filterRoles;
}

void ListSortFilterProxyModel::setSourceModel(QAbstractItemModel *sourceModel)
{
	QSortFilterProxyModel::setSourceModel(sourceModel);
}

void ListSortFilterProxyModel::setFilterRegExpStr(const QString &patternCore)
{
	setFilterRegExp(QRegExp(patternCore,
	    Qt::CaseInsensitive, QRegExp::FixedString));
}

bool ListSortFilterProxyModel::filterAcceptsRow(int sourceRow,
    const QModelIndex &sourceParent) const
{
	/*
	 * Adapted from
	 * qtbase/src/corelib/itemmodels/qsortfilterproxymodel.cpp .
	 */

	if (filterRegExp().isEmpty()) {
		return true;
	}

	QModelIndex sourceIndex(
	    sourceModel()->index(sourceRow, filterKeyColumn(), sourceParent));
	/* The column may not exist. */
	return filterAcceptsItem(sourceIndex);
}

bool ListSortFilterProxyModel::filterAcceptsItem(
    const QModelIndex &sourceIdx) const
{
	if (!sourceIdx.isValid()) {
		return false;
	}

	const QAbstractItemModel *model = sourceModel();

	if (m_filterRoles.contains(-1)) {
		const QHash<int, QByteArray> roleNames(model->roleNames());
		QHash<int, QByteArray>::const_iterator it;
		for (it = roleNames.constBegin(); it != roleNames.constEnd(); ++it) {
			const QString key(model->data(sourceIdx, it.key()).toString());
			if (key.contains(filterRegExp())) {
				return true;
			}
		}
	} else {
		foreach (int role, m_filterRoles) {
			const QString key(model->data(sourceIdx, role).toString());
			if (key.contains(filterRegExp())) {
				return true;
			}
		}
	}

	return false;
}
