/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QQmlEngine> /* qmlRegisterType */
#include <QSqlRecord>

#include "src/models/databoxmodel.h"

void DataboxModelEntry::declareQML(void)
{
	qmlRegisterType<DataboxModelEntry>("cz.nic.mobileDatovka.modelEntries", 1, 0, "DataboxModelEntry");
	qRegisterMetaType<DataboxModelEntry>("DataboxModelEntry");

	qRegisterMetaType<DataboxModelEntry *>("DataboxModelEntry *");
	qRegisterMetaType<DataboxModelEntry *>("const DataboxModelEntry *");
}

DataboxModelEntry::DataboxModelEntry(QObject *parent)
    : QObject(parent),
    m_dbID(),
    m_dbType(),
    m_dbName(),
    m_dbAddress(),
    m_dbIC(),
    m_dbEffectiveOVM(),
    m_dbSendOptions()
{
}

DataboxModelEntry::DataboxModelEntry(const DataboxModelEntry &dme)
    : QObject(Q_NULLPTR),
    m_dbID(dme.m_dbID),
    m_dbType(dme.m_dbType),
    m_dbName(dme.m_dbName),
    m_dbAddress(dme.m_dbAddress),
    m_dbIC(dme.m_dbIC),
    m_dbEffectiveOVM(dme.m_dbEffectiveOVM),
    m_dbSendOptions(dme.m_dbSendOptions)
{
}

DataboxModelEntry::DataboxModelEntry(const QString &dbID, const QString &dbType,
    const QString &dbName, const QString &dbAddress, const QString &dbIC,
    const QString &dbEffectiveOVM, const QString &dbSendOptions)
    : QObject(Q_NULLPTR),
    m_dbID(dbID),
    m_dbType(dbType),
    m_dbName(dbName),
    m_dbAddress(dbAddress),
    m_dbIC(dbIC),
    m_dbEffectiveOVM(dbEffectiveOVM),
    m_dbSendOptions(dbSendOptions)
{
}

DataboxModelEntry &DataboxModelEntry::operator=(const DataboxModelEntry &entry)
{
	m_dbID = entry.m_dbID;
	m_dbType = entry.m_dbType;
	m_dbName = entry.m_dbName;
	m_dbAddress = entry.m_dbAddress;
	m_dbIC = entry.m_dbIC;
	m_dbEffectiveOVM = entry.m_dbEffectiveOVM;
	m_dbSendOptions = entry.m_dbSendOptions;

	return *this;
}

QString DataboxModelEntry::dbID(void) const
{
	return m_dbID;
}

void DataboxModelEntry::setDbID(const QString &dbID)
{
	m_dbID = dbID;
}

QString DataboxModelEntry::dbType(void) const
{
	return m_dbType;
}

void DataboxModelEntry::setDbType(const QString &dbType)
{
	m_dbType = dbType;
}

QString DataboxModelEntry::dbName(void) const
{
	return m_dbName;
}

void DataboxModelEntry::setDbName(const QString &dbName)
{
	m_dbName = dbName;
}

QString DataboxModelEntry::dbAddress(void) const
{
	return m_dbAddress;
}

void DataboxModelEntry::setDbAddress(const QString &dbAddress)
{
	m_dbAddress = dbAddress;
}

QString DataboxModelEntry::dbIC(void) const
{
	return m_dbIC;
}

void DataboxModelEntry::setDbIC(const QString &dbIC)
{
	m_dbIC = dbIC;
}

QString DataboxModelEntry::dbEffectiveOVM(void) const
{
	return m_dbEffectiveOVM;
}

void DataboxModelEntry::setDbEffectiveOVM(const QString &dbEffectiveOVM)
{
	m_dbEffectiveOVM = dbEffectiveOVM;
}

QString DataboxModelEntry::dbSendOptions(void) const
{
	return m_dbSendOptions;
}

void DataboxModelEntry::setDbSendOptions(const QString &dbSendOptions)
{
	m_dbSendOptions = dbSendOptions;
}

DataboxModelEntry *DataboxModelEntry::fromVariant(const QVariant &entryVariant)
{
	if (!entryVariant.canConvert<QObject *>()) {
		return Q_NULLPTR;
	}
	QObject *obj = qvariant_cast<QObject *>(entryVariant);
	return qobject_cast<DataboxModelEntry *>(obj);
}

void DataboxListModel::declareQML(void)
{
	qmlRegisterType<DataboxListModel>("cz.nic.mobileDatovka.models", 1, 0, "DataboxListModel");
	qRegisterMetaType<DataboxListModel>("DataboxListModel");
	qRegisterMetaType<DataboxListModel::Roles>("DataboxListModel::Roles");

	qRegisterMetaType<DataboxListModel *>("DataboxListModel *");
	qRegisterMetaType<DataboxListModel *>("const DataboxListModel *");
}

DataboxListModel::DataboxListModel(QObject *parent)
    : QAbstractListModel(parent),
    m_boxIds(),
    m_entries()
{
}

DataboxListModel::DataboxListModel(const DataboxListModel &model,
    QObject *parent)
    : QAbstractListModel(parent),
    m_boxIds(model.m_boxIds),
    m_entries(model.m_entries)
{
}

int DataboxListModel::rowCount(const QModelIndex &parent) const
{
	return !parent.isValid() ? m_boxIds.size() : 0;
}

QHash<int, QByteArray> DataboxListModel::roleNames(void) const
{
	static QHash<int, QByteArray> roles;
	if (roles.isEmpty()) {
		roles[ROLE_DB_ID] = "rDbID";
		roles[ROLE_DB_TYPE] = "rDbType";
		roles[ROLE_DB_NAME] = "rDbName";
		roles[ROLE_DB_ADDRESS] = "rDbAddress";
		roles[ROLE_DB_IC] = "rDbIc";
		roles[ROLE_DB_EFFECTIVE_OVM] = "rDbEffectiveOvm";
		roles[ROLE_DB_SEND_OPTION] = "rDbSendOption";
		roles[ROLE_DB_SELECTED] = "rDbSelected";
	}
	return roles;
}

QVariant DataboxListModel::data(const QModelIndex &index, int role) const
{
	if ((index.row() < 0) || (index.row() >= m_boxIds.size())) {
		return QVariant();
	}

	const QString &boxId(m_boxIds.at(index.row()));
	const DataboxModelEntry entry(m_entries[boxId].entry);

	switch (role) {
	case ROLE_DB_ID:
		return entry.dbID();
		break;
	case ROLE_DB_TYPE:
		return entry.dbType();
		break;
	case ROLE_DB_NAME:
		return entry.dbName();
		break;
	case ROLE_DB_ADDRESS:
		return entry.dbAddress();
		break;
	case ROLE_DB_IC:
		return entry.dbIC();
		break;
	case ROLE_DB_EFFECTIVE_OVM:
		return entry.dbEffectiveOVM();
		break;
	case ROLE_DB_SEND_OPTION:
		return entry.dbSendOptions();
		break;
	case ROLE_DB_SELECTED:
		return m_entries[boxId].selected;
		break;
	default:
		/* Do nothing. */
		break;
	}

	return QVariant();
}

QList<DataboxModelEntry> DataboxListModel::allEntries(void) const
{
	QList<DataboxModelEntry> entries;

	foreach (const QString &boxId, m_boxIds) {
		QMap<QString, InternalEntry>::const_iterator it =
		    m_entries.find(boxId);
		if (Q_UNLIKELY(it == m_entries.end())) {
			continue;
		}
		if (Q_UNLIKELY(it->entry.dbID().isEmpty())) {
			continue;
		}
		entries.append(it->entry);
	}

	return entries;
}

bool DataboxListModel::addEntry(const DataboxModelEntry &entry)
{
	const QString key(entry.dbID());

	if (Q_UNLIKELY(key.isEmpty())) {
		return false;
	}

	QMap<QString, InternalEntry>::const_iterator it = m_entries.find(key);
	if (it != m_entries.end()) {
		return false;
	}

	beginInsertRows(QModelIndex(), rowCount(), rowCount());
	m_boxIds.append(key);
	m_entries[key] = InternalEntry(entry, false);
	endInsertRows();

	return true;
}

bool DataboxListModel::addEntry(const DataboxModelEntry *entry)
{
	if (Q_UNLIKELY(Q_NULLPTR == entry)) {
		return false;
	}

	return addEntry(*entry);
}

bool DataboxListModel::removeEntry(const QString &boxId)
{
	if (Q_UNLIKELY(boxId.isEmpty())) {
		return false;
	}

	QMap<QString, InternalEntry>::const_iterator it = m_entries.find(boxId);
	if (it == m_entries.end()) {
		return false;
	}

	for (int r = (m_boxIds.size() - 1); r >= 0; --r) {
		if (m_boxIds.at(r) == boxId) {
			beginRemoveRows(QModelIndex(), r, r);
			m_boxIds.removeAt(r);
			endRemoveRows();
		}
	}

	m_entries.remove(boxId);
	return true;
}

DataboxModelEntry *DataboxListModel::entry(const QString &boxId) const
{
	if (Q_UNLIKELY(boxId.isEmpty())) {
		Q_ASSERT(0);
		return new (::std::nothrow) DataboxModelEntry;
	}

	return new (::std::nothrow) DataboxModelEntry(
	    m_entries.value(boxId,
	        InternalEntry(DataboxModelEntry(), false)).entry);
}

bool DataboxListModel::selectEntry(const QString &boxId, bool selected)
{
	if (Q_UNLIKELY(boxId.isEmpty())) {
		return false;
	}

	QMap<QString, InternalEntry>::iterator it = m_entries.find(boxId);
	if (it == m_entries.end()) {
		return false;
	}

	for (int i = 0; i < m_boxIds.size(); ++i) {
		if (boxId == m_boxIds.at(i)) {
			it->selected = selected;

			emit dataChanged(QAbstractListModel::index(i, 0),
			    QAbstractListModel::index(i, 0));
			return true;
		}
	}

	return false;
}

bool DataboxListModel::selectEntries(const QStringList &boxIds,
    bool selected)
{
	if (Q_UNLIKELY(boxIds.size() == 0)) {
		return false;
	}

	bool ret = false;
	foreach (const QString &boxId, boxIds) {
		/* Beware of short-circuit evaluation. */
		ret = selectEntry(boxId, selected) || ret;
	}

	return ret;
}

QStringList DataboxListModel::boxIds(void) const
{
	return m_boxIds;
}

int DataboxListModel::setQuery(QSqlQuery &query, const QString &dbId,
    bool isAppend)
{
	int databoxCnt = 0;

	if (query.record().count() != 3) {
		return databoxCnt;
	}

	beginResetModel();

	if (!isAppend) {
		m_boxIds.clear();
		m_entries.clear();
	}

	query.first();
	while (query.isActive() && query.isValid()) {
		if (dbId != query.value(0).toString()) {
			// some model items are not available in local database
			DataboxModelEntry entry(
			    query.value(0).toString(), QString(),
			    query.value(1).toString(), query.value(2).toString(),
			    QString(), QString(), QString());
			m_boxIds.append(entry.dbID());
			m_entries[entry.dbID()] = InternalEntry(entry, false);
			databoxCnt++;
		}
		query.next();
	}

	endResetModel();

	return databoxCnt;
}

Qt::ItemFlags DataboxListModel::flags(const QModelIndex &index) const
{
	return QAbstractListModel::flags(index);
}

void DataboxListModel::clearAll(void)
{
	beginResetModel();
	m_boxIds.clear();
	m_entries.clear();
	endResetModel();
}

DataboxListModel *DataboxListModel::fromVariant(const QVariant &modelVariant)
{
	if (!modelVariant.canConvert<QObject *>()) {
		return Q_NULLPTR;
	}
	QObject *obj = qvariant_cast<QObject *>(modelVariant);
	return qobject_cast<DataboxListModel *>(obj);
}
