/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractListModel>
#include <QList>
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"

/*!
 * @brief Model used for account selection for back-up and restore.
 */
class BackupRestoreSelectionModel : public QAbstractListModel {
	Q_OBJECT

public:

	/*!
	 * @brief Describes model entries.
	 */
	class Entry {
	public:
		/*!
		 * @brief Empty constructor.
		 */
		Entry(void)
		    : selected(false), accountName(), acntId(), boxId(), subdir()
		{ }

		/*!
		 * @brief Constructor.
		 */
		Entry(bool m, const QString &a, const AcntId &aId,
		    const QString &b, const QString &s)
		    : selected(m), accountName(a), acntId(aId), boxId(b),
		      subdir(s)
		{ }

		bool selected;
		QString accountName;
		AcntId acntId;
		QString boxId;
		QString subdir;
	};

	/*!
	 * @brief Roles which this model holds.
	 */
	enum Roles {
		ROLE_ACCOUNT_NAME = Qt::UserRole, /*!< Account name. */
		ROLE_USERNAME, /*!< Primary login to access this account by this application. */
		ROLE_TESTING, /*!< Whether this is a testing account. */
		ROLE_BOX_ID, /*!< Data box identifier. */
		ROLE_SELECTED, /*!< Account is selected. */
		ROLE_SUBDIR /*!< Account backup subdirectory. */
	};
	Q_ENUM(Roles)

	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit BackupRestoreSelectionModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 *
	 * @param[in] model Model to be copied.
	 * @param[in] parent Pointer to parent object.
	 */
	explicit BackupRestoreSelectionModel(
	    const BackupRestoreSelectionModel &model,
	    QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Returns number of rows under given parent.
	 *
	 * @param[in] parent Parent index.
	 * @return Number of rows.
	 */
	virtual
	int rowCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the model's role names.
	 *
	 * @return Model's role names.
	 */
	virtual
	QHash<int, QByteArray> roleNames(void) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the data stored under the given role.
	 *
	 * @param[in] index Position.
	 * @param[in] role  Role if the position.
	 * @return Data or invalid QVariant if no matching data found.
	 */
	virtual
	QVariant data(const QModelIndex &index,
	    int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Used for checkable elements.
	 *
	 * @param[in] index Index which to obtain flags for.
	 */
	virtual
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Appends data into the model.
	 *
	 * @param[in] selected Selection state.
	 * @param[in] acntName Account name.
	 * @param[in] acntId Account identifier.
	 * @param[in] dbId Data box identifier.
	 * @param[in] subdir Account backup subdirectory.
	 */
	void appendData(bool selected, const QString &acntName,
	    const AcntId &acntId, const QString &dbId, const QString &subdir);

	/*!
	 * @brief Set one item selection to model.
	 *
	 * @note Emits dataChanged signal.
	 *
	 * @param[in] row Item index in QML model.
	 * @param[in] select True to select, false to unselect.
	 */
	Q_INVOKABLE
	void setSelected(int row, bool select);

	/*!
	 * @brief Returns number of selected items.
	 *
	 * @return Number of selected rows.
	 */
	Q_INVOKABLE
	int numSelected(void) const;

	/*!
	 * @brief Set all selection to model.
	 *
	 * @note Emits dataChanged signal.
	 *
	 * @param[in] select True to select, false to unselect.
	 */
	Q_INVOKABLE
	void setAllSelected(bool select);

	/*!
	 * @brief Clear the model.
	 */
	Q_INVOKABLE
	void clearAll(void);

	/*!
	 * @brief Get list of ids of selected accounts.
	 *
	 * @return List of selected account ids.
	 */
	QList<AcntId> selectedAccountIds(void) const;

	/*!
	 * @brief Get account info from model.
	 *
	 * @param[in] acntId Account identifier.
	 * @return Account data from model.
	 */
	Entry accountEntry(const AcntId &acntId) const;

	/*!
	 * @brief Converts QVariant (obtained from QML) into a pointer.
	 *
	 * @note Some weird stuff happens in QML when passing instances
	 *     directly as constant reference. Wrong constructors are called
	 *     and no data are passed.
	 * @note QML passes objects (which were created in QML) as QVariant
	 *     values holding pointers. You therefore may call invokable methods
	 *     with QVariant arguments from QML.
	 * @note If you use
	 *     qRegisterMetaType<Type *>("Type *") and
	 *     qRegisterMetaType<Type *>("const Type *")
	 *     then QML will be able to call invokable methods without explicit
	 *     conversion from QVariant arguments.
	 *     Q_DECLARE_METATYPE(Type *) is not needed.
	 *
	 * @param[in] variant QVariant holding the pointer.
	 * @return Pointer if it could be acquired, Q_NULLPTR else. This
	 *     function does not allocate a new instance.
	 */
	static
	BackupRestoreSelectionModel *fromVariant(const QVariant &modelVariant);

private:
	QList<Entry> m_entries; /*!< List of model entries. */
	int m_numSelected; /*!< Total number of selected entries. */
};

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(BackupRestoreSelectionModel)
Q_DECLARE_METATYPE(BackupRestoreSelectionModel::Roles)
