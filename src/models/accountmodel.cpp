/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QQmlEngine> /* qmlRegisterType */
#include <QSettings>

#include "src/datovka_shared/crypto/crypto_pwd.h"
#include "src/datovka_shared/crypto/crypto_wrapped.h"
#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/models/accountmodel.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/settings/prefs_specific.h"

void AccountListModel::declareQML(void)
{
	qmlRegisterType<AccountListModel>("cz.nic.mobileDatovka.models", 1, 0, "AccountListModel");
	qRegisterMetaType<AccountListModel>("AccountListModel");
	qRegisterMetaType<AccountListModel::Roles>("AccountListModel::Roles");

	qRegisterMetaType<AccountListModel *>("AccountListModel *");
	qRegisterMetaType<AccountListModel *>("const AccountListModel *");
}

AccountListModel::AccountListModel(QObject *parent)
    : QAbstractListModel(parent),
    m_acntIds(),
    m_accountsPtr(Q_NULLPTR)
{
}

AccountListModel::AccountListModel(const AccountListModel &model,
    QObject *parent)
    : QAbstractListModel(parent),
    m_acntIds(model.m_acntIds),
    m_accountsPtr(Q_NULLPTR)
{
	setAccounts(model.m_accountsPtr);
}

void AccountListModel::setAccounts(AccountsMap *accounts)
{
	if (m_accountsPtr != Q_NULLPTR) {
		disconnect(m_accountsPtr, SIGNAL(accountDataChanged(AcntId)),
		    this, SLOT(handleAccountDataChange(AcntId)));
		m_accountsPtr = Q_NULLPTR;
	}
	/* Automatically handle signalled changes. */
	if (accounts != Q_NULLPTR) {
		m_accountsPtr = accounts;
		connect(m_accountsPtr, SIGNAL(accountDataChanged(AcntId)),
		    this, SLOT(handleAccountDataChange(AcntId)));
	}
}

int AccountListModel::rowCount(const QModelIndex &parent) const
{
	return !parent.isValid() ? m_acntIds.size() : 0;

}

QHash<int, QByteArray> AccountListModel::roleNames(void) const
{
	static QHash<int, QByteArray> roles;
	if (roles.isEmpty()) {
		roles[ROLE_ACCOUNT_NAME] = "rAcntName";
		roles[ROLE_USER_NAME] = "rUserName";
		roles[ROLE_TEST_ACCOUNT] = "rTestAccount";
		roles[ROLE_STORE_INTO_DB] = "rStoreIntoDb";
		roles[ROLE_RECEIVED_UNREAD] = "rRcvdUnread";
		roles[ROLE_RECEIVED_TOTAL] = "rRcvdTotal";
		roles[ROLE_SENT_UNREAD] = "rSntUnread";
		roles[ROLE_SENT_TOTAL] = "rSntTotal";
	}
	return roles;
}

QVariant AccountListModel::data(const QModelIndex &index, int role) const
{
	if (Q_UNLIKELY(m_accountsPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QVariant();
	}

	if ((index.row() < 0) || (index.row() >= m_acntIds.size())) {
		return QVariant();
	}

	const AcntId acntId = m_acntIds.at(index.row());
	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return QVariant();
	}

	if (Q_UNLIKELY(!m_accountsPtr->contains(acntId))) {
		Q_ASSERT(0);
		return QVariant();
	}
	const AcntData &acntData((*m_accountsPtr)[acntId]);

	switch (role) {
	case ROLE_ACCOUNT_NAME:
		return acntData.accountName();
		break;
	case ROLE_USER_NAME:
		return acntData.userName();
		break;
	case ROLE_TEST_ACCOUNT:
		return acntData.isTestAccount();
		break;
	case ROLE_STORE_INTO_DB:
		return PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr, acntData);
		break;
	case ROLE_RECEIVED_UNREAD:
		return acntData._receivedNew();
		break;
	case ROLE_RECEIVED_TOTAL:
		return acntData._receivedTotal();
		break;
	case ROLE_SENT_UNREAD:
		return acntData._sentNew();
		break;
	case ROLE_SENT_TOTAL:
		return acntData._sentTotal();
		break;
	default:
		/* Do nothing. */
		break;
	}

	return QVariant();
}

Qt::ItemFlags AccountListModel::flags(const QModelIndex &index) const
{
	return QAbstractListModel::flags(index);
}

void AccountListModel::loadAccountsFromSettings(const QSettings &settings)
{
	debugFuncCall();

	if (Q_UNLIKELY(m_accountsPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	m_accountsPtr->loadFromSettings(QString(), settings);

	/* Clear present rows. */
	this->removeRows(0, this->rowCount());

	beginResetModel();

	/* For all credentials. */
	foreach(const QString &group, AccountsMap::sortedCredentialGroups(settings)) {
		AcntId acntId(
		    settings.value(group + "/" + CredNames::userName, QString()).toString(),
		    settings.value(group + "/" + CredNames::testAcnt, QString()).toBool());

		/* Add user name into the model. */
		m_acntIds.append(acntId);
	}

	endResetModel();
}

void AccountListModel::saveAccountsToSettings(const QString &pinVal,
    QSettings &settings) const
{
	if (Q_UNLIKELY(m_accountsPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	QString groupName;

	for (int row = 0; row < m_acntIds.size(); ++row) {
		const AcntId &acntId(m_acntIds.at(row));
		Q_ASSERT(acntId.isValid());
		const AcntData &acntData((*m_accountsPtr)[acntId]);
		Q_ASSERT(acntId.username() == acntData.userName());

		groupName = CredNames::regularCreds;
		groupName.append(QString::number(row));

		acntData.saveToSettings(pinVal, QString(), settings, groupName);
	}
}

enum AccountListModel::AddAcntResult AccountListModel::addAccount(
    const AcntData &acntData, QModelIndex *idx)
{
	if (Q_UNLIKELY(m_accountsPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return AA_ERR;
	}

	AcntId acntId(acntData.userName(), acntData.isTestAccount());
	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return AA_ERR;
	}

	if (m_accountsPtr->contains(acntId)) {
		logWarningNL("Account with username '%s' already exists.",
		    acntId.username().toUtf8().constData());
		return AA_EXISTS;
	}

	Q_ASSERT(!m_acntIds.contains(acntId));

	beginInsertRows(QModelIndex(), rowCount(), rowCount());

	(*m_accountsPtr)[acntId] = acntData;

	m_acntIds.append(acntId);

	endInsertRows();

	if (0 != idx) {
		*idx = index(m_acntIds.size() - 1, 0, QModelIndex());
	}

	return AA_SUCCESS;
}

void AccountListModel::deleteAccount(const AcntId &acntId)
{
	if (Q_UNLIKELY(m_accountsPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	QModelIndex acntIdx(acntIndex(acntId));

	if (!acntIdx.isValid()) {
		return;
	}

	int row = acntIdx.row();

	beginRemoveRows(QModelIndex(), row, row);

	m_acntIds.removeAt(row);

	m_accountsPtr->remove(acntId);

	endRemoveRows();
}

AcntId AccountListModel::acntId(const QModelIndex &index) const
{
	if (Q_UNLIKELY(!index.isValid())) {
		return AcntId();
	}

	int row = index.row();

	if ((row >= 0) && (row < m_acntIds.size())) {
		return m_acntIds[row];
	} else {
		return AcntId();
	}
}

QModelIndex AccountListModel::acntIndex(const AcntId &acntId) const
{
	if (Q_UNLIKELY(m_accountsPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QModelIndex();
	}

	for (int row = 0; row < m_acntIds.size(); ++row) {
		if (m_acntIds.at(row) == acntId) {
			Q_ASSERT(m_accountsPtr->contains(acntId));
			return index(row, 0, QModelIndex());
		}
	}

	return QModelIndex();
}

void AccountListModel::updateCounters(const AcntId &acntId, int recNew,
    int recTot, int sntNew, int sntTot)
{
	if (Q_UNLIKELY(m_accountsPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return;
	}

	if ((recNew < 0) && (recTot < 0) && (sntTot < 0)) {
		return;
	}

	if (Q_UNLIKELY(!m_acntIds.contains(acntId) ||
	        !m_accountsPtr->contains(acntId))) {
		Q_ASSERT(0);
		return;
	}

	if (recNew >= 0) {
		(*m_accountsPtr)[acntId]._setReceivedNew(recNew);
	}

	if (recTot >= 0) {
		(*m_accountsPtr)[acntId]._setReceivedTotal(recTot);
	}

	if (sntNew >= 0) {
		(*m_accountsPtr)[acntId]._setSentNew(sntNew);
	}

	if (sntTot >= 0) {
		(*m_accountsPtr)[acntId]._setSentTotal(sntTot);
	}

	handleAccountDataChange(acntId);
}

AccountListModel *AccountListModel::fromVariant(const QVariant &modelVariant)
{
	if (!modelVariant.canConvert<QObject *>()) {
		return Q_NULLPTR;
	}
	QObject *obj = qvariant_cast<QObject *>(modelVariant);
	return qobject_cast<AccountListModel *>(obj);
}

void AccountListModel::handleAccountDataChange(const AcntId &acntId)
{
	QModelIndex acntIdx(acntIndex(acntId));

	if (acntIdx.isValid()) {
		emit dataChanged(acntIdx, acntIdx);
	}
}
