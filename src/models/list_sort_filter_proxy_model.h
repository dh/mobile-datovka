/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QSortFilterProxyModel>

class ListSortFilterProxyModel : public QSortFilterProxyModel {
	Q_OBJECT
public:
	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit ListSortFilterProxyModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 * @note This is a dummy function. Calling this constructor causes an
	 *     assertion failure.
	 *
	 * @param[in] model Model to be copied.
	 * @param[in] parent Pointer to parent object.
	 */
	explicit ListSortFilterProxyModel(const ListSortFilterProxyModel &model,
	    QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Set the role where the key is used for filtering.
	 *
	 * @param[in] role Role number, -1 for all roles.
	 */
	void setFilterRole(int role);

	/*!
	 * @brief Set roles which are used for filtering.
	 *
	 * @param[in] roles Role list. If list contains -1 or is empty
	 *                  then all roles are used for filtering.
	 */
	Q_INVOKABLE
	void setFilterRoles(const QList<int> &roles);

	/*!
	 * @brief Return roles which are used for filtering.
	 *
	 * @return List of role numbers, may be empty.
	 */
	const QList<int> &filterRoles(void) const;

	/*!
	 * @brief Set source model.
	 *
	 * @param[in] sourceModel Source model to be used.
	 */
	Q_INVOKABLE virtual
	void setSourceModel(QAbstractItemModel *sourceModel) Q_DECL_OVERRIDE;

public slots:
	/*!
	 * @brief Sets regular expression according to expression core.
	 *
	 * @param[in] patternCore Regular expression core.
	 */
	Q_INVOKABLE
	void setFilterRegExpStr(const QString &patternCore);

protected:
	/*!
	 * @brief Returns true if the item in the row indicated by the
	 *     given source row and source parent should be included in the
	 *     model; otherwise returns false.
	 *
	 * @param[in] sourceRow    Row number.
	 * @param[in] sourceParent Parent index.
	 * @return Whether the row indicated should be included in the model.
	 */
	virtual
	bool filterAcceptsRow(int sourceRow,
	    const QModelIndex &sourceParent) const Q_DECL_OVERRIDE;

private:
	/*!
	 * @brief Return the role where the key is used to filter the content.
	 *
	 * @note The method is private so it cannot be used.
	 *
	 * @return Role number.
	 */
	int filterRole(void) const;

	/*!
	 * @brief Returns true if the item should be included in the model.
	 *
	 * @param[in] sourceIdx Source index.
	 * @return Whether the item meets the criteria.
	 */
	bool filterAcceptsItem(const QModelIndex &sourceIdx) const;

	QList<int> m_filterRoles; /*!< Roles used for filtering. */
};

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(ListSortFilterProxyModel)
