/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QQmlEngine> /* qmlRegisterType */
#include <QSqlRecord>
#include <QStringBuilder>

#include "src/files.h" /* TODO -- Do something with it. */
#include "src/models/filemodel.h"
#include "src/qml_identifiers/qml_account_id.h"

FileListModel::Entry::Entry(const Entry &other)
    : m_fileId(other.m_fileId),
    m_fileName(other.m_fileName),
    m_data(other.m_data),
    m_fileSize(other.m_fileSize),
    m_filePath(other.m_filePath)
{
}

FileListModel::Entry::Entry(int fileId, const QString &fileName,
    const QByteArray &data, qint64 fileSize, const QString &filePath)
    : m_fileId(fileId),
    m_fileName(fileName),
    m_data(data),
    m_fileSize(fileSize),
    m_filePath(filePath)
{
}

int FileListModel::Entry::fileId(void) const
{
	return m_fileId;
}

const QString &FileListModel::Entry::fileName(void) const
{
	return m_fileName;
}

void FileListModel::Entry::setFileName(const QString &fileName)
{
	m_fileName = fileName;
}

/*!
 * @brief Creates human readable size description string.
 *
 * @param[in] dataSize Size in bytes.
 * @return String containing units.
 */
static
QString approximateDataSize(qint64 dataSize)
{
	if (dataSize >= 1024) {
		qint64 kBytes = dataSize >> 10; /* dataSize / 1024 */
		/* (dataSize % 1024) >= 512 -> increase dicision by 1 */
		kBytes += (dataSize & 0x0200) ? 1 : 0; /* round((float)(dataSize / 1024)) */
		return QStringLiteral("~") % QString::number(kBytes) % QStringLiteral(" kB");
	} else {
		return QStringLiteral("~") % QString::number(dataSize) % QStringLiteral(" B");
	}
}

QString FileListModel::Entry::fileSizeStr(void) const
{
	if (m_fileSize >= 0) {
		return approximateDataSize(m_fileSize);
	} else {
		return QString();
	}
}

const QByteArray &FileListModel::Entry::binaryContent(void) const
{
	return m_data;
}

void FileListModel::Entry::setBinaryContent(const QByteArray &data)
{
	m_data = data;
}

qint64 FileListModel::Entry::fileSize(void) const
{
	return m_fileSize;
}

void FileListModel::Entry::setFileSize(qint64 fileSize)
{
	m_fileSize = fileSize;
}

const QString &FileListModel::Entry::filePath(void) const
{
	return m_filePath;
}

void FileListModel::Entry::setFilePath(const QString &filePath)
{
	m_filePath = filePath;
}

void FileListModel::declareQML(void)
{
	qmlRegisterType<FileListModel>("cz.nic.mobileDatovka.models", 1, 0, "FileListModel");
	qRegisterMetaType<FileListModel>("FileListModel");
	qRegisterMetaType<FileListModel::Roles>("FileListModel::Roles");

	qRegisterMetaType<FileListModel *>("FileListModel *");
	qRegisterMetaType<FileListModel *>("const FileListModel *");
}

FileListModel::FileListModel(QObject *parent)
    : QAbstractListModel(parent),
    m_files()
{
}

FileListModel::FileListModel(const FileListModel &model, QObject *parent)
    : QAbstractListModel(parent),
    m_files(model.m_files)
{
}

int FileListModel::rowCount(const QModelIndex &parent) const
{
	return !parent.isValid() ? m_files.size() : 0;
}

QHash<int, QByteArray> FileListModel::roleNames(void) const
{
	static QHash<int, QByteArray> roles;
	if (roles.isEmpty()) {
		roles[ROLE_FILE_ID] = "rFileId";
		roles[ROLE_FILE_NAME] = "rFileName";
		roles[ROLE_FILE_SIZE_STR] = "rFileSizeStr";
		roles[ROLE_BINARY_DATA] = "rBinaryContent";
		roles[ROLE_FILE_SIZE_BYTES] = "rFileSizeBytes";
		roles[ROLE_FILE_PATH] = "rFilePath";
	}
	return roles;
}

QVariant FileListModel::data(const QModelIndex &index, int role) const
{
	if ((index.row() < 0) || (index.row() >= m_files.size())) {
		return QVariant();
	}

	const Entry &file(m_files[index.row()]);

	switch (role) {
	case ROLE_FILE_ID:
		return file.fileId();
		break;
	case ROLE_FILE_NAME:
		return file.fileName();
		break;
	case ROLE_FILE_SIZE_STR:
		return file.fileSizeStr();
		break;
	case ROLE_BINARY_DATA:
		return file.binaryContent();
		break;
	case ROLE_FILE_SIZE_BYTES:
		return file.fileSize();
		break;
	case ROLE_FILE_PATH:
		return file.filePath();
		break;
	default:
		/* Do nothing. */
		break;
	}

	return QVariant();
}

QList<FileListModel::Entry> FileListModel::allEntries(void) const
{
	QList<FileListModel::Entry> entries;

	foreach (const FileListModel::Entry &file, m_files) {
		entries.append(file);
	}

	return entries;
}

QString FileListModel::filePathFromRow(int row) const
{
	if (Q_UNLIKELY((row < 0) || (row >= m_files.size()))) {
		Q_ASSERT(0);
		return QString();
	}

	return m_files[row].filePath();
}

qint64 FileListModel::dataSizeSum(void) const
{
	qint64 sum = 0;
	foreach (const Entry &entry, m_files) {
		sum += entry.fileSize();
	}
	return sum;
}

Qt::ItemFlags FileListModel::flags(const QModelIndex &index) const
{
	return QAbstractListModel::flags(index);
}

void FileListModel::setQuery(QSqlQuery &query)
{
	if (query.record().count() != 3) {
		return;
	}

	beginResetModel();

	m_files.clear();

	query.first();
	while (query.isActive() && query.isValid()) {
		m_files.append(Entry(query.value(0).toInt(),
		    query.value(1).toString(), QByteArray(),
		    query.value(2).toLongLong(), QString()));
		query.next();
	}

	endResetModel();
}

void FileListModel::appendFileEntry(const Entry &entry)
{
	beginInsertRows(QModelIndex(), rowCount(), rowCount());
	m_files.append(entry);
	endInsertRows();
}

void FileListModel::appendFileFromPath(int fileId, const QString &fileName,
    const QString &filePath, qint64 fileSizeBytes)
{
	beginInsertRows(QModelIndex(), rowCount(), rowCount());
	m_files.append(FileListModel::Entry(fileId, fileName, QByteArray(),
	    fileSizeBytes, filePath));
	endInsertRows();
}

void FileListModel::clearAll(void)
{
	beginResetModel();
	m_files.clear();
	endResetModel();
}

FileListModel *FileListModel::fromVariant(const QVariant &modelVariant)
{
	if (!modelVariant.canConvert<QObject *>()) {
		return Q_NULLPTR;
	}
	QObject *obj = qvariant_cast<QObject *>(modelVariant);
	return qobject_cast<FileListModel *>(obj);
}

bool FileListModel::setFromDb(const QmlAcntId *qAcntId, const QString &msgIdStr)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	bool ok = false;
	qint64 msgId = msgIdStr.toLongLong(&ok);
	if (!ok || (msgId < 0)) {
		return false;
	}

	return Files::setAttachmentModel(*this, qAcntId, msgId);
}

void FileListModel::removeItem(int row)
{
	if ((row < 0) || (row >= m_files.size())) {
		return;
	}

	beginRemoveRows(QModelIndex(), row, row);
	m_files.removeAt(row);
	endRemoveRows();
}
