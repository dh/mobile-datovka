/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QQmlEngine> /* qmlRegisterType */

#include "src/records_management/models/upload_hierarchy_qml_proxy_model.h"

void UploadHierarchyQmlProxyModel::declareQML(void)
{
	qmlRegisterType<UploadHierarchyQmlProxyModel>("cz.nic.mobileDatovka.models", 1, 0, "UploadHierarchyQmlProxyModel");
	qRegisterMetaType<UploadHierarchyQmlProxyModel>("UploadHierarchyQmlProxyModel");
}

UploadHierarchyQmlProxyModel::UploadHierarchyQmlProxyModel(QObject *parent)
    : RecMgmt::UploadHierarchyProxyModel(parent)
{
}

UploadHierarchyQmlProxyModel::UploadHierarchyQmlProxyModel(
    const UploadHierarchyQmlProxyModel &other, QObject *parent)
    : RecMgmt::UploadHierarchyProxyModel(parent)
{
	Q_UNUSED(other);
}

void UploadHierarchyQmlProxyModel::setSourceModel(QAbstractItemModel *sourceModel)
{
	RecMgmt::UploadHierarchyProxyModel::setSourceModel(sourceModel);
}

int UploadHierarchyQmlProxyModel::mapToSourceContent(int proxyRow) const
{
	const QModelIndex proxyIndex(index(proxyRow, 0, QModelIndex()));
	return RecMgmt::UploadHierarchyProxyModel::mapToSource(proxyIndex).row();
}

void UploadHierarchyQmlProxyModel::setFilterRole(int role)
{
	RecMgmt::UploadHierarchyProxyModel::setFilterRole(role);
}

void UploadHierarchyQmlProxyModel::sortContent(void)
{
	RecMgmt::UploadHierarchyProxyModel::sort(0, Qt::AscendingOrder);
}

void UploadHierarchyQmlProxyModel::setFilterRegExpStr(const QString &patternCore)
{
	setFilterRegExp(QRegExp(patternCore,
	    Qt::CaseInsensitive, QRegExp::FixedString));
}
