/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <algorithm> /* ::std::sort */
#include <QQmlEngine> /* qmlRegisterType */

#include "src/datovka_shared/localisation/localisation.h"
#include "src/records_management/models/upload_hierarchy_list_model.h"

void UploadHierarchyListModel::declareQML(void)
{
	qmlRegisterType<UploadHierarchyListModel>("cz.nic.mobileDatovka.models", 1, 0, "UploadHierarchyListModel");
	qRegisterMetaType<UploadHierarchyListModel>("UploadHierarchyListModel");
	qRegisterMetaType<UploadHierarchyListModel::Roles>("UploadHierarchyListModel::Roles");

	qRegisterMetaType<UploadHierarchyListModel *>("UploadHierarchyListModel *");
	qRegisterMetaType<UploadHierarchyListModel *>("const UploadHierarchyListModel *");
}

UploadHierarchyListModel::UploadHierarchyListModel(QObject *parent)
    : QAbstractListModel(parent),
    m_hierarchy(),
    m_workingRoot(Q_NULLPTR),
    m_selected()
{
}

UploadHierarchyListModel::UploadHierarchyListModel(
    const UploadHierarchyListModel &other, QObject *parent)
    : QAbstractListModel(parent),
    m_hierarchy(other.m_hierarchy),
    m_workingRoot(m_hierarchy.root()),
    m_selected(other.m_selected)
{
}

QModelIndex UploadHierarchyListModel::index(int row, int column,
    const QModelIndex &parent) const
{
	if (!hasIndex(row, column, parent)) {
		return QModelIndex();
	}

	quintptr internalId = 0;

	if (!parent.isValid()) {
		/* List model has always invalid parent. */
		if (Q_UNLIKELY(m_workingRoot == Q_NULLPTR)) {
			Q_ASSERT(0);
			return QModelIndex();
		}

		internalId = (quintptr)m_workingRoot->sub().at(row);
	} else {
		const RecMgmt::UploadHierarchyResp::NodeEntry *entry =
		    (RecMgmt::UploadHierarchyResp::NodeEntry *)parent.internalId();
		internalId = (quintptr)entry->sub().at(row);
	}

	return createIndex(row, column, internalId);
}

QModelIndex UploadHierarchyListModel::parent(const QModelIndex &index) const
{
	if (!index.isValid()) {
		return QModelIndex();
	}

	const RecMgmt::UploadHierarchyResp::NodeEntry *iEntry =
	    (RecMgmt::UploadHierarchyResp::NodeEntry *)index.internalId();
	if (Q_UNLIKELY(iEntry == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QModelIndex();
	}
	const RecMgmt::UploadHierarchyResp::NodeEntry *pEntry = iEntry->super();

	if ((pEntry != Q_NULLPTR) &&
	    ((pEntry != m_workingRoot) || showRootName())) {
		if (pEntry == m_workingRoot) {
			/* Root node is shown and parent is root. */
			return QModelIndex();
		} else {
			const RecMgmt::UploadHierarchyResp::NodeEntry *ppEntry =
			    pEntry->super();
			int row = 0;
			/* Find position of parent. */
			for ( ; row < ppEntry->sub().size(); ++row) {
				if (pEntry == ppEntry->sub().at(row)) {
					break;
				}
			}
			Q_ASSERT(row < ppEntry->sub().size());
			return createIndex(row, 0, (quintptr)pEntry);
		}
	} else {
		return QModelIndex();
	}
}

int UploadHierarchyListModel::rowCount(const QModelIndex &parent) const
{
	if (parent.column() > 0) {
		return 0;
	}

	if (!parent.isValid()) {
		/* List model has always invalid parent. */

		if ((!m_hierarchy.isValid()) || (m_workingRoot == Q_NULLPTR)) {
			/* Invalid hierarchy. */
			return 0;
		}

		return m_workingRoot->sub().size();
	} else {
		const RecMgmt::UploadHierarchyResp::NodeEntry *entry =
		    (RecMgmt::UploadHierarchyResp::NodeEntry *)parent.internalId();
		return entry->sub().size();
	}
}

QHash<int, QByteArray> UploadHierarchyListModel::roleNames(void) const
{
	static QHash<int, QByteArray> roles;
	if (roles.isEmpty()) {
		roles[ROLE_NAME] = "rName";
		roles[ROLE_FILTER_DATA] = "rFilterData";
		roles[ROLE_FILTER_DATA_RECURSIVE] = "rFilterDataRecursive";
		roles[ROLE_ID] = "rId";
		roles[ROLE_LEAF] = "rLeaf";
		roles[ROLE_SELECTABLE] = "rSelectable";
		roles[ROLE_SELECTED] = "rSelected";
		roles[ROLE_SELECTED_RECURSIVE] = "rSelectedRecursive";
	}
	return roles;
}

/*!
 * @brief Return all data related to node which can be used for
 *     filtering.
 *
 * @param[in] entry Node identifier.
 * @return List of strings.
 */
static
QStringList filterData(const RecMgmt::UploadHierarchyResp::NodeEntry *entry)
{
	if (Q_UNLIKELY(entry == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QStringList();
	}

	return QStringList(entry->name()) + entry->metadata();
}

/*!
 * @brief Returns list of all (meta)data (including children).
 *
 * @param[in] entry Node identifying the root.
 * @param[in] takeSuper Set true when data of superordinate node should
 *                      be taken into account.
 * @return List of all gathered data according to which can be filtered.
 */
static
QStringList filterDataRecursive(
    const RecMgmt::UploadHierarchyResp::NodeEntry *entry, bool takeSuper)
{
	if (Q_UNLIKELY(entry == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QStringList();
	}

	QStringList res(filterData(entry));
	foreach (const RecMgmt::UploadHierarchyResp::NodeEntry *sub, entry->sub()) {
		res += filterDataRecursive(sub, false);
	}
	if (takeSuper) {
		/*
		 * Add also filter data from superordinate node. This has the
		 * effect that all sub-nodes (including those not matching the
		 * filter) of a node which matches the entered filter are
		 * going to be also displayed.
		 */
		const RecMgmt::UploadHierarchyResp::NodeEntry *sup = entry->super();
		if (Q_UNLIKELY(sup == Q_NULLPTR)) {
			return res;
		}
		res += filterData(sup);
	}

	return res;
}

/* Return true if entry is selectable. */
#define isSelectable(entry) \
	(!(entry)->id().isEmpty())

/*!
 * @brief Check whether identifier of node is in set.
 *
 * @param[in] entry Node entry.
 * @param[in] idMap Identifier map to search in.
 * @param[in] takeSub Whether sub-nodes are to be taken into
 *                    consideration.
 * @return true if node identifier is in set or any of its sub-nodes if
 *     takeSub enabled.
 */
static
bool idInSelection(const RecMgmt::UploadHierarchyResp::NodeEntry *entry,
    const QMap<QString, QString> &idMap, bool takeSub)
{
	if (Q_UNLIKELY(entry == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	int present = false;
	const QString &id(entry->id());
	if (!id.isEmpty()) {
		present = idMap.contains(id);
	}

	if (takeSub) {
		int i = 0;
		while ((!present) && (i < entry->sub().size())) {
			present = idInSelection(entry->sub().at(i), idMap, true);
			++i;
		}
	}

	return present;
}

QVariant UploadHierarchyListModel::data(const QModelIndex &index,
    int role) const
{
	if (!index.isValid()) {
		return QVariant();
	}

	const RecMgmt::UploadHierarchyResp::NodeEntry *entry =
	    (RecMgmt::UploadHierarchyResp::NodeEntry *)index.internalId();
	if (Q_UNLIKELY(entry == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QVariant();
	}

	switch (role) {
	case Qt::DisplayRole:
		return entry->name();
		break;
	case ROLE_NAME:
		return entry->name();
		break;
	case ROLE_FILTER_DATA:
		return filterData(entry).join(QStringLiteral("\n"));
		break;
	case ROLE_FILTER_DATA_RECURSIVE:
		return filterDataRecursive(entry, true);
		break;
	case ROLE_ID:
		return entry->id();
		break;
	case ROLE_LEAF:
		return entry->sub().size() <= 0;
		break;
	case ROLE_SELECTABLE:
		return isSelectable(entry);
		break;
	case ROLE_SELECTED:
		return idInSelection(entry, m_selected, false);
		break;
	case ROLE_SELECTED_RECURSIVE:
		return idInSelection(entry, m_selected, true);
		break;
	default:
		return QVariant();
		break;
	}
}

Qt::ItemFlags UploadHierarchyListModel::flags(const QModelIndex &index) const
{
	if (!index.isValid()) {
		return Qt::NoItemFlags;
	}

	Qt::ItemFlags flags =
	    QAbstractItemModel::flags(index) & ~Qt::ItemIsEditable;

	const RecMgmt::UploadHierarchyResp::NodeEntry *entry =
	    (RecMgmt::UploadHierarchyResp::NodeEntry *)index.internalId();
	if (Q_UNLIKELY(entry == Q_NULLPTR)) {
		Q_ASSERT(0);
		return Qt::NoItemFlags;
	}
	if (!isSelectable(entry)) {
		flags &= ~Qt::ItemIsSelectable;
	}

	return flags;
}

void UploadHierarchyListModel::setHierarchy(
    const RecMgmt::UploadHierarchyResp &uhr)
{
	beginResetModel();
	m_hierarchy = uhr;
	m_workingRoot = m_hierarchy.root();
	m_selected.clear();
	endResetModel();
}

void UploadHierarchyListModel::navigateSuper(void)
{
	if (m_workingRoot != Q_NULLPTR) {
		const RecMgmt::UploadHierarchyResp::NodeEntry *sup =
		    m_workingRoot->super();
		/* Ascend the hierarchy only if the root node has a name. */
		if ((!m_workingRoot->name().isEmpty()) && (sup != Q_NULLPTR)) {
			beginResetModel();
			m_workingRoot = sup;
			endResetModel();
		}
	}
}

void UploadHierarchyListModel::navigateSub(int row)
{
	if (Q_UNLIKELY(row < 0) || (row >= rowCount())) {
		Q_ASSERT(0);
		return;
	}

	if (m_workingRoot != Q_NULLPTR) {
		const RecMgmt::UploadHierarchyResp::NodeEntry *sub =
		    m_workingRoot->sub().at(row);
		/*
		 * Descend the hierarchy only if the target node is not a leaf.
		 */
		if ((sub != Q_NULLPTR) && (sub->sub().size() > 0)) {
			beginResetModel();
			m_workingRoot = sub;
			endResetModel();
		}
	}
}

/*!
 * @brief Return entry name.
 *
 * @param[in] entry Entry pointer.
 * @param[in] takeSuper If true, then full path across all
 *                      superordinate nodes is taken.
 * @param[in] sep Separator to be used. It is always appended to
 *                the end of the returned string.
 * @return Name or path of names to the entry.
 */
static
QString entryName(const RecMgmt::UploadHierarchyResp::NodeEntry *entry,
    bool takeSuper, const QString &sep)
{
	if (Q_UNLIKELY(entry == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QString();
	}

	QString nodeName(entry->name() + sep);
	if (takeSuper) {
		entry = entry->super();
		while ((entry != Q_NULLPTR) && (!entry->name().isEmpty())) {
			nodeName = entry->name() + sep + nodeName;
			entry = entry->super();
		}
	}

	if ((!sep.isEmpty()) && (0 != nodeName.indexOf(sep))) {
		nodeName = sep + nodeName;
	}

	return nodeName;
}

QString UploadHierarchyListModel::navigatedRootName(bool takeSuper,
    const QString &sep) const
{
	if ((!m_hierarchy.isValid() || (m_workingRoot == Q_NULLPTR))) {
		return sep;
	}

	return entryName(m_workingRoot, takeSuper, sep);
}

bool UploadHierarchyListModel::toggleNodeSelection(int row)
{
	if (Q_UNLIKELY(row < 0) || (row >= rowCount())) {
		Q_ASSERT(0);
		return false;
	}

	if (m_workingRoot != Q_NULLPTR) {
		const RecMgmt::UploadHierarchyResp::NodeEntry *sub =
		    m_workingRoot->sub().at(row);
		if ((sub != Q_NULLPTR) && isSelectable(sub)) {
			if (idInSelection(sub, m_selected, false)) {
				m_selected.remove(sub->id());
			} else {
				m_selected.insert(sub->id(),
				    entryName(sub, true, "/")); /* TODO -- Make the separator configurable. */
			}
			QModelIndex changedIdx(index(row, 0, QModelIndex()));
			QVector<int> roles(2);
			roles[0] = ROLE_SELECTED;
			roles[1] = ROLE_SELECTED_RECURSIVE;
			emit dataChanged(changedIdx, changedIdx);
			return true;
		}
	}

	return false;
}

QStringList UploadHierarchyListModel::selectedIds(void) const
{
	return m_selected.keys();
}

/*!
 * @brief Used for sorting hierarchy names.
 */
class HierarchyNameLess {
public:
	bool operator()(const QString &a, const QString &b) const
	{
		return Localisation::stringCollator.compare(a, b) < 0;
	}
};

QStringList UploadHierarchyListModel::selectedFullNames(bool sort) const
{
	QStringList values(m_selected.values());

	if (sort) {
		::std::sort(values.begin(), values.end(), HierarchyNameLess());
	}

	return values;
}

UploadHierarchyListModel *UploadHierarchyListModel::fromVariant(
    const QVariant &modelVariant)
{
	if (!modelVariant.canConvert<QObject *>()) {
		return Q_NULLPTR;
	}
	QObject *obj = qvariant_cast<QObject *>(modelVariant);
	return qobject_cast<UploadHierarchyListModel *>(obj);
}

bool UploadHierarchyListModel::showRootName(void) const
{
	return m_hierarchy.isValid() && (m_workingRoot != Q_NULLPTR) &&
	    !m_workingRoot->name().isEmpty();
}
