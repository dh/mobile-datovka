/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractListModel>
#include <QMap>
#include <QString>

#include "src/datovka_shared/records_management/json/upload_hierarchy.h"

/*!
 * @brief Upload hierarchy list model.
 */
class UploadHierarchyListModel : public QAbstractListModel {
	Q_OBJECT

public:
	/*!
	 * @brief Roles which this model supports.
	 */
	enum Roles {
		ROLE_NAME = Qt::UserRole,
		ROLE_FILTER_DATA,
		ROLE_FILTER_DATA_RECURSIVE,
		ROLE_ID,
		ROLE_LEAF,
		ROLE_SELECTABLE,
		ROLE_SELECTED, /* The actual node is selected. */
		ROLE_SELECTED_RECURSIVE /* Actual node or any sub-node is selected. */
	};
	Q_ENUM(Roles)

	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Pointer to parent object.
	 */
	explicit UploadHierarchyListModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 *
	 * @param[in] other Model to be copied.
	 * @param[in] parent Pointer to parent object.
	 */
	explicit UploadHierarchyListModel(const UploadHierarchyListModel &other,
	    QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Return index specified by supplied parameters.
	 *
	 * @param[in] row    Item row.
	 * @param[in] column Parent column.
	 * @param[in] parent Parent index.
	 * @return Index to desired element or invalid index on error.
	 */
	virtual
	QModelIndex index(int row, int column,
	    const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Return parent index of the item with the given index.
	 *
	 * @param[in] index Child node index.
	 * @return Index of the parent node or invalid index on error.
	 */
	virtual
	QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Return number of rows under the given parent.
	 *
	 * @param[in] parent Parent node index.
	 * @return Number of rows.
	 */
	virtual
	int rowCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the model's role names.
	 *
	 * @return Model's role names.
	 */
	virtual
	QHash<int, QByteArray> roleNames(void) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Return data stored in given location under given role.
	 *
	 * @param[in] index Index specifying the item.
	 * @param[in] role  Data role.
	 * @return Data from model.
	 */
	virtual
	QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns item flags for given index.
	 *
	 * @brief[in] index Index specifying the item.
	 * @return Item flags.
	 */
	virtual
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Set content data.
	 *
	 * @param[in] uhr Upload hierarchy response structure.
	 */
	void setHierarchy(const RecMgmt::UploadHierarchyResp &uhr);

	/*!
	 * @brief Ascend the tree hierarchy. Set superordinate node to act
	 *    as working root.
	 */
	Q_INVOKABLE
	void navigateSuper(void);

	/*!
	 * @brief Descend the tree hierarchy. Set a subordinate node to act
	 *     as working root.
	 *
	 * @param[in] row Sub-node index to be selected.
	 */
	Q_INVOKABLE
	void navigateSub(int row);

	/*!
	 * @brief Return path to working root node.
	 *
	 * @param[in] takeSuper If true, then full path across all
	 *                      superordinate nodes is taken.
	 * @param[in] sep Separator to be used. It is always appended to
	 *                the end of the returned string.
	 * @return Name or path of names to the working root node.
	 */
	Q_INVOKABLE
	QString navigatedRootName(bool takeSuper, const QString &sep) const;

	/*!
	 * @brief Toggles the node selection.
	 *
	 * @note Emits dataChanged() signal.
	 *
	 * @param[in] row Sub-node index to be used.
	 * @return True if selection was toggled.
	 */
	Q_INVOKABLE
	bool toggleNodeSelection(int row);

	/*!
	 * @brief Return unsorted list of selected node identifiers.
	 *
	 * @return List of node identifiers.
	 */
	Q_INVOKABLE
	QStringList selectedIds(void) const;

	/*!
	 * @brief Return list of selected node names.
	 *
	 * @param[in] sort Set to true if list should be sorted alpabetically.
	 * @return List of full names.
	 */
	Q_INVOKABLE
	QStringList selectedFullNames(bool sort) const;

	/*!
	 * @brief Converts QVariant (obtained from QML) into a pointer.
	 *
	 * @note Some weird stuff happens in QML when passing instances
	 *     directly as constant reference. Wrong constructors are called
	 *     and no data are passed.
	 * @note QML passes objects (which were created in QML) as QVariant
	 *     values holding pointers. You therefore may call invokable methods
	 *     with QVariant arguments from QML.
	 * @note If you use
	 *     qRegisterMetaType<Type *>("Type *") and
	 *     qRegisterMetaType<Type *>("const Type *")
	 *     then QML will be able to call invokable methods without explicit
	 *     conversion from QVariant arguments.
	 *     Q_DECLARE_METATYPE(Type *) is not needed.
	 *
	 * @param[in] variant QVariant holding the pointer.
	 * @return Pointer if it could be acquired, Q_NULLPTR else. This
	 *     function does not allocate a new instance.
	 */
	static
	UploadHierarchyListModel *fromVariant(const QVariant &modelVariant);

private:
	/*!
	 * @brief Check whether to show root name.
	 *
	 * @return True in root node contains a name.
	 */
	bool showRootName(void) const;

	RecMgmt::UploadHierarchyResp m_hierarchy; /*!< Upload hierarchy structure. */
	const RecMgmt::UploadHierarchyResp::NodeEntry *m_workingRoot; /*!< Node acting as working root. */
	QMap<QString, QString> m_selected; /*!< Keys are ids, values are full names. */
};

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(UploadHierarchyListModel)
Q_DECLARE_METATYPE(UploadHierarchyListModel::Roles)
