/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QObject>
#include <QString>

#include "src/auxiliaries/ios_helper.h"
#include "src/json/backup.h"
#include "src/qml_identifiers/qml_account_id.h"

class BackupRestoreSelectionModel; /* Forward declaration. */
class QDateTime; /* Forward declaration. */

/*
 * Class BackupRestoreData provides interface between QML and backup core.
 * Class is initialised in the main function (main.cpp)
 */
class BackupRestoreData : public QObject {
	Q_OBJECT

public:
	enum BackupRestoreType {
		BRT_UNKNOWN = 0,
		BRT_BACKUP,
		BRT_TRANSFER
	};
	Q_ENUM(BackupRestoreType)

	/*!
	 * @brief Declare various properties to the QML system.
	 */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit BackupRestoreData(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 *
	 * @param[in] other Object to be copied.
	 */
	BackupRestoreData(const BackupRestoreData &other);

	/*!
	 * @brief Fill backup account model.
	 *
	 * @param[in,out] acntModel Account model.
	 */
	Q_INVOKABLE static
	void fillBackupModel(BackupRestoreSelectionModel *acntModel);

	/*!
	 * @brief Fill restore account model.
	 *
	 * @param[in,out] acntModel Account model.
	 */
	Q_INVOKABLE
	void fillRestoreModel(BackupRestoreSelectionModel *acntModel);

	/*!
	 * @brief Backup one account.
	 *
	 * @param[in] qAcntId Account id.
	 * @param[in] targetPath Target path for backup.
	 * @param[in] includeZfoDb Include zfo database into backup.
	 * @return True if success.
	 */
	Q_INVOKABLE
	bool backUpAccount(const QmlAcntId *qAcntId, const QString &targetPath,
	    bool includeZfoDb);

	/*!
	 * @brief Backup all selected accounts.
	 *
	 * @param[in] acntModel Backup account selection model.
	 * @param[in] targetPath Target path for backup.
	 * @param[in] includeZfoDb Include zfo database into backup.
	 * @return True if success.
	 */
	Q_INVOKABLE
	bool backUpSelectedAccounts(BackupRestoreSelectionModel *acntModel,
	    const QString &targetPath, bool includeZfoDb);

	/*!
	 * @brief Check if backup of one account can be processed.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] targetPath Target path for backup.
	 * @param[in] includeZfoDb Include zfo database into size.
	 * @param[in] transfer True if backup is transfer.
	 * @return True if it is possible to backup one account.
	 */
	Q_INVOKABLE
	bool canBackUpAccount(const QmlAcntId *qAcntId,
	    const QString &targetPath, bool includeZfoDb, bool transfer);

	/*!
	 * @brief Check if backup of selected accounts can be processed.
	 *
	 * @param[in] acntModel Backup account selection model.
	 * @param[in] targetPath Target path for backup.
	 * @param[in] includeZfoDb Include zfo database into size.
	 * @param[in] transfer True if backup is transfer.
	 * @return True if it is possible to backup all accounts and data.
	 */
	Q_INVOKABLE
	bool canBackupSelectedAccounts(BackupRestoreSelectionModel *acntModel,
	    const QString &targetPath, bool includeZfoDb, bool transfer);

	/*!
	 * @brief Load JSON file content.
	 *
	 * @param[in] jsonPath Path where backup JSON file is located.
	 * @return Return backup type.
	 */
	Q_INVOKABLE
	enum BackupRestoreType loadBackupJson(const QString &jsonPath);

	/*!
	 * @brief Restore complete application data from transfer.
	 *
	 * @return True if success.
	 */
	Q_INVOKABLE
	bool restoreDataFromTransfer(void);

	/*!
	 * @brief Restore selected accounts from backup.
	 *
	 * @param[in] acntModel Backup account selection model.
	 * @param[in] sourcePath Path where backup data are located.
	 * @param[in] includeZfoDb Include zfo database.
	 * @return True if success.
	 */
	Q_INVOKABLE
	bool restoreSelectedAccounts(BackupRestoreSelectionModel *acntModel,
	    const QString &sourcePath, bool includeZfoDb);

	/*!
	 * @brief Check if restore of selected accounts can be processed.
	 *
	 * @param[in] acntModel Backup account selection model.
	 * @param[in] srcJsonPath Path where backup json file is located.
	 * @param[in] includeZfoDb Include zfo database.
	 * @return True if it is possible to restore selected accounts data.
	 */
	Q_INVOKABLE
	bool canRestoreSelectedAccounts(
	    BackupRestoreSelectionModel *acntModel, const QString &srcJsonPath,
	    bool includeZfoDb);

	/*!
	 * @brief Stop worker.
	 *
	 * @param[in] stop True if worker pool has stopped.
	 */
	Q_INVOKABLE
	void stopWorker(bool stop);

	/*!
	 * @brief Backup complete datovka data for transfer to other device.
	 *
	 * @param[in] targetPath Target path for backup saving.
	 * @return True if success.
	 */
	Q_INVOKABLE
	bool transferCompleteDatovkaData(const QString &targetPath);

signals:
	/*!
	 * @brief Send backup date time info to QML.
	 *
	 * @param[in] backupDateTime Backup date time text.
	 */
	void bacupDateTextSig(QString backupDateTime);

	/*!
	 * @brief Send restore info to QML.
	 *
	 * @param[in] actionInfo Action info text.
	 */
	void actionTextSig(QString actionInfo);

	/*!
	 * @brief Send restore size info to QML.
	 *
	 * @param[in] sizeInfo Size info text.
	 */
	void sizeTextSig(QString sizeInfo);

	/*!
	 * @brief Set some QML elements visibility.
	 *
	 * @param[in] visible True if element can be seen.
	 */
	void visibleZfoSwitchSig(bool visible);

	/*!
	 * @brief Set some QML elements visibility.
	 *
	 * @param[in] restore True if element can be seen.
	 */
	void canRestoreFromTransferSig(bool restore);

private:
	/*!
	 * @brief Backup one account.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] targetPath Target path for backup.
	 * @param[in] dateTime Backup date time.
	 * @param[out] iCloudFiles List of files for iCloud upload.
	 * @return Valid entry on success, invalid entry on failure.
	 */
	Json::Backup::MessageDb backUpOneAccount(const AcntId &acntId,
	    const QString &targetPath, const QDateTime &dateTime,
	    QList<IosHelper::FileICloud> &iCloudFiles);

	/*!
	 * @brief Back up accounts.
	 *
	 * @param[in] acntIds Account identifier list.
	 * @param[in] targetPath Target path for backup.
	 * @param[in] includeZfoDb Include zfo database into backup.
	 * @return True if success.
	 */
	bool backUpAccounts(const QList<AcntId> &acntIds,
	    const QString &targetPath, bool includeZfoDb);

	/*!
	 * @brief Check if backup can be processed.
	 *
	 * @param[in] acntIdList Account identifier (can be null).
	 * @param[in] targetPath Target path for backup.
	 * @param[in] includeZfoDb Include zfo database into size.
	 * @param[in] transfer True if backup is transfer.
	 * @return True if it is possible to backup all accounts and data.
	 */
	bool canBackUpAccounts(const QList<AcntId> &acntIdList,
	    const QString &targetPath, bool includeZfoDb, bool transfer);

	/*!
	 * @brief Compute account backup data size in bytes.
	 *
	 * @param[in] acntId Account identifier.
	 * @return Account backup data size in bytes.
	 */
	qint64 getAccountBackupSizeBytes(const AcntId &acntId);

	/*!
	 * @brief Compute target location free space in bytes.
	 *
	 * @param[in] targetPath Target path for backup.
	 * @return Target location free space in bytes.
	 */
	qint64 computeAvailableSpace(const QString &targetPath);

	/*!
	 * @brief Check if can restore from transfer.
	 *
	 * @param[in] transferTotalSizeBytes Source path.
	 * @return True if can restore from transfer.
	 */
	bool canRestoreFromTransfer(qint64 transferTotalSizeBytes);

	/*!
	 * @brief Restore from accounts backup.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] accountName Account name.
	 * @param[in] subdir Backup subdirectory name.
	 * @param[in] sourceDir Backup full path.
	 * @return True if success.
	 */
	bool restoreAccount(const AcntId &acntId, const QString &accountName,
	    const QString &subdir, const QString &sourceDir);

	/*!
	 * @brief Send size info to QML.
	 *
	 * @param[in] backupSize Backup size in bytes.
	 * @param[in] targetSize Free space size in bytes in target location.
	 */
	void sendSizeInfoToQml(qint64 backupSize, qint64 targetSize);

	QString m_jsonPath; /*!< Path to JSON file. */
	Json::Backup m_loadedBackup; /*!< Loaded backup content. */
};

/* Declare BackupRestoreType to QML. */
Q_DECLARE_METATYPE(BackupRestoreData::BackupRestoreType)
