/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QScopedPointer>

#include "src/datovka_shared/gov_services/service/gov_service.h"
#include "src/datovka_shared/gov_services/service/gov_services_all.h"
#include "src/datovka_shared/isds/message_interface.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/log/memory_log.h"
#include "src/dialogues/dialogues.h"
#include "src/global.h"
#include "src/gov_services/models/gov_form_list_model.h"
#include "src/gov_services/models/gov_service_list_model.h"
#include "src/gov_wrapper.h"
#include "src/isds/isds_wrapper.h"
#include "src/qml_identifiers/qml_account_id.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/sqlite/account_db.h"
#include "src/datovka_shared/identifiers/account_id.h"

GovWrapper::GovWrapper(IsdsWrapper *isds, QObject *parent)
    : QObject(parent),
    m_govServices(),
    m_isds(isds)
{
	m_govServices = Gov::allServiceMap();
}

GovWrapper::~GovWrapper(void)
{
	Gov::clearServiceMap(m_govServices);
}

void GovWrapper::loadServicesToModel(const QmlAcntId *qAcntId,
   GovServiceListModel *srvcModel) const
{
	debugFuncCall();

	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(srvcModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access gov services model.");
		Q_ASSERT(0);
		return;
	}

	const Isds::DbOwnerInfoExt2 dbOwnerInfo(
	    GlobInstcs::accountDbPtr->getOwnerInfo(qAcntId->username()));

	srvcModel->clearAll();

	if (qAcntId->testing()) {
		/*
		 * The e-gov services aren't available in the testing
		 * environment.
		 */
		logWarningNL("%s",
		    "Cannot access e-gov services from an testing account.");
		return;
	}

	foreach (const QString &key, m_govServices.keys()) {
		const Gov::Service *cgs = m_govServices.value(key);
		if (Q_UNLIKELY(cgs == Q_NULLPTR)) {
			Q_ASSERT(0);
			continue;
		}
		/* Enlist only services which can be used. */
		if (cgs->canSend(dbOwnerInfo.dbType())) {
			srvcModel->appendService(cgs);
		} else {
			logInfo("User '%s' cannot use the e-gov service '%s'.",
			    qAcntId->username().toUtf8().constData(),
			    cgs->internalId().toUtf8().constData());
		}
	}
}

void GovWrapper::loadFormToModel(const QmlAcntId *qAcntId,
    const QString &serviceInternalId, GovFormListModel *formModel) const
{
	debugFuncCall();

	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	const Gov::Service *cgs = m_govServices.value(serviceInternalId, Q_NULLPTR);
	if (Q_UNLIKELY(cgs == Q_NULLPTR)) {
		logErrorNL("Cannot access gov service '%s'.",
		    serviceInternalId.toUtf8().constData());
		Q_ASSERT(0);
		return;
	}
	QScopedPointer<Gov::Service> gs(cgs->createNew());
	if (Q_UNLIKELY(gs.isNull())) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(formModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access gov form model.");
		Q_ASSERT(0);
		return;
	}

	/* Set content from data box. */
	const Isds::DbOwnerInfoExt2 dbOwnerInfo(
	    GlobInstcs::accountDbPtr->getOwnerInfo(qAcntId->username()));
	gs->setOwnerInfoFields(dbOwnerInfo);

	Gov::Service *oldService = formModel->setService(gs.take());
	if (oldService != Q_NULLPTR) {
		delete oldService; oldService = Q_NULLPTR;
	}
}

bool GovWrapper::sendGovRequest(const QmlAcntId *qAcntId,
    const QString &serviceInternalId, GovFormListModel *formModel) const
{
	debugFuncCall();

	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	const Gov::Service *cgs = m_govServices.value(serviceInternalId, Q_NULLPTR);
	if (Q_UNLIKELY(cgs == Q_NULLPTR)) {
		logErrorNL("Cannot access e-gov service '%s'.",
		    serviceInternalId.toUtf8().constData());
		Q_ASSERT(0);
		return false;
	}

	if (Q_UNLIKELY(formModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access e-gov form model.");
		Q_ASSERT(0);
		return false;
	}

	const Gov::Service *gs = formModel->service();
	if (Q_UNLIKELY(gs == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access e-gov service from form model.");
		Q_ASSERT(0);
		return false;
	}

	{
		QString service = tr("Request: %1").arg(gs->fullName());
		service.append("\n");
		service.append(tr("Recipient: %1").arg(gs->instituteName()));

		int msgResponse = Dialogues::message(Dialogues::QUESTION,
		    tr("Send e-gov request"),
		    tr("Do you want to send the e-gov request to data box '%1'?").arg(gs->boxId()),
		    service, Dialogues::NO | Dialogues::YES, Dialogues::NO);
		if (msgResponse == Dialogues::NO) {
			return false;
		}
	}

	if (GlobInstcs::accountDbPtr == Q_NULLPTR) {
		return false;
	}

	/* Set message content according to model data. */
	if (!gs->haveAllMandatoryFields()) {
		logErrorNL("The e-gov service '%s' is missing some mandatory data.",
		    gs->internalId().toUtf8().constData());
		return false;
	}

	const Isds::Message msg(gs->dataMessage());
	if (Q_UNLIKELY(msg.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	if (Q_NULLPTR != m_isds) {
		return m_isds->sendGovRequest((AcntId)*qAcntId, msg);
	}

	return false;
}
