/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/common.h"
#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/box_interface.h"
#include "src/datovka_shared/isds/type_conversion.h"
#include "src/global.h"
#include "src/isds/conversion/isds_conversion.h"
#include "src/net/db_wrapper.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/settings/prefs_specific.h"
#include "src/setwrapper.h"
#include "src/sqlite/account_db.h"
#include "src/sqlite/zfo_db.h"

QString DbWrapper::createAccountInfoStringForQml(
    const Isds::DbOwnerInfoExt2 &dbOwnerInfo)
{
	QString dbInfo(divStart);

	dbInfo.append(strongInfoLine(tr("Data box ID"), dbOwnerInfo.dbID()));
	dbInfo.append(strongInfoLine(tr("Data box type"),
	    Isds::dbType2Str(dbOwnerInfo.dbType())));
	if (!dbOwnerInfo.ic().isEmpty()) {
		dbInfo.append(strongInfoLine(tr("IČO"), dbOwnerInfo.ic()));
	}
	if (!dbOwnerInfo.personName().givenNames().isEmpty()) {
		dbInfo.append(strongInfoLine(tr("Given names"),
		    dbOwnerInfo.personName().givenNames()));
	}
	if (!dbOwnerInfo.personName().lastName().isEmpty()) {
		dbInfo.append(strongInfoLine(tr("Surname"),
		    dbOwnerInfo.personName().lastName()));
	}
	if (!dbOwnerInfo.birthInfo().date().toString().isEmpty()) {
		dbInfo.append(strongInfoLine(tr("Date of birth"),
		    dbOwnerInfo.birthInfo().date().toString()));
	}
	if (!dbOwnerInfo.birthInfo().city().isEmpty()) {
		dbInfo.append(strongInfoLine(tr("City of birth"),
		    dbOwnerInfo.birthInfo().city()));
	}
	if (!dbOwnerInfo.birthInfo().county().isEmpty()) {
		dbInfo.append(strongInfoLine(tr("County of birth"),
		    dbOwnerInfo.birthInfo().county()));
	}
	if (!dbOwnerInfo.birthInfo().state().isEmpty()) {
		dbInfo.append(strongInfoLine(tr("State of birth"),
		    dbOwnerInfo.birthInfo().state()));
	}
	if (!dbOwnerInfo.firmName().isEmpty()) {
		dbInfo.append(strongInfoLine(tr("Company name"),
		    dbOwnerInfo.firmName()));
	}
	/* Street of residence can be empty in some cases */
	if (!dbOwnerInfo.address().street().isEmpty()) {
		dbInfo.append(strongInfoLine(tr("Street of residence"),
		    dbOwnerInfo.address().street()));
	}
	dbInfo.append(strongInfoLine(tr("Number in street"),
	    dbOwnerInfo.address().numberInStreet()));
	dbInfo.append(strongInfoLine(tr("Number in municipality"),
	    dbOwnerInfo.address().numberInMunicipality()));
	dbInfo.append(strongInfoLine(tr("Zip code"),
	    dbOwnerInfo.address().zipCode()));
	/*
	 * TODO: adDistrict we don't use now.
	 * Note: District collumn is missing in the db table.
	 */
	if (!dbOwnerInfo.address().district().isEmpty()) {
		dbInfo.append(strongInfoLine(tr("District of residence"),
		    dbOwnerInfo.address().district()));
	}
	dbInfo.append(strongInfoLine(tr("City of residence"),
	    dbOwnerInfo.address().city()));
	if (!dbOwnerInfo.address().state().isEmpty()) {
		dbInfo.append(strongInfoLine(tr("State of residence"),
		    dbOwnerInfo.address().state()));
	}
	if (!dbOwnerInfo.nationality().isEmpty()) {
		dbInfo.append(strongInfoLine(tr("Nationality"),
		    dbOwnerInfo.nationality()));
	}
	dbInfo.append(strongInfoLine(tr("Databox state"),
	    IsdsConversion::dbStateToText(dbOwnerInfo.dbState())));
	dbInfo.append(strongInfoLine(tr("Open addressing"),
	    (dbOwnerInfo.dbOpenAddressing() == Isds::Type::BOOL_TRUE)
	        ? tr("Yes") : tr("No")));

	dbInfo.append(divEnd);

	return dbInfo;
}

bool DbWrapper::insertMessageListToDb(const AcntId &acntId,
    enum MessageDb::MessageType messageType,
    const QList<Isds::Envelope> &envelopes,
    QList<qint64> &messageChangedStatusList, QString &txt,
    QList<qint64> &listOfNewMsgIds)
{
	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		txt = tr("Internal error!");
		Q_ASSERT(0);
		return false;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[acntId]));
	if (msgDb == Q_NULLPTR) {
		txt = tr("Cannot open message database!");
		return false;
	}

	bool isSentMessage = (messageType == MessageDb::TYPE_SENT);
	int newMsgs = 0;

	msgDb->beginTransaction();
	foreach (const Isds::Envelope &envelope, envelopes) {
		bool hasFiles;
		int msgStatus =
		    msgDb->getMessageStatusFromDb(envelope.dmId(), hasFiles);
		// -1 = message is not in the database, mesasge is new
		if (-1 == msgStatus) {
			newMsgs++;
			if (!msgDb->insertOrUpdateMessageEnvelopeInDb(
			    envelope.dmId(), messageType, envelope)) {
				txt = tr("Message %1 envelope insertion failed!").
				    arg(envelope.dmId());
			} else {
				// New message envelope has been saved into
				// database. Append message id to list
				// of complete messages to be downloaded.
				listOfNewMsgIds.append(envelope.dmId());
			}
		} else {
			if (!msgDb->updateMessageEnvelopeInDb(envelope)) {
				txt = tr("Message %1 envelope update failed!").
				    arg(envelope.dmId());
			}
			if (isSentMessage) {
				if (msgStatus != envelope.dmMessageStatus() && hasFiles) {
					messageChangedStatusList.append(envelope.dmId());
				}
			}
		}
	}
	msgDb->commitTransaction();

	if (!isSentMessage) {
		if (newMsgs > 0) {
			txt = tr("%1: new messages: %2")
			    .arg(acntId.username()).arg(QString::number(newMsgs));
		} else {
			txt = tr("%1: No new messages.").arg(acntId.username());
		}
	}

	return true;
}

bool DbWrapper::insertCompleteMessageToDb(const AcntId &acntId,
    const Isds::Message &message, enum MessageDb::MessageType messageType,
    QString &txt)
{
	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		txt = tr("Internal error!");
		return false;
	}

	const bool dataOnDisk = PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr,
	    (*GlobInstcs::acntMapPtr)[acntId]);

	/* Open file database */
	FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId.username(), dataOnDisk);
	if (fDb == Q_NULLPTR) {
		txt = tr("Cannot open file database!");
		return false;
	}

	qint64 dmId = message.envelope().dmId();

	/* Insert or update attachment files */
	fDb->beginTransaction();
	foreach (const Isds::Document &document, message.documents()) {
		if (!fDb->insertUpdateFileIntoDb(dmId, document)) {
			txt = tr("File %1 insertion failed!").
			    arg(document.fileDescr());
			return false;
		}
	}
	fDb->commitTransaction();

	/* Open message database */
	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId.username(), dataOnDisk);
	if (msgDb == Q_NULLPTR) {
		txt = tr("Cannot open message database!");
		goto fail;
	}

	/* Insert or update message envelope data */
	if (!msgDb->insertOrUpdateMessageEnvelopeInDb(dmId, messageType,
	    message.envelope())) {
		txt = tr("Message %1 envelope update failed!").
		    arg(dmId);
		goto fail;
	} else {
		msgDb->setAttachmentDownloaded(dmId, true);
	}

	return true;
fail:
	fDb->rollbackTransaction();
	return false;
}

bool DbWrapper::insertPwdExpirationToDb(const AcntId &acntId,
    const QDateTime &expirDate)
{
	if (GlobInstcs::accountDbPtr != Q_NULLPTR) {
		return GlobInstcs::accountDbPtr->updatePwdExpirInDb(
		    acntId.username(), expirDate);
	} else {
		Q_ASSERT(0);
		return false;
	}
}

bool DbWrapper::insertAccountInfoToDb(const AcntId &acntId,
    const Isds::DbOwnerInfoExt2 &dbOwnerInfo)
{
	if (GlobInstcs::accountDbPtr != Q_NULLPTR) {
		return GlobInstcs::accountDbPtr->insertAccountInfoIntoDb(
		    acntId.username(), dbOwnerInfo);
	} else {
		Q_ASSERT(0);
		return false;
	}
}

bool DbWrapper::insertDTInfoToDb(const QString &dbID,
    const Isds::DTInfoOutput &dtInfo)
{
	if (GlobInstcs::accountDbPtr != Q_NULLPTR) {
		return GlobInstcs::accountDbPtr->insertDTInfoIntoDb(
		    dbID, dtInfo);
	} else {
		Q_ASSERT(0);
		return false;
	}
}

bool DbWrapper::insertUserInfoToDb(const AcntId &acntId,
    const Isds::DbUserInfoExt2 &dbUserInfo)
{
	if (GlobInstcs::accountDbPtr != Q_NULLPTR) {
		return GlobInstcs::accountDbPtr->insertUserInfoIntoDb(
		    acntId.username(), dbUserInfo);
	} else {
		Q_ASSERT(0);
		return false;
	}
}

bool DbWrapper::updateAuthorInfo(const AcntId &acntId,
    enum Isds::Type::SenderType userType, const QString &authorName,
    qint64 msgId, QString &txt)
{
	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		txt = tr("Internal error!");
		Q_ASSERT(0);
		return false;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[acntId]));
	if (msgDb == Q_NULLPTR) {
		txt = tr("Cannot open message database!");
		return false;
	}

	QString data = IsdsConversion::senderTypeToDescr(userType);

	if (!authorName.isEmpty()) {
		data += ", " + authorName;
	}

	return msgDb->updateMessageAuthorInfoInDb(msgId, data);
}

bool DbWrapper::insertMesasgeDeliveryInfoToDb(const AcntId &acntId,
    const QList<Isds::Event> &events, qint64 msgId,  QString &txt)
{
	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		txt = tr("Internal error!");
		Q_ASSERT(0);
		return false;
	}

	bool ret = true;

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[acntId]));
	if (msgDb == Q_NULLPTR) {
		txt = tr("Cannot open message database!");
		return false;
	}

	ret = ret && msgDb->beginTransaction();
	foreach (const Isds::Event &event, events) {
		ret = ret && msgDb->insertEventsIntoDb(msgId, event);
	}
	ret = ret && msgDb->commitTransaction();

	return ret;
}

bool DbWrapper::insertZfoToDb(qint64 msgId, bool isTestAccount, int zfoSize,
    const QByteArray &zfoData)
{
	if (Q_UNLIKELY(GlobInstcs::zfoDbPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	return GlobInstcs::zfoDbPtr->insertZfoToDb(msgId, isTestAccount,
	    zfoSize, zfoData.toBase64(),
	    (GlobalSettingsQmlWrapper::zfoDbSizeMBs() * 1000000));
}
