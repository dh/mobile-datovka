/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject> /* Q_GADGET */
#include <QString>

#include "src/datovka_shared/settings/account.h"

class QSettings; /* Forward declaration. */

class AcntDataPrivate;
/*!
 * @brief Holds account settings and other data.
 */
class AcntData : public AcntSettings {
	Q_DECLARE_PRIVATE(AcntData)
	Q_GADGET

public:
	/*!
	 * @brief Expose login method identifier from AcntSettings.
	 *
	 * @note Cannot use 'using AcntSettings::LoginMethod' here.
	 */
	enum LoginMethod {
		LIM_UNKNOWN, /*!< Unknown method. */
		LIM_UNAME_PWD, /*!< User name and password. */
		LIM_UNAME_CRT, /*!< User name and certificate. */
		LIM_UNAME_PWD_CRT, /*!< User name, password and certificate. */
		LIM_UNAME_PWD_HOTP, /*!< User name, password and HOTP. */
		LIM_UNAME_PWD_TOTP, /*!< User name, password and TOTP (SMS). */
		LIM_UNAME_MEP /*!< User name, mobile key (MEP). */
	};
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
	Q_ENUM(LoginMethod)
#else /* < Qt-5.5 */
	Q_ENUMS(LoginMethod)
#endif /* >= Qt-5.5 */

	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	AcntData(void);
	AcntData(const AcntData &other);
#ifdef Q_COMPILER_RVALUE_REFS
	AcntData(AcntData &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
	virtual /* To keep the compiler quiet, the destructor needs not to be virtual here. */
	~AcntData(void);

	AcntData &operator=(const AcntData &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
	AcntData &operator=(AcntData &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

	bool operator==(const AcntData &other) const;
	bool operator!=(const AcntData &other) const;

	friend void swap(AcntData &first, AcntData &second) Q_DECL_NOTHROW;

	/* Translates enum LoginMethod. */
	enum LoginMethod loginMethod(void) const;
	void setLoginMethod(enum LoginMethod method);

	/* Certificate passphrase. Is not saved. */
	const QString &_passphrase(void) const;
	void _setPassphrase(const QString &pp);
#ifdef Q_COMPILER_RVALUE_REFS
	void _setPassphrase(QString &&pp);
#endif /* Q_COMPILER_RVALUE_REFS */
	/*
	 * Values below here are displayed, but have nothing to do with
	 * actual account.
	 *
	 * TODO -- Move it somewhere into the model.
	 */
	int _receivedNew(void) const;
	void _setReceivedNew(int rn);
	int _receivedTotal(void) const;
	void _setReceivedTotal(int rt);
	int _sentNew(void) const;
	void _setSentNew(int sn);
	int _sentTotal(void) const;
	void _setSentTotal(int st);

protected:
	/* This class uses the d_ptr from its parent class. */
	/* Allow subclasses to initialise with their own specific Private. */
	AcntData(AcntDataPrivate *d);

private:
	/* Allow parent code to instantiate *Private subclass. */
	virtual
	bool ensurePrivate(void) Q_DECL_OVERRIDE;
};

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(AcntData::LoginMethod)

void swap(AcntData &first, AcntData &second) Q_DECL_NOTHROW;
