/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QSettings>

#include "src/datovka_shared/log/log.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/settings/convert_for_compatibility.h"

/*!
 * @brief Rename some credentials keys.
 */
static
void convertEntryNames(QSettings &settings)
{
	foreach (const QString credGroup, AccountsMap::sortedCredentialGroups(settings)) {
		settings.beginGroup(credGroup);

		const QStringList keys = settings.childKeys();
		if (keys.size() > 0) {
			logInfoNL("Converting '%s' to compatible keys.", credGroup.toUtf8().constData());
		}
		foreach (const QString &key, keys) {
			if (key == "account_name") {
				QString val = settings.value(key, "").toStringList().join(", ");
				settings.remove(key);
				settings.setValue(CredNames::acntName, val);
			} else if (key == "user_name") {
				QString val = settings.value(key, "").toString();
				settings.remove(key);
				settings.setValue(CredNames::userName, val);
			} else if (key == "cert_path") {
				QString val = settings.value(key, "").toString();
				settings.remove(key);
				settings.setValue(CredNames::p12File, val);
			}
		}

		settings.endGroup();
	}

	settings.sync();
}

/*!
 * @brief Rename login method names.
 */
static
void convertLoginMethodNames(QSettings &settings)
{
	foreach (const QString credGroup, AccountsMap::sortedCredentialGroups(settings)) {
		settings.beginGroup(credGroup);

		foreach (const QString &key, settings.childKeys()) {
			if (key == "login_method") {
				const QString val = settings.value(key, "").toString();
				QString newVal;
				if (val == "pwd") {
					newVal = "username";
				} else if (val == "cert") {
					newVal = "user_certificate";
				} /* Values 'hotp' and 'totp' remain.*/
				if (!newVal.isEmpty()) {
					logInfoNL("Converting '%s' of '%s' from '%s' to '%s'.",
					    key.toUtf8().constData(),
					    credGroup.toUtf8().constData(),
					    val.toUtf8().constData(),
					    newVal.toUtf8().constData());
					settings.setValue(key, newVal);
				}
			}
		}

		settings.endGroup();
	}

	settings.sync();
}

void iniPreferencesForCompatibility(QSettings &settings)
{
	convertEntryNames(settings);
	convertLoginMethodNames(settings);
}
