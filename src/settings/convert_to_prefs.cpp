/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QSet>
#include <QSettings>

#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/settings/prefs.h"
#include "src/datovka_shared/settings/prefs_helper.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/settings/convert_to_prefs.h"

/*!
 * @brief Convert global settings.
 */
static
void convertGLobals(QSettings &settings, Prefs &prefs)
{
	settings.beginGroup("GLOBALS");
	const QStringList keys = settings.childKeys();
	if (keys.size() > 0) {
		logInfoNL("%s", "Converting global settings.");
	}
	foreach (const QString &key, keys) {
		if (key == "complete_msgs") {
			prefs.setBoolVal(
			    "action.sync_account.tie.action.download_message",
			    settings.value(key).toBool());
		} else if (key == "only_new_msgs") {
			prefs.setBoolVal(
			    "action.sync_account.restrict_to.new_messages.enabled",
			    settings.value(key).toBool());
		} else if (key == "debug_verbosity_level") {
			bool iOk = false;
			qint64 val = settings.value(key).toLongLong(&iOk);
			if (iOk && (val >= 0)) {
				prefs.setIntVal("logging.verbosity.level.debug",
				    val);
			}
		} else if (key == "log_verbosity_level") {
			bool iOk = false;
			qint64 val = settings.value(key).toLongLong(&iOk);
			if (iOk && (val >= 0)) {
				prefs.setIntVal("logging.verbosity.level.log",
				    val);
			}
		} else if (key == "language") {
			QString val = settings.value(key).toString();
			if (!val.isEmpty()) {
				prefs.setStrVal("translation.language", val);
			}
		} else if (key == "font_size") {
			bool iOk = false;
			qint64 val = settings.value(key).toLongLong(&iOk);
			if (iOk && (val >= 0)) {
				prefs.setIntVal("font.size", val);
			}
		} else if (key == "msg_lifetime") {
			bool iOk = false;
			qint64 val = settings.value(key).toLongLong(&iOk);
			if (iOk && (val >= 0)) {
				prefs.setIntVal(
				    "storage.database.envelopes.keep_downloaded.days",
				    val);
			}
		} else if (key == "file_lifetime") {
			bool iOk = false;
			qint64 val = settings.value(key).toLongLong(&iOk);
			if (iOk && (val >= 0)) {
				prefs.setIntVal(
				    "storage.database.attachments.keep_downloaded.days",
				    val);
			}
		} else if (key == "dbs_location") {
			QString val = settings.value(key).toString();
			if (!val.isEmpty()) {
				prefs.setStrVal("storage.database.path", val);
			}
		} else if (key == "zfo_db_max_size") {
			bool iOk = false;
			qint64 val = settings.value(key).toLongLong(&iOk);
			if (iOk && (val >= 0)) {
				prefs.setIntVal(
				    "storage.database.zfos.size.max.MB", val);
			}
		} else if (key == "pin_inact_timeout") {
			bool iOk = false;
			qint64 val = settings.value(key).toLongLong(&iOk);
			/*
			 * Original configuration value was stored in seconds.
			 * New value stores milliseconds.
			 */
			if (iOk && (val >= 0)) {
				prefs.setIntVal("ui.inactivity.lock.timeout.ms",
				    val * 1000);
			}
		} else if (key == "last_update") {
			QString val = settings.value(key).toString();
			QDateTime dateTime(QDateTime::fromString(val, Qt::ISODate));
			if (dateTime.isValid()) {
				prefs.setDateTimeVal(
				    "action.sync_account.last_invocation.datetime",
				    dateTime);
			}
		}
	}

	settings.remove("complete_msgs");
	settings.remove("only_new_msgs");
	settings.remove("debug_verbosity_level");
	settings.remove("log_verbosity_level");
	settings.remove("language");
	settings.remove("font_size");
	settings.remove("msg_lifetime");
	settings.remove("file_lifetime");
	settings.remove("dbs_location");
	settings.remove("zfo_db_max_size");
	settings.remove("pin_inact_timeout");
	settings.remove("last_update");

	settings.endGroup();

	settings.sync();
}

/*!
 * @brief Convert local storage credential settings.
 */
static
void convertCredetialsStorage(QSettings &settings, Prefs &prefs)
{
	foreach (const QString credGroup, AccountsMap::sortedCredentialGroups(settings)) {
		settings.beginGroup(credGroup);

		const QStringList keys = settings.childKeys();
		{
#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
			QSet<QString> keySet(keys.begin(), keys.end());
#else /* < Qt-5.14.0 */
			QSet<QString> keySet = keys.toSet();
#endif /* >= Qt-5.14.0 */
			Q_ASSERT(keySet.contains(CredNames::userName));
			Q_ASSERT(keySet.contains(CredNames::testAcnt));
		}

		const QString acntId = PrefsHelper::accountIdentifier(
		        settings.value(CredNames::userName).toString(),
		        settings.value(CredNames::testAcnt).toBool());

		foreach (const QString &key, keys) {
			if (key == "use_local_storage_db") {
				logInfoNL(
				    "Converting %s storage settings to preferences.",
				    credGroup.toUtf8().constData());

				bool val = settings.value(key, true).toBool();
				settings.remove(key);
				prefs.setBoolVal(
				    "account." + acntId + ".storage.databases.on_disk.enabled",
				    val);
			}
		}

		settings.endGroup();
	}
}

void iniPreferencesToPrefs(QSettings &settings, Prefs &prefs)
{
	convertGLobals(settings, prefs);
	convertCredetialsStorage(settings, prefs);
}
