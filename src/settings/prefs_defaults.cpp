/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/datovka_shared/io/prefs_db.h"
#include "src/datovka_shared/localisation/localisation.h"
#include "src/datovka_shared/settings/prefs.h"
#include "src/font/font.h"
#include "src/io/filesystem.h"
#include "src/settings/prefs_defaults.h"
#include "src/setwrapper.h"

void PrefsDefaults::eraseVanished(PrefsDb *prefsDb)
{
	if (Q_UNLIKELY(prefsDb == Q_NULLPTR)) {
		return;
	}

	/* The listed values are no more used in the application. */

	prefsDb->erase("page.account_list.on.start.banner.mojeid.visibility.count");
}

void PrefsDefaults::setDefaults(Prefs &prefs)
{
	prefs.setDefault(Prefs::Entry(
	    "action.sync_account.tie.action.download_message",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "action.sync_account.restrict_to.new_messages.enabled",
	    PrefsDb::VAL_BOOLEAN, true));
	prefs.setDefault(Prefs::Entry(
	    "page.account_list.on.start.tie.action.sync_all_accounts",
	    PrefsDb::VAL_BOOLEAN, false));
	prefs.setDefault(Prefs::Entry(
	    "page.account_list.on.start.banner.mojeid2.visibility.count",
	    PrefsDb::VAL_INTEGER, 10));

	prefs.setDefault(Prefs::Entry(
	    "logging.verbosity.level.debug",
	    PrefsDb::VAL_INTEGER, 1));
	prefs.setDefault(Prefs::Entry(
	    "logging.verbosity.level.log",
	    PrefsDb::VAL_INTEGER, 1));

	prefs.setDefault(Prefs::Entry(
	    "translation.language",
	    PrefsDb::VAL_STRING, Localisation::langSystem));

	prefs.setDefault(Prefs::Entry(
	    "font.size",
	    PrefsDb::VAL_INTEGER, 16));
	prefs.setDefault(Prefs::Entry(
	    "font.type",
	    PrefsDb::VAL_STRING, Font::fontSystem));

	prefs.setDefault(Prefs::Entry(
	    "storage.database.envelopes.keep_downloaded.days",
	    PrefsDb::VAL_INTEGER, 0)); /* 0 stands for do not delete. */
	prefs.setDefault(Prefs::Entry(
	    "storage.database.attachments.keep_downloaded.days",
	    PrefsDb::VAL_INTEGER, 0)); /* 0 stands for do not delete. */
	prefs.setDefault(Prefs::Entry(
	    "storage.database.path",
	    PrefsDb::VAL_STRING, dfltDbAndConfigLoc()));
	prefs.setDefault(Prefs::Entry(
	    "storage.database.zfos.size.max.MB",
	    PrefsDb::VAL_INTEGER, 200));

	prefs.setDefault(Prefs::Entry(
	    "ui.inactivity.lock.timeout.ms",
	    PrefsDb::VAL_INTEGER, 0)); /* 0 stands for do not lock */

	prefs.setDefault(Prefs::Entry(
	    "action.sync_account.last_invocation.datetime",
	    PrefsDb::VAL_DATETIME, QDateTime()));

	prefs.setDefault(Prefs::Entry(
	    "accounts.password.expiration.notify_ahead.days",
	    PrefsDb::VAL_INTEGER, 7));
	prefs.setDefault(Prefs::Entry(
	    "accounts.message.deletion.notify_ahead.days",
	    PrefsDb::VAL_INTEGER, 7));
}
