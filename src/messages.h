/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>
#include <QVariant>

#include "src/qml_identifiers/qml_account_id.h"
#include "src/qml_isds/message_interface.h"

class AccountListModel; /* Forward declaration. */
class DataboxListModel; /* Forward declaration. */
class MessageListModel; /* Forward declaration. */

/*
 * Class Messages provides interface between QML and message database.
 * Class is initialised in the main function (main.cpp)
 */
class Messages : public QObject {
    Q_OBJECT

public:
	/* See class MessageDb. */
	enum MessageType {
		TYPE_RECEIVED = 0x01, /*!< One is received. */
		TYPE_SENT = 0x02 /*!< Two is sent. */
	};
	Q_ENUM(MessageType)
	Q_DECLARE_FLAGS(MessageTypes, MessageType)
	Q_FLAG(MessageTypes)

	/*!
	 * @brief Declare various properties to the QML system.
	 */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit Messages(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Load contacts from database and fill QML list view via model.
	 *
	 * @param[in,out] databoxModel Data box model to be set.
	 * @param[in] qAcntId Account identifier.
	 * @param[in] dbId Account data box ID.
	 */
	Q_INVOKABLE static
	void fillContactList(DataboxListModel *databoxModel,
	    const QmlAcntId *qAcntId, const QString &dbId);

	/*!
	 * @brief Load messages from database and fill QML list view via model.
	 *
	 * @param[in,out] messageModel Message model to be set.
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgType Message orientation.
	 */
	Q_INVOKABLE static
	void fillMessageList(MessageListModel *messageModel,
	    const QmlAcntId *qAcntId, enum MessageType msgType);

	/*!
	 * @brief Returns HTML-formatted string containing message description.
	 *
	 * @note QML has no notion about qint64, therefore the id is passed
	 *     via a string.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgIdStr String containing message identifier.
	 * @return Non-empty string on success.
	 */
	Q_INVOKABLE static
	QString getMessageDetail(const QmlAcntId *qAcntId,
	    const QString &msgIdStr);

	/*!
	 * @brief Get message envelope data to QML and set recipient into model.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] dmId Message identifier.
	 * @param[in,out] databoxModel Data box model to be set.
	 * @return Pointer to message envelope data object.
	 */
	Q_INVOKABLE
	QmlIsdsEnvelope *getMsgEnvelopeDataAndSetRecipient(const QmlAcntId *qAcntId,
	    qint64 msgId, DataboxListModel *databoxModel);

	/*!
	 * @brief Set message in database as locally read.
	 *
	 * @param[in,out] messageModel Message model to be set.
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgId Message identifier.
	 * @param[in] isRead True if read.
	 */
	Q_INVOKABLE static
	void markMessageAsLocallyRead(MessageListModel *messageModel,
	    const QmlAcntId *qAcntId, qint64 msgId, bool isRead);

	/*!
	 * @brief Set all messages in database as locally read/unread.
	 *
	 * @param[in,out] messageModel Message model to be set.
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgType Message orientation.
	 * @param[in] isRead True if read.
	 */
	Q_INVOKABLE static
	void markMessagesAsLocallyRead(MessageListModel *messageModel,
	    const QmlAcntId *qAcntId, enum MessageType msgType, bool isRead);

	/*!
	 * @brief Override message as having its attachments having downloaded.
	 *
	 * @param[in] dmId Message id.
	 * @param[in] forceDownloaded Attachments downloaded state.
	 */
	Q_INVOKABLE static
	void overrideDownloaded(MessageListModel *messageModel,
	    qint64 dmId, bool forceDownloaded);

	/*!
	 * @brief Update records management icon after message upload.
	 *
	 * @param[in,out] messageModel Message model to be set.
	 * @param[in] qAcntId Account identifier.
	 * @param[in] dmId Message id.
	 * @param[in] isUploadRm Set whether to force records management
	 *                       upload state.
	 */
	Q_INVOKABLE
	void updateRmStatus(MessageListModel *messageModel,
	    const QmlAcntId *qAcntId, qint64 dmId, bool isUploadRm);

	/*!
	 * @brief Delete selected message from databases.
	 *
	 * @param[in] accountModel Account model.
	 * @param[in] messageModel Message model.
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgId Identifier of the message to be deleted.
	 */
	Q_INVOKABLE static
	void deleteMessageFromDbs(AccountListModel *accountModel,
	    MessageListModel *messageModel, const QmlAcntId *qAcntId,
	    qint64 msgId);

	/*!
	 * @brief Move or create new databases to new location.
	 *
	 * @param[in] newLocation New databases location (folder).
	 * @return True on success, false of failure.
	 */
	Q_INVOKABLE
	bool moveOrCreateNewDbsToNewLocation(const QString &newLocation);

	/*!
	 * @brief Search messages via all accounts.
	 *
	 * @param[in,out] messageModel Message model to be set.
	 * @param[in] phrase Search phrase.
	 * @param[in] msgTypes Sought message types.
	 * @return Number of messages in the search result.
	 */
	Q_INVOKABLE static
	int searchMsg(MessageListModel *messageModel, const QString &phrase,
	    MessageTypes msgTypes);

	/*!
	 * @brief Delete messages from all message databases together with files
	 *        where lifetime expired.
	 *
	 * @param[in] days Lifetime in days.
	 */
	void deleteExpiredMessagesFromDbs(int days);

signals:
	/*!
	 * @brief Update message detail info.
	 *
	 * @param[in] msgHtmlInfo Message detail info string.
	 */
	void updateMessageDetail(QString msgHtmlInfo);

private:
	/*!
	 * @brief Specifies whether the files should be moved or created from
	 *     scratch.
	 */
	enum ReloactionAction {
		RA_NONE, /*!< Don't do anything. */
		RA_RELOCATE, /*!< relocate existing ones. */
		RA_CREATE_NEW /*!< Create new empty databases. .*/
	};

	/*!
	 * @brief Generates a dialogue and asks the user what he wants to do.
	 *
	 * @return The chosen action.
	 */
	static
	enum ReloactionAction askAction(void);

	/*!
	 * @brief Return size of all databases in bytes.
	 *
	 * @return Total size of all database files or -1 on error.
	 */
	static
	int getAllDbSize(void);

	/*!
	 * @brief Estimate new database size in new location.
	 *
	 * @return Estimated required size.
	 */
	static
	int estimateAllDbSize(void);

	/*!
	 * @brief Relocates account database files.
	 *
	 * @param[in] newLocationDir New location directory.
	 * @param[in] action Desired action to preform.
	 * @return True on success, false of failure.
	 */
	static
	bool relocateDatabases(const QString &newLocationDir,
	    enum ReloactionAction action);
};

Q_DECLARE_OPERATORS_FOR_FLAGS(Messages::MessageTypes)

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(Messages::MessageType)
Q_DECLARE_METATYPE(Messages::MessageTypes)
