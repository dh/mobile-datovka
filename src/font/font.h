/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QFont>
#include <QString>

class Font {

private:
	/*!
	 * @brief Private constructor.
	 */
	Font(void);

public:
	/*!
	 * @brief Remember supplied font as the default system font.
	 *
	 * @note The problem is that QApplication::font() returns the used font
	 *      which may be the default system font if the font has not been
	 *      set by the user.
	 *      But after QApplication::setFont() has been called the call to
	 *      QApplication::font() returns the font which has been assigned
	 *      using the setFont() method.
	 *      It is therefore better to remember the default application font
	 *      before changing its value.
	 *
	 * @param[in] font Font to be identified as default system font.
	 */
	static
	void setSystemFont(const QFont &font);

	/*!
	 * @brief Return font.
	 *
	 * @param[in] fCode Font code.
	 * @return Font or system font if \a fCode is unknown.
	 */
	static
	QFont font(const QString &fCode);

	/*!
	 * @brief Returns font code.
	 *
	 * @param[in] fCode Font code.
	 * @return Font code or system font code if \a fCode is unknown.
	 */
	static
	QString fontCode(const QString &fCode);

	/*
	 * Using QString instead of char* causes problems with initialisation
	 * order, where the instances may be used before actually being created.
	 */
	static
	const char *fontSystem; /*!< System font code. */
	static
	const char *fontRoboto; /*!< Roboto font code. */
	static
	const char *fontSourceSansPro; /*!< Source Sans Pro font code. */

private:
	/*!
	 * @brief Return remembered system font.
	 *
	 * @return Remembered system font. If no font has been remembered then
	 *     it returns the value of QApplication::font().
	 */
	static
	QFont systemFont(void);

	static
	QFont *sysFontCopy; /*!< Font to be used as default system font. */
};
