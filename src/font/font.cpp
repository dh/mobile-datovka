/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QApplication>
#include <QFontDatabase>

#include "src/datovka_shared/log/log.h"
#include "src/font/font.h"

const char *Font::fontSystem = "system";
const char *Font::fontRoboto = "roboto";
const char *Font::fontSourceSansPro = "source_sans_pro";

/*
 * Cannot use an object instance here because of the unpredictable static
 * initialisation order which may call the constructor of QFont().
 * The constructor may then read an uninitialised default value causing
 * the application to crash.
 */
QFont *Font::sysFontCopy = Q_NULLPTR;

void Font::setSystemFont(const QFont &font)
{
	sysFontCopy = new (::std::nothrow) QFont(font);
}

/*!
 * @brief Obtain font from font database.
 *
 * @param[in,out] id Font id in the font database. If -1 then font is gong
 *                   to be loaded from the supplied file.
 * @param[in] fileName File name to load the font from if \a id is -1.
 * @param[in] dfltFont Default font to be returned if font cannot be accessed
 *                     or loaded from file.
 * @return Desired font of \a dfltFont on error.
 */
static
QFont getFont(int &id, const QString &fileName, const QFont &dfltFont)
{
	if (id == -1) {
		if (Q_UNLIKELY(fileName.isEmpty())) {
			Q_ASSERT(0);
			return dfltFont;
		}
		id = QFontDatabase::addApplicationFont(fileName);
		if (Q_UNLIKELY(id == -1)) {
			logErrorNL("Cannot load font from file '%s'.",
			    fileName.toUtf8().constData());
			return dfltFont;
		}
	}
	return QFont(QFontDatabase::applicationFontFamilies(id).at(0));
}

QFont Font::font(const QString &fCode)
{
	static int idRoboto = -1;
	static int idSourceSansRegular = -1;

	if (fCode == fontRoboto) {
		return getFont(idRoboto, ":/font/Roboto-Regular.ttf",
		    systemFont());
	} else if (fCode == fontSourceSansPro) {
		return getFont(idSourceSansRegular,
		    ":/font/SourceSansPro-Regular.ttf", systemFont());
	} else {
		return systemFont();
	}
}

QString Font::fontCode(const QString &fCode)
{
	if (fCode == fontRoboto) {
		return fontRoboto;
	} else if (fCode == fontSourceSansPro) {
		return fontSourceSansPro;
	} else {
		return fontSystem;
	}
}

QFont Font::systemFont(void)
{
	if (sysFontCopy != Q_NULLPTR) {
		return *sysFontCopy;
	} else {
		return QApplication::font();
	}
}
