/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QString>

#include "src/datovka_shared/io/sqlite/db_single.h"
#include "src/datovka_shared/isds/box_interface.h"
#include "src/datovka_shared/isds/box_interface2.h"
#include "src/net/db_wrapper.h"

class QDateTime; /* Forward declaration. */

/*!
 * @brief Encapsulates account database.
 */
class AccountDb : public SQLiteDbSingle {

public:
	/* Use parent class constructor. */
	using SQLiteDbSingle::SQLiteDbSingle;

	/* Make some inherited methods public. */
	using SQLiteDbSingle::closeDb;

	/*!
	 * @brief Return data box identifier.
	 *
	 * @param[in] userName Account user name.
	 * @param[in] defaultValue Default value.
	 * @return Databox id string.
	 */
	QString dbId(const QString &userName,
	    const QString &defaultValue = QString()) const;

	/*!
	 * @brief Check if databox has effective OVM.
	 *
	 * @param[in] userName User name identifying account.
	 * @return True if databox is effective OVM.
	 */
	bool boxEffectiveOVM(const QString &userName) const;

	/*!
	 * @brief Delete account info from db.
	 *
	 * @param[in] userName Account user name.
	 * @return True on success.
	 */
	bool deleteAccountInfoFromDb(const QString &userName) const;

	/*!
	 * @brief Delete user info from db.
	 *
	 * @param[in] userName Account user name.
	 * @return True on success.
	 */
	bool deleteUserInfoFromDb(const QString &userName) const;

	/*!
	 * @brief Delete long term storage info from db.
	 *
	 * @param[in] dbID Data box id.
	 * @return True on success.
	 */
	bool deleteDTInfoFromDb(const QString &dbID) const;

	/*!
	 * @brief Get account info and user info from db as html text.
	 *
	 * @param[in] userName Account user name.
	 * @return Html string of account detail.
	 */
	QString accountDetailHtml(const QString &userName) const;

	/*!
	 * @brief Insert account info into db.
	 *
	 * @param[in] userName Account user name.
	 * @param[in] dbOwnerInfo Account (owner) info structure.
	 * @return True on success.
	 */
	bool insertAccountInfoIntoDb(const QString &userName,
	    const Isds::DbOwnerInfoExt2 &dbOwnerInfo);

	/*!
	 * @brief Insert user info into db.
	 *
	 * @param[in] userName Account user name.
	 * @param[in] dbUserInfo User info structure.
	 * @return True on success.
	 */
	bool insertUserInfoIntoDb(const QString &userName,
	    const Isds::DbUserInfoExt2 &dbUserInfo);

	/*!
	 * @brief Insert long term storage info into db.
	 *
	 * @param[in] dbID Data box id.
	 * @param[in] dtInfo Long term storage info structure.
	 * @return True on success.
	 */
	bool insertDTInfoIntoDb(const QString &dbID,
	    const Isds::DTInfoOutput &dtInfo);

	/*!
	 * @brief Update pwd expiration in db.
	 *
	 * @param[in] userName Account user name.
	 * @param[in] date New expiration date.
	 * @return True on success.
	 */
	bool updatePwdExpirInDb(const QString &userName, const QDateTime &date);

	/*!
	 * @brief Get account/owner info from database.
	 *
	 * @param[in] key Key value.
	 * @return DbOwnerInfo structure - account info.
	 */
	Isds::DbOwnerInfoExt2 getOwnerInfo(const QString &key) const;

	/*!
	 * @brief Get long term storage info from database.
	 *
	 * @param[in] dbID Data box id.
	 * @return Long term storage info structure.
	 */
	Isds::DTInfoOutput getDTInfo(const QString &dbID) const;

	/*!
	 * @brief Checks whether password expires in given period for all accounts.
	 *
	 * @param[in] days Amount of days to check the expiration.
	 * @return List of expired usernames with password expiration date.
	 */
	QStringList getPasswordExpirationList(int days) const;

protected:
	/*!
	 * @brief Returns list of tables.
	 *
	 * @return List of pointers to tables.
	 */
	virtual
	QList<class SQLiteTbl *> listOfTables(void) const Q_DECL_OVERRIDE;

private:
	static
	const QVector<QString> dsPrintedAttribs;
	static
	const QVector<QString> ownerPrintedAttribs;
	static
	const QVector<QString> userPrintedAttribs;
};
