/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QString>

#include "src/datovka_shared/io/sqlite/db.h"
#include "src/datovka_shared/isds/message_interface.h"
#include "src/files.h"
#include "src/messages.h"
#include "src/models/filemodel.h"

/*!
 * @brief Encapsulates message database.
 */
class FileDb : public SQLiteDb {

public:
	explicit FileDb(const QString &connectionName);

	/*!
	 * @brief Delete files form db witch are older than date.
	 *
	 * @param[in] days How many days.
	 * @return List of message IDs where files were deleted.
	 */
	QList<qint64> cleanFilesInDb(int days);

	/*!
	 * @brief Copy db.
	 *
	 * @param[in] newFileName New file path.
	 * @return True on success.
	 *
	 * @note The copy is continued to be used. Original is closed.
	 */
	bool copyDb(const QString &newFileName);

	/*!
	 * @brief Open a new empty database file.
	 *
	 * @param[in] newFileName New file path.
	 * @return True on success.
	 */
	bool reopenDb(const QString &newFileName);

	/*!
	 * @brief Move db.
	 *
	 * @param[in] newFileName New file path.
	 * @return True on success.
	 */
	bool moveDb(const QString &newFileName);

	/*!
	 * @brief Delete all files related to message with given id.
	 *
	 * @param[in] dmId Message Id.
	 * @return True on success.
	 */
	bool deleteFilesFromDb(qint64 dmId);

	/*!
	 * @brief Get file database size in bytes.
	 *
	 * @return True size in bytes.
	 */
	int getDbSizeInBytes(void) const;

	/*!
	 * @brief Get file content from db.
	 *
	 * @param[in] fileId File Id.
	 * @return File data.
	 */
	Isds::Document getFileFromDb(int fileId) const;

	/*!
	 * @brief Get list of files from db.
	 *
	 * @param[in] dmId Message Id.
	 * @return List of files.
	 */
	QList<Isds::Document> getFilesFromDb(qint64 dmId) const;

	/*!
	 * @brief Set attachment model from db for QML listview.
	 *
	 * @param[in,out] model Model to be set.
	 * @param[in] dmId Message Id.
	 */
	void setFileModelFromDb(FileListModel &model, qint64 dmId) const;

	/*!
	 * @brief Insert or update file data into files table.
	 *
	 * @param[in] dmId Message Id.
	 * @param[in] document Struct holds file data.
	 * @return True on success.
	 */
	bool insertUpdateFileIntoDb(qint64 dmId, const Isds::Document &document);

	/*!
	 * @brief Open database file.
	 *
	 * @param[in] fileName File name.
	 * @param[in] dbInMemory Whether to create db in memory.
	 * @return True on success.
	 */
	bool openDb(const QString &fileName, bool storeToDisk);

	/*!
	 * @brief Search messages with attachment names matching supplied phrase.
	 *
	 * @param[in] phrase Search phrase.
	 * @return List of message IDs whose attachment names contain the phrase.
	 */
	QList<qint64> searchAttachmentName(const QString &phrase) const;

	/* Make some inherited methods public. */
	using SQLiteDb::vacuum;

protected:
	/*!
	 * @brief Returns list of tables.
	 *
	 * @return List of pointers to tables.
	 */
	virtual
	QList<class SQLiteTbl *> listOfTables(void) const Q_DECL_OVERRIDE;

	/*!
	 * @brief This function is used to make database content consistent
	 *     (e.g. adding missing columns or entries).
	 *
	 * @return True on success.
	 */
	virtual
	bool assureConsistency(void) Q_DECL_OVERRIDE;
};
