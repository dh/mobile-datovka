/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QMap>
#include <QPair>
#include <QString>
#include <QVector>

#include "src/datovka_shared/io/db_tables.h"
#include "src/sqlite/file_db_tables.h"

namespace FlsTbl {
	const QString tabName("files");

	const QVector< QPair<QString, enum EntryType> > knownAttrs = {
	{"id", DB_INTEGER}, /* NOT NULL*/
	{"dmID",  DB_INTEGER}, /* NOT NULL*/
	{"dmFileDescr", DB_TEXT},
	{"dmUpFileGuid", DB_TEXT},
	{"dmFileGuid", DB_TEXT},
	{"dmMimeType", DB_TEXT},
	{"dmFormat", DB_TEXT},
	{"dmFileMetaType", DB_TEXT},
	{"dmEncodedContent", DB_TEXT},
	{"_dmFileSize", DB_INTEGER},
	{"_dmDownloadDate", DB_DATETIME}
	/*
	 * PRIMARY KEY (id),
	 * FOREIGN KEY(dmID) REFERENCES messages ("dmID")
	 */
	};

	const QMap<QString, QString> colConstraints = {
	    {"id", "NOT NULL"},
	    {"dmID", "NOT NULL"}
	};

	const QString &tblConstraint(
	    ",\n"
	    "        PRIMARY KEY (id),\n"
	    "        FOREIGN KEY(dmID) REFERENCES messages (dmID)"
	);

	const QMap<QString, SQLiteTbl::AttrProp> attrProps = {
	{"id",               {DB_INTEGER, ""}},
	{"dmID",             {DB_INTEGER, ""}},
	{"dmFileDescr",      {DB_TEXT, SQLiteTbls::tr("File name")}},
	{"dmUpFileGuid",     {DB_TEXT, ""}},
	{"dmFileGuid",       {DB_TEXT, ""}},
	{"dmMimeType",       {DB_TEXT, SQLiteTbls::tr("Mime type")}},
	{"dmFormat",         {DB_TEXT, ""}},
	{"dmFileMetaType",   {DB_TEXT, ""}},
	{"dmEncodedContent", {DB_TEXT, ""}},
	{"_dmFileSize",      {DB_INTEGER, ""}},
	{"_dmDownloadDate",  {DB_DATETIME, ""}}
	};
} /* namespace FlsTbl */
SQLiteTbl flsTbl(FlsTbl::tabName, FlsTbl::knownAttrs, FlsTbl::attrProps,
    FlsTbl::colConstraints, FlsTbl::tblConstraint);
