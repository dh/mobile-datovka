/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDir>
#include <QFile>
#include <QMutexLocker>
#include <QSet>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>

#include "src/common.h"
#include "src/datovka_shared/isds/type_conversion.h"
#include "src/datovka_shared/log/log.h"
#include "src/io/filesystem.h"
#include "src/sqlite/dbs.h"
#include "src/sqlite/file_db.h"
#include "src/sqlite/file_db_tables.h"

FileDb::FileDb(const QString &connectionName)
    : SQLiteDb(connectionName)
{
}

QList<qint64> FileDb::cleanFilesInDb(int days)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QSet<qint64> msgIdSet;

	/* count split date based on days */
	QDate now = QDate::currentDate();
	now = now.addDays(-1*days);

	QString queryStr = "SELECT dmID FROM files WHERE _dmDownloadDate < :date";
	if (!query.prepare(queryStr)) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return QList<qint64>();
	}
	query.bindValue(":date", now);

	if (query.exec() && query.isActive()) {
		query.first();
		while (query.isValid()) {
			msgIdSet.insert(query.value(0).toLongLong());
			query.next();
		}
	} else {
		return QList<qint64>();
	}

	queryStr = "DELETE FROM files WHERE _dmDownloadDate < :date";
	if (!query.prepare(queryStr)) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return QList<qint64>();
	}
	query.bindValue(":date", now);

	if (!query.exec()) {
		return QList<qint64>();
	}

#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
	return QList<qint64>(msgIdSet.begin(), msgIdSet.end());
#else /* < Qt-5.14.0 */
	return msgIdSet.toList();
#endif /* >= Qt-5.14.0 */
}

bool FileDb::copyDb(const QString &newFileName)
{
	return SQLiteDb::copyDb(newFileName, SQLiteDb::CREATE_MISSING);
}

bool FileDb::reopenDb(const QString &newFileName)
{
	return SQLiteDb::reopenDb(newFileName, SQLiteDb::CREATE_MISSING);
}

bool FileDb::moveDb(const QString &newFileName)
{
	return SQLiteDb::moveDb(newFileName, SQLiteDb::CREATE_MISSING);
}

bool FileDb::deleteFilesFromDb(qint64 dmId)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "DELETE FROM files WHERE dmID = :dmID";
	if (!query.prepare(queryStr)) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":dmID", dmId);
	if (!query.exec()) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	return true;
fail:
	return false;
}

int FileDb::getDbSizeInBytes(void) const
{
	QFileInfo fi(m_db.databaseName());
	return fi.size();
}

void FileDb::setFileModelFromDb(FileListModel &model, const qint64 dmId) const
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "SELECT id, dmFileDescr, _dmFileSize "
	    "FROM files WHERE dmID = :dmID";
	if (!query.prepare(queryStr)) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return;
	}
	query.bindValue(":dmID", dmId);

	if (query.exec() && query.isActive()) {
		model.setQuery(query);
	}
}

Isds::Document FileDb::getFileFromDb(int fileId) const
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	Isds::Document document;

	QString queryStr = "SELECT dmFileDescr, dmUpFileGuid, dmFileGuid, "
	    "dmMimeType, dmFormat, dmFileMetaType, dmEncodedContent "
	    "FROM files WHERE id = :id";
	if (!query.prepare(queryStr)) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":id", fileId);
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		document.setFileDescr(query.value(0).toString());
		document.setUpFileGuid(query.value(1).toString());
		document.setFileGuid(query.value(2).toString());
		document.setMimeType(query.value(3).toString());
		document.setFormat(query.value(4).toString());
		document.setFileMetaType(
		    Isds::str2FileMetaType(query.value(5).toString()));
		document.setBase64Content(query.value(6).toString());
		return document;
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
	}
fail:
	return Isds::Document();
}

QList<Isds::Document> FileDb::getFilesFromDb(qint64 dmId) const
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QList<Isds::Document> documents;

	QString queryStr = "SELECT dmFileDescr, dmUpFileGuid, dmFileGuid, "
	    "dmMimeType, dmFormat, dmFileMetaType, dmEncodedContent "
	    "FROM files WHERE dmID = :dmID";
	if (!query.prepare(queryStr)) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":dmID", dmId);
	if (query.exec() && query.isActive()) {
		query.first();
		while (query.isValid()) {
			Isds::Document document;
			document.setFileDescr(query.value(0).toString());
			document.setUpFileGuid(query.value(1).toString());
			document.setFileGuid(query.value(2).toString());
			document.setMimeType(query.value(3).toString());
			document.setFormat(query.value(4).toString());
			document.setFileMetaType(
			    Isds::str2FileMetaType(query.value(5).toString()));
			document.setBase64Content(query.value(6).toString());
			documents.append(document);
			query.next();
		}
		return documents;
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
	}
fail:
	return QList<Isds::Document>();
}

bool FileDb::insertUpdateFileIntoDb(qint64 dmId, const Isds::Document &document)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	int id = -1;

	QString queryStr = "SELECT id FROM files WHERE "
	    "dmID = :dmID AND dmFileDescr = :dmFileDescr";
	if (!query.prepare(queryStr)) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":dmID", dmId);
	query.bindValue(":dmFileDescr", document.fileDescr());
	if (query.exec() && query.isActive()) {
		query.first();
		if (query.isValid()) {
			id = query.value(0).toInt();
		}
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	if (-1 != id) {
		queryStr = "UPDATE files SET dmID = :dmID, "
		    "dmFileDescr = :dmFileDescr, dmUpFileGuid = :dmUpFileGuid, "
		    "dmFileGuid = :dmFileGuid, dmMimeType = :dmMimeType, "
		    "dmFormat = :dmFormat, dmFileMetaType = :dmFileMetaType, "
		    "dmEncodedContent = :dmEncodedContent, _dmFileSize = :_dmFileSize, "
		    "_dmDownloadDate = :_dmDownloadDate "
		    "WHERE id = :id";
	} else {
		queryStr = "INSERT INTO files ("
		    "dmID, dmFileDescr, dmUpFileGuid, dmFileGuid, dmMimeType, "
		    "dmFormat, dmFileMetaType, dmEncodedContent, _dmFileSize, "
		    "_dmDownloadDate"
		    ") VALUES ("
		    ":dmID, :dmFileDescr, :dmUpFileGuid, :dmFileGuid, :dmMimeType, "
		    ":dmFormat, :dmFileMetaType, :dmEncodedContent, :_dmFileSize, "
		    ":_dmDownloadDate)";
	}

	if (!query.prepare(queryStr)) {
		logErrorNL("Cannot prepare SQL query:, %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	query.bindValue(":dmID", dmId);
	query.bindValue(":dmFileDescr", document.fileDescr());
	query.bindValue(":dmUpFileGuid", document.upFileGuid());
	query.bindValue(":dmFileGuid", document.fileGuid());
	query.bindValue(":dmMimeType", document.mimeType());
	query.bindValue(":dmFormat", document.format());
	query.bindValue(":dmFileMetaType",
	    Isds::fileMetaType2Str(document.fileMetaType()));
	query.bindValue(":dmEncodedContent", document.base64Content());
	query.bindValue(":_dmFileSize", document.binaryContent().size());
	query.bindValue(":_dmDownloadDate",
	    QDate::currentDate().toString(DATE_DB_FORMAT));
	if (-1 != id) {
		query.bindValue(":id", id);
	}

	if (!query.exec()) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	return true;
fail:
	return false;
}

bool FileDb::openDb(const QString &fileName, bool storeToDisk)
{
	SQLiteDb::OpenFlags flags = SQLiteDb::CREATE_MISSING;
	flags |= storeToDisk ? SQLiteDb::NO_OPTIONS : SQLiteDb::FORCE_IN_MEMORY;

	return SQLiteDb::openDb(fileName, flags);
}

QList<qint64> FileDb::searchAttachmentName(const QString &phrase) const
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QList<qint64> msgIds;

	QString queryStr = "SELECT DISTINCT dmID FROM files WHERE "
	    "dmFileDescr LIKE '%'||:_phrase||'%'";

	if (!query.prepare(queryStr)) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":_phrase", phrase);

	if (query.exec() && query.isActive()) {
		query.first();
		while (query.isValid()) {
			msgIds.append(query.value(0).toLongLong());
			query.next();
		}
		return msgIds;
	} else {
		/*
		 * SQL query can return empty search result. It is not error.
		 * Show log information like warning.
		*/
		logWarningNL(
		    "Cannot execute SQL query or empty search result: %s",
		    query.lastError().text().toUtf8().constData());
	}
fail:
	return QList<qint64>();
}

QList<class SQLiteTbl *> FileDb::listOfTables(void) const
{
	QList<class SQLiteTbl *> tables;
	tables.append(&flsTbl);
	return tables;
}

/*!
 * @brief Check whether file size is held in an integer-type column.
 *
 * @param[in] db Database object.
 * @return  1 if file size is an INTEGER type
 * @return  0 if file size is not an integer type (probably TEXT)
 * @return -1 on any error
 */
static
int fileSizeColIsInt(QSqlDatabase &db)
{
	QSqlQuery query(db);
	QString queryStr("PRAGMA table_info(files)");
	if (!query.prepare(queryStr)) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return -1;
	}

	if (query.exec() && query.isActive()) {
		query.first();
		while (query.isValid()) {
			const int colNum = query.value(0).toInt();
			if (colNum == 9) {
				const QString colName(query.value(1).toString());
				if (colName == QStringLiteral("_dmFileSize")) {
					const QString colType(query.value(2).toString());
					if (Q_UNLIKELY(colType.isEmpty())) {
						return -1;
					}
					return (colType == QStringLiteral("INTEGER")) ? 1 : 0;
				} else {
					/* The column with the index 9 is not _dmFileSize. */
					return -1;
				}
			}

			query.next();
		}
	}

	return -1;
}

/*!
 * @brief Recompute the binary file sizes.
 *
 * @param[in] db Database object.
 * @return True on success.
 */
static
bool fileSizeColRecompute(QSqlDatabase &db)
{
	QSqlQuery query(db);
	QString queryStr;

	QList<int> ids;
	queryStr = "SELECT id FROM files";
	if (Q_UNLIKELY(!query.prepare(queryStr) || !query.exec() || !query.isActive())) {
		logErrorNL("Cannot prepare or execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}

	query.first();
	while (query.isValid()) {
		ids.append(query.value(0).toInt());
		query.next();
	}

	foreach (int id, ids) {
		queryStr = "SELECT dmEncodedContent FROM files WHERE id = :id";
		if (!query.prepare(queryStr)) {
			logErrorNL("Cannot prepare SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			return false;
		}
		query.bindValue(":id", id);
		if (Q_UNLIKELY(!query.exec() || !query.isActive())) {
			logErrorNL("Cannot execute SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			return false;
		}
		query.first();
		const qint64 fileSize =
		    QByteArray::fromBase64(query.value(0).toByteArray()).size();

		queryStr = "UPDATE files SET _dmFileSize = :fileSize WHERE id = :id";
		if (!query.prepare(queryStr)) {
			logErrorNL("Cannot prepare SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			return false;
		}
		query.bindValue(":fileSize", fileSize);
		query.bindValue(":id", id);
		if (Q_UNLIKELY(!query.exec())) {
			logErrorNL("Cannot execute SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			return false;
		}
	}

	return true;
}

/*!
 * @brief Change the type of _dmFileSize column to INTEGER and recompute the file size.
 *
 * @note ALTER TABLE does not support column modification. Instead the table
 *    must be renamed a new table must be created and data must be copied.
 *
 * @param[in,out] fDb File database.
 * @param[in]     db Internal database object of the file database.
 */
static
bool fileSizeColMakeInt(FileDb &fDb, QSqlDatabase &db)
{
	QSqlQuery query(db);
	QString queryStr;

	fDb.beginTransaction();

	queryStr = "ALTER TABLE files RENAME TO _files_old";
	if (!query.prepare(queryStr) || !query.exec()) {
		logErrorNL("Cannot prepare or execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	if (!flsTbl.createEmpty(db)) {
		logErrorNL("%s", "Cannot create empty files table.");
		goto fail;
	}

	queryStr = "INSERT INTO files "
	    "(id, dmID, dmFileDescr, dmUpFileGuid, dmFileGuid, dmMimeType, dmFormat, dmFileMetaType, dmEncodedContent, _dmFileSize, _dmDownloadDate) "
	    "SELECT id, dmID, dmFileDescr, dmUpFileGuid, dmFileGuid, dmMimeType, dmFormat, dmFileMetaType, dmEncodedContent, null, _dmDownloadDate "
	    "FROM _files_old";
	if (!query.prepare(queryStr) || !query.exec()) {
		logErrorNL("Cannot prepare or execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	if (!fileSizeColRecompute(db)) {
		logErrorNL("%s", "Cannot recompute file sizes.");
		goto fail;
	}

	queryStr = "DROP TABLE _files_old";
	if (!query.prepare(queryStr) || !query.exec()) {
		logErrorNL("Cannot prepare or execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	fDb.commitTransaction();

	/* Cannot vacuum from within a transaction. */
	fDb.vacuum();

	return true;

fail:
	fDb.rollbackTransaction();
	return false;
}

bool FileDb::assureConsistency(void)
{
	QMutexLocker locker(&m_lock);

	switch (fileSizeColIsInt(m_db)) {
	case 1:
		logInfoNL("Consistency check of database '%s' seems to be OK.",
		    fileName().toUtf8().constData());
		return true;
		break;
	case 0:
		/* Continue after switch. */
		break;
	case -1:
	default:
		logErrorNL("Consistency check of database '%s' failed.",
		    fileName().toUtf8().constData());
		return false;
		break;
	}

	if (!fileSizeColMakeInt(*this, m_db)) {
		logErrorNL("%s", "Failed converting file size column type.");
		return false;
	}

	return true;
}
