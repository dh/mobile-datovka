/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/sqlite/dbs.h"

QDateTime dateTimeFromDbFormat(const QString &dateTimeDbStr)
{
	QDateTime dateTime = QDateTime::fromString(dateTimeDbStr,
	    DATETIME_DB_FORMAT);
	dateTime.setTimeSpec(Qt::UTC);
	return dateTime.toLocalTime();
}

QString dateTimeStrFromDbFormat(const QString &dateTimeDbStr,
    const QString &tgtFmt)
{
	QDateTime dateTime = dateTimeFromDbFormat(dateTimeDbStr);
	if (dateTime.isValid()) {
		return dateTime.toString(tgtFmt);
	} else {
		return QString();
	}
}

QString dateTimeToDbFormatStr(const QDateTime &dateTime)
{
	if (dateTime.isValid()) {
		return dateTime.toTimeSpec(Qt::UTC).toString(DATETIME_DB_FORMAT);
	} else {
		return QString();
	}
}

QString qDateToDbFormat(const QDate &date)
{
	return (!date.isNull()) ? date.toString(DATE_DB_FORMAT) : QString();
}

QDate dateFromDbFormat(const QString &dateDbStr)
{
	return QDate::fromString(dateDbStr, DATE_DB_FORMAT);
}

int remainsDaysToMsgDeletion(const QString &acceptanceDateTimeDbStr)
{
	QDateTime dateTime = dateTimeFromDbFormat(acceptanceDateTimeDbStr);
	if (!dateTime.isValid()) {
		return -1;
	}

	QDate msgDate(dateTime.date());
	/*
	 * The message delete/move operation is started past midnight for
	 * those messages which, according to the interpretation of the law,
	 * are to be deleted that day (because more than 90 full days have
	 * passed after their delivery).
	 * A message is usually deleted on the 91st day from acceptance.
	*/
	int remainsDays = QDate::currentDate().daysTo(msgDate) + 91;
	return (remainsDays > 0) ? remainsDays : 0;
}
