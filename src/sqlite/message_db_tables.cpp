/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QMap>
#include <QPair>
#include <QString>
#include <QVector>

#include "src/datovka_shared/io/db_tables.h"
#include "src/sqlite/message_db_tables.h"

namespace MsgsTbl {
	const QString tabName("messages");

	const QVector< QPair<QString, enum EntryType> > knownAttrs = {
	{"dmID", DB_INTEGER}, /* NOT NULL */
	{"dbIDSender", DB_TEXT},
	{"dmSender", DB_TEXT},
	{"dmSenderAddress", DB_TEXT},
	{"dmSenderType", DB_INTEGER},
	{"dmRecipient", DB_TEXT},
	{"dmRecipientAddress", DB_TEXT},
	{"dmAmbiguousRecipient", DB_TEXT},
	{"dmSenderOrgUnit", DB_TEXT},
	{"dmSenderOrgUnitNum", DB_TEXT},
	{"dbIDRecipient", DB_TEXT},
	{"dmRecipientOrgUnit", DB_TEXT},
	{"dmRecipientOrgUnitNum", DB_TEXT},
	{"dmToHands", DB_TEXT},
	{"dmAnnotation", DB_TEXT},
	{"dmRecipientRefNumber", DB_TEXT},
	{"dmSenderRefNumber", DB_TEXT},
	{"dmRecipientIdent", DB_TEXT},
	{"dmSenderIdent", DB_TEXT},
	{"dmLegalTitleLaw", DB_TEXT},
	{"dmLegalTitleYear", DB_TEXT},
	{"dmLegalTitleSect", DB_TEXT},
	{"dmLegalTitlePar", DB_TEXT},
	{"dmLegalTitlePoint", DB_TEXT},
	{"dmPersonalDelivery", DB_BOOLEAN},
	{"dmAllowSubstDelivery", DB_BOOLEAN},
	{"dmQTimestamp", DB_TEXT},
	{"dmDeliveryTime", DB_DATETIME},
	{"dmAcceptanceTime", DB_DATETIME},
	{"dmMessageStatus", DB_INTEGER},
	{"dmAttachmentSize", DB_INTEGER},
	{"dmType", DB_TEXT},
	{"_dmMessageType", DB_INTEGER},
	{"_dmDownloadDate", DB_DATETIME},
	{"_dmCustomData", DB_TEXT},
	{"_dmAttachDownloaded", DB_BOOLEAN},
	{"_dmReadLocally", DB_BOOLEAN}
	/*
	 * PRIMARY KEY ("dmID"),
	 * CHECK ("dmPersonalDelivery" IN (0, 1)),
	 * CHECK ("dmAllowSubstDelivery" IN (0, 1))
	*/
	};

	const QMap<QString, QString> colConstraints = {
	    {"dmID", "NOT NULL"}
	};

	const QString tblConstraint(
	    ",\n"
	    "        PRIMARY KEY (dmID),\n"
	    "        CHECK (dmPersonalDelivery IN (0, 1)),\n"
	    "        CHECK (dmAllowSubstDelivery IN (0, 1))"
	);

	const QMap<QString, SQLiteTbl::AttrProp> attrProps = {
	{"dmID",                  {DB_INTEGER, SQLiteTbls::tr("ID")}},
	{"dbIDSender",            {DB_TEXT, ""}},
	{"dmSender",              {DB_TEXT, SQLiteTbls::tr("Sender")}},
	{"dmSenderAddress",       {DB_TEXT, SQLiteTbls::tr("Sender address")}},
	{"dmSenderType",          {DB_INTEGER, ""}},
	{"dmRecipient",           {DB_TEXT, SQLiteTbls::tr("Recipient")}},
	{"dmRecipientAddress",    {DB_TEXT, SQLiteTbls::tr("Recipient address")}},
	{"dmAmbiguousRecipient",  {DB_TEXT, ""}},
	{"dmSenderOrgUnit",       {DB_TEXT, ""}},
	{"dmSenderOrgUnitNum",    {DB_TEXT, ""}},
	{"dbIDRecipient",         {DB_TEXT, ""}},
	{"dmRecipientOrgUnit",    {DB_TEXT, ""}},
	{"dmRecipientOrgUnitNum", {DB_TEXT, ""}},
	{"dmToHands",             {DB_TEXT, SQLiteTbls::tr("To hands")}},
	{"dmAnnotation",          {DB_TEXT, SQLiteTbls::tr("Subject")}},
	{"dmRecipientRefNumber",  {DB_TEXT, SQLiteTbls::tr("Your reference number")}},
	{"dmSenderRefNumber",     {DB_TEXT, SQLiteTbls::tr("Our reference number")}},
	{"dmRecipientIdent",      {DB_TEXT, SQLiteTbls::tr("Your file mark")}},
	{"dmSenderIdent",         {DB_TEXT, SQLiteTbls::tr("Our file mark")}},
	{"dmLegalTitleLaw",       {DB_TEXT, SQLiteTbls::tr("Law")}},
	{"dmLegalTitleYear",      {DB_TEXT, SQLiteTbls::tr("Year")}},
	{"dmLegalTitleSect",      {DB_TEXT, SQLiteTbls::tr("Section")}},
	{"dmLegalTitlePar",       {DB_TEXT, SQLiteTbls::tr("Paragraph")}},
	{"dmLegalTitlePoint",     {DB_TEXT, SQLiteTbls::tr("Letter")}},
	{"dmPersonalDelivery",    {DB_BOOLEAN, ""}},
	{"dmAllowSubstDelivery",  {DB_BOOLEAN, ""}},
	{"dmQTimestamp",          {DB_TEXT, ""}},
	{"dmDeliveryTime",        {DB_DATETIME, SQLiteTbls::tr("Delivered")}},
	{"dmAcceptanceTime",      {DB_DATETIME, SQLiteTbls::tr("Accepted")}},
	{"dmMessageStatus",       {DB_INTEGER, SQLiteTbls::tr("Status")}},
	{"dmAttachmentSize",      {DB_INTEGER, SQLiteTbls::tr("Attachment size")}},
	{"dmType",                {DB_TEXT, ""}},
	{"_dmMessageType",        {DB_INTEGER, ""}},
	{"_dmDownloadDate",       {DB_DATETIME, ""}},
	{"_dmCustomData",         {DB_TEXT, ""}},
	{"_dmAttachDownloaded",   {DB_BOOLEAN, ""}},
	{"_dmReadLocally",        {DB_BOOLEAN, ""}}
	};
} /* namespace MsgsTbl */
SQLiteTbl msgsTbl(MsgsTbl::tabName, MsgsTbl::knownAttrs, MsgsTbl::attrProps,
    MsgsTbl::colConstraints, MsgsTbl::tblConstraint);

namespace EvntsTbl {
	const QString tabName("events");

	const QVector< QPair<QString, enum EntryType> > knownAttrs = {
	{"id", DB_INTEGER}, /* NOT NULL */
	{"dmID", DB_INTEGER}, /* NOT NULL */
	{"dmEventTime", DB_TEXT},
	{"dmEventDescr", DB_TEXT}
	/*
	 * PRIMARY KEY (id),
	 * FOREIGN KEY(dmID) REFERENCES messages ("dmID")
	 */
	};

	const QMap<QString, QString> colConstraints = {
	    {"id", "NOT NULL"},
	    {"dmID", "NOT NULL"}
	};

	const QString &tblConstraint(
	    ",\n"
	    "        PRIMARY KEY (id),\n"
	    "        FOREIGN KEY(dmID) REFERENCES messages (dmID)"
	);

	const QMap<QString,SQLiteTbl:: AttrProp> attrProps = {
	{"id",           {DB_INTEGER, ""}},
	{"dmID",         {DB_INTEGER, ""}},
	{"dmEventTime",  {DB_TEXT, ""}},
	{"dmEventDescr", {DB_TEXT, ""}}
	};
} /* namespace EvntsTbl */
SQLiteTbl evntsTbl(EvntsTbl::tabName, EvntsTbl::knownAttrs,
    EvntsTbl::attrProps, EvntsTbl::colConstraints, EvntsTbl::tblConstraint);
