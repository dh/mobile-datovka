/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QFile>
#include <QSettings>
#include <QUrl>

#include "src/datovka_shared/isds/box_interface.h"
#include "src/datovka_shared/log/log.h"
#include "src/dialogues/dialogues.h"
#include "src/global.h"
#include "src/models/accountmodel.h"
#include "src/qml_identifiers/qml_account_id.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/settings/prefs_specific.h"
#include "src/setwrapper.h"
#include "src/sqlite/account_db.h"
#include "src/sqlite/file_db_container.h"
#include "src/sqlite/message_db_container.h"
#include "src/wrap_accounts.h"

Accounts::Accounts(QObject *parent)
    : QObject(parent)
{
}

QString Accounts::boxId(const QmlAcntId *qAcntId)
{
	if ((GlobInstcs::accountDbPtr != Q_NULLPTR) && (qAcntId != Q_NULLPTR)) {
		return GlobInstcs::accountDbPtr->dbId(qAcntId->username());
	} else {
		Q_ASSERT(0);
		return QString();
	}
}

bool Accounts::boxEffectiveOVM(const QmlAcntId *qAcntId)
{
	if ((GlobInstcs::accountDbPtr != Q_NULLPTR) && (qAcntId != Q_NULLPTR)) {
		return GlobInstcs::accountDbPtr->boxEffectiveOVM(
		    qAcntId->username());
	} else {
		Q_ASSERT(0);
		return false;
	}
}

void Accounts::updateOneAccountCounters(AccountListModel *accountModel,
    const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY((GlobInstcs::logPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		return;
	}

	debugFuncCall();

	if (Q_UNLIKELY(accountModel == Q_NULLPTR)) {
		/* accountModel can be NULL when app is shutdown */
		logWarningNL("%s", "Cannot access account model.");
		return;
	}

	loadOneAccountCounters(*accountModel, *qAcntId);
}

QString Accounts::fillAccountInfo(const QmlAcntId *qAcntId)
{
	debugFuncCall();

	if ((GlobInstcs::accountDbPtr != Q_NULLPTR) && (qAcntId != Q_NULLPTR)) {
		return GlobInstcs::accountDbPtr->accountDetailHtml(
		    qAcntId->username());
	} else {
		Q_ASSERT(0);
		return QString();
	}
}

void Accounts::getAccountData(const QmlAcntId *qAcntId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (!GlobInstcs::acntMapPtr->contains(*qAcntId)) {
		return;
	}

	const AcntData &acntData((*GlobInstcs::acntMapPtr)[*qAcntId]);

	emit sendAccountData(acntData.accountName(), acntData.userName(),
	    acntData.loginMethod(), acntData.password(), acntData.mepToken(),
	    acntData.isTestAccount(), acntData.rememberPwd(),
	    PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr, acntData),
	    acntData.syncWithAll(), acntData.p12File());
}

int Accounts::actDTType(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY((qAcntId == Q_NULLPTR) ||
		GlobInstcs::accountDbPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return -1;
	}

	QString dbID = boxId(qAcntId);

	Isds::DTInfoOutput dtInfo(GlobInstcs::accountDbPtr->getDTInfo(dbID));

	if (dtInfo.isNull()) {
		Q_ASSERT(0);
		return -1;
	}

	return dtInfo.actDTType();
}

bool Accounts::isDTCapacityFull(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY((qAcntId == Q_NULLPTR) ||
		GlobInstcs::accountDbPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	QString dbID = boxId(qAcntId);

	Isds::DTInfoOutput dtInfo(GlobInstcs::accountDbPtr->getDTInfo(dbID));

	if (dtInfo.isNull()) {
		Q_ASSERT(0);
		return false;
	}

	return (dtInfo.actDTCapacity()-dtInfo.actDTCapUsed() == 0);
}

bool Accounts::createAccount(AccountListModel *accountModel,
    enum AcntData::LoginMethod loginMethod, const QString &acntName,
    const QString &userName, const QString &pwd, const QString &mepToken,
    bool isTestAccount, bool rememberPwd, bool storeToDisk, bool syncWithAll,
    const QString &certPath)
{
	debugFuncCall();

	AcntData acntData;
	QString errTxt;
	QString newUserName = userName.trimmed();

	if (Q_UNLIKELY(accountModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access account model.");
		Q_ASSERT(0);
		return false;
	}

	if (AcntData::LIM_UNAME_MEP == loginMethod) {
		if (newUserName.isEmpty() || acntName.isEmpty()
		        || mepToken.isEmpty()) {
			errTxt = tr("Username, communication code or account name has not been specified. These fields must be filled in.");
			goto fail;
		}
	} else if (newUserName.isEmpty() || acntName.isEmpty()
	        || pwd.isEmpty()) {
		errTxt = tr("Username, password or account name has not been specified. These fields must be filled in.");
		goto fail;
	}

	acntData.setAccountName(acntName.trimmed());
	acntData.setUserName(newUserName);
	acntData.setPassword(pwd);
	acntData.setMepToken(mepToken.trimmed());
	acntData.setLoginMethod(loginMethod);
	acntData.setTestAccount(isTestAccount);
	acntData.setRememberPwd(rememberPwd);
	acntData.setP12File(certPath);
	acntData.setSyncWithAll(syncWithAll);

	if (accountModel->addAccount(acntData) == AccountListModel::AA_EXISTS) {
		errTxt = tr("Account with username '%1' already exists.").arg(newUserName);
		goto fail;
	}

	PrefsSpecific::setDataOnDisk(*GlobInstcs::prefsPtr, acntData,
	    storeToDisk);

	return true;
fail:

	Dialogues::errorMessage(Dialogues::CRITICAL,
	    tr("Creating account: %1").arg(newUserName),
	    tr("Account '%1' could not be created.").arg(acntName),
	    errTxt);

	return false;
}

bool Accounts::updateAccount(AccountListModel *accountModel,
    enum AcntData::LoginMethod loginMethod, const QString &acntName,
    const QString &userName, const QString &pwd, const QString &mepToken,
    bool isTestAccount, bool rememberPwd, bool storeToDisk, bool syncWithAll,
    const QString &certPath)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::setPtr == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	if (Q_UNLIKELY(accountModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access account model.");
		Q_ASSERT(0);
		return false;
	}

	if (acntName.isEmpty()) {
		Dialogues::errorMessage(Dialogues::CRITICAL,
		    tr("Problem while updating account"),
		    tr("Account name has not been specified!"),
		    tr("This field must be filled in."));
		return false;
	}

	AcntId acntId;
	acntId.setUsername(userName.trimmed());
	acntId.setTesting(isTestAccount);

	if (!GlobInstcs::acntMapPtr->contains(acntId)) {
		return false;
	}

	AcntData &acntData((*GlobInstcs::acntMapPtr)[acntId]);

	/* update account map */
	acntData.setLoginMethod(loginMethod);
	acntData.setAccountName(acntName.trimmed());
	acntData.setPassword(pwd);
	acntData.setMepToken(mepToken.trimmed());
	acntData.setTestAccount(isTestAccount);
	acntData.setRememberPwd(rememberPwd);

	const bool oldStoreToDisk = PrefsSpecific::dataOnDisk(
	    *GlobInstcs::prefsPtr, acntData);
	/* Reopen database if storeToDisk changed. */
	if (oldStoreToDisk != storeToDisk) {
		MessageDb *mDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
		    oldStoreToDisk);
		if (mDb != Q_NULLPTR) {
			QString mDbFileName(mDb->fileName());
			/* Reopen database. */
			GlobInstcs::messageDbsPtr->deleteDb(mDb);
			GlobInstcs::messageDbsPtr->accessMessageDb(
			    GlobalSettingsQmlWrapper::dbPath(),
			    acntId.username(), storeToDisk);
			if (oldStoreToDisk) {
				/* Delete databases from storage. */
				QFile::remove(mDbFileName);
			}
		}
		FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
		    oldStoreToDisk);
		if (fDb != Q_NULLPTR) {
			QString fDbFileName(fDb->fileName());
			/* Reopen database. */
			GlobInstcs::fileDbsPtr->deleteDb(fDb);
			GlobInstcs::fileDbsPtr->accessFileDb(
			    GlobalSettingsQmlWrapper::dbPath(),
			    acntId.username(), storeToDisk);
			if (oldStoreToDisk) {
				/* Delete databases from storage. */
				QFile::remove(fDbFileName);
			}
		}

		/* Inform the model that message counters have changed. */
		accountModel->updateCounters(acntId, 0, 0, 0, 0);
	}

	acntData.setSyncWithAll(syncWithAll);
	acntData.setP12File(certPath);
	PrefsSpecific::setDataOnDisk(*GlobInstcs::prefsPtr, acntData,
	    storeToDisk);

	/* Model data have changed. */
	emit GlobInstcs::acntMapPtr->accountDataChanged(acntId);

	/* Delete isds session context */
	emit removeIsdsCtx(acntId);

	return true;
}

bool Accounts::removeAccount(AccountListModel *accountModel,
    const QmlAcntId *qAcntId, bool showDialogue)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::accountDbPtr == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	if (Q_UNLIKELY(accountModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access account model.");
		Q_ASSERT(0);
		return false;
	}

	if (showDialogue) {
		int msgResponse = Dialogues::message(Dialogues::QUESTION,
		    tr("Remove account: %1").arg(qAcntId->username()),
		    tr("Do you want to remove the account '%1'?").arg(qAcntId->username()),
		    tr("Note: It will also remove all related local databases and account information."),
		    Dialogues::NO | Dialogues::YES, Dialogues::NO);
		if (msgResponse == Dialogues::NO) {
			return false;
		}
	}

	if (!GlobInstcs::acntMapPtr->contains(*qAcntId)) {
		return false;
	}

	const bool dataOnDisk = PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr,
	    (*GlobInstcs::acntMapPtr)[*qAcntId]);

	/* Delete file database */
	FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(), dataOnDisk);
	if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access file database.");
		return false;
	}
	if (!GlobInstcs::fileDbsPtr->deleteDb(fDb)) {
		logErrorNL("%s", "Could not delete file database.");
		return false;
	}

	/* Delete message database */
	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(), dataOnDisk);
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return false;
	}
	if (!GlobInstcs::messageDbsPtr->deleteDb(msgDb)) {
		logErrorNL("%s", "Could not delete message database.");
		return false;
	}

	/* Delete account info from account database */
	if (!GlobInstcs::accountDbPtr->deleteAccountInfoFromDb(qAcntId->username())) {
		logErrorNL("%s", "Could not delete account info.");
		return false;
	}

	/* Delete user info from account database */
	if (!GlobInstcs::accountDbPtr->deleteUserInfoFromDb(qAcntId->username())) {
		logErrorNL("%s", "Could not delete user info.");
		return false;
	}

	/* Delete isds session context */
	emit removeIsdsCtx(*qAcntId);

	/* Delete account from settings */
	accountModel->deleteAccount(*qAcntId);

	return true;
}

void Accounts::loadModelCounters(AccountListModel *accountModel)
{
	debugFuncCall();

	if (Q_UNLIKELY(accountModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access account model.");
		Q_ASSERT(0);
		return;
	}

	for (int row = 0; row < accountModel->rowCount(); ++row) {
		const AcntId acntId = accountModel->acntId(accountModel->index(row, 0));
		if (Q_UNLIKELY(!acntId.isValid())) {
			Q_ASSERT(0);
			continue;
		}
		loadOneAccountCounters(*accountModel, acntId);
	}
}

bool Accounts::prepareChangeUserName(AccountListModel *accountModel,
    enum AcntData::LoginMethod loginMethod, const QString &acntName,
    const QString &newUserName, const QString &pwd, const QString &mepToken,
    bool isTestAccount, bool rememberPwd, bool storeToDisk, bool syncWithAll,
    const QString &certPath, const QString &oldUserName)
{
	debugFuncCall();

	/* Show confirm dialogue about use name changing */
	int msgResponse = Dialogues::message(Dialogues::QUESTION,
	    tr("Change user name: %1").arg(oldUserName),
	    tr("Do you want to change the username from '%1' to '%2' for the account '%3'?")
	        .arg(oldUserName).arg(newUserName).arg(acntName),
	    tr("Note: It will also change all related local database names and account information."),
	    Dialogues::NO | Dialogues::YES, Dialogues::NO);
	if (msgResponse == Dialogues::NO) {
		return false;
	}

	return createAccount(accountModel, loginMethod, acntName,
	    newUserName, pwd, mepToken, isTestAccount, rememberPwd, storeToDisk,
	    syncWithAll, certPath);
}

/*!
 * @brief Check whether the box identifier matches the username of a known account.
 *
 * @param[in] dbId Data box ID.
 * @param[in] username User name of already existing account.
 * @return True if username corresponds to data box id, false on any error.
 */
static
bool boxMatchesUser(const QString &dbId, const AcntId &acntId)
{
	if (Q_UNLIKELY(dbId.isEmpty() || (!acntId.isValid()))) {
		Q_ASSERT(0);
		return false;
	}

	return dbId == GlobInstcs::accountDbPtr->dbId(acntId.username());
}

/*!
 * @brief Change file database name to new username.
 *
 * @param[in]  oldUserName Original user name of account.
 * @param[in]  newUserName New user name identifying account.
 * @param[in]  storeToDisk True if database store to local storage.
 * @param[out] errTxt Error description.
 * @return True on success.
 */
static
bool changeFileDbName(const QString &oldUserName,
    const QString &newUserName, bool storeToDisk, QString &errTxt)
{
	debugFuncCall();

	FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), oldUserName, storeToDisk);
	if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
		errTxt = Accounts::tr("Cannot access file database for the username '%1'.")
		    .arg(oldUserName);
		return false;
	}

	/* Rename file database */
	QString currentDbFileName = fDb->fileName();
	currentDbFileName.replace(oldUserName, newUserName);
	if (!fDb->moveDb(currentDbFileName)) {
		errTxt = Accounts::tr("Cannot change file database to username '%1'.")
		    .arg(newUserName);
		return false;
	}

	return true;
}

/*!
 * @brief Change message database name to new user name.
 *
 * @param[in] oldUserName Original user name of account.
 * @param[in] newUserName New user name identifying account.
 * @param[in] storeToDisk True if database store to local storage.
 * @param[out] errTxt Error description.
 * @return True if success.
 */
static
bool changeMessageDbName(const QString &oldUserName,
    const QString &newUserName, bool storeToDisk, QString &errTxt)
{
	debugFuncCall();

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), oldUserName, storeToDisk);
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		errTxt = Accounts::tr("Cannot access message database for username '%1'.")
		    .arg(oldUserName);
		return false;
	}

	/* Rename message database */
	QString currentDbFileName = msgDb->fileName();
	currentDbFileName.replace(oldUserName, newUserName);
	if (!msgDb->moveDb(currentDbFileName)) {
		errTxt = Accounts::tr("Cannot change message database to username '%1'.")
		    .arg(newUserName);
		return false;
	}

	return true;
}

bool Accounts::changeAccountUserName(AccountListModel *accountModel,
    const QString &acntName, const QString &newUserName, bool storeToDisk,
    const QString &oldUserName, const QString &newDbId)
{
	debugFuncCall();

	QString errTxt;

	const AcntId acntId =
	    GlobInstcs::acntMapPtr->acntIdFromUsername(oldUserName);
	const AcntId newAcntId =
	    GlobInstcs::acntMapPtr->acntIdFromUsername(newUserName);

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		goto exit;
	}

	/* Get account model */
	if (Q_UNLIKELY(accountModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access account model.");
		Q_ASSERT(0);
		goto exit;
	}

	/* First: Check whether new box id corresponds with the old data box id. */
	if (!boxMatchesUser(newDbId, acntId)) {
		errTxt = tr("Data box identifier related to the new username '%1' doesn't correspond with the data box identifier related to the old username '%2'.").arg(newUserName).arg(oldUserName);
		goto fail;
	}

	/* Second: Change file database name or exit on error. */
	if (!changeFileDbName(oldUserName, newUserName, storeToDisk, errTxt)) {
		errTxt = tr("Cannot change the file database to match the new username '%1'.").arg(newDbId);
		goto fail;
	}

	/* Third: Change message database name or rollback on error. */
	if (!changeMessageDbName(oldUserName, newUserName, storeToDisk, errTxt)) {
		errTxt = tr("Cannot change the message database to match the new username '%1'.").arg(newDbId);
		goto rollbackFilesChanges;
	}

	/* Fourth: Delete old account info and user info from account database */
	GlobInstcs::accountDbPtr->deleteAccountInfoFromDb(oldUserName);
	GlobInstcs::accountDbPtr->deleteUserInfoFromDb(oldUserName);

	/* Last: Delete original/old account from model and settings */
	deleteAccountFromModel(accountModel, acntId);

	logInfoNL("Username has been changed to '%s'.",
	     newUserName.toUtf8().constData());

	Dialogues::errorMessage(Dialogues::INFORMATION,
	    tr("Change username: %1").arg(oldUserName),
	    tr("A new username '%1' for the account '%2' has been set.")
	        .arg(newUserName).arg(acntName),
	    tr("Account will use the new settings."));

	return true;

rollbackFilesChanges:

	/* Change back file database name */
	if (changeFileDbName(newUserName, oldUserName, storeToDisk, errTxt)) {
		logErrorNL("Cannot restore file database name '%s'.",
		    oldUserName.toUtf8().constData());
	}

fail:
	/* Show error dialogue with message what happened */
	Dialogues::errorMessage(Dialogues::CRITICAL,
	    tr("Username problem: %1").arg(oldUserName),
	    tr("The new username '%1' for account '%2' has not been set!")
	        .arg(newUserName).arg(acntName),
	    errTxt + QStringLiteral(" ") +
	    tr("Account will use the original settings."));

exit:
	/* Remove new account */
	deleteAccountFromModel(accountModel, newAcntId);

	/* Delete new account info and user info from account database */
	GlobInstcs::accountDbPtr->deleteAccountInfoFromDb(newUserName);
	GlobInstcs::accountDbPtr->deleteUserInfoFromDb(newUserName);

	return false;
}

void Accounts::deleteAccountFromModel(AccountListModel *accountModel,
    const AcntId &acntId)
{
	if (Q_UNLIKELY((accountModel == Q_NULLPTR) || (!acntId.isValid()))) {
		Q_ASSERT(0);
		return;
	}

	/* Remove old account from model */
	accountModel->deleteAccount(acntId);
	/* Delete ISDS session context of old account  */
	emit removeIsdsCtx(acntId);
	/* Load message counters of all accounts */
	loadModelCounters(accountModel);
}

void Accounts::loadOneAccountCounters(AccountListModel &accountModel,
    const AcntId &acntId)
{
	if (Q_UNLIKELY((GlobInstcs::setPtr == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(!acntId.isValid())) {
		return;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
	    PrefsSpecific::dataOnDisk(
		*GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[acntId]));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logWarningNL("Cannot open message database for '%s'.",
		    acntId.username().toUtf8().constData());
		return;
	}

	accountModel.updateCounters(acntId,
	    msgDb->getNewMessageCountFromDb(MessageDb::TYPE_RECEIVED),
	    msgDb->getMessageCountFromDb(MessageDb::TYPE_RECEIVED),
	    msgDb->getNewMessageCountFromDb(MessageDb::TYPE_SENT),
	    msgDb->getMessageCountFromDb(MessageDb::TYPE_SENT));
}

void Accounts::getPasswordExpirationInfo(void)
{
	debugSlotCall();

	int days = GlobalSettingsQmlWrapper::pwdExpirationDays();
	if (days <= 0)  {
		return;
	}

	QStringList exprList =
	    GlobInstcs::accountDbPtr->getPasswordExpirationList(days);
	if (exprList.isEmpty()) {
		return;
	}

	QString detailText;
	foreach (const QString &expItem, exprList) {
		detailText.append(expItem);
		detailText.append("\n");
	}

	Dialogues::message(Dialogues::INFORMATION,
	    tr("Password expiration"),
	    tr("Passwords of users listed below have expired or are going to expire within few days. "
	        "You may change the passwords from this application if they haven't already expired. "
	        "Expired passwords can only be changed in the ISDS web portal."),
	    detailText, Dialogues::OK, Dialogues::OK);
}
