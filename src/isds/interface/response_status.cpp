/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QString>

#include "src/isds/interface/response_status.h"

/* Null objects - for convenience. */
static const QString nullString;

/*!
 * @brief PIMPL ResponseStatus class.
 */
class Isds::ResponseStatusPrivate {
	//Q_DISABLE_COPY(ResponseStatusPrivate)
public:
	ResponseStatusPrivate(void)
	    : m_code(), m_message()
	{ }

	ResponseStatusPrivate &operator=(const ResponseStatusPrivate &other)
	{
		m_code = other.m_code;
		m_message = other.m_message;

		return *this;
	}

	bool operator==(const ResponseStatusPrivate &other) const
	{
		return (m_code == other.m_code) &&
		    (m_message == other.m_message);
	}

	QString m_code; /*!< Status code. */
	QString m_message; /*!< Status description message. */
};

Isds::ResponseStatus::ResponseStatus(void)
    : d_ptr(Q_NULLPTR)
{
}

Isds::ResponseStatus::ResponseStatus(const ResponseStatus &other)
    : d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) ResponseStatusPrivate) : Q_NULLPTR)
{
	Q_D(ResponseStatus);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::ResponseStatus::ResponseStatus(ResponseStatus &&other) Q_DECL_NOEXCEPT
    : d_ptr(other.d_ptr.take()) //d_ptr(::std::move(other.d_ptr))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Isds::ResponseStatus::~ResponseStatus(void)
{
}

/*!
 * @brief Ensures private response status presence.
 *
 * @note Returns if private response status could not be allocated.
 */
#define ensureResponseStatusPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			ResponseStatusPrivate *p = new (::std::nothrow) ResponseStatusPrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Isds::ResponseStatus &Isds::ResponseStatus::operator=(const ResponseStatus &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureResponseStatusPrivate(*this);
	Q_D(ResponseStatus);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::ResponseStatus &Isds::ResponseStatus::operator=(ResponseStatus &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Isds::ResponseStatus::operator==(const ResponseStatus &other) const
{
	Q_D(const ResponseStatus);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Isds::ResponseStatus::operator!=(const ResponseStatus &other) const
{
	return !operator==(other);
}

bool Isds::ResponseStatus::isNull(void) const
{
	Q_D(const ResponseStatus);
	return d == Q_NULLPTR;
}

const QString &Isds::ResponseStatus::code(void) const
{
	Q_D(const ResponseStatus);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_code;
}

void Isds::ResponseStatus::setCode(const QString &c)
{
	ensureResponseStatusPrivate();
	Q_D(ResponseStatus);
	d->m_code = c;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::ResponseStatus::setCode(QString &&c)
{
	ensureResponseStatusPrivate();
	Q_D(ResponseStatus);
	d->m_code = ::std::move(c);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::ResponseStatus::message(void) const
{
	Q_D(const ResponseStatus);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_message;
}

void Isds::ResponseStatus::setMessage(const QString &m)
{
	ensureResponseStatusPrivate();
	Q_D(ResponseStatus);
	d->m_message = m;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::ResponseStatus::setMessage(QString &&m)
{
	ensureResponseStatusPrivate();
	Q_D(ResponseStatus);
	d->m_message = ::std::move(m);
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Isds::ResponseStatus::ok(void) const
{
	return code() == QLatin1String("0000");
}

void Isds::swap(ResponseStatus &first, ResponseStatus &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}
