/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QString>
#include <QTime>
#include <QUrl>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/types.h"
#include "src/datovka_shared/log/log.h"
#include "src/isds/isds_const.h"
#include "src/isds/isds_login.h"
#include "src/isds/services/login_interface.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"

#ifdef Q_COMPILER_RVALUE_REFS
#  define macroStdMove(x) ::std::move(x)
#else /* Q_COMPILER_RVALUE_REFS */
#  define macroStdMove(x) (x)
#endif /* Q_COMPILER_RVALUE_REFS */

/*!
 * @brief Logout OTP connection and remove session form the container.
 *
 * @param[in,out] sessions Session container.
 * @param[in]     acntId Account identifier.
 */
static
void logoutOtp(Sessions &sessions, const AcntId &acntId)
{
	debugFuncCall();

	if (Q_UNLIKELY(!sessions.holdsSession(acntId))) {
		Q_ASSERT(0);
		return;
	}

	Isds::Session *session = sessions.session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		logErrorNL("Invalid or empty ISDS session for username '%s'.",
		    acntId.username().toUtf8().constData());
		Q_ASSERT(0);
		return;
	}

	session->ctx()->setUrl(Isds::createOtpLogoutUrlString(acntId.testing()));

	if (Isds::Type::ERR_SUCCESS == Isds::logoutMepOtp(*session)) {
		sessions.removeSession(acntId);
	}
}

void Isds::Login::closeAllOtpConnections(Sessions &sessions)
{
	debugFuncCall();

	const QList<AcntId> acntIds = sessions.keys();
	foreach (const AcntId &acntId, acntIds) {
		const Isds::Session *session = sessions.session(acntId);
		if (Q_UNLIKELY(session == Q_NULLPTR)) {
			Q_ASSERT(0);
			continue;
		}

		const Isds::Context *ctx = session->ctx();
		switch (ctx->loginMethod()) {
		case AcntData::LIM_UNAME_PWD_HOTP:
		case AcntData::LIM_UNAME_PWD_TOTP:
		case AcntData::LIM_UNAME_MEP:
			if (Isds::LS_LOGGED_IN == ctx->loginState()) {
				logoutOtp(sessions, acntId);
			}
			break;
		default:
			break;
		}
	}
}

bool Isds::Login::isLoggedIn(const Sessions &sessions, const AcntId &acntId)
{
	debugFuncCall();

	if (sessions.holdsSession(acntId)) {
		return Isds::LS_LOGGED_IN ==
		    sessions.session(acntId)->ctx()->loginState();
	}
	return false;
}

bool Isds::Login::loginBasicAuth(Sessions &sessions, const AcntId &acntId,
    QString &errTxt)
{
	debugFuncCall();

	if (Q_UNLIKELY(!sessions.holdsSession(acntId))) {
		Q_ASSERT(0);
		return false;
	}

	/* Get session. */
	Isds::Session *session = sessions.session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		logErrorNL("Invalid or empty ISDS session for username '%s'.",
		    acntId.username().toUtf8().constData());
		Q_ASSERT(0);
		return false;
	}

	/* Send basic authentication with username and password. */
	if (Isds::Type::ERR_SUCCESS != Isds::loginOperation(*session,
	        LOGIN_SERVICE, Q_NULLPTR, &errTxt)) {
		return false;
	}

	/* Login successful for these two login methods. */
	const enum AcntData::LoginMethod lim = session->ctx()->loginMethod();
	if ((lim == AcntData::LIM_UNAME_PWD) ||
	    (lim == AcntData::LIM_UNAME_PWD_CRT)) {
		session->ctx()->setLoginState(Isds::LS_LOGGED_IN);
	}

	return true;
}

/*!
 * @brief Remove login endpoint path from session URL.
 *
 * @param[in] url Session login URL.
 * @return URL string without login endpoint string.
 */
static
QString removeLoginServiceFromUrl(const QUrl &url)
{
	QString endPointUrl(url.toString());
	return endPointUrl.remove(LOGIN_SERVICE, Qt::CaseSensitive);
}

/*!
 * @brief Ping ISDS server with new cookies.
 *
 * @param[in] session ISDS session.
 * @return True if login with new cookies success.
 */
static
bool testNewCookies(Isds::Session &session)
{
	/*
	 * Session has cookies, removing password and security code from
	 * context.
	 */
	session.ctx()->setPassword(QString());

	/* Received cookies. Ping ISDS server using new cookies only. */
	QString errTxt;
	if (Isds::Type::ERR_SUCCESS == Isds::loginOperation(session, QString(),
	    Q_NULLPTR, &errTxt)) {
		/* Logged. */
		session.ctx()->setLoginState(Isds::LS_LOGGED_IN);
		session.ctx()->setUrl(
		    removeLoginServiceFromUrl(session.ctx()->url()));
		return true;
	} else {
		session.ctx()->setLastIsdsMsg(macroStdMove(errTxt));
		return false;
	}
}

bool Isds::Login::loginHotp(Sessions &sessions, const AcntId &acntId,
    const QString &otpCode, QString &errTxt)
{
	debugFuncCall();

	if (Q_UNLIKELY(!sessions.holdsSession(acntId))) {
		Q_ASSERT(0);
		return false;
	}

	/* Get session and set OTP code. */
	Isds::Session *session = sessions.session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		logErrorNL("Invalid or empty ISDS session for username '%s'.",
		    acntId.username().toUtf8().constData());
		Q_ASSERT(0);
		return false;
	}
	session->ctx()->setPassword(session->ctx()->password() + otpCode);

	/* Send basic authentication (username, password + OTP). */
	if (!loginBasicAuth(sessions, acntId, errTxt)) {
		return false;
	}

	/* Received cookies. Ping ISDS server using new cookies. */
	if (!testNewCookies(*session)) {
		errTxt = session->ctx()->lastIsdsMsg();
		return false;
	}

	return true;
}

/*!
 * @brief Run final MEP login phase.
 *
 * @param[in] session ISDS session.
 * @param[in] mepRunning True if MEP is in progress.
 * @param[out] errTxt Last error message from ISDS.
 * @return True if login success.
 */
static
bool loginMepFinal(Isds::Session &session, bool mepRunning, QString &errTxt)
{
	if (!mepRunning) {
		errTxt = session.ctx()->lastIsdsMsg();
		return false;
	}

	/* Send basic authentication with MEP communication code. */
	session.ctx()->setUrl(Isds::createMepLoginUrlString(
	    session.ctx()->acntId().testing()));
	if (Isds::Type::ERR_SUCCESS != Isds::loginOperation(session,
	        LOGIN_SERVICE, Q_NULLPTR, &errTxt)) {
		return false;
	}

	/* Received cookies. Ping ISDS server using new cookies. */
	if (!testNewCookies(session)) {
		errTxt = session.ctx()->lastIsdsMsg();
		return false;
	}

	return true;
}

/*!
 * @brief Wait for user MEP reply.
 *
 * @param[in] session ISDS session.
 * @param[in] mepRunning True if MEP is in progress.
 * @return Mep resolution state.
 */
static
enum Isds::Type::MepResolution waitForMepReply(Isds::Session &session,
    const volatile bool &mepRunning)
{
	Isds::Type::MepResolution rsCode = Isds::Type::MR_ACK_REQUESTED;

	if (!mepRunning) {
		return Isds::Type::MR_UNKNOWN;
	}

	const int delaySec = 1;
	do {
		if (!mepRunning) {
			/* MEP login was cancelled from QML */
			return Isds::Type::MR_UNKNOWN;
		}

		{
			QTime dieTime = QTime::currentTime().addSecs(delaySec);
			while (QTime::currentTime() < dieTime) {
				QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
			}
		}

		if (Isds::waitForMep(session, rsCode) != Isds::Type::ERR_SUCCESS) {
			return Isds::Type::MR_UNRECOGNISED;
		}

	} while (rsCode == Isds::Type::MR_ACK_REQUESTED);

	return rsCode;
}

bool Isds::Login::loginMep(Sessions &sessions, const AcntId &acntId,
    volatile bool &mepRunning, QString &errTxt)
{
	debugFuncCall();

	if (Q_UNLIKELY((!sessions.holdsSession(acntId)))) {
		Q_ASSERT(0);
		return false;
	}

	/* Get session. */
	Isds::Session *session = sessions.session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		logErrorNL("Invalid or empty ISDS session for username '%s'.",
		    acntId.username().toUtf8().constData());
		Q_ASSERT(0);
		return false;
	}

	bool success = false;
	mepRunning = true;

	switch (waitForMepReply(*session, mepRunning)) {
	case Isds::Type::MR_ACK:
		success = loginMepFinal(*session, mepRunning, errTxt);
		break;
	case Isds::Type::MR_ACK_EXPIRED:
		logErrorNL("MEP login response expired for '%s'.",
		    acntId.username().toUtf8().constData());
		errTxt = tr("MEP confirmation timeout.");
		break;
	case Isds::Type::MR_UNKNOWN:
		logErrorNL("MEP login was cancelled for '%s'.",
		    acntId.username().toUtf8().constData());
		errTxt = tr("MEP login was cancelled.");
		break;
	case Isds::Type::MR_UNRECOGNISED:
		logErrorNL("MEP login has failed for '%s'.",
		    acntId.username().toUtf8().constData());
		errTxt = tr("Wrong MEP login credentials. Invalid username or communication code.");
		break;
	default:
		logErrorNL("MEP login failed for '%s'.",
		    acntId.username().toUtf8().constData());
		errTxt = tr("MEP login failed.");
		break;
	}

	mepRunning = false;
	return success;
}

bool Isds::Login::loginTotp(Sessions &sessions, const AcntId &acntId,
    const QString &smsCode, QString &errTxt)
{
	debugFuncCall();

	if (Q_UNLIKELY((!sessions.holdsSession(acntId)))) {
		Q_ASSERT(0);
		return false;
	}

	/* Get session and set OTP code. */
	Isds::Session *session = sessions.session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		logErrorNL("Invalid or empty ISDS session for username '%s'.",
		    acntId.username().toUtf8().constData());
		Q_ASSERT(0);
		return false;
	}
	session->ctx()->setPassword(session->ctx()->password() + smsCode);

	/* Send basic authentication with SMS code. */
	if (Isds::Type::ERR_SUCCESS != Isds::loginOperation(*session,
	        QString(), Q_NULLPTR, &errTxt)) {
		return false;
	}

	/* Received cookies. Ping ISDS server using new cookies. */
	if (!testNewCookies(*session)) {
		errTxt = session->ctx()->lastIsdsMsg();
		return false;
	}

	return true;
}
