/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>

#include "src/messages.h"
#include "src/settings/account.h"
#include "src/worker/task_send_message.h"

class AccountListModel; /* Forward declaration. */
class AcntId; /* Forward declaration. */
class FileListModel; /* Forward declaration. */
class QmlAcntId; /* Forward declaration. */

/*
 * Class IsdsWrapper provides interface between QML and ISDS core.
 * Class is initialised in main function (main.cpp).
 */
class IsdsWrapper : public QObject {
	Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 */
	explicit IsdsWrapper(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Log out and close all active OTP connections, clear cookies.
	 */
	void closeAllOtpConnections(void);

	/*!
	 * @brief Change ISDS login password.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] oldPwd Current/old password string.
	 * @param[in] newPwd New password string.
	 * @param[in] otpCode OTP code string (may be null).
	 * @return True if password was changed.
	 */
	Q_INVOKABLE
	bool changePassword(const QmlAcntId *qAcntId, const QString &oldPwd,
	    const QString &newPwd, const QString &otpCode);

	/*!
	 * @brief Call an ISDS action from QML.
	 *
	 * @param[in] isdsAction ISDS action identifier.
	 * @param[in] qAcntId Account identifier.
	 */
	Q_INVOKABLE
	void doIsdsAction(const QString &isdsAction, const QmlAcntId *qAcntId);

	/*!
	 * @brief Downloads complete message.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] messageType Message orientation.
	 * @param[in] msgId Message ID.
	 */
	Q_INVOKABLE
	void downloadMessage(const QmlAcntId *qAcntId,
	    enum Messages::MessageType messageType, qint64 msgId);

	/*!
	 * @brief Find data box info.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] dbID Data box ID.
	 * @param[in] dbType Data box Type.
	 * @return Data box detail info string.
	 */
	Q_INVOKABLE
	QString findDatabox(const QmlAcntId *qAcntId, const QString &dbID,
	    const QString &dbType);

	/*!
	 * @brief Full-text data-box search based on search criteria.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in,out] databoxModel Data box list model.
	 * @param[in] phrase World to be found.
	 * @param[in] searchType Data box field specification for search.
	 * @param[in] searchScope Data box type search restriction.
	 * @param[in] page Page number to be shown.
	 * @return Number of results.
	 */
	Q_INVOKABLE
	int findDataboxFulltext(const QmlAcntId *qAcntId,
	    DataboxListModel *databoxModel, const QString &phrase,
	    const QString &searchType, const QString &searchScope,
	    int page);

	/*!
	 * @brief Get data box ID from ISDS.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @return Return data box ID.
	 */
	Q_INVOKABLE
	QString getAccountDbId(const QmlAcntId *qAcntId);

	/*!
	 * @brief Download account info.
	 *
	 * @param[in] qAcntId Account identifier.
	 */
	Q_INVOKABLE
	void getAccountInfo(const QmlAcntId *qAcntId);

	/*!
	 * @brief Return ISDS login method identifier.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @return Return ISDS login method identifier.
	 */
	Q_INVOKABLE static
	enum AcntData::LoginMethod getAccountLoginMethod(
	    const QmlAcntId *qAcntId);

	/*!
	 * @brief Import ZFO files to local database.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] fileList File list or dir path.
	 * @param[in] authenticate True if message must be authenticate in ISDS.
	 * @return Info/error text.
	 */
	Q_INVOKABLE
	QString importZfoMessages(const QmlAcntId *qAcntId,
	    const QStringList &fileList, bool authenticate);

	/*!
	 * @brief Do actions if QML input dialogue was cancelled.
	 *
	 * @param[in] isdsAction ISDS Action string.
	 * @param[in] qAcntId Account identifier.
	 */
	Q_INVOKABLE
	void inputDialogCancel(const QString &isdsAction,
	    const QmlAcntId *qAcntId);

	/*!
	 * @brief Password string from QML input dialogue.
	 *
	 * @param[in] isdsAction ISDS Action string.
	 * @param[in] loginMethod Login method type.
	 * @param[in] qAcntId Account identifier.
	 * @param[in] accountName Account name string.
	 * @param[in] pwd Password or OTP code.
	 */
	Q_INVOKABLE
	void inputDialogReturnPassword(const QString &isdsAction,
	    enum AcntData::LoginMethod loginMethod, const QmlAcntId *qAcntId,
	    const QString &pwd);

	/*!
	 * @brief Check if new password is valid for ISDS.
	 *
	 * @param[in] password Password to be checked.
	 * @return True if password is correct.
	 */
	Q_INVOKABLE
	bool isCorrectPassword(const QString &password);

	/*!
	 * @brief Check if account is logged into isds.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @return True if account is logged into isds.
	 */
	Q_INVOKABLE
	bool isLogged(const QmlAcntId *qAcntId);

	/*!
	 * @brief Cancel MEP login.
	 */
	Q_INVOKABLE
	void loginMepCancel(void);

	/*!
	 * @brief Check if MEP login is running.
	 *
	 * @return True if MEP login is processed.
	 */
	Q_INVOKABLE
	bool loginMepRunning(void);

	/*!
	 * @brief Sent GOV request.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] msg Gov message.
	 * @return True if message was sent.
	 */
	bool sendGovRequest(const AcntId &acntId, const Isds::Message &msg);

	/*!
	 * @brief Send message.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] dmID Message ID.
	 * @param[in] dmAnnotation Message subject string.
	 * @param[in] databoxModel Recipient data box model.
	 * @param[in] attachModel Attachment model.
	 * @param[in] dmLegalTitleLaw Mandate Law string.
	 * @param[in] dmLegalTitleYear Mandate Year string.
	 * @param[in] dmLegalTitleSect Mandate Section string.
	 * @param[in] dmLegalTitlePar Mandate Paragraph string.
	 * @param[in] dmLegalTitlePoint Mandate Letter string.
	 * @param[in] dmToHands Mesasge to hands string.
	 * @param[in] dmRecipientRefNumber Your ref. number string.
	 * @param[in] dmRecipientIdent Your identification string.
	 * @param[in] dmSenderRefNumber Our ref. number string.
	 * @param[in] dmSenderIdent Our identification string.
	 * @param[in] dmOVM True if send message as OVM.
	 * @param[in] dmPublishOwnID True if add sender name to message.
	 * @param[in] dmAllowSubstDelivery True if allow acceptance by fiction.
	 * @param[in] dmPersonalDelivery True if personal delivery.
	 * @param[in] dmType Message type [K,V,I,O,...].
	 */
	Q_INVOKABLE
	void sendMessage(const QmlAcntId *qAcntId, qint64 dmID,
	    const QString &dmAnnotation, const DataboxListModel *databoxModel,
	    const FileListModel *attachModel,
	    const QString &dmLegalTitleLaw, const QString &dmLegalTitleYear,
	    const QString &dmLegalTitleSect, const QString &dmLegalTitlePar,
	    const QString &dmLegalTitlePoint, const QString &dmToHands,
	    const QString &dmRecipientRefNumber, const QString &dmRecipientIdent,
	    const QString &dmSenderRefNumber, const QString &dmSenderIdent,
	    bool dmOVM, bool dmPublishOwnID, bool dmAllowSubstDelivery,
	    bool dmPersonalDelivery, const QString &dmType);

	/*!
	 * @brief Sent SMS request.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] oldPwd Current/old password string.
	 * @return True if SMS was sent.
	 */
	Q_INVOKABLE
	bool sendSMS(const QmlAcntId *qAcntId, const QString &oldPwd);

	/*!
	 * @brief Sync all account at one time.
	 */
	Q_INVOKABLE
	void syncAllAccounts(void);

	/*!
	 * @brief Download message list of one account.
	 *
	 * @param[in] qAcntId Account identifier.
	 */
	Q_INVOKABLE
	void syncOneAccount(const QmlAcntId *qAcntId);

	/*!
	 * @brief Download received message list.
	 *
	 * @param[in] qAcntId Account identifier.
	 */
	Q_INVOKABLE
	void syncSingleAccountReceived(const QmlAcntId *qAcntId);

	/*!
	 * @brief Download list of sent messages.
	 *
	 * @param[in] qAcntId Account identifier.
	 */
	Q_INVOKABLE
	void syncSingleAccountSent(const QmlAcntId *qAcntId);

public slots:
	/*!
	 * @brief Remove ISDS context for account account. Run this slot
	 *     when account is deleted or its setting has been updated.
	 *
	 * @param[in] acntId Account identifier.
	 */
	void removeIsdsCtx(const AcntId &acntId);

private:
	/*!
	 * @brief Perform an login action.
	 *
	 * @param[in] isdsAction ISDS action identifier.
	 * @param[in] acntId Account identifier.
	 */
	void doLoginAction(const QString &isdsAction, const AcntId &acntId);

	/*!
	 * @brief Perform some post action when login failed.
	 *
	 * @param[in] isdsAction ISDS action identifier.
	 * @param[in] acntId Account identifier.
	 * @param[in] errTxt Last error message.
	 */
	void doLoginFailAction(const QString &isdsAction, const AcntId &acntId,
	    QString errTxt);

	/*!
	 * @brief Perform an ISDS action.
	 *
	 * @param[in] isdsAction ISDS action identifier.
	 * @param[in] acntId Account identifier.
	 */
	void doIsdsAction(const QString &isdsAction, const AcntId &acntId);

	/*!
	 * @brief Download account info.
	 *
	 * @param[in] acntId Account identifier.
	 */
	void getAccountInfo(const AcntId &acntId);

	/*!
	 * @brief Check if account has all required login data.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] isdsAction ISDS action identifier.
	 * @return True if login method has all required login data.
	 */
	bool hasAllLoginData(const AcntId &acntId,
	    const QString &isdsAction);

	/*!
	 * @brief Show error dialogue when login to data box failed.
	 *
	 * @param[in] isdsAction ISDS login action.
	 * @param[in] acntId Account identifier.
	 * @param[in] errTxt Last error message.
	 */
	void showLoginErrorDlg(const QString &isdsAction,
	    const AcntId &acntId, const QString &errTxt);

	/*!
	 * @brief Wait for security OTP code.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] isdsAction ISDS action identifier.
	 */
	void waitForHotpCode(const AcntId &acntId, const QString &isdsAction);

	/*!
	 * @brief Wait for SMS code.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] isdsAction ISDS action identifier.
	 */
	void waitForSmsCode(const AcntId &acntId, const QString &isdsAction);

	bool m_mepRunning; /*!< True if MEP login phase is running. */
	/* Used to collect sending results. */
	QSet<QString> m_transactIds; /*!< Temporary transaction identifiers. */
	QList<TaskSendMessage::ResultData> m_sentMsgResultList; /*!< Send status list. */

public slots:
	/*!
	 * @brief Do post actions when download account info finished.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] success True if success.
	 * @param[in] errTxt Error description.
	 */
	void downloadAccountInfoFinished(const AcntId &acntId,
	    bool success, const QString &errTxt);

	/*!
	 * @brief Do post actions when download delivery info finished.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] msgId Message ID.
	 * @param[in] success True if success.
	 * @param[in] errTxt Error description.
	 */
	void downloadDeliveryInfoFinished(const AcntId &acntId,
	    qint64 msgId, bool success, const QString &errTxt);

	/*!
	 * @brief Do post actions when download message finished.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] msgId Message ID.
	 * @param[in] success True if success.
	 * @param[in] errTxt Error description.
	 */
	void downloadMessageFinished(const AcntId &acntId, qint64 msgId,
	    bool success, const QString &errTxt);

	/*!
	 * @brief Do post actions when download message list finished.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] success True if success.
	 * @param[in] statusBarText Text for status bar.
	 * @param[in] errTxt Error description.
	 * @param[in] isMsgReceived True if message type is received.
	 */
	void downloadMessageListFinished(const AcntId &acntId,
	    bool success, const QString &statusBarText, const QString &errTxt,
	    bool isMsgReceived);

	/*!
	 * @brief Do some actions when import of one ZFO has been finished.
	 *
	 * @param[in] zfoNumber ZFO number in import process.
	 * @param[in] zfoTotal Overall number of ZFO for import.
	 * @param[in] infoText Import result description (may be NULL).
	 */
	void importZFOFinished(int zfoNumber, int zfoTotal,
	    QString infoText);

	/*!
	 * @brief Do post actions when send message finished.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] transactId Unique transaction identifier.
	 * @param[in] dbIDRecipient Message recipient data box ID string.
	 * @param[in] dmRecipient Recipient full name.
	 * @param[in] msgId Send message ID.
	 * @param[in] success True if success.
	 * @param[in] errTxt Error description.
	 */
	void sendMessageFinished(const AcntId &acntId,
	    const QString &transactId, const QString &dbIDRecipient,
	    const QString &dmRecipient, qint64 msgId, bool success,
	    const QString &errTxt);

signals:
	/*!
	 * @brief Update message model in QML.
	 *
	 * @param[in] msgId Message id.
	 */
	void downloadMessageFinishedSig(qint64 msgId);

	/*!
	 * @brief Update account and message model in QML.
	 *
	 * @param[in] isMsgReceived Message type, true = received.
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void downloadMessageListFinishedSig(bool isMsgReceived,
	    QString userName, bool testing);

	/*!
	 * @brief Update account info in QML.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void downloadAccountInfoFinishedSig(QString userName, bool testing);

	/*!
	 * @brief Send open QML input dialogue request to QML.
	 *
	 * @param[in] isdsAction ISDS Action string.
	 * @param[in] loginMethod Login method type.
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 * @param[in] title Dialogue title.
	 * @param[in] text Dialogue text.
	 * @param[in] placeholderText Dialogue placeholder text.
	 * @param[in] hidePwd True means the password will be hidden.
	 */
	void openDialogRequest(const QString &isdsAction,
	    enum AcntData::LoginMethod loginMethod, QString userName,
	    bool testing, QString title, QString text, QString placeholderText,
	    bool hidePwd);

	/*!
	 * @brief Signal is emitted when ISDS password can be changed.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void runChangePasswordSig(QString userName, bool testing);

	/*!
	 * @brief Signal is emitted when account username can be changed.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void runChangeUserNameSig(QString userName, bool testing);

	/*!
	 * @brief Run download message ISDS action from QML.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void runDownloadMessageSig(QString userName, bool testing);

	/*!
	 * @brief Run full-text data-box search action from QML.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void runFindDataboxFulltextSig(QString userName, bool testing);

	/*!
	 * @brief Run find data box action from QML.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void runFindDataboxSig(QString userName, bool testing);

	/*!
	 * @brief Run download account info action from QML.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void runGetAccountInfoSig(QString userName, bool testing);

	/*!
	 * @brief Run import and verify ZFO message action from QML.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void runImportZfoMessageSig(QString userName, bool testing);

	/*!
	 * @brief Run send message ISDS action from QML.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void runSendMessageSig(QString userName, bool testing);

	/*!
	 * @brief Run send gov message ISDS action from QML.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void runSendGovMessageSig(QString userName, bool testing);

	/*!
	 * @brief Run sync all accounts action from QML.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void runSyncAllAccountsSig(QString userName, bool testing);

	/*!
	 * @brief Run sync one account action from QML.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void runSyncOneAccountSig(QString userName, bool testing);

	/*!
	 * @brief Run sync received message list action from QML.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void runSyncSingleAccountReceivedSig(QString userName, bool testing);

	/*!
	 * @brief Run sync sent message list action from QML.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void runSyncSingleAccountSentSig(QString userName, bool testing);

	/*!
	 * @brief Do some post QML action after message sending.
	 *
	 * @param[in] closePage True close send message page.
	 */
	void sentMessageFinished(bool closePage);

	/*!
	 * @brief Send search result info to QML.
	 *
	 * @param[in] totalCount Total number of data boxes.
	 * @param[in] currentCount Number of data boxes on the current page.
	 * @param[in] position Page number.
	 * @param[in] lastPage True if search result are last page.
	 */
	void sendSearchResultsSig(int totalCount, int currentCount,
	    int position, bool lastPage);

	/*!
	 * @brief Set new status bar text and active busy indicator to QML.
	 *
	 * @param[in] txt Text message for status bar.
	 * @param[in] busy True means the status bar busy indicator is
	 *            active and shown, false = disabled and hidden.
	 * @param[in] isVisible True means the status bar is visible,
	 *            false = hidden.
	 */
	void statusBarTextChanged(QString txt, bool busy, bool isVisible);

	/*!
	 * @brief Set new status bar text from MEP login to QML.
	 *
	 * @param[in] txt Text message for status bar.
	 */
	void statusBarTextMepChanged(QString txt);

	/*!
	 * @brief Signal is emitted when login to ISDS of new account fails.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] testing True if account is ISDS test environment.
	 */
	void unsuccessfulLoginToIsdsSig(QString userName, bool testing);

	/*!
	 * @brief Send info to QML that ZFO import has been finished.
	 *
	 * @param[in] txt Result text.
	 */
	void zfoImportFinishedSig(QString txt);

	/*!
	 * @brief Send info to QML that one ZFO file has been imported.
	 *
	 * @param[in] txt Result text.
	 */
	void zfoImportProgressSig(QString txt);

	/*!
	 * @brief Signal is emitted when login waits for MEP notification.
	 *
	 * @param[in] userName Account username identifier.
	 * @param[in] show True if show MEP waiting dialogue.
	 */
	void showMepWaitDialog(QString userName, bool show);
};
