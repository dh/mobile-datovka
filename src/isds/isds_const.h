/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

/* HTTP request timeout, default 5 minutes */
#define ISDS_TIMEOUT 300000

/* Application name prefix for MEP notification */
#define APP_NAME_PREFIX "mobilní"

/* BASE ISDS URLs  */
#define ISDS_URL "https://ws1.mojedatovaschranka.cz/"
#define ISDS_CERT_URL "https://ws1c.mojedatovaschranka.cz/certds/"
#define ISDS_OTP_URL "https://www.mojedatovaschranka.cz/"
#define ISDS_URL_TESTING "https://ws1.czebox.cz/"
#define ISDS_CERT_URL_TESTING "https://ws1c.czebox.cz/certds/"
#define ISDS_OTP_URL_TESTING "https://www.czebox.cz/"

/* ISDS SOAP endpoint services */
#define LOGIN_SERVICE "DS/dz"
#define FIND_SERVICE "DS/df"
#define MESSAGE_SERVICE "DS/dx"
#define DB_SERVICE "DS/DsManage"
#define OTP_CHNG_PWD_SERVICE "asws/changePassword"

/* Login and logout locators */
#define OTP_LOGIN_LOCATOR "as/processLogin?"
#define OTP_LOGOUT_LOCATOR "as/processLogout?"

/* Url parameters */
#define PRM_TYPE_HOTP "type=hotp"
#define PRM_TYPE_TOTP "type=totp"
#define PRM_TYPE_MEP "type=mep-ws"
#define PRM_MEP_APP_NAME "&applicationName="
#define PRM_TOTP_SEND_SMS "&sendSms=true"
#define PRM_URI_PRODUCTION "&uri=https://www.mojedatovaschranka.cz/apps/"
#define PRM_URI_TESTING "&uri=https://www.czebox.cz/apps/"

/* Specifies XML namespace URL for some ISDS SOAP XML documents. */
#define ISDS_XML_NS "http://isds.czechpoint.cz/v20"

/* ISDS OTP SEND MSG CODE */
#define OTP_SEND_SMS "authentication.info.totpSended"

/* ISDS OTP ERROR CODE */
#define OTP_ERROR_NOT_LOGGED "authentication.error.userIsNotAuthenticated"
#define OTP_ERROR_USER_BLOCKED "authentication.error.intruderDetected"
#define OTP_PWD_EXPIR "authentication.error.paswordExpired"
#define OTP_BAD_ROLE "authentication.error.badRole"
#define OTP_ERROR_SEND_QUICKLY "authentication.info.cannotSendQuickly"
#define OTP_ERROR_NOT_SEND "authentication.info.totpNotSended"
#define OTP_ERROR_NOT_ORDERED "authentication.info.totpNotOrdered"

/* ISDS MEP ERROR CODE */
#define MEP_ERROR_USER_NOT_EXISTS "authentication.error.userIsNotExists"
#define MEP_ERROR_USER_NOT_SET "authentication.error.mep.userid.notset"
