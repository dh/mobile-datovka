/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/datovka_shared/isds/message_interface.h"

class QByteArray; /* Forward declaration. */

namespace Isds {

	/*!
	 * @brief Read message data and return message instance.
	 *
	 * @note Doesn't know RT_INCOMING_MESSAGE and RT_DELIVERYINFO.
	 *
	 * @param[in]  data Message data, may be CMS or its content.
	 * @param[out] ok Set to false on failure.
	 * @return Message instance, null value on error.
	 */
	Message toMessage(const QByteArray &data, bool *ok = Q_NULLPTR);
	Message toMessage(QByteArray &&data, bool *ok = Q_NULLPTR);

}
