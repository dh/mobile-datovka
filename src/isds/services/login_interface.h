/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/datovka_shared/isds/types.h"

class QString; /* Forward declaration. */

namespace Isds {

	class DbStatus; /* Forward declaration. */
	class DmStatus; /* Forward declaration. */
	class Session; /* Forward declaration. */

	/*!
	 * @brief Change password for OTP login method using <ChangePasswordOTP>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     otpMethod OTP login method.
	 * @param[in]     oldPwd Old password.
	 * @param[in]     newPwd New password.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error changePasswordOTP(Session &session,
	    enum Type::OtpMethod otpMethod, const QString &oldPwd,
	    const QString &newPwd, DbStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Change password using <ChangeISDSPassword>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     oldPwd Old password.
	 * @param[in]     newPwd New password.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error changeISDSPassword(Session &session,
	    const QString &oldPwd, const QString &newPwd,
	    DbStatus *status = Q_NULLPTR, QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Send SMS code using <SendSMSCode>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error sendSMSCode(Session &session,
	    DbStatus *status = Q_NULLPTR, QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Performs a login procedure. Uses <DummyOperation>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     soapEndPoint SOAP end point for login.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error loginOperation(Session &session,
	    const QString &soapEndPoint, DmStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Log out MEP or OTP data box.
	 *
	 * @param[in,out] session Communication session.
	 * @return Error status.
	 */
	enum Type::Error logoutMepOtp(Session &session);

	/*!
	 * @brief Wait for MEP response code.
	 *
	 * @param[in,out] session Communication session.
	 * @param[out]    rsCode Response code from MEP.
	 * @return Error status.
	 */
	enum Type::Error waitForMep(Session &session,
	    enum Type::MepResolution &rsCode);
}
