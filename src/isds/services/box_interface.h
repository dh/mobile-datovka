/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/datovka_shared/isds/box_interface.h"
#include "src/datovka_shared/isds/box_interface2.h"
#include "src/datovka_shared/isds/types.h"

class QDateTime; /* Forward declaration. */
class QString; /* Forward declaration. */

namespace Isds {

	class DbStatus; /* Forward declaration. */
	class Session; /* Forward declaration. */

	/*!
	 * @brief Download user password info using <GetPasswordInfo>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[out]    expiration Downloaded user password expiration date.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error getPasswordInfo(Session &session, QDateTime &expiration,
	    DbStatus *status = Q_NULLPTR, QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Download data box owner info using <GetOwnerInfoFromLogin2>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[out]    dbOwnerInfo Downloaded owner info.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error getOwnerInfo2(Session &session,
	    DbOwnerInfoExt2 &dbOwnerInfo, DbStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Download data box user info using <GetUserInfoFromLogin2>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[out]    dbUserInfo Downloaded user info.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error getUserInfo2(Session &session,
	    DbUserInfoExt2 &dbUserInfo, DbStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Search for data box using <FindDataBox2>.
	 *
	 * @note Currently only the search according to the box identifier is
	 *     implemented. Other criteria values are more or less ignored.
	 *     As a result only a list of 0 or 1 results can be returned.
	 * @todo Implement full functionality.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     criteria Search criteria.
	 * @param[out]    foundBoxes List of found data boxes.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error findDatabox2(Session &session,
	    const DbOwnerInfoExt2 &criteria, QList<DbOwnerInfoExt2> &foundBoxes,
	    DbStatus *status = Q_NULLPTR, QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Search data boxes using <ISDSSearch3>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     query Sought phrase.
	 * @param[in]     searchType Search type.
	 * @param[in]     boxType Value BT_SYSTEM means to search in all box types.
	 *                        Value BT_OVM_MAIN means to search in non-subsidiary OVM box types.
	 *                        Pass BT_NULL to let server use the default value which is BT_SYSTEM.
	 * @param[in]     page Page number.
	 * @param[in]     pageSize Number of items into the page.
	 * @param[in]     highlighting True if result will be highlighting.
	 * @param[out]    foundBoxes List of found data boxes.
	 * @param[out]    totalCount Total number of data boxes.
	 * @param[out]    currentCount Number of data boxes on the current page.
	 * @param[out]    position Number of page.
	 * @param[out]    lastPage True if search result are last page.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error isdsSearch3(Session &session, const QString &query,
	    enum Type::FulltextSearchType searchType, enum Type::DbType boxType,
	    long int page, long int pageSize, enum Type::NilBool highlighting,
	    QList<DbResult2> &foundBoxes, long int &totalCount,
	    long int &currentCount, long int &position, bool &lastPage,
	    DbStatus *status = Q_NULLPTR, QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Download long term storage info using <DTInfo>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     dbID Data box ID.
	 * @param[out]    dtInfo Downloaded long term storage info.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error dtInfo(Session &session, const QString &dbID,
	    DTInfoOutput &dtInfo, DbStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);
}
