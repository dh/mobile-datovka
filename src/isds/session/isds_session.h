/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS, Q_DISABLE_COPY */
#include <QMutex>
#include <QString>

#include "src/isds/session/isds_context.h"

class AcntData; /* Forward declaration. */

namespace Isds {

	/*!
	 * @brief ISDS communication context together with a mutex lock.
	 */
	class Session {
		Q_DECLARE_TR_FUNCTIONS(Session)

	public:
		/*!
		 * @brief Destructor.
		 */
		~Session(void);

		/*!
		 * @brief Creates a session. Session is guaranteed to be fully
		 *    created.
		 *
		 * @param[in] acntData Account data.
		 * @return Pointer to newly created instance on success,
		 *    Q_NULLPTR on failure.
		 */
		static
		Session *createSession(const AcntData &acntData);

		/*!
		 * @brief Access the ISDS context.
		 *
		 * @return Pointer to ISDS context structure.
		 */
		Context *ctx(void);
		const Context *ctx(void) const;

		/*!
		 * @brief Assign ISDS context structure to session.
		 *
		 * @param[in] ctx ISDS communication context.
		 */
		void setCtx(const Context &ctx);

		/*!
		 * @brief Set login URL according to the login method type.
		 */
		void setLoginUrl(void);

		/*!
		 * @brief Set user login certificate.
		 *
		 * @param[in] certPath Path to certificate file.
		 * @param[in] keyPwd Private key password.
		 * @param[out] errTxt Error description.
		 * @return True if success.
		 */
		bool setCertificate(const QString &certPath,
		    const QString &keyPwd, QString &errTxt);

		/*!
		 * @brief Access mutex.
		 *
		 * @return Pointer to mutex.
		 */
		QMutex *mutex(void);

	private:
		Q_DISABLE_COPY(Session)

		/*!
		 * @brief Constructor.
		 */
		Session(void);

		Context m_ctx; /*!< ISDS communication context. */
		QMutex m_mutex; /*!< Mutex. */
	};

	/*!
	 * @brief Create HOTP login URL string.
	 *
	 * @param[in] testing True if account is in ISDS test environment.
	 * @return URL string.
	 */
	QString createHotpLoginUrlString(bool testing);

	/*!
	 * @brief Create MEP login URL string.
	 *
	 * @param[in] testing True if account is in ISDS test environment.
	 * @return URL string.
	 */
	QString createMepLoginUrlString(bool testing);

	/*!
	 * @brief Create TOTP login URL string.
	 *
	 * @param[in] testing True if account is in ISDS test environment.
	 * @return URL string.
	 */
	QString createTotpLoginUrlString(bool testing);

	/*!
	 * @brief Create OTP logout URL string.
	 *
	 * @param[in] testing True if account is in ISDS test environment.
	 * @return URL string.
	 */
	QString createOtpLogoutUrlString(bool testing);

}
