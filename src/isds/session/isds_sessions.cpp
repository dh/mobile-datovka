/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/datovka_shared/log/log.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"

Sessions::Sessions(void)
    : m_sessions()
{
}

Sessions::~Sessions(void)
{
	/* Free all contexts. */
	foreach (Isds::Session *session, m_sessions) {
		delete session;
	}
}

QList<AcntId> Sessions::keys(void) const
{
	return m_sessions.keys();
}

bool Sessions::holdsSession(const AcntId &acntId) const
{
	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return false;
	}
	return m_sessions.constFind(acntId) != m_sessions.constEnd();
}

bool Sessions::insertSession(const AcntId &acntId, Isds::Session *session)
{
	debugFuncCall();

	/* acntId should not exist in this container. */
	if (Q_UNLIKELY(holdsSession(acntId) || (session == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	m_sessions.insert(acntId, session);
	return true;
}

void Sessions::removeSession(const AcntId &acntId)
{
	debugFuncCall();

	if (Q_UNLIKELY(!holdsSession(acntId))) {
		return;
	}

	Isds::Session *session = m_sessions.take(acntId);
	delete session;
}

Isds::Session *Sessions::session(const AcntId &acntId) const
{
	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return Q_NULLPTR;
	}

	return m_sessions.value(acntId, Q_NULLPTR);
}
