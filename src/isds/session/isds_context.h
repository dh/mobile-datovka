/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QNetworkCookie>
#include <QScopedPointer>

#include "src/settings/account.h"

class AcntId; /* Forward declaration. */
class QSslCertificate; /* Forward declaration. */
class QSslKey; /* Forward declaration. */
class QString; /* Forward declaration. */
class QUrl; /* Forward declaration. */

namespace Isds {

	/*!
	 * @brief Log-in status.
	 */
	enum LoginState {
		LS_NOT_LOGGED_IN = 0, /* Not logged in to the ISDS server. */
		LS_IN_PROGRESS, /* Login is in progress. */
		LS_LOGGED_IN /* Successfully logged in. */
	};

	class ContextPrivate;
	/*!
	 * @brief ISDS communication context.
	 */
	class Context {
		Q_DECLARE_PRIVATE(Context)

	public:
		Context(void);
		Context(const Context &other);
#ifdef Q_COMPILER_RVALUE_REFS
		Context(Context &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~Context(void);

		Context &operator=(const Context &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		Context &operator=(Context &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const Context &other) const;
		bool operator!=(const Context &other) const;

		friend void swap(Context &first, Context &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/* Login method. */
		/*
		 * TODO -- Move the declaration of the login method enumeration
		 *    values to the ISDS-related header files.
		 */
		enum AcntData::LoginMethod loginMethod(void) const;
		void setLoginMethod(enum AcntData::LoginMethod lim);

		/* acntId */
		const AcntId &acntId(void) const;
		void setAcntId(const AcntId &aid);
#ifdef Q_COMPILER_RVALUE_REFS
		void setAcntId(AcntId &&aid);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* URL */
		const QUrl &url(void) const;
		void setUrl(const QUrl &url);
#ifdef Q_COMPILER_RVALUE_REFS
		void setUrl(QUrl &&url);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* password */
		const QString &password(void) const;
		void setPassword(const QString &pwd);
#ifdef Q_COMPILER_RVALUE_REFS
		void setPassword(QString &&pwd);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* SSL certificate */
		const QSslCertificate &sslCertificate(void) const;
		void setSslCertificate(const QSslCertificate &sc);
#ifdef Q_COMPILER_RVALUE_REFS
		void setSslCertificate(QSslCertificate &&sc);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* SSL key */
		const QSslKey &sslKey(void) const;
		void setSslKey(const QSslKey &sk);
#ifdef Q_COMPILER_RVALUE_REFS
		void setSslKey(QSslKey &&sk);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* cookies */
		const QList<QNetworkCookie> &cookies(void) const;
		void setCookies(const QList<QNetworkCookie> &cl);
#ifdef Q_COMPILER_RVALUE_REFS
		void setCookies(QList<QNetworkCookie> &&cl);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* last ISDS notification */
		const QString &lastIsdsMsg(void) const;
		void setLastIsdsMsg(const QString &lim);
#ifdef Q_COMPILER_RVALUE_REFS
		void setLastIsdsMsg(QString &&lim);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* login state */
		enum LoginState loginState(void) const;
		void setLoginState(enum LoginState lst);

	private:
		QScopedPointer<ContextPrivate> d_ptr; // ::std::unique_ptr ?
	};

	void swap(Context &first, Context &second) Q_DECL_NOTHROW;
}
