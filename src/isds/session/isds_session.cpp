/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QFileInfo>
#include <QSslCertificate>
#include <QSslKey>
#include <QStringBuilder>
#include <QUrl>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/log/log.h"
#include "src/io/filesystem.h"
#include "src/isds/isds_const.h"
#include "src/isds/session/isds_session.h"

#ifdef Q_COMPILER_RVALUE_REFS
#  define macroStdMove(x) ::std::move(x)
#else /* Q_COMPILER_RVALUE_REFS */
#  define macroStdMove(x) (x)
#endif /* Q_COMPILER_RVALUE_REFS */

Isds::Session::Session(void)
    : m_ctx(),
    m_mutex()
{
}

Isds::Session::~Session(void)
{
}

/*!
 * @brief Create OTP URL string.
 *
 * @param[in] testing True if account is for the ISDS test environment.
 * @param[in] params Otp url parameters for login.
 * @return URL string.
 */
static
QString createOtpUrlString(bool testing, const QString &params) {
	if (testing) {
		return QStringLiteral(ISDS_OTP_URL_TESTING) % params
		    % QStringLiteral(PRM_URI_TESTING);
	} else {
		return QStringLiteral(ISDS_OTP_URL) % params
		    % QStringLiteral(PRM_URI_PRODUCTION);
	}
}

/*!
 * @brief Get base URL from login method and account type.
 *
 * @param[in] loginMethod Login method type.
 * @return Base url for http request.
 */
static
QUrl getBaseUrlFromLoginMethod(const AcntData &acntData)
{
	QUrl url;

	switch (acntData.loginMethod()) {
	case AcntData::LIM_UNAME_PWD:
		(acntData.isTestAccount()) ?
			url.setUrl(ISDS_URL_TESTING) :
			url.setUrl(ISDS_URL);
		break;
	case AcntData::LIM_UNAME_PWD_CRT:
		(acntData.isTestAccount()) ?
			url.setUrl(ISDS_CERT_URL_TESTING) :
			url.setUrl(ISDS_CERT_URL);
		break;
	case AcntData::LIM_UNAME_PWD_HOTP:
	case AcntData::LIM_UNAME_PWD_TOTP:
	case AcntData::LIM_UNAME_MEP:
		(acntData.isTestAccount()) ?
			url.setUrl(ISDS_OTP_URL_TESTING) :
			url.setUrl(ISDS_OTP_URL);
		break;
	default:
		Q_ASSERT(0);
		break;
	}

	return url;
}

static
void prepareIsdsContext(Isds::Context &isdsCtx, const AcntData &acntData)
{
	isdsCtx.setLoginMethod(acntData.loginMethod());
	isdsCtx.setAcntId(AcntId(acntData.userName(), acntData.isTestAccount()));
	isdsCtx.setUrl(getBaseUrlFromLoginMethod(acntData));
	isdsCtx.setPassword(
	    (AcntData::LIM_UNAME_MEP == acntData.loginMethod()) ?
	        acntData.mepToken() : acntData.password());
	/* TODO -- Set remaining values. */
}

Isds::Session *Isds::Session::createSession(const AcntData &acntData)
{
	if (Q_UNLIKELY(!acntData.isValid())) {
		Q_ASSERT(0);
		return Q_NULLPTR;
	}

	Session *session = new (::std::nothrow) Session;
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		Q_ASSERT(0);
		return Q_NULLPTR;
	}

	prepareIsdsContext(session->m_ctx, acntData);

	return session;
}

Isds::Context *Isds::Session::ctx(void)
{
	return &m_ctx;
}

const Isds::Context *Isds::Session::ctx(void) const
{
	return &m_ctx;
}

void Isds::Session::setCtx(const Context &ctx)
{
	m_ctx = ctx;
}

void Isds::Session::setLoginUrl(void)
{
	if (m_ctx.loginMethod() == AcntData::LIM_UNAME_PWD_HOTP) {
		m_ctx.setUrl(Isds::createHotpLoginUrlString(
		    m_ctx.acntId().testing()));
	} else if (m_ctx.loginMethod() == AcntData::LIM_UNAME_PWD_TOTP) {
		m_ctx.setUrl(Isds::createTotpLoginUrlString(
		    m_ctx.acntId().testing()));
	} else if (m_ctx.loginMethod() == AcntData::LIM_UNAME_MEP) {
		m_ctx.setUrl(Isds::createMepLoginUrlString(
		    m_ctx.acntId().testing()));
	}
}

bool Isds::Session::setCertificate(const QString &certPath,
    const QString &keyPwd, QString &errTxt)
{
	QFile certFile(certPath);

#if defined Q_OS_IOS

	// iOS base sandbox path is different after application was updated.
	// We cannot use static path for certificate on iOS like Android.
	QFileInfo fi(certFile);
	certFile.setFileName(joinDirs(appCertDirPath(), fi.fileName()));

#endif
	QFileInfo finfo(certFile);
	if (!certFile.open(QIODevice::ReadOnly)) {
		logErrorNL("Cannot open certificate from '%s'.",
		    certPath.toUtf8().constData());
		errTxt = tr("Cannot open certificate '%1'.").arg(
		    finfo.fileName());
		return false;
	}

	// is PEM format
	QString ext = finfo.suffix().toLower();
	if (ext == "pem") {
		QByteArray certificate = certFile.readAll();
		QSslCertificate cert(certificate, QSsl::Pem);
		QSslKey key(certificate, QSsl::Rsa,
		    QSsl::Pem, QSsl::PrivateKey, keyPwd.toUtf8());
			m_ctx.setSslKey(macroStdMove(key));
			m_ctx.setSslCertificate(macroStdMove(cert));
	// is PKCS12 format
	} else if (ext == "p12" || ext == "pfx") {
		QSslCertificate cert;
		QSslKey key;
		QList<QSslCertificate> importedCerts;
		if (QSslCertificate::importPkcs12(&certFile, &key,
		    &cert, &importedCerts, keyPwd.toUtf8())) {
			m_ctx.setSslKey(macroStdMove(key));
			m_ctx.setSslCertificate(macroStdMove(cert));
		} else {
			logErrorNL("Cannot parse certificate from '%s'.",
			    certPath.toUtf8().constData());
			errTxt = tr("Cannot parse certificate '%1'.").arg(
			    finfo.fileName());
			certFile.close();
			return false;
		}
	} else {
		logErrorNL("Unknown format of certificate '%s'.",
		    certPath.toUtf8().constData());
		errTxt = tr("Unknown format of certificate '%1'.").arg(
		    finfo.fileName());
		certFile.close();
		return false;
	}
	certFile.close();

	return true;
}

QMutex *Isds::Session::mutex(void)
{
	return &m_mutex;
}

QString Isds::createHotpLoginUrlString(bool testing)
{
	return createOtpUrlString(testing, QStringLiteral(OTP_LOGIN_LOCATOR)
	    % QStringLiteral(PRM_TYPE_HOTP));
}

QString Isds::createMepLoginUrlString(bool testing) {

	const QString params = QStringLiteral(OTP_LOGIN_LOCATOR)
	    % QStringLiteral(PRM_TYPE_MEP) % QStringLiteral(PRM_MEP_APP_NAME)
	    % QStringLiteral(APP_NAME_PREFIX) % QStringLiteral(" ")
	    % QStringLiteral(APP_NAME) % QStringLiteral("-")
	    % QStringLiteral(VERSION);
	return createOtpUrlString(testing, params);
}

QString Isds::createTotpLoginUrlString(bool testing)
{
	return createOtpUrlString(testing, QStringLiteral(OTP_LOGIN_LOCATOR)
	     % QStringLiteral(PRM_TYPE_TOTP) % QStringLiteral(PRM_TOTP_SEND_SMS));
}

QString Isds::createOtpLogoutUrlString(bool testing)
{
	return createOtpUrlString(testing, QStringLiteral(OTP_LOGOUT_LOCATOR));
}
