/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */

class AcntId; /* Forward declaration. */
class QString; /* Forward declaration. */
class Sessions; /* Forward declaration. */

namespace Isds {

	class Login {
		Q_DECLARE_TR_FUNCTIONS(Login)

	public:
		/*!
		 * @brief Logout and close all active OTP connections.
		 *
		 * @param[in,out] sessions Session container.
		 */
		static
		void closeAllOtpConnections(Sessions &sessions);

		/*!
		 * @brief Test if user is logged in to a data box.
		 *
		 * @param[in] sessions Session container.
		 * @param[in] acntId Account identifier.
		 * @return True if user is logged in to a data box.
		 */
		static
		bool isLoggedIn(const Sessions &sessions, const AcntId &acntId);

		/*!
		 * @brief Run login using basic authentication.
		 *
		 * @param[in,out] sessions Session container.
		 * @param[in] acntId Account identifier.
		 * @param[out] errTxt Last error message from ISDS.
		 * @return True if login was successful.
		 */
		static
		bool loginBasicAuth(Sessions &sessions, const AcntId &acntId,
		    QString &errTxt);

		/*!
		 * @brief Run HOTP login with security code.
		 *
		 * @param[in,out] sessions Session container.
		 * @param[in] acntId Account identifier.
		 * @param[in] otpCode Otp security code.
		 * @param[out] errTxt Last error message from ISDS.
		 * @return True if login was successful.
		 */
		static
		bool loginHotp(Sessions &sessions, const AcntId &acntId,
		    const QString &otpCode, QString &errTxt);

		/*!
		 * @brief Login to ISDS using MEP.
		 *
		 * @param[in,out] sessions Session container.
		 * @param[in] acntId Account identifier.
		 * @param[in,out] mepRunning True if MEP is in progress.
		 * @param[out] errTxt Last error message from ISDS.
		 * @return True if login was successful.
		 */
		static
		bool loginMep(Sessions &sessions, const AcntId &acntId,
		    volatile bool &mepRunning, QString &errTxt);

		/*!
		 * @brief Run TOTP login with SMS code.
		 *
		 * @param[in,out] sessions Session container.
		 * @param[in] acntId Account identifier.
		 * @param[in] smsCode Otp sms code.
		 * @param[out] errTxt Last error message from ISDS.
		 * @return True if login was successful.
		 */
		static
		bool loginTotp(Sessions &sessions, const AcntId &acntId,
		    const QString &smsCode, QString &errTxt);
	};
}
