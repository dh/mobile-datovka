/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/isds/conversion/isds_conversion.h"

/* MEP STATES */
#define MRSTATE_UNRECOGNISED -1
#define MRSTATE_ACK_REQUESTED 1
#define MRSTATE_MR_ACK 2
#define MRSTATE_ACK_EXPIRED 3

QString IsdsConversion::userTypeToDescr(enum Isds::Type::UserType type)
{
	switch (type) {
	case Isds::Type::UT_PRIMARY:
		return tr("Primary user");
		break;
	case Isds::Type::UT_ENTRUSTED:
		return tr("Entrusted user");
		break;
	case Isds::Type::UT_ADMINISTRATOR:
		return tr("Administrator");
		break;
	case Isds::Type::UT_OFFICIAL:
		return tr("Official");
		break;
	case Isds::Type::UT_OFFICIAL_CERT:
		return tr("Official");
		break;
	case Isds::Type::UT_LIQUIDATOR:
		return tr("Liquidator");
		break;
	case Isds::Type::UT_RECEIVER:
		return tr("Receiver");
		break;
	case Isds::Type::UT_GUARDIAN:
		return tr("Guardian");
		break;
	default:
		return tr("Unknown");
		break;
	}
}

QString IsdsConversion::senderTypeToDescr(enum Isds::Type::SenderType type)
{
	/* Missing ST_NULL, ST_OFFICIAL_CERT, ST_RECEIVER, ST_GUARDIAN. */
	switch (type) {
	case Isds::Type::ST_PRIMARY:
		return tr("Primary user");
		break;
	case Isds::Type::ST_ENTRUSTED:
		return tr("Entrusted user");
		break;
	case Isds::Type::ST_ADMINISTRATOR:
		return tr("Administrator");
		break;
	case Isds::Type::ST_OFFICIAL:
		return tr("Official");
		break;
	case Isds::Type::ST_VIRTUAL:
		return tr("Virtual");
		break;
	case Isds::Type::ST_LIQUIDATOR:
		return tr("Liquidator");
		break;
	default:
		return tr("Unknown");
		break;
	}
}

QString IsdsConversion::dbStateToText(int state)
{
	switch (state) {
	case Isds::Type::BS_ACCESSIBLE:
		return tr("The data box is accessible. "
		    "It is possible to send messages into it. "
		    "It can be looked up on the Portal.");
		break;
	case Isds::Type::BS_TEMP_INACCESSIBLE:
		return tr(
		    "The data box is temporarily inaccessible (at own request). "
		    "It may be made accessible again at some point in the future.");
		break;
	case Isds::Type::BS_NOT_YET_ACCESSIBLE:
		return tr("The data box is so far inactive. "
		    "The owner of the box has to log into the web interface at first in order to activate the box.");
		break;
	case Isds::Type::BS_PERM_INACCESSIBLE:
		return tr("The data box is permanently inaccessible. "
		    "It is waiting to be deleted (but it may be made accessible again).");
		break;
	case Isds::Type::BS_REMOVED:
		return tr(
		    "The data box has been deleted (nonetheless it exists in ISDS).");
		break;
	case Isds::Type::BS_TEMP_UNACCESSIBLE_LAW:
		return tr(
		    "The data box is temporarily inaccessible (because of the reasons enumerated in the law at the time of access denial). "
		    "It may be made accessible again at some point in the future.");
		break;
	default:
		return tr("An error occurred while checking the status.");
		break;
	}
}

QString IsdsConversion::dmTypeToText(const QString &val)
{
	if (val.size() != 1) {
		return QString();
	}

	switch (val[0].toLatin1()) {
	case Isds::Type::MT_A:
		return tr(
		    "Subsidised postal data message, initiating reply postal data message");
		break;
	case Isds::Type::MT_B:
		return tr(
		    "Subsidised postal data message, initiating reply postal data message - used for sending reply");
		break;
	case Isds::Type::MT_C:
		return tr(
		    "Subsidised postal data message, initiating reply postal data message - unused for sending reply, expired");
		break;
	case Isds::Type::MT_E:
		return tr(
		    "Postal data message sent using a subscription (prepaid credit)");
		break;
	case Isds::Type::MT_G:
		return tr(
		    "Postal data message sent in endowment mode by another data box to the benefactor account");
		break;
	case Isds::Type::MT_K:
		return tr("Postal data message");
		break;
	case Isds::Type::MT_I:
		return tr("Initiating postal data message");
		break;
	case Isds::Type::MT_O:
		return tr(
		    "Reply postal data message; sent at the expense of the sender of the initiating postal data message");
		break;
	case Isds::Type::MT_V:
		return tr(
		    "Public message (recipient or sender is a public authority)");
		break;
	case Isds::Type::MT_X:
		return tr(
		    "Initiating postal data message - unused for sending reply, expired");
		break;
	case Isds::Type::MT_Y:
		return tr(
		    "Initiating postal data message - used for sending reply");
		break;
	default:
		return tr("Unrecognised message type");
		break;
	}
}

QString IsdsConversion::dmMessageStatusToText(int status)
{
	switch (status) {
	case Isds::Type::MS_POSTED:
		/* Zprava byla podana (vznikla v ISDS). */
		return tr("Message has been submitted (has been created in ISDS)");
		break;
	case Isds::Type::MS_STAMPED:
		/*
		 * Datová zprava vcetne pisemnosti podepsana casovym razitkem.
		 */
		return tr("Data message including its attachments signed with time-stamp.");
		break;
	case Isds::Type::MS_INFECTED:
		/*
		 * Zprava neprosla AV kontrolou; nakazena pisemnost je smazana;
		 * konecny stav zpravy pred smazanim.
		 */
		return tr("Message did not pass through AV check; "
		    "infected paper deleted; final status before deletion.");
		break;
	case Isds::Type::MS_DELIVERED:
		/* Zprava dodana do ISDS (zapsan cas dodani). */
		return tr("Message handed into ISDS "
		    "(delivery time recorded).");
		break;
	case Isds::Type::MS_ACCEPTED_FICT:
		/*
		 * Uplynulo 10 dnu od dodani verejne zpravy, ktera dosud nebyla
		 * dorucena prihlasenim (predpoklad dorucení fikci u neOVM DS);
		 * u komercni zpravy nemuze tento stav nastat.
		 */
		return tr("10 days have passed since the delivery of "
		    "the public message which has not been accepted by "
		    "logging-in (assumption of delivery by fiction in nonOVM "
		    "DS); this state cannot occur for commercial messages.");
		break;
	case Isds::Type::MS_ACCEPTED:
		/*
		 * Osoba opravnena cist tuto zpravu se prihlasila - dodana
		 * zprava byla dorucena.",
		 */
		return tr("A person authorised to read this message "
		    "has logged in -- delivered message has been accepted.");
		break;
	case Isds::Type::MS_READ:
		/* Zprava byla prectena (na portale nebo akci ESS). */
		return tr("Message has been read (on the portal or "
		    "by ESS action).");
		break;
	case Isds::Type::MS_UNDELIVERABLE:
		/*
		 * Zprava byla oznacena jako nedorucitelna, protoze DS adresata
		 * byla zpetne znepristupnena.
		 */
		return tr("Message marked as undeliverable because "
		    "the target databox has been made inaccessible.");
		break;
	case Isds::Type::MS_REMOVED:
		/*
		 * Obsah zpravy byl smazan, obalka zpravy vcetne hashu
		 * presunuta do archivu.
		 */
		return tr("Message content deleted, envelope "
		    "including hashes has been moved into archive.");
		break;
	case Isds::Type::MS_IN_VAULT:
		/* Zprava je v Datovem trezoru. */
		return tr("Message resides in data vault.");
		break;
	default:
		return QString();
		break;
	}
}

enum Isds::Type::MepResolution IsdsConversion::isdsMepStatusToMepResolution(
    int status)
{
	switch (status) {
	case MRSTATE_ACK_REQUESTED:
		return Isds::Type::MR_ACK_REQUESTED;
		break;
	case MRSTATE_MR_ACK:
		return Isds::Type::MR_ACK;
		break;
	case MRSTATE_ACK_EXPIRED:
		return Isds::Type::MR_ACK_EXPIRED;
		break;
	case MRSTATE_UNRECOGNISED:
		return Isds::Type::MR_UNRECOGNISED;
		break;
	default:
		return Isds::Type::MR_UNKNOWN;
		break;
	}
}
