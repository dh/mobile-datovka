/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

#include "src/datovka_shared/isds/types.h"

namespace Isds {

	enum Type::NilBool str2BoolType(const QString &s);
	const QString &boolType2Str(enum Type::NilBool b);

	/*
	 * The ISDS interface is inconsistent. Some services (e.g. ISDSSearch2)
	 * use "0" and "1" to indicate boolean values.
	 */
	enum Type::NilBool numStr2BoolType(const QString &s);
	const QString &boolType2NumStr(enum Type::NilBool b);

	enum Type::DbType str2FulltextDbType(const QString &s);
	const QString &fulltextDbType2Str(enum Type::DbType bt);

	bool str2dbSendOptions(const QString &s, bool &act, bool &pub, bool &com);
	const QString &dbSendOptions2str(bool act, bool pub, bool com);

}
