/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/isds/interface/response_status.h"

/*
 * Here you can find functions needed for reading status values of the service
 * responses.
 *
 * It doesn't matter whether the responses are still encapsulated in the SOAP
 * envelope. The code is able to handle both situations.
 */

class QByteArray; /* Forward declaration. */
class QString; /* Forward declaration. */

namespace Isds {

	namespace Xml {

		/*!
		 * @brief Constructs DbStatus from supplied XML data.
		 *
		 * @param[in]  xmlData Response XML data.
		 * @param[out] ok Set to false on a XML error.
		 * @return Null value if dbStatus could not be read.
		 */
		DbStatus toDbStatus(const QByteArray &xmlData,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Read dbStatus content from supplied XML response.
		 *
		 * @param[in]  xmlData Response XML data.
		 * @param[out] message String obtained from the dbStatusMessage
		 *     element.
		 * @return True if the dbStatus content could be read and if the
		 *     obtained dbStatusCode is equal to "0000". False if the
		 *     dbStatus could not be read or if the obtained
		 *     dbStatusCode isn't equal to "0000".
		 */
		bool dbStatusOk(const QByteArray &xmlData, QString &message);

		/*!
		 * @brief Constructs DmStatus from supplied XML data.
		 *
		 * @param[in]  xmlData Response XML data.
		 * @param[out] ok Set to false on a XML error.
		 * @return Null value if dmStatus could not be read.
		 */
		DmStatus toDmStatus(const QByteArray &xmlData,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Read dmStatus content from supplied XML response.
		 *
		 * @param[in]  xmlData Response XML data.
		 * @param[out] message String obtained from the dmStatusMessage
		 *     element.
		 * @return True if the dmStatus content could be read and if the
		 *     obtained dmStatusCode is equal to "0000". False if the
		 *     dmStatus could not be read or if the obtained
		 *     dmStatusCode isn't equal to "0000".
		 */
		bool dmStatusOk(const QByteArray &xmlData, QString &message);

	}

}
