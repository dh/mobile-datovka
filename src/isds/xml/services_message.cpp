/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QByteArray>
#include <QString>
#include <QXmlStreamReader>

#include "src/isds/xml/services_message.h"

QByteArray Isds::Xml::cmsZfoFromSoap(const QByteArray &xmlData, bool *ok)
{
	QXmlStreamReader xml(xmlData);

	while (!xml.atEnd() && !xml.hasError()) {
		QXmlStreamReader::TokenType tokenType = xml.readNext();
		if (tokenType == QXmlStreamReader::StartDocument) {
			continue;
		}
		if ((tokenType == QXmlStreamReader::StartElement) &&
		    (xml.name() == QLatin1String("dmSignature"))) {
			xml.readNext();
			if (ok != Q_NULLPTR) {
				*ok = true;
			}
			return QByteArray::fromBase64(xml.text().toUtf8());
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return QByteArray();
}
