/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>

#include "src/datovka_shared/isds/message_interface.h"

class QDateTime; /* Forward declaration. */
class QDomDocument; /* Forward declaration. */
class QDomElement; /* Forward declaration. */

namespace Isds {

	namespace Xml {

		/*!
		 * @brief Requested message list type.
		 */
		enum RequestMessageList {
			RML_RCVD,
			RML_SNT
		};

		/*!
		 * @brief Signed message type.
		 */
		enum RequestSignedMessageData {
			RSMD_DELINFO,
			RSMD_RCVD_MSG,
			RSMD_SNT_MSG
		};

		/*!
		 * @brief Determine the document type.
		 *
		 * @note It is able to determine the
		 *     RT_PLAIN_SIGNED_INCOMING_MESSAGE,
		 *     RT_PLAIN_SIGNED_OUTGOING_MESSAGE,
		 *     RT_PLAIN_SIGNED_DELIVERYINFO
		 *     types. Is does not handle CMS data.
		 *     It ignores RT_INCOMING_MESSAGE and RT_DELIVERYINFO.
		 *
		 * @param[in] dom DOM document.
		 * @return Raw type, RT_UNKNOWN on error.
		 */
		enum Type::RawType guessRawType(const QDomDocument &dom);

		/*!
		 * @brief Read envelope from message or delivery info DOM.
		 *
		 * @param[in]  dom DOM document.
		 * @param[out] ok Set to false on error.
		 * @return Envelope, null value on error.
		 */
		Envelope readEnvelope(const QDomDocument &dom,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Read document list from message DOM.
		 *
		 * @param[in]  dom DOM document.
		 * @param[out] ok Set to false on error.
		 * @return Document list, empty list on error.
		 */
		QList<Document> readDocuments(const QDomDocument &dom,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Read message list from XML data.
		 *
		 * @param[in]  xmlData Response XML data.
		 * @param[out] ok Set to false on error.
		 * @return Envelope list, empty list on error.
		 */
		QList<Envelope> readMessageList(const QByteArray &xmlData,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Read create message response from XML data.
		 *
		 * @param[in] xmlData Response XML data.
		 * @return Identifier of the newly created message on success,
		 *     -1 on failure.
		 */
		qint64 readCreateMessageResponse(const QByteArray &xmlData);

		/*!
		 * @brief Read authenticate message response from XML data.
		 *
		 * @param[in]  xmlData Response XML data.
		 * @param[out] ok Set to false on error.
		 * @return True if message is valid.
		 */
		bool readAuthenticateMessageResponse(const QByteArray &xmlData,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Read message author info from XML data.
		 *
		 * @param[in]  xmlData Response XML data.
		 * @param[out] userType Sender type.
		 * @param[out] authorName Author name.
		 * @return False on XML error or if userType element not found.
		 */
		bool readMessageAuthor(const QByteArray &xmlData,
		    enum Type::SenderType &userType, QString &authorName);

		/*!
		 * @brief Construct <DummyOperation> request XML.
		 *
		 * @return Request XML data.
		 */
		QByteArray soapRequestDummyOperation(void);

		/*!
		 * @brief Constructs <MarkMessageAsDownloaded> request XML.
		 *
		 * @param[in] dmId Message identifier.
		 * @return Request XML data.
		 */
		QByteArray soapRequestMarkMessageAsDownloaded(qint64 dmId);

		/*!
		 * @brief Constructs <GetMessageAuthor> request XML.
		 *
		 * @param[in] dmId Message identifier.
		 * @return Request XML data.
		 */
		QByteArray soapRequestGetMessageAuthor(qint64 dmId);

		/*!
		 * @brief Constructs <AuthenticateMessage> request XML.
		 *
		 * @param[in] rawData Message or delivery info in raw binary format.
		 * @return Request XML data.
		 */
		QByteArray soapRequestAuthenticateMessage(
		    const QByteArray &rawData);

		/*!
		 * @brief Constructs <GetListOfReceivedMessages> or
		 *     <GetListOfSentMessages> request XML.
		 *
		 * @param[in] rml Determines request type.
		 * @param[in] fromTime Delivery time window start.
		 * @param[in] toTime Delivery time window end.
		 * @param[in] orgUnitNum Recipient or sender organisation unit.
		 * @param[in] statusFilter Bit mask determining requested message types.
		 * @param[in] offset Sequence number of the first requested record.
		 * @param[in] limit Number of requested records.
		 * @return Request XML data.
		 */
		QByteArray soapRequestMessageList(enum RequestMessageList rml,
		    const QDateTime &fromTime, const QDateTime &toTime,
		    long int orgUnitNum, Type::DmFiltStates statusFilter,
		    unsigned long int offset, unsigned long int limit);

		/*!
		 * @brief Constructs <GetSignedDeliveryInfo>,
		 *     <SignedMessageDownload> or <SignedSentMessageDownload>
		 *     request XML.
		 *
		 * @param[in] rsmd Determines request type.
		 * @param[in] dmId Message identifier.
		 * @return Request XML data.
		 */
		QByteArray soapRequestGetSignedMessage(
		    enum RequestSignedMessageData rsmd, qint64 dmId);

		/*!
		 * @brief Constructs <CreateMessage> request XML.
		 *
		 * @param[in]  message Message data.
		 * @param[out] ok Set to false on error.
		 * @return XML request data.
		 */
		QByteArray soapRequestCreateMessage(const Message &message,
		    bool *ok = Q_NULLPTR);
	}

}
