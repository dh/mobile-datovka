/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#include <QDateTime>

#include "src/datovka_shared/isds/box_interface.h"
#include "src/datovka_shared/isds/box_interface2.h"

class QDomDocument; /* Forward declaration. */
class QString; /* Forward declaration. */

namespace Isds {

	namespace Xml {

		/*!
		 * @brief Read user password expiration date from XML.
		 *
		 * @param[in]  xmlData XML data.
		 * @param[out] ok Set to false on failure.
		 * @return Expiration date, null value if password does not expire.
		 */
		QDateTime readPasswordInfoExpiration(const QByteArray &xmlData,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Read owner info from DOM.
		 *
		 * @param[in]  dom DOM document.
		 * @param[out] ok Set to false on error.
		 * @return Owner info, null value on error.
		 */
		DbOwnerInfoExt2 readOwnerInfoExt2(const QDomDocument &dom,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Read owner info data and return owner info instance.
		 *
		 * @param[in]  xmlData XML data.
		 * @param[out] ok Set to false on failure.
		 * @return Owner info instance, null value on error.
		 */
		DbOwnerInfoExt2 toOwnerInfo(const QByteArray &xmlData,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Read user info from DOM.
		 *
		 * @param[in]  dom DOM document.
		 * @param[out] ok Set to false on error.
		 * @return User info, null value on error.
		 */
		DbUserInfoExt2 readUserInfoExt2(const QDomDocument &dom,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Read user info data and return user info instance.
		 *
		 * @param[in]  xmlData XML data.
		 * @param[out] ok Set to false on failure.
		 * @return User info instance, null value on error.
		 */
		DbUserInfoExt2 toUserInfoExt2(const QByteArray &xmlData,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Read data boxes and return list of data boxes.
		 *
		 * @param[in]  xmlData XML data.
		 * @param[out] totalCount Total number of data boxes.
		 * @param[out] currentCount Number of data boxes on the current page.
		 * @param[out] position Number of page.
		 * @param[out] lastPage True if search result are last page.
		 * @param[out] ok Set to false on failure.
		 * @return List of data boxes.
		 */
		QList<DbResult2> toDbResult2(const QByteArray &xmlData,
		    long int &totalCount, long int &currentCount,
		    long int &position, bool &lastPage, bool *ok = Q_NULLPTR);

		/*!
		 * @brief Read long term storage info from DOM.
		 *
		 * @param[in]  dom DOM document.
		 * @param[out] ok Set to false on error.
		 * @return Long term storage info, null value on error.
		 */
		DTInfoOutput readDTInfo(const QDomDocument &dom,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Read long term storage info data and return
		 *        long term storage info instance.
		 *
		 * @param[in]  xmlData XML data.
		 * @param[out] ok Set to false on failure.
		 * @return Long term storage instance, null value on error.
		 */
		DTInfoOutput toDTInfo(const QByteArray &xmlData,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Constructs <GetPasswordInfo> request XML.
		 *
		 * @return Request XML data.
		 */
		QByteArray soapRequestGetPasswordInfo(void);

		/*!
		 * @brief Constructs <GetOwnerInfoFromLogin2> request XML.
		 *
		 * @return Request XML data.
		 */
		QByteArray soapRequestGetOwnerInfoFromLogin2(void);

		/*!
		 * @brief Constructs <GetUserInfoFromLogin2> request XML.
		 *
		 * @return Request XML data.
		 */
		QByteArray soapRequestGetUserInfoFromLogin2(void);

		/*!
		 * @brief Constructs <FindDataBox2> request XML.
		 *
		 * @note Currently only the search according to the box
		 *     identifier is implemented. Other criteria values are
		 *     more or less ignored. As a result only a list on 0 or 1
		 *     results can be returned.
		 * @todo Implement full functionality.
		 *
		 * @param[in] dbOwnerInfo Owner info.
		 * @return Request XML data.
		 */
		QByteArray soapRequestFindDataBox2(
		    const DbOwnerInfoExt2 &dbOwnerInfo);

		/*!
		 * @brief Constructs <ISDSSearch3> request XML.
		 *
		 * @param[in] query Sought phrase.
		 * @param[in] searchType Search type.
		 * @param[in] searchScope Search Databox Type string.
		 * @param[in] page Page number.
		 * @param[in] pageSize Number of items into the page.
		 * @param[in] highlighting True if result will be highlighting.
		 * @return Request XML data.
		 */
		QByteArray soapRequestIsdsSearch3(const QString &query,
		    enum Type::FulltextSearchType searchType,
		    enum Type::DbType boxType, long int page, long int pageSize,
		    enum Type::NilBool highlighting);

		/*!
		 * @brief Constructs <DTInfo> request XML.
		 *
		 * @param[in] dbID Data box ID.
		 * @return Request XML data.
		 */
		QByteArray soapRequestDTInfo(const QString &dbID);
	}
}
