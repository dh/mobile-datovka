/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QByteArray>

#include "src/datovka_shared/isds/type_conversion.h"
#include "src/isds/xml/helper.h"

QDomDocument Isds::Xml::toDomDocument(const QByteArray &xmlData, bool *ok)
{
	QDomDocument dom;
	/* Namespace processing must be enabled. */
	bool iOk = dom.setContent(xmlData, true);
	if (ok != Q_NULLPTR) {
		*ok = iOk;
	}
	return dom;
}

QDomElement Isds::Xml::findSingleDomElement(const QDomDocument &dom,
    const QString &tagName)
{
	if (Q_UNLIKELY(tagName.isEmpty())) {
		Q_ASSERT(0);
		return QDomElement();
	}

	QDomNodeList nodeList = dom.elementsByTagName(tagName);
	if (Q_UNLIKELY(nodeList.size() != 1)) {
		return QDomElement();
	}
	return nodeList.at(0).toElement();
}

/*!
 * @brief Check whether element node contains the "nil" attribute
 *     (e.g. <p:dmToHands xsi:nil="true"></p:dmToHands>).
 *
 * @param[in] elem DOM element node.
 * @return True if the attribute is set to true.
 */
static
bool attributeNilTrue(const QDomElement &elem)
{
	if (Q_UNLIKELY(elem.isNull())) {
		return false;
	}

	bool iOk = false;
	const QString value = Isds::Xml::readAttributeValue(elem,
	    QLatin1String("nil"), false, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return false;
	}
	/* The attribute may not be present. */
	if (value.isNull()) {
		return false;
	}
	return Isds::variant2NilBool(value) == Isds::Type::BOOL_TRUE;
}

QString Isds::Xml::readSingleChildStringValue(const QDomElement &elem, bool *ok)
{
	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QString();
	}

	bool allowNil = attributeNilTrue(elem);

	const QDomNode childNode = elem.firstChild();
	if (Q_UNLIKELY(childNode.isNull())) {
		if (ok != Q_NULLPTR) {
			*ok = allowNil;
		}
		return QString();
	}
	if (Q_UNLIKELY(childNode.nodeType() != QDomNode::TextNode)) {
		Q_ASSERT(0);
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QString();
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return childNode.nodeValue();
}

QString Isds::Xml::readAttributeValue(const QDomElement &elem,
    const QString &attribute, bool errorMissing, bool *ok)
{
	if (Q_UNLIKELY(elem.isNull() || attribute.isEmpty())) {
		Q_ASSERT(0);
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QString();
	}

	QString value;
	const QDomNamedNodeMap attributeMap = elem.attributes();
	if (attributeMap.contains(attribute)) {
		QDomNode node = attributeMap.namedItem(attribute);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::AttributeNode)) {
			Q_ASSERT(0);
			goto fail;
		}
		node = node.firstChild();
		if (Q_UNLIKELY(node.nodeType() != QDomNode::TextNode)) {
			Q_ASSERT(0);
			goto fail;;
		}

		value = node.nodeValue();
	} else if (errorMissing) {
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return value;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return QString();
}
