/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QFileInfo>
#include <QSslCertificate>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/type_conversion.h"
#include "src/datovka_shared/log/log.h"
#include "src/dialogues/dialogues.h"
#include "src/global.h"
#include "src/io/filesystem.h"
#include "src/isds/conversion/isds_type_conversion.h"
#include "src/isds/isds_login.h"
#include "src/isds/isds_tasks.h"
#include "src/isds/isds_wrapper.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/qml_identifiers/qml_account_id.h"
#include "src/settings/accounts.h"
#include "src/worker/emitter.h"

IsdsWrapper::IsdsWrapper(QObject *parent)
    : QObject(parent),
    m_mepRunning(false),
    m_transactIds()
{
	if (GlobInstcs::msgProcEmitterPtr == Q_NULLPTR) {
		logErrorNL("%s", "Cannot connect to status message emitter.");
		return;
	}
	/* Worker-related processing signals. */
	connect(GlobInstcs::msgProcEmitterPtr,
	    SIGNAL(downloadAccountInfoFinishedSignal(AcntId, bool, QString)),
	    this,
	    SLOT(downloadAccountInfoFinished(AcntId, bool, QString)));

	connect(GlobInstcs::msgProcEmitterPtr,
	    SIGNAL(downloadDeliveryInfoFinishedSignal(AcntId, qint64, bool, QString)),
	    this,
	    SLOT(downloadDeliveryInfoFinished(AcntId, qint64, bool, QString)));

	connect(GlobInstcs::msgProcEmitterPtr,
	    SIGNAL(downloadMessageFinishedSignal(AcntId, qint64, bool, QString)),
	    this,
	    SLOT(downloadMessageFinished(AcntId, qint64, bool, QString)));

	connect(GlobInstcs::msgProcEmitterPtr,
	    SIGNAL(downloadMessageListFinishedSignal(AcntId, bool, QString, QString, bool)),
	    this,
	    SLOT(downloadMessageListFinished(AcntId, bool, QString, QString, bool)));

	connect(GlobInstcs::msgProcEmitterPtr,
	    SIGNAL(importZFOFinishedSignal(int, int, QString)),
	    this, SLOT(importZFOFinished(int, int, QString)));

	connect(GlobInstcs::msgProcEmitterPtr,
	    SIGNAL(sendMessageFinishedSignal(AcntId, QString, QString, QString, qint64, bool, QString)),
	    this,
	    SLOT(sendMessageFinished(AcntId, QString, QString, QString, qint64, bool, QString)));
}

void IsdsWrapper::closeAllOtpConnections(void)
{
	if (Q_UNLIKELY(GlobInstcs::isdsSessionsPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	Isds::Login::closeAllOtpConnections(*GlobInstcs::isdsSessionsPtr);
}

bool IsdsWrapper::changePassword(const QmlAcntId *qAcntId, const QString &oldPwd,
    const QString &newPwd, const QString &otpCode)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	return Isds::Tasks::changePassword(*qAcntId, oldPwd, newPwd, otpCode);
}

void IsdsWrapper::doIsdsAction(const QString &isdsAction,
    const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}
	doIsdsAction(isdsAction, *qAcntId);
}

void IsdsWrapper::downloadMessage(const QmlAcntId *qAcntId,
    enum Messages::MessageType messageType, qint64 msgId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	emit statusBarTextChanged(tr("Downloading message %1")
	    .arg(QString::number(msgId)), true, true);

	Isds::Tasks::downloadMessage(*qAcntId, messageType, msgId);
}

QString IsdsWrapper::findDatabox(const QmlAcntId *qAcntId, const QString &dbID,
    const QString &dbType)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QString();
	}

	emit statusBarTextChanged(tr("Finding databoxes"), true, true);

	return Isds::Tasks::findDatabox(*qAcntId, dbID, Isds::str2DbType(dbType));
}

int IsdsWrapper::findDataboxFulltext(const QmlAcntId *qAcntId,
    DataboxListModel *databoxModel, const QString &phrase,
    const QString &searchType, const QString &searchScope, int page)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return 0;
	}
	emit statusBarTextChanged(tr("Finding databoxes"), true, true);

	long int totalCount = 0;
	long int currentCount = 0;
	long int position = 0;
	bool lastPage = false;

	long int cnt = Isds::Tasks::findDataboxFulltext(*qAcntId, databoxModel,
	    phrase, Isds::str2FulltextSearchType(searchType),
	    Isds::str2FulltextDbType(searchScope), page, totalCount,
	    currentCount, position, lastPage);

	emit statusBarTextChanged(QString(), false, false);

	if (cnt > 1) {
		emit sendSearchResultsSig(totalCount, currentCount,
		    position, lastPage);
	}
	return cnt;
}

QString IsdsWrapper::getAccountDbId(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QString();
	}

	return Isds::Tasks::getAccountDbId(*qAcntId);
}

void IsdsWrapper::getAccountInfo(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	getAccountInfo(*qAcntId);
}

enum AcntData::LoginMethod IsdsWrapper::getAccountLoginMethod(
    const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return AcntData::LIM_UNKNOWN;
	}

	AcntData &acntData((*GlobInstcs::acntMapPtr)[*qAcntId]);
	return acntData.loginMethod();
}

QString IsdsWrapper::importZfoMessages(const QmlAcntId *qAcntId,
    const QStringList &fileList, bool authenticate)
{
	if (fileList.isEmpty()) {
		return tr("No zfo files or selected folder.");
	}

	/* Get user names of all accounts if an user name is not specified */
	QList<AcntId> acntIdList;
	if ((Q_NULLPTR == qAcntId) || (!qAcntId->isValid())) {
		acntIdList = GlobInstcs::acntMapPtr->keys();
	} else {
		acntIdList.append(*qAcntId);
	}

	if (acntIdList.isEmpty()) {
		return tr("No account for zfo import.");
	}

	/* Test, if folder or file(s) was selected */
	QStringList filePathList = fileList;
	if (1 == fileList.count()) {
		if (!QFileInfo(fileList.at(0)).isFile()) {
			filePathList = zfoFilesFromDir(fileList.at(0));
			if (filePathList.isEmpty()) {
				return tr("No zfo files in the selected directory.");
			}
		}
	}

	if (filePathList.count() < 1) {
		return tr("No zfo file for import.");
	}

	/* User must be logged to ISDS if messages must be verified */
	if (authenticate) {
		foreach (const AcntId &acntId, acntIdList) {
			/* Check if an account is logged */
			if (!Isds::Login::isLoggedIn(
			        *GlobInstcs::isdsSessionsPtr, acntId)) {
				acntIdList.removeOne(acntId);
			}
		}
		if (acntIdList.isEmpty()) {
			return tr("ZFO messages cannot be imported because there is no active account for their verification.");
		}
	}

	emit statusBarTextChanged(tr("ZFO import"), true, true);

	return Isds::Tasks::importZfoMessages(acntIdList, filePathList,
	    authenticate);
}

void IsdsWrapper::inputDialogCancel(const QString &isdsAction,
    const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	if (isdsAction == "addNewAccount" || isdsAction == "changeUserName") {
		emit unsuccessfulLoginToIsdsSig(qAcntId->username(),
		    qAcntId->testing());
	}
	emit statusBarTextChanged("", false, false);
}

void IsdsWrapper::inputDialogReturnPassword(const QString &isdsAction,
     enum AcntData::LoginMethod loginMethod, const QmlAcntId *qAcntId,
     const QString &pwd)
{
	debugSlotCall();

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (!GlobInstcs::isdsSessionsPtr->holdsSession(*qAcntId)) {
		return;
	}

	QString errTxt;
	switch (loginMethod) {
	case AcntData::LIM_UNAME_PWD:
		GlobInstcs::isdsSessionsPtr->session(*qAcntId)->
		    ctx()->setPassword(pwd);
		doLoginAction(isdsAction, *qAcntId);
		break;
	case AcntData::LIM_UNAME_MEP:
		GlobInstcs::isdsSessionsPtr->session(*qAcntId)->
		    ctx()->setPassword(pwd);
		doLoginAction(isdsAction, *qAcntId);
		break;
	case AcntData::LIM_UNAME_PWD_HOTP:
		if (!Isds::Login::loginHotp(*GlobInstcs::isdsSessionsPtr,
		        *qAcntId, pwd, errTxt)) {
			doLoginFailAction(isdsAction, *qAcntId, errTxt);
			return;
		}
		emit statusBarTextChanged(QString(), false, false);
		getAccountInfo(*qAcntId);
		doIsdsAction(isdsAction, *qAcntId);
		break;
	case AcntData::LIM_UNAME_PWD_TOTP:
		if (!Isds::Login::loginTotp(*GlobInstcs::isdsSessionsPtr,
		        *qAcntId, pwd, errTxt)) {
			doLoginFailAction(isdsAction, *qAcntId, errTxt);
			return;
		}
		emit statusBarTextChanged(QString(), false, false);
		getAccountInfo(*qAcntId);
		doIsdsAction(isdsAction, *qAcntId);
		break;
	case AcntData::LIM_UNAME_PWD_CRT:
		if (GlobInstcs::isdsSessionsPtr->session(*qAcntId)->setCertificate(
		        (*GlobInstcs::acntMapPtr)[*qAcntId].p12File(),
		        pwd, errTxt)) {
			doLoginAction(isdsAction, *qAcntId);
		} else {
			showLoginErrorDlg(isdsAction, *qAcntId, errTxt);
			GlobInstcs::isdsSessionsPtr->removeSession(*qAcntId);
		}
		break;
	default:
		Q_ASSERT(0);
		break;
	}
}

bool IsdsWrapper::isCorrectPassword(const QString &password)
{
	if (password.isEmpty()) {
		return false;
	}
	if (password.length() < 8 || password.length() > 32) {
		return false;
	}
	if (!password.contains(QRegExp(".*[a-z]+.*"))) {
		return false;
	}
	if (!password.contains(QRegExp(".*[A-Z]+.*"))) {
		return false;
	}
	if (!password.contains(QRegExp(".*[0-9]+.*"))) {
		return false;
	}

	return true;
}

bool IsdsWrapper::isLogged(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY((GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	return Isds::Login::isLoggedIn(*GlobInstcs::isdsSessionsPtr, *qAcntId);
}

void IsdsWrapper::loginMepCancel(void)
{
	m_mepRunning = false;
}

bool IsdsWrapper::loginMepRunning(void)
{
	return m_mepRunning;
}

bool IsdsWrapper::sendGovRequest(const AcntId &acntId,
    const Isds::Message &msg)
{
	m_sentMsgResultList.clear();
	return Isds::Tasks::sendGovRequest(acntId, msg, m_transactIds);
}

void IsdsWrapper::sendMessage(const QmlAcntId *qAcntId, qint64 dmID,
    const QString &dmAnnotation, const DataboxListModel *databoxModel,
    const FileListModel *attachModel,
    const QString &dmLegalTitleLaw, const QString &dmLegalTitleYear,
    const QString &dmLegalTitleSect, const QString &dmLegalTitlePar,
    const QString &dmLegalTitlePoint, const QString &dmToHands,
    const QString &dmRecipientRefNumber, const QString &dmRecipientIdent,
    const QString &dmSenderRefNumber, const QString &dmSenderIdent,
    bool dmOVM, bool dmPublishOwnID, bool dmAllowSubstDelivery,
    bool dmPersonalDelivery, const QString &dmType)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}
	m_sentMsgResultList.clear();

	emit statusBarTextChanged(tr("%1: sending message")
	    .arg(qAcntId->username()), true, true);

	Isds::Tasks::sendMessage(*qAcntId, dmID, dmAnnotation, databoxModel,
	attachModel, dmLegalTitleLaw, dmLegalTitleYear, dmLegalTitleSect,
	dmLegalTitlePar, dmLegalTitlePoint, dmToHands, dmRecipientRefNumber,
	dmRecipientIdent, dmSenderRefNumber, dmSenderIdent, dmOVM,
	dmPublishOwnID, dmAllowSubstDelivery, dmPersonalDelivery, dmType,
	m_transactIds);
}

bool IsdsWrapper::sendSMS(const QmlAcntId *qAcntId, const QString &oldPwd)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	/* Show send SMS dialogue. */
	int msgResponse = Dialogues::message(Dialogues::QUESTION,
	    tr("SMS code: %1").arg(qAcntId->username()),
	    tr("Account '%1' requires SMS code for password changing.").
	        arg(qAcntId->username()),
	    tr("Do you want to send SMS code now?"),
	    Dialogues::NO | Dialogues::YES, Dialogues::YES);
	if (msgResponse == Dialogues::NO) {
		return false;
	}

	return Isds::Tasks::sendSMS(*qAcntId, oldPwd);
}

void IsdsWrapper::syncAllAccounts(void)
{
	debugSlotCall();

	if (Q_UNLIKELY(GlobInstcs::acntMapPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	foreach (const AcntId &acntId, GlobInstcs::acntMapPtr->keys()) {

		AcntData &acntData((*GlobInstcs::acntMapPtr)[acntId]);
		if (!acntData.syncWithAll()) {
			continue;
		}

		doIsdsAction("syncOneAccount", acntId);
	}
}

void IsdsWrapper::syncOneAccount(const QmlAcntId *qAcntId)
{
	syncSingleAccountSent(qAcntId);
	syncSingleAccountReceived(qAcntId);
}

void IsdsWrapper::syncSingleAccountReceived(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	emit statusBarTextChanged(tr("%1: downloading...")
	    .arg(qAcntId->username()), true, true);

	Isds::Tasks::syncSingleAccount(*qAcntId, Messages::TYPE_RECEIVED);
}

void IsdsWrapper::syncSingleAccountSent(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	emit statusBarTextChanged(tr("%1: downloading...")
	    .arg(qAcntId->username()), true, true);

	Isds::Tasks::syncSingleAccount(*qAcntId, Messages::TYPE_SENT);
}

void IsdsWrapper::removeIsdsCtx(const AcntId &acntId)
{
	if (Q_UNLIKELY(GlobInstcs::isdsSessionsPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}
	GlobInstcs::isdsSessionsPtr->removeSession(acntId);
}

void IsdsWrapper::doLoginAction(const QString &isdsAction,
    const AcntId &acntId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	/* Check if account has all required data for login. */
	if (!hasAllLoginData(acntId, isdsAction)) {
		return;
	}

	const AcntData acntData((*GlobInstcs::acntMapPtr)[acntId]);

	/* Login to ISDS server. */
	emit statusBarTextChanged(tr("Logging in to data box %1").
	    arg(acntId.username()), true, true);

	/* HOTP login do separately. */
	if (acntData.loginMethod() == AcntData::LIM_UNAME_PWD_HOTP) {
		waitForHotpCode(acntId, isdsAction);
		return;
	}

	/* Send basic authorization (username, password:otp or MEP code). */
	QString errTxt;
	if (!Isds::Login::loginBasicAuth(*GlobInstcs::isdsSessionsPtr, acntId,
	        errTxt)) {
		doLoginFailAction(isdsAction, acntId, errTxt);
		return;
	}

	if (acntData.loginMethod() == AcntData::LIM_UNAME_PWD_TOTP) {
		/* Basic login and SMS request succeeded. Wait for SMS login. */
		waitForSmsCode(acntId, isdsAction);
		return;
	} else if (acntData.loginMethod() == AcntData::LIM_UNAME_MEP) {
		/* Basic login succeeded. Run MEP login. */
		emit statusBarTextMepChanged(tr("Waiting for confirmation ..."));
		emit showMepWaitDialog(acntId.username(), true);
		if (!Isds::Login::loginMep(*GlobInstcs::isdsSessionsPtr, acntId,
		        m_mepRunning, errTxt)) {
			doLoginFailAction(isdsAction, acntId, errTxt);
			emit showMepWaitDialog(acntId.username(), false);
			return;
		}
		emit showMepWaitDialog(acntId.username(), false);
	}

	/* Login succeeded, update account info, password expiration. */
	emit statusBarTextChanged("", false, false);
	getAccountInfo(acntId);
	doIsdsAction(isdsAction, acntId);
}

void IsdsWrapper::doLoginFailAction(const QString &isdsAction,
     const AcntId &acntId, QString errTxt)
{
	if (errTxt.isEmpty()) {
		errTxt	= tr("Internal error.");
	}

	/* Show error dialogue. */
	emit statusBarTextChanged(tr("Login failed."), false, true);
	showLoginErrorDlg(isdsAction, acntId, errTxt);

	/* Remove the fail session from container */
	GlobInstcs::isdsSessionsPtr->removeSession(acntId);

	/* Post QML actions */
	if (isdsAction == "addNewAccount" || isdsAction == "changeUserName") {
		emit unsuccessfulLoginToIsdsSig(acntId.username(), acntId.testing());
	}
	if (isdsAction == "sendMessage") {
		emit sentMessageFinished(false);
	}
}

void IsdsWrapper::doIsdsAction(const QString &isdsAction, const AcntId &acntId)
{
	debugFuncCall();

	/* If user is logged to ISDS then run ISDS action else run login phase */
	if (Isds::Login::isLoggedIn(*GlobInstcs::isdsSessionsPtr, acntId)) {
		if (isdsAction == "downloadMessage") {
			emit runDownloadMessageSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "addNewAccount") {
			emit runGetAccountInfoSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "changePassword") {
			emit runChangePasswordSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "changeUserName") {
			emit runChangeUserNameSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "getAccountInfo") {
			getAccountInfo(acntId);
		} else if (isdsAction == "syncOneAccount") {
			emit runSyncOneAccountSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "syncSingleAccountReceived") {
			emit runSyncSingleAccountReceivedSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "syncSingleAccountSent") {
			emit runSyncSingleAccountSentSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "findDataboxFulltext") {
			emit runFindDataboxFulltextSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "findDatabox") {
			emit runFindDataboxSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "sendMessage") {
			emit runSendMessageSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "sendGovMessage") {
			emit runSendGovMessageSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "importZfoMessage") {
			emit runImportZfoMessageSig(acntId.username(),
			    acntId.testing());
		}
	} else {
		doLoginAction(isdsAction, acntId);
	}
}

void IsdsWrapper::getAccountInfo(const AcntId &acntId)
{
	emit statusBarTextChanged(tr("Getting account info %1")
	    .arg(acntId.username()), true, true);
	Isds::Tasks::getAccountInfo(acntId);
}

bool IsdsWrapper::hasAllLoginData(const AcntId &acntId,
    const QString &isdsAction)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
		(GlobInstcs::isdsSessionsPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	const AcntData &acntData((*GlobInstcs::acntMapPtr)[acntId]);

	/* Create session if not exists. */
	if (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)) {
		Isds::Session *session = Isds::Session::createSession(acntData);
		if (session == Q_NULLPTR) {
			logErrorNL("Cannot create session for username %s",
			    acntId.username().toUtf8().constData());
			return false;
		}

		/* Set login URL. */
		session->setLoginUrl();

		if (!GlobInstcs::isdsSessionsPtr->insertSession(acntId, session)) {
			logErrorNL("Cannot insert session (%s) into container.",
			    acntId.username().toUtf8().constData());
			delete session;
			return false;
		}
	}

	/* Has MEP account communication code in the ISDS context. */
	if (acntData.loginMethod() == AcntData::LIM_UNAME_MEP  &&
	        GlobInstcs::isdsSessionsPtr->session(acntId)->
	        ctx()->password().isEmpty()) {
		emit openDialogRequest(isdsAction, AcntData::LIM_UNAME_MEP,
		    acntData.userName(), acntData.isTestAccount(),
		    tr("Enter communication code: %1").arg(acntData.userName()),
		    tr("Communication code for account '%1' is required.").
		        arg(acntData.accountName()),
		    tr("Enter communication code"),
		    false);
		return false;
	}

	/* Test if other accounts have set password in the ISDS context. */
	if (GlobInstcs::isdsSessionsPtr->holdsSession(acntId) &&
	        GlobInstcs::isdsSessionsPtr->session(acntId)->
	        ctx()->password().isEmpty()) {
		emit openDialogRequest(isdsAction, AcntData::LIM_UNAME_PWD,
		    acntData.userName(), acntData.isTestAccount(),
		    tr("Enter password: %1").arg(acntData.userName()),
		    tr("Password for account '%1' missing.").
		        arg(acntData.accountName()),
		    tr("Enter password"),
		    true);
		return false;
	}

	/* Is TOTP login, show confirm dialogue for SMS code sending. */
	if (acntData.loginMethod() == AcntData::LIM_UNAME_PWD_TOTP) {
		int msgResponse = Dialogues::message(Dialogues::QUESTION,
		    tr("SMS code: %1").arg(acntData.userName()),
		    tr("Account '%1' requires authentication with SMS code.").
		        arg(acntData.accountName()),
		    tr("Do you want to send SMS code now?"),
		    Dialogues::NO | Dialogues::YES, Dialogues::YES);
		if (msgResponse == Dialogues::NO) {
			return false;
		}
	}

	/* Certificate login, set private key password. */
	if (acntData.loginMethod() == AcntData::LIM_UNAME_PWD_CRT  &&
	        GlobInstcs::isdsSessionsPtr->session(acntId)->
	        ctx()->sslCertificate().isNull()) {
		emit openDialogRequest(isdsAction, AcntData::LIM_UNAME_PWD_CRT,
		    acntId.username(), acntId.testing(),
		    tr("Enter certificate password: %1").arg(acntId.username()),
		    tr("Certificate password for account '%1' is required.").
		        arg(acntData.accountName()),
		    tr("Enter certificate password"),
		    true);
		return false;
	}

	return true;
}

void IsdsWrapper::showLoginErrorDlg(const QString &isdsAction,
    const AcntId &acntId, const QString &errTxt)
{
	if (isdsAction == "addNewAccount") {
		Dialogues::errorMessage(Dialogues::CRITICAL,
		    tr("New account problem: %1").arg(acntId.username()),
		    tr("New account could not be created for username '%1'.")
		        .arg(acntId.username()),
		    errTxt + "\n\n"
		    + tr("Note: You must change your password via the ISDS web portal if this is your first login to the data box. You can start using mobile Datovka to access the data box after changing."));
	} else if (isdsAction == "changeUserName") {
		Dialogues::errorMessage(Dialogues::CRITICAL,
		    tr("Username change: %1").arg(acntId.username()),
		    tr("Username could not be changed on the new username '%1'.")
		        .arg(acntId.username()),
		    errTxt);
	} else {
		Dialogues::errorMessage(Dialogues::CRITICAL,
		    tr("Login problem: %1").arg(acntId.username()),
		    tr("Error while logging in with username '%1'.")
		        .arg(acntId.username()),
		    errTxt);
	}
}

void IsdsWrapper::waitForHotpCode(const AcntId &acntId,
    const QString &isdsAction)
{
	debugFuncCall();

	emit openDialogRequest(isdsAction, AcntData::LIM_UNAME_PWD_HOTP,
	    acntId.username(), acntId.testing(),
	    tr("Enter security code: %1").arg(acntId.username()),
	    tr("Security code for '%1' required").arg(acntId.username()),
	    tr("Enter security code"),
	    false);
}

void IsdsWrapper::waitForSmsCode(const AcntId &acntId,
    const QString &isdsAction)
{
	debugFuncCall();

	emit openDialogRequest(isdsAction, AcntData::LIM_UNAME_PWD_TOTP,
	    acntId.username(), acntId.testing(),
	    tr("Enter SMS code: %1").arg(acntId.username()),
	    tr("SMS code for '%1' required").arg(acntId.username()),
	    tr("Enter SMS code"),
	    false);
}

void IsdsWrapper::downloadAccountInfoFinished(const AcntId &acntId,
    bool success, const QString &errTxt)
{
	if (!success) {
		emit statusBarTextChanged(QString(), false, false);
		Dialogues::errorMessage(Dialogues::WARNING,
		    tr("Account info: %1").arg(acntId.username()),
		    tr("Failed to get account info for username %1.")
		        .arg(acntId.username()),
		    errTxt);
	} else {
		emit downloadAccountInfoFinishedSig(acntId.username(),
		    acntId.testing());
		emit statusBarTextChanged(errTxt, false, true);
	}
}

void IsdsWrapper::downloadDeliveryInfoFinished(const AcntId &acntId,
    qint64 msgId, bool success, const QString &errTxt)
{
	if (!success) {
		emit statusBarTextChanged(QString(), false, false);
		Dialogues::errorMessage(Dialogues::WARNING,
		    tr("Delivery info: %1").arg(acntId.username()),
		    tr("Failed to download delivery info for message %1.")
		        .arg(msgId),
		    errTxt);
	} else {
		emit statusBarTextChanged(errTxt, false, true);
	}
}

void IsdsWrapper::downloadMessageFinished(const AcntId &acntId, qint64 msgId,
    bool success, const QString &errTxt)
{
	if (!success) {
		emit statusBarTextChanged(QString(), false, false);
		Dialogues::errorMessage(Dialogues::CRITICAL,
		    tr("Downloading message: %1").arg(acntId.username()),
		    tr("Failed to download complete message %1.").arg(msgId),
		    errTxt);
		return;
	} else {
		emit downloadMessageFinishedSig(msgId);
		emit statusBarTextChanged(tr("Message %1 has been downloaded")
		    .arg(msgId), false, true);
	}
}

void IsdsWrapper::downloadMessageListFinished(const AcntId &acntId,
    bool success, const QString &statusBarText, const QString &errTxt,
    bool isMsgReceived)
{
	emit downloadMessageListFinishedSig(isMsgReceived, acntId.username(),
	    acntId.testing());

	if (!success) {
		emit statusBarTextChanged(QString(), false, false);
		Dialogues::errorMessage(Dialogues::CRITICAL,
		    tr("%1: messages").arg(acntId.username()),
		    tr("Failed to download list of messages for user name '%1'.")
		        .arg(acntId.username()), errTxt);
	} else {
		emit statusBarTextChanged(statusBarText, false, true);
	}
}

void IsdsWrapper::importZFOFinished(int zfoNumber, int zfoTotal,
    QString infoText)
{
	if (zfoNumber < zfoTotal) {
		emit zfoImportProgressSig(QString::number(zfoNumber) + ". " + infoText);
		emit statusBarTextChanged(
		    tr("ZFO import: %1/%2").arg(zfoNumber).arg(zfoTotal), true,
		    true);
	} else {
		emit zfoImportProgressSig(QString::number(zfoNumber) + ". " + infoText);
		emit zfoImportFinishedSig(tr("ZFO import has been finished with the result:"));
		emit statusBarTextChanged(tr("ZFO import: done"), false, true);
	}
}

void IsdsWrapper::sendMessageFinished(const AcntId &acntId,
    const QString &transactId, const QString &dbIDRecipient,
    const QString &dmRecipient, qint64 msgId, bool success,
    const QString &errTxt)
{
	if (m_transactIds.end() == m_transactIds.find(transactId)) {
		/* Nothing found. */
		return;
	}

	/* Gather data. */
	m_sentMsgResultList.append(TaskSendMessage::ResultData(
	    success, errTxt, dbIDRecipient, dmRecipient, msgId));

	if (!m_transactIds.remove(transactId)) {
		logErrorNL("%s",
		    "Unable to remove a transaction identifier from list of unfinished transactions.");
	}

	if (!m_transactIds.isEmpty()) {
		/* Still has some pending transactions. */
		return;
	}

	/* All transactions finished. */

	int sentErrCnt = 0;
	QString dialogueInfoText;

	foreach (const TaskSendMessage::ResultData &rData, m_sentMsgResultList) {
		if (rData.result) {
			dialogueInfoText += tr(
			    "Message has been sent to <b>'%1'</b> <i>(%2)</i> as message number <b>%3</b>.").
			    arg(rData.dmRecipient).
			    arg(rData.dbIDRecipient).
			    arg(rData.dmId) + "<br/><br/>";
		} else {
			++sentErrCnt;
			dialogueInfoText += tr(
			    "Message has NOT been sent to <b>'%1'</b> <i>(%2)</i>. ISDS returns: %3").
			    arg(rData.dmRecipient).
			    arg(rData.dbIDRecipient).
			    arg(rData.errInfo) + "<br/><br/>";
		}
	}

	m_sentMsgResultList.clear();

	emit statusBarTextChanged(tr("Sending finished"), false, true);

	if (0 == sentErrCnt) {
		Dialogues::errorMessage(Dialogues::INFORMATION,
		    tr("%1: Message has been sent").arg(acntId.username()),
		    tr("Message has successfully been sent to all recipients."),
		    dialogueInfoText);
		emit sentMessageFinished(true);
	} else {
		Dialogues::errorMessage(Dialogues::CRITICAL,
		    tr("%1: Error sending message!").arg(acntId.username()),
		    tr("Message has NOT been sent to all recipients."),
		    dialogueInfoText);
		emit sentMessageFinished(false);
	}
}
