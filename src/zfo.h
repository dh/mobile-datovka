/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>

#include "src/qml_identifiers/qml_account_id.h"

/*
 * Class Zfo provides interface between QML and zfo database.
 * Class is initialised in the main function (main.cpp)
 */
class Zfo : public QObject {
    Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit Zfo(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Get Zfo content.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgId String with message id.
	 * @return Zfo content.
	 */
	Q_INVOKABLE static
	QByteArray getZfoContentFromDb(const QmlAcntId *qAcntId, qint64 msgId);

	/*!
	 * @brief Get Zfo size in bytes.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgId String with message id.
	 * @return Size of zfo file in bytes.
	 */
	Q_INVOKABLE static
	int getZfoSizeFromDb(const QmlAcntId *qAcntId, qint64 msgId);

	/*!
	 * @brief Release db - delete oldest zfo files and vacuum db.
	 *
	 * @param[in] releaseSpaceInMB How many mega bytes should be released.
	 */
	Q_INVOKABLE static
	void reduceZfoDbSize(unsigned int releaseSpaceInMB);
};
