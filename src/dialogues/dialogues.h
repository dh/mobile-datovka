/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>

/*!
 * @brief Encapsulates dialogues.
 */
class Dialogues : public QObject {
	Q_OBJECT

public:
	/*
	 * See TextInput documentation.
	 * QQuickTextField header file, where similar enum values are defined,
	 * cannot be accessed directly.
	 */
	enum EchoMode {
		EM_NORMAL,
		EM_PWD,
		EM_NOECHO,
		EM_PWD_ECHOONEDIT
	};
	Q_ENUM(EchoMode)

	/*!
	 * @brief Dialogue icon decoration.
	 */
	enum Icon {
		NO_ICON,
		QUESTION,
		INFORMATION,
		WARNING,
		CRITICAL
	};
	Q_ENUM(Icon)

	/*!
	 * @brief Dialogue button declaration.
	 */
	enum Button {
		OK = 0x00000400,
		CANCEL = 0x00400000,
		YES = 0x00004000,
		NO = 0x00010000,
		NO_BUTTON = 0x00000000
	};
	Q_ENUM(Button)
	Q_DECLARE_FLAGS(Buttons, Button)

	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	/*!
	 * @brief Generates a text edit dialogue. Paste button is added on
	 *     Android devices.
	 *
	 * @param[in] parent Parent window or widget.
	 * @param[in] title Window title.
	 * @param[in] message Text shown in dialogue window.
	 * @param[in] echoMode Text input filed echo mode.
	 * @param[in] text Text put into text field.
	 * @param[in] placeholedText Placeholder text displayed in text field.
	 * @param[out] ok If is not null, then will be set to true if ok was
	 *                pressed.
	 * @param[in] inputMethodHints Hints for input method.
	 * @return Text from text field of dialogue was accepted.
	 */
	static
	QString getText(QObject *parent, const QString &title,
	    const QString &message, enum EchoMode echoMode = EM_NORMAL,
	    const QString &text = QString(),
	    const QString &placeholderText = QString(), bool *ok = Q_NULLPTR,
	    Qt::InputMethodHints inputMethodHints = Qt::ImhNone);

	/*!
	 * @brief Show message and await response.
	 *
	 * @param[in] icon Icon identifying importance of the message.
	 * @param[in] title Title of the dialogue window.
	 * @param[in] text Main message of the dialogue.
	 * @param[in] infoText Additional informative text.
	 * @param[in] buttons Standard buttons to be displayed.
	 * @param[in] dfltButton Identifies default action button.
	 * @param[in] customButtons List of pairs of custom button strings and values.
	 * @param[out] selected Value of selected custom button.
	 * @return Dialogue code or standard button value, -1 if custom button
	 *     was selected and custom button was selected then \a selected is
	 *     set to value of custom button.
	 */
	static
	int message(enum Icon icon, const QString &title,
	    const QString &text, const QString &infoText,
	    Buttons buttons = NO_BUTTON, enum Button dfltButton = NO_BUTTON,
	    const QList< QPair<QString, int> > &customButtons = QList< QPair<QString, int> >(),
	    int *selected = Q_NULLPTR);

	/*!
	 * @brief Show error message that can only be acknowledged.
	 *
	 * @param[in] icon Icon identifying importance of the message.
	 * @param[in] title Title of the dialogue window.
	 * @param[in] text Main message of the dialogue.
	 * @param[in] infoText Additional informative text.
	 */
	static
	void errorMessage(enum Icon icon, const QString &title,
	    const QString &text, const QString &infoText);
};

Q_DECLARE_OPERATORS_FOR_FLAGS(Dialogues::Buttons)

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(Dialogues::EchoMode)
Q_DECLARE_METATYPE(Dialogues::Icon)
Q_DECLARE_METATYPE(Dialogues::Button)
