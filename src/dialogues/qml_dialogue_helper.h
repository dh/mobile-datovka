/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>
#include <QString>
#include <QWindow>

/*!
 * @brief Class defining enumeration types.
 *
 * @note The enumeration types cannot be defined in a class with a private
 * constructor because of qmlRegisterType(). Also, because the types are
 * registered before main window is loaded the constructor is called and that
 * causes trouble on Android and iOS devices.
 */
class QmlDlgHelper : public QObject {
	Q_OBJECT

public:
	/*!
	 * @brief Emits accepted signal.
	 *
	 * @param[in] input Value to emit together with the signal.
	 */
	Q_INVOKABLE
	void emitAccepted(const QString &input);

	/*!
	 * @brief Emits rejected signal.
	 */
	Q_INVOKABLE
	void emitRejected(void);

	/*!
	 * @brief Emits closed signal.
	 *
	 * @param[in] button Pressed button code.
	 * @param[in] customButton Custom button value.
	 */
	Q_INVOKABLE
	void emitClosed(int button, int customButton);

	/*!
	 * @brief Return pointer to top level window.
	 *
	 * @return non-null pointer to window if top level window could be
	 *     acquired.
	 */
	static
	QWindow *topLevelWindow(void);

	/*
	 * Cannot be a static object. Android code complains:
	 * Illegal attempt to connect to object that is in a different thread
	 * than the QML engine.
	 */
	static
	QmlDlgHelper *dlgEmitter; /*!< Object used to invoke QML dialogues. */

signals:
	/*!
	 * @brief This signal should be emitted when QML text input dialogue
	 *     should be displayed.
	 */
	void dlgGetText(const QString &title, const QString &message,
	    int echoMode, const QString &text, const QString &placeholderText,
	    int inputMethodHints, bool explicitPasteMenu);

	/*!
	 * @brief This signal should be emitted when QML text input dialogue
	 *     was accepted.
	 */
	void accepted(const QString &input);

	/*!
	 * @brief This signal should be emitted when QML text input dialogue
	 *     was rejected.
	 */
	void rejected(void);

	/*!
	 * @brief This signal should be emitted when QML message dialogue
	 *     should be displayed.
	 */
	void dlgMessage(int icon, const QString &title, const QString &message,
	    const QString &infoMessage, int buttons);

	/*!
	 * @brief This signal should be emitted when QML message dialogue
	 *    was closed.
	 */
	void closed(int button, int customButton);
};
