/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QInputDialog>
#include <QLineEdit>
#include <QString>
#include <QTimer>

/*!
 * @brief Text input dialogue with paste button.
 *
 * @note This class is a nasty hack to explicitly allow pasting text from
 *     clipboard on Android devices.
 */
class WidgetInputDialogue : public QInputDialog {
	Q_OBJECT

private:
	/*!
	 * @brief Private constructor.
	 *
	 * @param[in] parent Parent widget.
	 * @param[in] flags Flags s used in QInputDialog.
	 */
	explicit WidgetInputDialogue(QWidget *parent = Q_NULLPTR,
	    Qt::WindowFlags flags = Qt::WindowFlags());

public:
	/*!
	 * @brief Destructor.
	 */
	~WidgetInputDialogue(void);

	/*!
	 * @brief Generates a text edit dialogue. Paste button is added on
	 *     Android devices.
	 */
	static QString getText(QWidget *parent, const QString &title,
	    const QString &label, QLineEdit::EchoMode mode = QLineEdit::Normal,
	    const QString &text = QString(), bool *ok = Q_NULLPTR,
	    Qt::WindowFlags flags = Qt::WindowFlags(),
	    Qt::InputMethodHints inputMethodHints = Qt::ImhNone);

private slots:
	/*!
	 * @brief Activates pasting button according to clipboard content.
	 */
	void activatePasteButton(void);

private:
	/*!
	 * @brief This method adds the paste button.
	 *
	 * @note Must be called after the whole dialogue is constructed.
	 */
	void extendDialoguePaste(void);

	/*!
	 * @brief Find text field in the dialogue.
	 *
	 * @return Pointer to text field object if found.
	 */
	QObject *findEditArea(void) const;

	QTimer m_timer; /*!< Timer used to check clipboard and activate button. */
	QPushButton *m_pasteButton; /*!< Convenience pointer to paste button. */
	QObject *m_textField; /*!< Text field receiving the paste command. */
};
