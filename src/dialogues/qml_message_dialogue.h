/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QEventLoop>
#include <QObject>
#include <QString>
#include <QWindow>

#include "src/dialogues/dialogues.h"

/*!
 * @brief Encapsulates QML-based dialogues.
 */
class QmlMessageDialogue : public QObject {
	Q_OBJECT

private:
	/*!
	 * @brief Private constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit QmlMessageDialogue(QObject *parent = Q_NULLPTR,
	    QWindow *window = Q_NULLPTR);

public:
	/*!
	 * @brief Generates error message dialogue with.
	 *
	 * @param[in] parent Parent window.
	 * @param[in] icon Identifies importance level.
	 * @param[in] title Window title.
	 * @param[in] text Text shown in dialogue window.
	 * @param[in] infoText Informative text.
	 */
	static
	void errorMessage(QWindow *parent, enum Dialogues::Icon icon,
	    const QString &title, const QString &text, const QString &infoText);

private slots:
	/*!
	 * @brief This slot us called when dialogue window is closed.
	 *
	 * @param[in] button Pressed button code.
	 * @param[in] customButton Custom button value.
	 */
	void dlgClosed(int button, int customButton);

private:
	bool m_alreadyReturned; /*!< True if dialogue closed before loop start. */
	QEventLoop m_loop; /*!< Loop used for action blocking. */
};
