/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QPushButton>

#include "src/dialogues/widget_message_dialogue.h"

WidgetMessageDialogue::WidgetMessageDialogue(Icon icon, const QString &title,
    const QString &text, StandardButtons buttons,  QWidget *parent,
    Qt::WindowFlags f)
    : QMessageBox(icon, title, text, buttons,  parent, f)
{
}

int WidgetMessageDialogue::message(enum QMessageBox::Icon icon,
    const QString &title, const QString &text, const QString &infoText,
    QMessageBox::StandardButtons buttons,
    enum QMessageBox::StandardButton dfltButton,
    const QList< QPair<QString, int> > &customButtons, int *selected)
{
	WidgetMessageDialogue msgDlg(icon, title, text, buttons);

	if (!infoText.isEmpty()) {
		msgDlg.setInformativeText(infoText);
	}

	if (dfltButton != QMessageBox::NoButton) {
		msgDlg.setDefaultButton(dfltButton);
	}

	if (customButtons.isEmpty()) {
		return msgDlg.exec();
	}

	/* There some are custom buttons. */
	QList <QAbstractButton *> listOfButtons;
	for (int i = 0; i < customButtons.size(); ++i) {
		listOfButtons.append(msgDlg.addButton(customButtons.at(i).first,
		    QMessageBox::ActionRole));
	}
	int execRet = msgDlg.exec();
	QAbstractButton *clickedButton = msgDlg.clickedButton();
	for (int i = 0; i < customButtons.size(); ++i) {
		if (clickedButton == listOfButtons.at(i)) {
			/* Found selected button. */
			if (selected != Q_NULLPTR) {
				/* Non-standard button value. */
				*selected = customButtons.at(i).second;
			}
			return -1; /* Clicked non-standard button. */
		}
	}

	return execRet; /* Potentially clicked a standard button. */
}

void WidgetMessageDialogue::errorMessage(enum QMessageBox::Icon icon,
    const QString &title, const QString &text, const QString &infoText)
{
	WidgetMessageDialogue msgDlg(icon, title, text, QMessageBox::Ok);

	if (!infoText.isEmpty()) {
		msgDlg.setInformativeText(infoText);
	}

	msgDlg.setDefaultButton(QMessageBox::Ok);

	msgDlg.exec();
}
