/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QEventLoop>
#include <QObject>
#include <QString>
#include <QWindow>

#include "src/dialogues/dialogues.h"

class QQmlApplicationEngine; /* Forward declaration. */

/*!
 * @brief Encapsulates QML-based dialogues.
 */
class QmlInputDialogue : public QObject {
	Q_OBJECT

private:
	/*!
	 * @brief Private constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit QmlInputDialogue(QObject *parent = Q_NULLPTR,
	    QWindow *window = Q_NULLPTR);

public:
	/*!
	 * @brief Generates a text edit dialogue. Paste button is added on
	 *     Android devices.
	 *
	 * @param[in] parent Parent window.
	 * @param[in] title Window title.
	 * @param[in] message Text shown in dialogue window.
	 * @param[in] echoMode Text input filed echo mode.
	 * @param[in] text Text put into text field.
	 * @param[in] placeholedText Placeholder text displayed in text field.
	 * @param[out] ok If is not null, then will be set to true if ok was
	 *                pressed.
	 * @param[in] inputMethodHints Hints for input method.
	 * @return Text from text field of dialogue was accepted.
	 */
	static
	QString getText(QWindow *parent, const QString &title,
	    const QString &message,
	    enum Dialogues::EchoMode echoMode = Dialogues::EM_NORMAL,
	    const QString &text = QString(),
	    const QString &placeholderText = QString(), bool *ok = Q_NULLPTR,
	    Qt::InputMethodHints inputMethodHints = Qt::ImhNone);

	/*!
	 * @brief Set pointers to persistent (defined in QML) dialogues.
	 *
	 * @param[in] engine QML engine to search for dialogues.
	 */
	static
	void searchPersistentDialogues(QQmlApplicationEngine *engine);

private slots:
	/*!
	 * @brief This slot us called when dialogue window is accepted.
	 *
	 * @param[in] input Read text.
	 */
	void readGetText(const QString &input);

private:
	QEventLoop m_loop; /*!< Loop used for action blocking. */

	QObject *m_dlgTextInputElement; /*!< Text input object. */

	bool m_ok; /*!< Set from within the slot. */
	QString m_readText; /*!< Set from within the slot. */

	static
	QObject *s_dlgTextInput; /*!< Persistent text input dialogue. */
};
