/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QQmlApplicationEngine>
//#include <QQuickItem>

#include "src/dialogues/qml_dialogue_helper.h"
#include "src/dialogues/qml_input_dialogue.h"

QObject *QmlInputDialogue::s_dlgTextInput = Q_NULLPTR;

QmlInputDialogue::QmlInputDialogue(QObject *parent, QWindow *window)
    : QObject(parent),
    m_loop(this),
    m_dlgTextInputElement(Q_NULLPTR),
    m_ok(false),
    m_readText()
{
	Q_UNUSED(window)
}

#if 0
	/* Paint dialogue using QQuickView -- buggy on mobile devices. */
	QQuickView view;
	view.setSource(QUrl("qrc:/qml/dialogues/PasteInputDialogue.qml"));
	view.setModality(Qt::ApplicationModal);
	//view.show();

	QObject *root = view.rootObject(); /* Is QQuickItem.  */
	QOBject *dlg = root->findChild<QObject *>("dialogue", Qt::FindChildrenRecursively); /* Is not QQuickItem. */
#endif /* USE_QUICK_VUEW */

#if 0
	/* Paint dialogue using QQmlEngine -- buggy on mobile devices. */

	QQmlEngine engine;
	QQmlComponent component(&qmlEngine, QUrl("qrc:/qml/dialogues/PasteInputDialogue.qml"));

	QObject *root = component.create();
	QOBject *dlg = root->findChild<QObject *>("dialogue", Qt::FindChildrenRecursively);
#endif /* USE_QML_ENGINE */

#if 0
	/* Obtain objects whose values are going to be set. */
	QObject *dlgMessage = dlg->findChild<QObject *>("messageText",
	    Qt::FindChildrenRecursively);
	dialogue.m_dlgTextInputElement = dlg->findChild<QObject *>("textInput",
	    Qt::FindChildrenRecursively);
	if ((dlg == Q_NULLPTR) || (dlgMessage == Q_NULLPTR) ||
	    (dialogue.m_dlgTextInputElement == Q_NULLPTR)) {
		Q_ASSERT(0);
		if (ok != Q_NULLPTR) {
			*ok = dialogue.m_ok;
		}
		return QString();
	}

	/* Set dialogue content. */

	/* Must be set before setting echo mode. */
	dialogue.m_dlgTextInputElement->setProperty("inputMethodHints",
	    (int)inputMethodHints);
#if defined Q_OS_ANDROID
	/* Display explicit context paste menu on Android devices. */
	dlg->setProperty("explicitPasteMenu", true);
#endif /* Q_OS_ANDROID */
	dlg->setProperty("dlgEchoMode", echoMode);

	dlg->setProperty("title", title);
	dlgMessage->setProperty("text", message);
	dialogue.m_dlgTextInputElement->setProperty("text", text);
	dialogue.m_dlgTextInputElement->setProperty("placeholderText",
	    placeholderText);

	/*
	 * Show dialogue.
	 * NOTE: Use signals/slots for invoking dialogues?
	 */
	QMetaObject::invokeMethod(dlg, "open");
	//dlg->setProperty("visible", true);

	connect(dlg, SIGNAL(accepted()), &dialogue, SLOT(readGetText()));
	connect(dlg, SIGNAL(rejected()), &dialogue.m_loop, SLOT(quit()));
	dialogue.m_loop.exec();
#endif

QString QmlInputDialogue::getText(QWindow *parent, const QString &title,
    const QString &message, enum Dialogues::EchoMode echoMode,
    const QString &text, const QString &placeholderText, bool *ok,
    Qt::InputMethodHints inputMethodHints)
{
	QmlInputDialogue dialogue(parent, QmlDlgHelper::topLevelWindow());

	bool explicitPasteMenu = false;

#if defined Q_OS_ANDROID
	explicitPasteMenu = true;
#endif /* Q_OS_ANDROID */

	if (QmlDlgHelper::dlgEmitter == Q_NULLPTR) {
		Q_ASSERT(0);
		return QString();
	}

	/* Connects signals and slots that interrupt the event loop. */
	connect(QmlDlgHelper::dlgEmitter, SIGNAL(accepted(QString)),
	    &dialogue, SLOT(readGetText(QString)));
	connect(QmlDlgHelper::dlgEmitter, SIGNAL(rejected()),
	    &dialogue.m_loop, SLOT(quit()));

	/* Cause QML window to pop up. */
	emit QmlDlgHelper::dlgEmitter->dlgGetText(title, message, echoMode, text,
	    placeholderText, inputMethodHints, explicitPasteMenu);

	dialogue.m_loop.exec();

	if (ok != Q_NULLPTR) {
		*ok = dialogue.m_ok;
	}
	return dialogue.m_readText;
}

void QmlInputDialogue::searchPersistentDialogues(QQmlApplicationEngine *engine)
{
	if (engine == Q_NULLPTR) {
		Q_ASSERT(0);
		return;
	}

	QList<QObject *> objList = engine->rootObjects();
	if (objList.size() == 0) {
		return;
	}

	QObject *root = objList.first();
	s_dlgTextInput = root->findChild<QObject *>("dialogue",
	    Qt::FindChildrenRecursively); /* Is not s QQuickItem. */
	if (s_dlgTextInput == Q_NULLPTR) {
		return;
	}
}

void QmlInputDialogue::readGetText(const QString &input)
{
	m_ok = true; /* This slot is fired on accept. */

	m_readText = input;

	m_loop.quit();
}
