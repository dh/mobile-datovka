/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QThread>

#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/isds/services/login_interface.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/settings/accounts.h"
#include "src/worker/task_change_password.h"

TaskChangePassword::TaskChangePassword(const AcntId &acntId,
    enum Isds::Type::OtpMethod otpMethod, const QString &oldPwd,
    const QString &newPwd, const QString &otpCode)
    : m_result(DL_ERR),
    m_isdsText(),
    m_acntId(acntId),
    m_otpMethod(otpMethod),
    m_oldPwd(oldPwd),
    m_newPwd(newPwd),
    m_otpCode(otpCode)
{
}

void TaskChangePassword::run(void)
{
	/* ### Worker task begin. ### */

	logDebugLv0NL("%s", "--------------CHANGE PASSWORD TASK-------------");
	logDebugLv0NL("Starting in thread '%p'", QThread::currentThreadId());

	if (m_otpMethod == Isds::Type::OM_UNKNOWN) {
		m_result = changePassword(m_acntId, m_oldPwd, m_newPwd,
		    m_isdsText);
	} else {
		m_result = changePasswordOtp(m_acntId, m_oldPwd, m_newPwd,
		m_otpCode, m_otpMethod, m_isdsText);
	}

	logDebugLv0NL("Finished in thread '%p'", QThread::currentThreadId());
	logDebugLv0NL("%s", "-----------------------------------------------");

	/* ### Worker task end. ### */
}

/*!
 * @brief Translate communication error onto task error.
 */
static
enum TaskChangePassword::Result error2result(enum Isds::Type::Error error)
{
	switch (error) {
	case Isds::Type::ERR_SUCCESS:
		return TaskChangePassword::DL_SUCCESS;
		break;
	case Isds::Type::ERR_ISDS:
		return TaskChangePassword::DL_ISDS_ERROR;
		break;
	case Isds::Type::ERR_SOAP:
	case Isds::Type::ERR_XML:
		return TaskChangePassword::DL_XML_ERROR;
		break;
	default:
		return TaskChangePassword::DL_ERR;
		break;
	}
}

enum TaskChangePassword::Result TaskChangePassword::changePassword(
    const AcntId &acntId, const QString &oldPwd, const QString &newPwd,
    QString &lastError)
{
	if (Q_UNLIKELY((!acntId.isValid()) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)))) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	logDebugLv1NL("Sending from account '%s'",
	    acntId.username().toUtf8().constData());

	/* Send request */
	QByteArray xmlDataOut;
	session->ctx()->setPassword(oldPwd);
	enum Isds::Type::Error error = Isds::changeISDSPassword(*session,
	     oldPwd, newPwd, Q_NULLPTR, &lastError);
	if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
		session->ctx()->setPassword(oldPwd);
		return error2result(error);
	}

	session->ctx()->setPassword(newPwd);
	logDebugLv1NL("%s", "Password has been changed");
	return DL_SUCCESS;
}

enum TaskChangePassword::Result TaskChangePassword::changePasswordOtp(
    const AcntId &acntId, const QString &oldPwd, const QString &newPwd,
    const QString &otpCode, enum Isds::Type::OtpMethod otpMethod,
    QString &lastError)
{
	if (Q_UNLIKELY((!acntId.isValid()) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)))) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	AcntData acntData((*GlobInstcs::acntMapPtr)[acntId]);
	Isds::Session *tmpSession = Isds::Session::createSession(acntData);
	if (tmpSession == Q_NULLPTR) {
		logErrorNL("Cannot create session for username %s",
		    acntId.username().toUtf8().constData());
		return DL_ERR;
	}

	logDebugLv1NL("Sending from account '%s'",
	    acntId.username().toUtf8().constData());

	/* Send SOAP request with tmp session */
	tmpSession->ctx()->setCookies(session->ctx()->cookies());
	tmpSession->ctx()->setPassword(oldPwd + otpCode);
	enum Isds::Type::Error error = Isds::changePasswordOTP(*tmpSession,
	     otpMethod, oldPwd, newPwd, Q_NULLPTR, &lastError);
	delete tmpSession;
	if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
		session->ctx()->setPassword(oldPwd);
		return error2result(error);
	}

	GlobInstcs::isdsSessionsPtr->removeSession(acntId);
	logDebugLv1NL("%s", "Password has been changed");
	return DL_SUCCESS;
}
