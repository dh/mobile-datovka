/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/types.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing data-box info search.
 */
class TaskFindDatabox : public Task {
	Q_DECLARE_TR_FUNCTIONS(TaskFindDataboxFulltext)

public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		DL_SUCCESS, /*!< Operation was successful. */
		DL_ISDS_ERROR, /*!< Error communicating with ISDS. */
		DL_XML_ERROR, /*!< Error parsing XML. */
		DL_DB_INS_ERR, /*!< Error inserting into database. */
		DL_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] dbID Sought data-box ID.
	 * @param[in] dbType Sought data-box type.
	 */
	explicit TaskFindDatabox(const AcntId &acntId,
	    const QString &dbID, enum Isds::Type::DbType dbType);

	/*!
	 * @brief Performs actual data-box info search.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	/*!
	 * @brief Find data box.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] dbID Sought data-box ID.
	 * @param[in] dbType Sought data-box type.
	 * @param[out] dbInfo Data box detail info string.
	 * @param[out] lastError Last ISDS error message.
	 * @return Error state.
	 */
	static
	enum Result findDatabox(const AcntId &acntId,
	    const QString &dbID, enum Isds::Type::DbType dbType,
	    QString &dbInfo, QString &lastError);

	enum Result m_result; /*!< Return state. */
	QString m_dbInfo; /*!< Data box info string. */
	QString m_lastError; /*!< Last ISDS error message. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskFindDatabox(const TaskFindDatabox &);
	TaskFindDatabox &operator=(const TaskFindDatabox &);

	const AcntId m_acntId; /*!< Account id. */
	const QString m_dbID; /*!< Sought data-box ID. */
	enum Isds::Type::DbType m_dbType; /*!< Sought data-box type. */
};
