/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/box_interface2.h"
#include "src/datovka_shared/isds/types.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing full-text data-box search.
 */
class TaskFindDataboxFulltext : public Task {
	Q_DECLARE_TR_FUNCTIONS(TaskFindDataboxFulltext)

public:
	/*!
	 * @brief Return code describing what happened.
	 */
	enum Result {
		DL_SUCCESS, /*!< Operation was successful. */
		DL_ISDS_ERROR, /*!< Error communicating with ISDS. */
		DL_XML_ERROR, /*!< Error parsing XML. */
		DL_DB_INS_ERR, /*!< Error inserting into database. */
		DL_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] searchText Search text.
	 * @param[in] searchType Search type option.
	 * @param[in] searchScope Search scope option.
	 * @param[in] page Page to be obtain.
	 * @param[in] pageSize Number of items on the page.
	 * @param[in] highlighting Highlight search phrase in results.
	 */
	explicit TaskFindDataboxFulltext(const AcntId &acntId,
	    const QString &searchText,
	    enum Isds::Type::FulltextSearchType searchType,
	    enum Isds::Type::DbType searchScope, long int page,
	    long int pageSize, bool highlighting);

	/*!
	 * @brief Performs actual data-box search.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	/*!
	 * @brief Find data box.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] searchText Search text.
	 * @param[in] searchType Search type option.
	 * @param[in] searchScope Search scope option.
	 * @param[in] page Page to be obtain.
	 * @param[in] pageSize Number of items on the page.
	 * @param[in] highlighting Highlight search phrase in results.
	 * @param[out] dbList List of data boxes.
	 * @param[out] totalCount Total number of data boxes.
	 * @param[out] currentCount Number of data boxes on the current page.
	 * @param[out] position Number of page.
	 * @param[out] lastPage True if search result are last page.
	 * @param[out] lastError Last ISDS error message.
	 * @return Error state.
	 */
	static
	enum Result findDatabox(const AcntId &acntId, const QString &searchText,
	    enum Isds::Type::FulltextSearchType searchType,
	    enum Isds::Type::DbType searchScope, long int page,
	    long int pageSize, bool highlighting,
	    QList<Isds::DbResult2> &dbList, long int &totalCount,
	    long int &currentCount, long int &position, bool &lastPage,
	    QString &lastError);

	enum Result m_result; /*!< Return state. */
	QList<Isds::DbResult2> m_dbList; /*!< Returned list of data boxes. */
	long int m_totalCount; /*!< Total number of data boxes. */
	long int m_currentCount; /*!< Number of data boxes on the current page. */
	long int m_position; /*!< Number of page. */
	bool m_lastPage; /*!< True if search result are last page. */
	QString m_lastError; /*!< Last ISDS error message. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskFindDataboxFulltext(const TaskFindDataboxFulltext &);
	TaskFindDataboxFulltext &operator=(const TaskFindDataboxFulltext &);

	const AcntId m_acntId; /*!< Account id. */
	const QString m_searchText; /*!< Search phrase. */
	enum Isds::Type::FulltextSearchType m_searchType; /*!< Search type. */
	enum Isds::Type::DbType m_searchScope; /*!< Search scope. */
	long int m_page; /*!< Page to be obtain. */
	long int m_pageSize; /*!< Number of items on the page. */
	bool m_highlighting; /*!< Highlight search phrase in results. */
};
