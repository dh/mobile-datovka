/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing download account info.
 */
class TaskDownloadAccountInfo : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		DL_SUCCESS, /*!< Operation was successful. */
		DL_ISDS_ERROR, /*!< Error communicating with ISDS. */
		DL_XML_ERROR, /*!< Error parsing XML. */
		DL_DB_INS_ERR, /*!< Error inserting into database. */
		DL_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] storeToDb True if account info will store into database.
	 */
	explicit TaskDownloadAccountInfo(const AcntId &acntId, bool storeToDb);

	/*!
	 * @brief Performs actual account info download.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	/*!
	 * @brief Download account info.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] storeToDb True if account info will store into database.
	 * @param[out] dbID Holds and returns data box ID.
	 * @return Error state.
	 */
	static
	enum Result downloadAccountInfo(const AcntId &acntId,
	    bool storeToDb, QString &dbID, QString &errText);

	enum Result m_result; /*!< Return state. */
	QString m_dbID; /*!< Return data box ID. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskDownloadAccountInfo(const TaskDownloadAccountInfo &);
	TaskDownloadAccountInfo &operator=(const TaskDownloadAccountInfo &);

	const AcntId m_acntId; /*!< Account id. */
	bool m_storeToDb; /*!< True if account info will store into database. */
	QString m_erroTxt;
};
