/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing download delivery info.
 */
class TaskDownloadDeliveryInfo : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		DL_SUCCESS, /*!< Operation was successful. */
		DL_ISDS_ERROR, /*!< Error communicating with ISDS. */
		DL_XML_ERROR, /*!< Error parsing XML. */
		DL_DB_INS_ERR, /*!< Error inserting into database. */
		DL_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] msgId Message identifier.
	 */
	explicit TaskDownloadDeliveryInfo(const AcntId &acntId,
	    qint64 msgId);

	/*!
	 * @brief Performs actual message delivery info download.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	/*!
	 * @brief Download message delivery info.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] msgId Message identifier.
	 * @return Error state.
	 */
	static
	enum Result downloadDeliveryInfo(const AcntId &acntId,
	    qint64 msgId);

	enum Result m_result; /*!< Return state. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskDownloadDeliveryInfo(const TaskDownloadDeliveryInfo &);
	TaskDownloadDeliveryInfo &operator=(const TaskDownloadDeliveryInfo &);

	const AcntId m_acntId; /*!< Account id. */
	qint64 m_msgId; /*!< Message ID. */
};
