/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QThread>

#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/isds/services/box_interface.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/worker/emitter.h"
#include "src/worker/task_find_databox_fulltext.h"

TaskFindDataboxFulltext::TaskFindDataboxFulltext(const AcntId &acntId,
    const QString &searchText, enum Isds::Type::FulltextSearchType searchType,
    enum Isds::Type::DbType searchScope, long int page, long int pageSize,
    bool highlighting)
    : m_result(DL_ERR),
    m_dbList(),
    m_totalCount(0),
    m_currentCount(0),
    m_position(0),
    m_lastPage(false),
    m_lastError(),
    m_acntId(acntId),
    m_searchText(searchText),
    m_searchType(searchType),
    m_searchScope(searchScope),
    m_page(page),
    m_pageSize(pageSize),
    m_highlighting(highlighting)
{
}

void TaskFindDataboxFulltext::run(void)
{
	/* ### Worker task begin. ### */

	logDebugLv0NL("%s", "---------FULLTEXT FIND DATABOX TASK------------");
	logDebugLv0NL("Starting in thread '%p'", QThread::currentThreadId());

	m_result = findDatabox(m_acntId, m_searchText, m_searchType,
	    m_searchScope, m_page, m_pageSize, m_highlighting,
	    m_dbList, m_totalCount, m_currentCount, m_position, m_lastPage,
	    m_lastError);

	logDebugLv0NL("Finished in thread '%p'", QThread::currentThreadId());
	logDebugLv0NL("%s", "-----------------------------------------------");

	/* ### Worker task end. ### */
}

/*!
 * @brief Translate communication error onto task error.
 */
static
enum TaskFindDataboxFulltext::Result error2result(enum Isds::Type::Error error)
{
	switch (error) {
	case Isds::Type::ERR_SUCCESS:
		return TaskFindDataboxFulltext::DL_SUCCESS;
		break;
	case Isds::Type::ERR_ISDS:
		return TaskFindDataboxFulltext::DL_ISDS_ERROR;
		break;
	case Isds::Type::ERR_SOAP:
	case Isds::Type::ERR_XML:
		return TaskFindDataboxFulltext::DL_XML_ERROR;
		break;
	default:
		return TaskFindDataboxFulltext::DL_ERR;
		break;
	}
}

enum TaskFindDataboxFulltext::Result TaskFindDataboxFulltext::findDatabox(
    const AcntId &acntId, const QString &searchText,
    enum Isds::Type::FulltextSearchType searchType,
    enum Isds::Type::DbType searchScope, long int page, long int pageSize,
    bool highlighting, QList<Isds::DbResult2> &dbList, long int &totalCount,
    long int &currentCount, long int &position, bool &lastPage,
    QString &lastError)
{
	if (Q_UNLIKELY((!acntId.isValid()) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)))) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	logDebugLv1NL("Sending from account '%s'",
	    acntId.username().toUtf8().constData());

	/* Send SOAP request. */
	enum Isds::Type::Error error = Isds::isdsSearch3(*session,
	    searchText, searchType, searchScope, page, pageSize,
	    highlighting ? Isds::Type::BOOL_TRUE : Isds::Type::BOOL_FALSE,
	    dbList, totalCount, currentCount, position, lastPage,
	    Q_NULLPTR, &lastError);
	if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
		return error2result(error);
	}

	return DL_SUCCESS;
}
