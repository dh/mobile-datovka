/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QThread>

#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/isds/io/connection.h"
#include "src/isds/isds_const.h"
#include "src/isds/services/message_interface.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/net/db_wrapper.h"
#include "src/worker/emitter.h"
#include "src/worker/task_download_delivery_info.h"

TaskDownloadDeliveryInfo::TaskDownloadDeliveryInfo(const AcntId &acntId,
    qint64 msgId)
    : m_result(DL_ERR),
    m_acntId(acntId),
    m_msgId(msgId)
{
}

void TaskDownloadDeliveryInfo::run(void)
{
	/* ### Worker task begin. ### */

	logDebugLv0NL("%s", "--------------DELIVERY INFO TASK---------------");
	logDebugLv0NL("Starting in thread '%p'", QThread::currentThreadId());

	m_result = downloadDeliveryInfo(m_acntId, m_msgId);

	if (GlobInstcs::msgProcEmitterPtr != Q_NULLPTR) {
		emit GlobInstcs::msgProcEmitterPtr->downloadDeliveryInfoFinishedSignal(
		    m_acntId, m_msgId,
		    TaskDownloadDeliveryInfo::DL_SUCCESS == m_result,
		    QString());
	}

	logDebugLv0NL("Finished in thread '%p'", QThread::currentThreadId());
	logDebugLv0NL("%s", "-----------------------------------------------");

	/* ### Worker task end. ### */
}

/*!
 * @brief Translate communication error onto task error.
 */
static
enum TaskDownloadDeliveryInfo::Result error2result(enum Isds::Type::Error error)
{
	switch (error) {
	case Isds::Type::ERR_SUCCESS:
		return TaskDownloadDeliveryInfo::DL_SUCCESS;
		break;
	case Isds::Type::ERR_ISDS:
		return TaskDownloadDeliveryInfo::DL_ISDS_ERROR;
		break;
	case Isds::Type::ERR_SOAP:
	case Isds::Type::ERR_XML:
		return TaskDownloadDeliveryInfo::DL_XML_ERROR;
		break;
	default:
		return TaskDownloadDeliveryInfo::DL_ERR;
		break;
	}
}

enum TaskDownloadDeliveryInfo::Result TaskDownloadDeliveryInfo::downloadDeliveryInfo(
    const AcntId &acntId, qint64 msgId)
{
	if (Q_UNLIKELY((!acntId.isValid()) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)))) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	logDebugLv1NL("Sending from account '%s'",
	   acntId.username().toUtf8().constData());

	enum Isds::Type::Error error = Isds::Type::ERR_ERROR;
	Isds::Message message;
	error = Isds::getSignedDeliveryInfo(*session, msgId, message,
	    Q_NULLPTR, Q_NULLPTR);
	if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
		logErrorNL("ISDS: %s",
		    session->ctx()->lastIsdsMsg().toUtf8().constData());
		return error2result(error);
	}

	/* Store events from delivery info into database. */
	if (!message.envelope().dmEvents().isEmpty()) {
		QString errTxt;
		if (!DbWrapper::insertMesasgeDeliveryInfoToDb(acntId,
		        message.envelope().dmEvents(), msgId, errTxt)) {
			logWarningNL("DB: Insert error: %s",
			    errTxt.toUtf8().constData());
			return DL_DB_INS_ERR;
		}
	}

	logDebugLv1NL("%s", "Delivery info has been downloaded");

	return DL_SUCCESS;
}
