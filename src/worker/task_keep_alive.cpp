/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QThread>

#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/isds/services/message_interface.h"
#include "src/isds/session/isds_sessions.h"
#include "src/worker/task_keep_alive.h"

TaskKeepAlive::TaskKeepAlive(const AcntId &acntId)
    : m_isAlive(false),
    m_acntId(acntId)
{
}

void TaskKeepAlive::run(void)
{
	/* ### Worker task begin. ### */

	logDebugLv0NL("%s", "---------------KEEP ALIVE TASK----------------");
	logDebugLv0NL("Starting in thread '%p'", QThread::currentThreadId());


	if (Q_UNLIKELY((!m_acntId.isValid()) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (!GlobInstcs::isdsSessionsPtr->holdsSession(m_acntId)))) {
		Q_ASSERT(0);
		return;
	}

	Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(m_acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	m_isAlive = (Isds::Type::ERR_SUCCESS == Isds::dummyOperation(*session,
	     Q_NULLPTR, Q_NULLPTR));

	logDebugLv0NL("Finished in thread '%p'", QThread::currentThreadId());
	logDebugLv0NL("%s", "-----------------------------------------------");

	/* ### Worker task end. ### */
}
