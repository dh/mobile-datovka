/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/message_interface.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing send message.
 */
class TaskSendMessage : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		DL_SUCCESS, /*!< Operation was successful. */
		DL_ISDS_ERROR, /*!< Error communicating with ISDS. */
		DL_XML_ERROR, /*!< Error parsing XML. */
		DL_DB_INS_ERR, /*!< Error inserting into database. */
		DL_ERR /*!< Other error. */
	};

	/*!
	 * @brief Gives more detailed information about sending outcome.
	 */
	class ResultData {
	public:
		/*!
		 * @brief Constructors.
		 */
		ResultData(void);
		ResultData(bool res, const QString &eInfo,
		    const QString &recId, const QString &recName, qint64 mId);

		bool result; /*!< Return state. */
		QString errInfo; /*!< Error description. */
		QString dbIDRecipient; /*!< Recipient identifier. */
		QString dmRecipient; /*!< Recipient name. */
		qint64 dmId; /*!< Sent message identifier. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] message Message.
	 * @param[in] transactId Unique transaction identifier.
	 */
	explicit TaskSendMessage(const AcntId &acntId,
	    const Isds::Message &message, const QString &transactId);

	/*!
	 * @brief Performs actual send message.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	/*!
	 * @brief Send message.
	 *
	 * @param[in] acntId Account id.
	 * @param[in,out] msg Message envelope.
	 * @param[in] fileList List of files.
	 * @param[in] dmOVM True if send message as OVM.
	 * @param[in] dmPublishOwnID True if add sender name to message.
	 * @param[out] dmId Sent message ID.
	 * @param[out] lastError Last ISDS error message.
	 * @return Error state.
	 */
	static
	enum Result sendMessage(const AcntId &acntId,
	    Isds::Message &message, qint64 &dmId, QString &lastError);

	enum Result m_result; /*!< Return state. */
	qint64 m_dmId; /*!< Sent message ID. */
	QString m_lastError; /*!< Last ISDS error message. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskSendMessage(const TaskSendMessage &);
	TaskSendMessage &operator=(const TaskSendMessage &);

	const AcntId m_acntId; /*!< Account id. */
	Isds::Message m_message; /*!< Message subject string. */
	const QString m_transactId; /*!< Unique transaction identifier. */
};
