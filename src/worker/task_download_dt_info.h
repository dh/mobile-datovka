/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing long term storage info download.
 */
class TaskDTInfo : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		DL_SUCCESS, /*!< Operation was successful. */
		DL_ISDS_ERROR, /*!< Error communicating with ISDS. */
		DL_XML_ERROR, /*!< Error parsing XML. */
		DL_DB_INS_ERR, /*!< Error inserting into database. */
		DL_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] dbID Data box ID.
	 */
	explicit TaskDTInfo(const AcntId &acntId, const QString &dbID);

	/*!
	 * @brief Performs actual long term storage info download.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	enum Result m_result; /*!< Return state. */
	Isds::DTInfoOutput m_dtInfo; /*!< Long term storage info. */
	QString m_lastError; /*!< Last ISDS error message. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskDTInfo(const TaskDTInfo &);
	TaskDTInfo &operator=(const TaskDTInfo &);

	/*!
	 * @brief Download long term storage info.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] dbID Data box id.
	 * @param[out] dtInfo Long term storage info.
	 * @param[out] lastError Last ISDS error message.
	 * @return Error state.
	 */
	static
	enum Result downloadDTInfo(const AcntId &acntId, const QString &dbID,
	    Isds::DTInfoOutput &dtInfo, QString &lastError);

	const AcntId m_acntId; /*!< Account id. */
	const QString m_dbID; /*!< Data box id. */
};
