/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QByteArray>
#include <QFile>
#include <QThread>

#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/isds/services/message_interface.h"
#include "src/isds/services/message_interface_offline.h" /* Isds::toMessage() */
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/net/db_wrapper.h"
#include "src/sqlite/account_db.h"
#include "src/worker/emitter.h"
#include "src/worker/task_import_zfo.h"

TaskImportZfo::TaskImportZfo(
    const QList<AcntId> &acntIdList,
    const QString &fileName, bool authenticate, int zfoNumber, int zfoTotal)
    : m_result(IMP_ERR),
    m_errText(),
    m_acntIdList(acntIdList),
    m_fileName(fileName),
    m_auth(authenticate),
    m_zfoNumber(zfoNumber),
    m_zfoTotal(zfoTotal)
{
}

void TaskImportZfo::run(void)
{
	if (Q_UNLIKELY(m_fileName.isEmpty())) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(m_acntIdList.isEmpty())) {
		Q_ASSERT(0);
		return;
	}

	/* ### Worker task begin. ### */

	logDebugLv0NL("%s", "---------------IMPORT ZFO TASK----------------");
	logDebugLv0NL("Starting in thread '%p'", QThread::currentThreadId());

	m_result = importZfo(m_acntIdList, m_fileName, m_auth,
	    m_errText);

	if (GlobInstcs::msgProcEmitterPtr != Q_NULLPTR) {
		emit GlobInstcs::msgProcEmitterPtr->importZFOFinishedSignal(
		    m_zfoNumber, m_zfoTotal, m_errText);
	}

	logDebugLv0NL("Finished in thread '%p'", QThread::currentThreadId());
	logDebugLv0NL("%s", "-----------------------------------------------");

	/* ### Worker task end. ### */
}

enum TaskImportZfo::Result TaskImportZfo::importZfo(
    const QList<AcntId> &acntIdList, const QString &fileName, bool authenticate,
    QString &errText)
{
	if (Q_UNLIKELY(GlobInstcs::accountDbPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return IMP_ERR;
	}

	/* Open file and read its content */
	QFile file(fileName);
	if (!file.open(QIODevice::ReadOnly)) {
		errText = setErrorTextAndColor(QObject::tr("Cannot open ZFO file '%1'.").arg(fileName));
		return IMP_ZFO_ERR;
	}

	/* Parse zfo content and detect valid ZFO format */
	Isds::Message messsage = Isds::toMessage(file.readAll());
	file.close();
	if (messsage.isNull()) {
		errText = setErrorTextAndColor(QObject::tr("Wrong message format of ZFO file '%1'.").arg(fileName));
		return IMP_ZFO_FORMAT_ERR;
	}
	if (messsage.documents().isEmpty()) {
		errText = setErrorTextAndColor(QObject::tr("Wrong message data of ZFO file '%1'.").arg(fileName));
		return IMP_ZFO_DATA_ERR;
	}

	/* Recognise relevant accounts and message orientation */
	QList<AcntId> usrNameList; // usrNameList holds relevant user names for import.
	QList<bool> isSentList;  // isSentList holds msg orientation for user
				 // names from uNameList.
	foreach (const AcntId &acntId, acntIdList) {
		QString dbId(GlobInstcs::accountDbPtr->dbId(acntId.username()));
		if (dbId == messsage.envelope().dbIDRecipient()) {
			isSentList.append(false);
			usrNameList.append(acntId);
		} else if (dbId == messsage.envelope().dbIDSender()) {
			isSentList.append(true);
			usrNameList.append(acntId);
		}
	}

	/* If relevant account does not exist, return result info text and exit */
	if (usrNameList.isEmpty()) {
		errText = setErrorTextAndColor(QObject::tr("Relevant account does not exist for ZFO file '%1'.").arg(fileName));
		return IMP_NO_ACCOUNT_ERR;
	}

	/* Both lists must have same number of items */
	if (usrNameList.count() != isSentList.count()) {
		errText = QObject::tr("Import internal error.");
		return IMP_ERR;
	}

	/* Authenticate zfo message in the ISDS */
	if (authenticate) {

		AcntId acntId = acntIdList.at(0);
		if (Q_UNLIKELY((GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
			(!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)))) {
			Q_ASSERT(0);
			return IMP_ERR;
		}

		Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(acntId);
		if (Q_UNLIKELY(session == Q_NULLPTR)) {
			Q_ASSERT(0);
			return IMP_ERR;
		}

		/* Send SOAP request to verify message */
		bool valid = false;
		enum Isds::Type::Error error = Isds::authenticateMessage(
		    *session, messsage.raw(), valid, Q_NULLPTR, &errText);
		if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
			errText = setErrorTextAndColor(QObject::tr("Cannot authenticate the ZFO file '%1'.").arg(fileName));
			return IMP_ISDS_ERR;

		}
		if (!valid) {
			errText = setErrorTextAndColor(QObject::tr("ZFO file '%1' is not authentic (returns ISDS)!").arg(fileName));
			return IMP_AUTH_ERR;
		}
	}

	/* Store message into accounts database */
	for (int i = 0; i < usrNameList.count(); ++i) {
		if (!DbWrapper::insertCompleteMessageToDb(acntIdList.at(i), messsage,
		        isSentList.at(i) ? MessageDb::MessageType::TYPE_SENT
		            : MessageDb::MessageType::TYPE_RECEIVED,
		        errText)) {
			errText = setErrorTextAndColor(QObject::tr("Cannot store the ZFO file '%1' to local database.").arg(fileName));
			return IMP_DB_INS_ERR;
		}
	}

#if 0
	/* Save ZFO to zfo database */
	DbWrapper::insertZfoToDb(messsage.envelope().dmId(), true,
	    zfoContent.size(), zfoContent.toBase64());
#endif

	errText = QObject::tr("ZFO file '%1' has been imported.").arg(fileName);

	return IMP_SUCCESS;
}

QString TaskImportZfo::setErrorTextAndColor(const QString &text)
{
	return "<font color=\"red\">" + text + " " +
	    QObject::tr("File has not been imported!") + "</font>";
}
