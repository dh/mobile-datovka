/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>

#include "src/datovka_shared/identifiers/account_id.h"

/*!
 * @brief Message processing status emitter.
 */
class MessageProcessingEmitter : public QObject {
	Q_OBJECT

signals:
	/*!
	 * @brief Do some actions when download account info finished.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] success True if success.
	 * @param[in] errTxt Errot description.
	 */
	void downloadAccountInfoFinishedSignal(const AcntId &acntId,
	    bool success, const QString &errTxt);

	/*!
	 * @brief Do some actions when download delivery info finished.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] msgId Message ID.
	 * @param[in] success True if success.
	 * @param[in] errTxt Errot description.
	 */
	void downloadDeliveryInfoFinishedSignal(const AcntId &acntId,
	    qint64 msgId, bool success, const QString &errTxt);

	/*!
	 * @brief Do some actions when download message finished.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] msgId Message ID.
	 * @param[in] success True if success.
	 * @param[in] errTxt Errot description.
	 */
	void downloadMessageFinishedSignal(const AcntId &acntId, qint64 msgId,
	    bool success, const QString &errTxt);

	/*!
	 * @brief Do some actions when download message list finished.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] success True if success.
	 * @param[in] statusBarText Text for statusBar.
	 * @param[in] errTxt Errot description.
	 * @param[in] isMsgReceived True if message type is received.
	 */
	void downloadMessageListFinishedSignal(const AcntId &acntId,
	    bool success, const QString &statusBarText, const QString &errTxt,
	    bool isMsgReceived);

	/*!
	 * @brief Do some actions when import of one ZFO has been finished.
	 *
	 * @param[in] zfoNumber ZFO number in import process.
	 * @param[in] zfoTotal Overall number of ZFO for import.
	 * @param[in] infoText Import result description (may be NULL).
	 */
	void importZFOFinishedSignal(int zfoNumber, int zfoTotal,
	    QString infoText);

	/*!
	 * @brief Do some actions when send message finished.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] transactId Unique transaction identifier.
	 * @param[in] dbIDRecipient Message recipient databox ID string.
	 * @param[in] dmRecipient Recipient full name.
	 * @param[in] msgId Send message ID.
	 * @param[in] success True if success.
	 * @param[in] errTxt Errot description.
	 */
	void sendMessageFinishedSignal(const AcntId &acntId,
	    const QString &transactId, const QString &dbIDRecipient,
	    const QString &dmRecipient, qint64 msgId, bool success,
	    const QString &errTxt);

	/*!
	 * @brief Do some actions when records management sync has been finished.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] accNumber Account number in process.
	 * @param[in] accTotal Overall number of account for sync.
	 */
	void rmSyncFinishedSignal(const AcntId &acntId, int accNumber,
	    int accTotal);
};
