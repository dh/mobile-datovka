/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QThread>

#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/isds/services/message_interface.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/net/db_wrapper.h"
#include "src/worker/emitter.h"
#include "src/worker/task_send_message.h"

TaskSendMessage::ResultData::ResultData(void)
    : result(false),
    errInfo(),
    dbIDRecipient(),
    dmRecipient(),
    dmId(-1)
{
}

TaskSendMessage::ResultData::ResultData(bool res, const QString &eInfo,
    const QString &recId, const QString &recName, qint64 mId)
    : result(res),
    errInfo(eInfo),
    dbIDRecipient(recId),
    dmRecipient(recName),
    dmId(mId)
{
}

TaskSendMessage::TaskSendMessage(const AcntId &acntId,
    const Isds::Message &message, const QString &transactId)
    : m_result(DL_ERR),
    m_dmId(0),
    m_lastError(),
    m_acntId(acntId),
    m_message(message),
    m_transactId(transactId)
{
}

void TaskSendMessage::run(void)
{
	/* ### Worker task begin. ### */

	logDebugLv0NL("%s", "---------------SEND MESSAGE TASK---------------");
	logDebugLv0NL("Starting in thread '%p'", QThread::currentThreadId());

	m_result = sendMessage(m_acntId, m_message, m_dmId, m_lastError);

	if (GlobInstcs::msgProcEmitterPtr != Q_NULLPTR) {
		emit GlobInstcs::msgProcEmitterPtr->sendMessageFinishedSignal(
		    m_acntId, m_transactId, m_message.envelope().dbIDRecipient(),
		    m_message.envelope().dmRecipient(), m_dmId,
		    TaskSendMessage::DL_SUCCESS == m_result, m_lastError);
	}

	logDebugLv0NL("Finished in thread '%p'", QThread::currentThreadId());
	logDebugLv0NL("%s", "-----------------------------------------------");

	/* ### Worker task end. ### */
}

/*!
 * @brief Translate communication error onto task error.
 */
static
enum TaskSendMessage::Result error2result(enum Isds::Type::Error error)
{
	switch (error) {
	case Isds::Type::ERR_SUCCESS:
		return TaskSendMessage::DL_SUCCESS;
		break;
	case Isds::Type::ERR_ISDS:
		return TaskSendMessage::DL_ISDS_ERROR;
		break;
	case Isds::Type::ERR_SOAP:
	case Isds::Type::ERR_XML:
		return TaskSendMessage::DL_XML_ERROR;
		break;
	default:
		return TaskSendMessage::DL_ERR;
		break;
	}
}

enum TaskSendMessage::Result TaskSendMessage::sendMessage(const AcntId &acntId,
    Isds::Message &message, qint64 &dmId, QString &lastError)
{
	if (Q_UNLIKELY((!acntId.isValid()) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)))) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	logDebugLv1NL("Sending from account '%s'",
	    acntId.username().toUtf8().constData());

	enum Isds::Type::Error error = Isds::sendMessage(*session, message,
	    Q_NULLPTR, &lastError);
	if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
		return error2result(error);
	}

	/* Message must now have its unique identifier. */
	dmId = message.envelope().dmId();

	/* Message has been sent */
	logDebugLv1NL("Message has been sent to ISDS with ID %s",
	    QString::number(dmId).toUtf8().constData());

	/* Store message into db */
	if (!DbWrapper::insertCompleteMessageToDb(acntId, message,
	        MessageDb::TYPE_SENT, lastError)) {
		logErrorNL("DB: Insert error: %s",
		    lastError.toUtf8().constData());
		return DL_DB_INS_ERR;
	}

	return DL_SUCCESS;
}
