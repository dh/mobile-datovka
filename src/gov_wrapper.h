/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QMap>
#include <QObject>
#include <QString>

namespace Gov {
	class Service; /* Forward declaration. */
}
class GovFormListModel; /* Forward declaration. */
class GovServiceListModel; /* Forward declaration. */
class IsdsWrapper; /* Forward declaration. */
class QmlAcntId; /* Forward declaration. */

/*
 * Class GovWrapper provides interface between QML and Gov services.
 * Class is initialised in the main function (main.cpp)
 */
class GovWrapper : public QObject {
	Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] isds Pointer to ISDS wrapper.
	 * @param[in] parent Parent object.
	 */
	explicit GovWrapper(IsdsWrapper *isds, QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Destructor.
	 */
	virtual
	~GovWrapper(void);

	/*!
	 * @brief Load Gov services into QML.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in,out] srvcModel Gov model.
	 */
	Q_INVOKABLE
	void loadServicesToModel(const QmlAcntId *qAcntId,
	    GovServiceListModel *srvcModel) const;

	/*!
	 * @brief Load gof form data into QML model.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] serviceInternalId Internal service identifier.
	 * @param[in,out] formModel Gov form model to be set.
	 */
	Q_INVOKABLE
	void loadFormToModel(const QmlAcntId *qAcntId,
	    const QString &serviceInternalId,
	    GovFormListModel *formModel) const;

	/*!
	 * @brief Create and send gov service request.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] serviceInternalId Internal service identifier.
	 * @param[in] formModel Gov form model to be set.
	 */
	Q_INVOKABLE
	bool sendGovRequest(const QmlAcntId *qAcntId,
	    const QString &serviceInternalId,
	    GovFormListModel *formModel) const;

private:
	QMap<QString, const Gov::Service *> m_govServices; /*!< Holds pointers to all available Gov services. */
	IsdsWrapper *m_isds; /*!< Pointer to ISDS wrapper. */
};
