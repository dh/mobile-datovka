/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>
#include <QString>

/*!
 * @brief Just a convenience class to obtain some message information.
 */
class MsgInfo : public QObject {
	Q_OBJECT
	Q_PROPERTY(enum ZfoType type READ type WRITE setType NOTIFY typeChanged)
	Q_PROPERTY(QString idStr READ idStr WRITE setIdStr NOTIFY idStrChanged)
	Q_PROPERTY(QString annotation READ annotation WRITE setAnnotation NOTIFY annotationChanged)
	Q_PROPERTY(QString descrHtml READ descrHtml WRITE setDescrHtml NOTIFY descrHtmlChanged)
	Q_PROPERTY(QString emailBody READ emailBody WRITE setEmailBody NOTIFY emailBodyChanged)

public:
	/* Specifies type of ZFO. */
	enum ZfoType {
		TYPE_UNKNOWN, /*!< Unknown or unsupported type. */
		TYPE_MESSAGE, /*!< Message or message envelope. */
		TYPE_DELIVERY_INFO /*!< Delivery information. */
	};
	Q_ENUMS(ZfoType)

	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit MsgInfo(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 *
	 * @param[in] info Info to be copied.
	 */
	MsgInfo(const MsgInfo &info);

	MsgInfo(enum ZfoType type, const QString &idStr,
	    const QString &annotation, const QString &descrHtml,
	    const QString &emailBody, QObject *parent = Q_NULLPTR);

	enum ZfoType type(void) const;
	void setType(enum ZfoType type);

	QString idStr(void) const;
	void setIdStr(const QString &idStr);

	QString annotation(void) const;
	void setAnnotation(const QString &annotation);

	QString descrHtml(void) const;
	void setDescrHtml(const QString &descrHtml);

	QString emailBody(void) const;
	void setEmailBody(const QString &emailBody);

signals:
	void typeChanged(enum ZfoType newType);
	void idStrChanged(const QString &newIdStr);
	void annotationChanged(const QString &newAnnotation);
	void descrHtmlChanged(const QString &newDescrHtml);
	void emailBodyChanged(const QString &newEmailBody);

private:
	enum ZfoType m_type; 
	QString m_idStr; /*!< Message identifier as a string. */
	QString m_annotation; /*!< Message annotation. */
	QString m_descrHtml; /*!< Description in HTML. */
	QString m_emailBody; /*!< Email body. */
};

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(MsgInfo)
Q_DECLARE_METATYPE(MsgInfo::ZfoType)
