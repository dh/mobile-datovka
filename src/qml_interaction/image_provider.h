/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QPixmap>
#include <QQuickImageProvider>

#define IMAGE_PROVIDER_ID QLatin1String("images")
#define RM_SVG_LOGO_ID QLatin1String("rmlogo.svg") /* Records management logo. */

/*!
 * @brief Class provides an interface for supporting pixmaps
 *        and threaded image requests in QML.
 */
class ImageProvider : public QQuickImageProvider {

public:
	/*!
	 * @brief Constructor.
	 */
	ImageProvider(void);

	/*!
	 * @brief Transform image from stored byte array and return a pixmap.
	 *
	 * @param[in] id String identifying the image data.
	 * @param[in,out] size Output image size.
	 * @param[in] requestedSize Input image size (may be NULL).
	 * @return Plain pixmap image for QML.
	 */
	virtual
	QPixmap requestPixmap(const QString &id, QSize *size,
	    const QSize &requestedSize) Q_DECL_OVERRIDE;

	/*!
	 * @brief Get SVG image data.
	 *
	 * @param[in] id Image data identifier.
	 * @return Non-empty image data if id present.
	 */
	QByteArray svg(const QString &id) const;

	/*!
	 * @brief Set SVG image data.
	 *
	 * @param[in] id Image data identifier.
	 * @param[in] svgData Image data in SVG format.
	 * @return True if value inserted.
	 */
	bool setSvg(const QString &id, const QByteArray &svgData);

private:
	QMap<QString, QByteArray> m_svgImgs; /*!< Map of images in SVG format. */
};
