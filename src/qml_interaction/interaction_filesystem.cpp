/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QFileInfo>
#include <QQmlEngine> /* qmlRegisterType */
#include <QStandardPaths>

#include "src/qml_interaction/interaction_filesystem.h"

void InteractionFilesystem::declareQML(void)
{
	qmlRegisterType<InteractionFilesystem>("cz.nic.mobileDatovka.qmlInteraction", 1, 0, "InteractionFilesystem");
	qRegisterMetaType<InteractionFilesystem>("InteractionFilesystem");
	qRegisterMetaType<InteractionFilesystem::Location>("InteractionFilesystem::Location");
}

InteractionFilesystem::InteractionFilesystem(QObject *parent)
    : QObject(parent)
{
}

InteractionFilesystem::InteractionFilesystem(const InteractionFilesystem &inter)
    : QObject()
{
	Q_UNUSED(inter)
}

/*!
 * @brief Converts between location enums.
 *
 * @param[in] location Location as defined in QML convenience class.
 * @return Location as defined in QStandardPaths.
 */
static
enum QStandardPaths::StandardLocation toStandardLocation(
    enum InteractionFilesystem::Location location)
{
	switch (location) {
	case InteractionFilesystem::DESKTOP_LOCATION:
		return QStandardPaths::DesktopLocation;
		break;
	case InteractionFilesystem::DOCUMENTS_LOCATION:
		return QStandardPaths::DocumentsLocation;
		break;
	case InteractionFilesystem::DOWNLOAD_LOCATION:
		return QStandardPaths::DownloadLocation;
		break;
	case InteractionFilesystem::PICTURE_LOCATION:
		return QStandardPaths::PicturesLocation;
		break;
	case InteractionFilesystem::TEMP_LOCATION:
		return QStandardPaths::TempLocation;
		break;
	default :
		Q_ASSERT(0);
		return QStandardPaths::DesktopLocation;
		break;
	}
}

QString InteractionFilesystem::locate(enum Location location)
{
	const QStringList locations(
	    QStandardPaths::standardLocations(toStandardLocation(location)));
	if (!locations.isEmpty()) {
		return locations.at(0);
	} else {
		return QStringLiteral(".");
	}
}

QString InteractionFilesystem::absoluteDirPath(const QString &path)
{
	if (path.isEmpty()) {
		return QString();
	}

	QFileInfo fileInfo(path);
	if (fileInfo.isDir() && fileInfo.isReadable()) {
		return fileInfo.absoluteFilePath();
	}

	return QString();
}

QString InteractionFilesystem::absolutePath(const QString &path)
{
	if (path.isEmpty()) {
		return QString();
	}

	QFileInfo fileInfo(path);
	if (fileInfo.isFile()) {
		return fileInfo.absolutePath();
	}

	return QString();
}
