/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/datovka_shared/graphics/graphics.h"
#include "src/qml_interaction/image_provider.h"
#include "src/records_management.h"

#define DEFAULT_EDGE_LEN 128

ImageProvider::ImageProvider(void)
    : QQuickImageProvider(QQuickImageProvider::Pixmap),
    m_svgImgs()
{
}

QPixmap ImageProvider::requestPixmap(const QString &id, QSize *size,
    const QSize &requestedSize)
{
	if (Q_UNLIKELY(id.isEmpty())) {
		return QPixmap();
	}

	QByteArray svgData(svg(id));
	if (svgData.isEmpty()) {
		return QPixmap();
	}

	int edgeLen = DEFAULT_EDGE_LEN;
	if (requestedSize.isValid()) {
		int h = requestedSize.height();
		int w = requestedSize.width();
		edgeLen = (h < w) ? h : w;
	}
	QPixmap pixmap(Graphics::pixmapFromSvg(svgData, edgeLen));

	if (size != Q_NULLPTR) {
		*size = pixmap.size();
	}
	return pixmap;
}

QByteArray ImageProvider::svg(const QString &id) const
{
	if (id.isEmpty()) {
		return QByteArray();
	}

	QMap<QString, QByteArray>::const_iterator it(m_svgImgs.find(id));
	if (it != m_svgImgs.end()) {
		return it.value();
	}
	return QByteArray();
}

bool ImageProvider::setSvg(const QString &id, const QByteArray &svgData)
{
	if (Q_UNLIKELY(id.isEmpty())) {
		return false;
	}

	if (!svgData.isEmpty()) {
		m_svgImgs[id] = svgData;
	} else {
		m_svgImgs.remove(id);
	}
	return true;
}
