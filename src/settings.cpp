/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDateTime>
#include <QDir>
#include <QSettings>

#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/settings/records_management.h"
#include "src/global.h"
#include "src/io/filesystem.h"
#include "src/settings.h"

/* Name of Datovka configuration file */
#define SETTINGS_FILE_NAME "datovka.conf"
/* Names of setting items in GLOBALS group in the datovka.conf file */
#define SETTINGS_GLOBAL_GROUP "GLOBALS"

/*
 * These names are kept for backward compatibility purposes. They were formerly
 * in GLOBALS, now they are in separate group.
 */
#define SETTINGS_PIN_ALG "pin_alg"
#define SETTINGS_PIN_SALT "pin_salt"
#define SETTINGS_PIN_CODE "pin_code"

Settings::Settings(void)
    : PinSettings()
{
}

void Settings::saveToSettings(QSettings &settings) const
{
	debugFuncCall();

	settings.beginGroup(SETTINGS_GLOBAL_GROUP);

	if (!_pinVal.isEmpty()) {
		{
			/*
			 * Code in this block needs to be kept for a while
			 * (until the end of 2018?) because of keeping
			 * backward compatibility.
			 */
			Q_ASSERT(!pinAlg.isEmpty());
			settings.setValue(SETTINGS_PIN_ALG, pinAlg);
			Q_ASSERT(!pinSalt.isEmpty());
			settings.setValue(SETTINGS_PIN_SALT,
			    QString::fromUtf8(pinSalt.toBase64()));
			Q_ASSERT(!pinCode.isEmpty());
			settings.setValue(SETTINGS_PIN_CODE,
			    QString::fromUtf8(pinCode.toBase64()));
		}
	}

	settings.endGroup();

	/*
	 * The PIN settings are newly kept in a separate group.
	 */
	PinSettings::saveToSettings(settings);

	/* Records management settings. */
	if (GlobInstcs::recMgmtSetPtr != Q_NULLPTR) {
		GlobInstcs::recMgmtSetPtr->saveToSettings(_pinVal, settings);
	}
}

void Settings::loadFromSettings(const QSettings &settings)
{
	debugFuncCall();

	/*
	 * The PIN settings are newly kept in a separate group.
	 */
	PinSettings::loadFromSettings(settings);
	/* Clear read PIN settings if not entire data present. */
	if (!PinSettings::pinConfigured()) {
		PinSettings::updatePinSettings(*this, QString());

		logWarningNL("Trying to read PIN settings from '%s' group.",
		    SETTINGS_GLOBAL_GROUP);

		/*
		 * First the PIN settings were stored in 'GLOBALS' group.
		 * Therefore we need to keep this code functional at least for
		 * a while (until the end of 2018?).
		 */
		pinAlg = settings.value(
		    SETTINGS_GLOBAL_GROUP "/" SETTINGS_PIN_ALG,
		    QString()).toString();
		pinSalt = QByteArray::fromBase64(settings.value(
		    SETTINGS_GLOBAL_GROUP "/" SETTINGS_PIN_SALT,
		    QString()).toString().toUtf8());
		pinCode = QByteArray::fromBase64(settings.value(
		    SETTINGS_GLOBAL_GROUP "/" SETTINGS_PIN_CODE,
		    QString()).toString().toUtf8());
	}

	/* Records management settings. */
	if (GlobInstcs::recMgmtSetPtr != Q_NULLPTR) {
		GlobInstcs::recMgmtSetPtr->loadFromSettings(settings);
	}
}

QString Settings::settingsPath(void)
{
	QString settingsPath(dfltDbAndConfigLoc());

	if (settingsPath.isEmpty()) {
		return QString();
	}

	return settingsPath + QDir::separator() +
	    QStringLiteral(SETTINGS_FILE_NAME);
}
