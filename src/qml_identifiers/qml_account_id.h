/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>

#include "src/datovka_shared/identifiers/account_id.h"

class QmlAcntId : public QObject, public AcntId {
	Q_OBJECT

	Q_PROPERTY(QString username READ username WRITE setUsername NOTIFY usernameChanged)
	Q_PROPERTY(bool testing READ testing WRITE setTesting NOTIFY testingChanged)

public:
	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	explicit QmlAcntId(QObject *parent = Q_NULLPTR);
	explicit QmlAcntId(const AcntId &other);
#ifdef Q_COMPILER_RVALUE_REFS
	explicit QmlAcntId(AcntId &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
	QmlAcntId(const QmlAcntId &other);

	void setUsername(const QString &n);
#ifdef Q_COMPILER_RVALUE_REFS
	void setUsername(QString &&n);
#endif /* Q_COMPILER_RVALUE_REFS */
	void setTesting(bool t);

	/*!
	 * @brief Converts QVariant (obtained from QML) into a pointer.
	 *
	 * @note Some weird stuff happens in QML when passing instances
	 *     directly as constant reference. Wrong constructors are called
	 *     and no data are passed.
	 * @note QML passes objects (which were created in QML) as QVariant
	 *     values holding pointers. You therefore may call invokable methods
	 *     with QVariant arguments from QML.
	 * @note If you use
	 *     qRegisterMetaType<Type *>("Type *") and
	 *     qRegisterMetaType<Type *>("const Type *")
	 *     then QML will be able to call invokable methods without explicit
	 *     conversion from QVariant arguments.
	 *     Q_DECLARE_METATYPE(Type *) is not needed.
	 *
	 * @param[in] variant QVariant holding the pointer.
	 * @return Pointer if it could be acquired, Q_NULLPTR else. This
	 *     function does not allocate a new instance.
	 */
	static
	QmlAcntId *fromVariant(const QVariant &variant);

signals:
	void usernameChanged(const QString &u);
	void testingChanged(bool t);
};

Q_DECLARE_METATYPE(QmlAcntId)
