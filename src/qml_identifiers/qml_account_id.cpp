/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QQmlEngine> /* qmlRegisterType */

#include "src/qml_identifiers/qml_account_id.h"

void QmlAcntId::declareQML(void)
{
	qmlRegisterType<QmlAcntId>("cz.nic.mobileDatovka.qmlIdentifiers", 1, 0, "AcntId");
	qRegisterMetaType<QmlAcntId>("QmlAcntId");
	/*
	 * Calling qRegisterMetaType() with the pointer types enables automatic
	 * conversion of QML object (QVariants) to respective pointers.
	 */
	qRegisterMetaType<QmlAcntId *>("QmlAcntId *");
	qRegisterMetaType<QmlAcntId *>("const QmlAcntId *");
	/* TODO -- Explore whether QMetaType::registerConverter() can be used. */
}

QmlAcntId::QmlAcntId(QObject *parent)
    : QObject(parent),
    AcntId()
{
}

QmlAcntId::QmlAcntId(const AcntId &other)
    : QObject(Q_NULLPTR),
    AcntId(other)
{
}

#ifdef Q_COMPILER_RVALUE_REFS
QmlAcntId::QmlAcntId(AcntId &&other) Q_DECL_NOEXCEPT
    : QObject(Q_NULLPTR),
    AcntId(::std::move(other))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

QmlAcntId::QmlAcntId(const QmlAcntId &other)
    : QObject(Q_NULLPTR),
    AcntId(other)
{
}

void QmlAcntId::setUsername(const QString &n)
{
	AcntId::setUsername(n);
	emit usernameChanged(AcntId::username());
}

#ifdef Q_COMPILER_RVALUE_REFS
void QmlAcntId::setUsername(QString &&n)
{
	AcntId::setUsername(::std::move(n));
	emit usernameChanged(AcntId::username());
}
#endif /* Q_COMPILER_RVALUE_REFS */

void QmlAcntId::setTesting(bool t)
{
	AcntId::setTesting(t);
	emit testingChanged(AcntId::testing());
}

QmlAcntId *QmlAcntId::fromVariant(const QVariant &variant)
{
	if (Q_UNLIKELY(!variant.canConvert<QObject *>())) {
		return Q_NULLPTR;
	}
	QObject *obj = qvariant_cast<QObject *>(variant);
	return qobject_cast<QmlAcntId *>(obj);
}
