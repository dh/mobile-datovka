/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QChar>
#include <QDateTime>
#include <QObject>
#include <QString>

#include "src/datovka_shared/isds/message_interface.h"
#include "src/datovka_shared/isds/types.h"

class QmlIsdsEnvelope : public QObject, public Isds::Envelope {
	Q_OBJECT

	/* isNull */

	Q_PROPERTY(qint64 dmId READ dmId WRITE setDmId NOTIFY dmIdChanged)
	Q_PROPERTY(QString dmSenderOrgUnitNumStr READ dmSenderOrgUnitNumStr WRITE setDmSenderOrgUnitNumStr NOTIFY dmSenderOrgUnitNumStrChanged)
	Q_PROPERTY(QString dmRecipientOrgUnitNumStr READ dmRecipientOrgUnitNumStr WRITE setDmRecipientOrgUnitNumStr NOTIFY dmRecipientOrgUnitNumStrChanged)
	Q_PROPERTY(QString dmLegalTitleLawStr READ dmLegalTitleLawStr WRITE setDmLegalTitleLawStr NOTIFY dmLegalTitleLawStrChanged)
	Q_PROPERTY(QString dmLegalTitleYearStr READ dmLegalTitleYearStr WRITE setDmLegalTitleYearStr NOTIFY dmLegalTitleYearStrChanged)

	Q_PROPERTY(QString dmID READ dmID WRITE setDmID NOTIFY dmIDChanged)
	Q_PROPERTY(QString dbIDSender READ dbIDSender WRITE setDbIDSender NOTIFY dbIDSenderChanged)
	Q_PROPERTY(QString dmSender READ dmSender WRITE setDmSender NOTIFY dmSenderChanged)
	Q_PROPERTY(QString dmSenderAddress READ dmSenderAddress WRITE setDmSenderAddress NOTIFY dmSenderAddressChanged)
	Q_PROPERTY(enum Isds::Type::DbType dmSenderType READ dmSenderType WRITE setDmSenderType NOTIFY dmSenderTypeChanged)
	Q_PROPERTY(QString dmRecipient READ dmRecipient WRITE setDmRecipient NOTIFY dmRecipientChanged)
	Q_PROPERTY(QString dmRecipientAddress READ dmRecipientAddress WRITE setDmRecipientAddress NOTIFY dmRecipientAddressChanged)
	Q_PROPERTY(enum Isds::Type::NilBool dmAmbiguousRecipient READ dmAmbiguousRecipient WRITE setDmAmbiguousRecipient)

	/* dmOrdinal */
	Q_PROPERTY(enum Isds::Type::DmState dmMessageStatus READ dmMessageStatus WRITE setDmMessageStatus NOTIFY dmMessageStatusChanged)
	Q_PROPERTY(qint64 dmAttachmentSize READ dmAttachmentSize WRITE setDmAttachmentSize NOTIFY dmAttachmentSizeChanged)
	Q_PROPERTY(QDateTime dmDeliveryTime READ dmDeliveryTime WRITE setDmDeliveryTime NOTIFY dmDeliveryTimeChanged)
	Q_PROPERTY(QDateTime dmAcceptanceTime READ dmAcceptanceTime WRITE setDmAcceptanceTime NOTIFY dmAcceptanceTimeChanged)
	/* dmHash */
	Q_PROPERTY(QByteArray dmQTimestamp READ dmQTimestamp WRITE setDmQTimestamp NOTIFY dmQTimestampChanged)
	/* dmEvents */

	Q_PROPERTY(QString dmSenderOrgUnit READ dmSenderOrgUnit WRITE setDmSenderOrgUnit NOTIFY dmSenderOrgUnitChanged)
	Q_PROPERTY(qint64 dmSenderOrgUnitNum READ dmSenderOrgUnitNum WRITE setDmSenderOrgUnitNum NOTIFY dmSenderOrgUnitNumChanged)
	Q_PROPERTY(QString dbIDRecipient READ dbIDRecipient WRITE setDbIDRecipient NOTIFY dbIDRecipientChanged)
	Q_PROPERTY(QString dmRecipientOrgUnit READ dmRecipientOrgUnit WRITE setDmRecipientOrgUnit NOTIFY dmRecipientOrgUnitChanged)
	Q_PROPERTY(qint64 dmRecipientOrgUnitNum READ dmRecipientOrgUnitNum WRITE setDmRecipientOrgUnitNum NOTIFY dmRecipientOrgUnitNumChanged)
	Q_PROPERTY(QString dmToHands READ dmToHands WRITE setDmToHands NOTIFY dmToHandsChanged)
	Q_PROPERTY(QString dmAnnotation READ dmAnnotation WRITE setDmAnnotation NOTIFY dmAnnotationChanged)
	Q_PROPERTY(QString dmRecipientRefNumber READ dmRecipientRefNumber WRITE setDmRecipientRefNumber NOTIFY dmRecipientRefNumberChanged)
	Q_PROPERTY(QString dmSenderRefNumber READ dmSenderRefNumber WRITE setDmSenderRefNumber NOTIFY dmSenderRefNumberChanged)
	Q_PROPERTY(QString dmRecipientIdent READ dmRecipientIdent WRITE setDmRecipientIdent NOTIFY dmRecipientIdentChanged)
	Q_PROPERTY(QString dmSenderIdent READ dmSenderIdent WRITE setDmSenderIdent NOTIFY dmSenderIdentChanged)

	Q_PROPERTY(qint64 dmLegalTitleLaw READ dmLegalTitleLaw WRITE setDmLegalTitleLaw NOTIFY dmLegalTitleLawChanged)
	Q_PROPERTY(qint64 dmLegalTitleYear READ dmLegalTitleYear WRITE setDmLegalTitleYear NOTIFY dmLegalTitleYearChanged)
	Q_PROPERTY(QString dmLegalTitleSect READ dmLegalTitleSect WRITE setDmLegalTitleSect NOTIFY dmLegalTitleSectChanged)
	Q_PROPERTY(QString dmLegalTitlePar READ dmLegalTitlePar WRITE setDmLegalTitlePar NOTIFY dmLegalTitleParChanged)
	Q_PROPERTY(QString dmLegalTitlePoint READ dmLegalTitlePoint WRITE setDmLegalTitlePoint NOTIFY dmLegalTitlePointChanged)
	Q_PROPERTY(enum Isds::Type::NilBool dmPersonalDelivery READ dmPersonalDelivery WRITE setDmPersonalDelivery NOTIFY dmPersonalDeliveryChanged)
	Q_PROPERTY(enum Isds::Type::NilBool dmAllowSubstDelivery READ dmAllowSubstDelivery WRITE setDmAllowSubstDelivery NOTIFY dmAllowSubstDeliveryChanged)
	Q_PROPERTY(QChar dmType READ dmType WRITE setDmType NOTIFY dmTypeChanged)

	/* dmOVM */
	/* dmPublishOwnID */

public:
	Q_ENUM(Isds::Type::NilBool)
	Q_ENUM(Isds::Type::DbType)
	Q_ENUM(Isds::Type::DmState)

	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	explicit QmlIsdsEnvelope(QObject *parent = Q_NULLPTR);
	explicit QmlIsdsEnvelope(const Isds::Envelope &other);
#ifdef Q_COMPILER_RVALUE_REFS
	explicit QmlIsdsEnvelope(Isds::Envelope &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
	QmlIsdsEnvelope(const QmlIsdsEnvelope &other);

	void setDmId(qint64 id);
	bool setDmSenderOrgUnitNumStr(const QString &soun);
	bool setDmRecipientOrgUnitNumStr(const QString &roun);
	bool setDmLegalTitleLawStr(const QString &l);
	bool setDmLegalTitleYearStr(const QString &y);

	void setDmID(const QString &id);
	void setDbIDSender(const QString &sbi);
	void setDmSender(const QString &sn);
	void setDmSenderAddress(const QString &sa);
	void setDmSenderType(enum Isds::Type::DbType st);
	void setDmRecipient(const QString &rn);
	void setDmRecipientAddress(const QString &ra);
	void setDmAmbiguousRecipient(enum Isds::Type::NilBool ar);

	void setDmMessageStatus(enum Isds::Type::DmState s);
	void setDmAttachmentSize(qint64 as);
	void setDmDeliveryTime(const QDateTime &dt);
	void setDmAcceptanceTime(const QDateTime &at);
	void setDmQTimestamp(const QByteArray &ts);

	void setDmSenderOrgUnit(const QString &sou);
	void setDmSenderOrgUnitNum(qint64 soun);
	void setDbIDRecipient(const QString &rbi);
	void setDmRecipientOrgUnit(const QString &rou);
	void setDmRecipientOrgUnitNum(qint64 roun);
	void setDmToHands(const QString &th);
	void setDmAnnotation(const QString &a);
	void setDmRecipientRefNumber(const QString &rrn);
	void setDmSenderRefNumber(const QString &srn);
	void setDmRecipientIdent(const QString &ri);
	void setDmSenderIdent(const QString &si);

	void setDmLegalTitleLaw(qint64 l);
	void setDmLegalTitleYear(qint64 y);
	void setDmLegalTitleSect(const QString &s);
	void setDmLegalTitlePar(const QString &p);
	void setDmLegalTitlePoint(const QString &p);
	void setDmPersonalDelivery(enum Isds::Type::NilBool pd);
	void setDmAllowSubstDelivery(enum Isds::Type::NilBool sd);
	void setDmType(QChar t);

signals:
	void dmIdChanged(qint64 id);
	void dmSenderOrgUnitNumStrChanged(const QString &soun);
	void dmRecipientOrgUnitNumStrChanged(const QString &roun);
	void dmLegalTitleLawStrChanged(const QString &l);
	void dmLegalTitleYearStrChanged(const QString &y);

	void dmIDChanged(const QString &id);
	void dbIDSenderChanged(const QString &sbi);
	void dmSenderChanged(const QString &sn);
	void dmSenderAddressChanged(const QString &sa);
	void dmSenderTypeChanged(enum Isds::Type::DbType st);
	void dmRecipientChanged(const QString &rn);
	void dmRecipientAddressChanged(const QString &ra);
	void dmAmbiguousRecipientChanged(enum Isds::Type::NilBool ar);

	void dmMessageStatusChanged(enum Isds::Type::DmState s);
	void dmAttachmentSizeChanged(qint64 as);
	void dmDeliveryTimeChanged(const QDateTime &dt);
	void dmAcceptanceTimeChanged(const QDateTime &at);
	void dmQTimestampChanged(const QByteArray &ts);

	void dmSenderOrgUnitChanged(const QString &sou);
	void dmSenderOrgUnitNumChanged(qint64 soun);
	void dbIDRecipientChanged(const QString &rbi);
	void dmRecipientOrgUnitChanged(const QString &rou);
	void dmRecipientOrgUnitNumChanged(qint64 roun);
	void dmToHandsChanged(const QString &th);
	void dmAnnotationChanged(const QString &a);
	void dmRecipientRefNumberChanged(const QString &rrn);
	void dmSenderRefNumberChanged(const QString &srn);
	void dmRecipientIdentChanged(const QString &ri);
	void dmSenderIdentChanged(const QString &si);

	void dmLegalTitleLawChanged(qint64 l);
	void dmLegalTitleYearChanged(qint64 y);
	void dmLegalTitleSectChanged(const QString &s);
	void dmLegalTitleParChanged(const QString &p);
	void dmLegalTitlePointChanged(const QString &p);
	void dmPersonalDeliveryChanged(enum Isds::Type::NilBool pd);
	void dmAllowSubstDeliveryChanged(enum Isds::Type::NilBool sd);
	void dmTypeChanged(QChar t);
};

Q_DECLARE_METATYPE(QmlIsdsEnvelope)
