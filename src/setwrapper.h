/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>
#include <QString>

class AccountListModel; /* Forward declaration. */

/*
 * This class provide wrapper between
 * global settings class and QML settings page.
 * We need signal and slots for sending settings to/from QML page.
 * This object is initialized in main.cpp and register to QML.
 */
class GlobalSettingsQmlWrapper : public QObject {
    Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 */
	explicit GlobalSettingsQmlWrapper(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Get application version string.
	 *
	 * @return Application version string.
	*/
	Q_INVOKABLE static
	QString appVersion(void);

	/*!
	 * @brief Get attachments life value from global settings.
	 *
	 * @return Attachments life value.
	*/
	Q_INVOKABLE static
	int attachmentLifeDays(void);

	/*!
	 * @brief Set attachments life value to global settings.
	 *
	 * @param[in] attachmentLifeDays Attachments life value.
	*/
	Q_INVOKABLE static
	void setAttachmentLifeDays(int attachmentLifeDays);

	/*!
	 * @brief Get download complete messages from global settings.
	 *
	 * @return True if it is enabled.
	*/
	Q_INVOKABLE static
	bool downloadCompleteMsgs(void);

	/*!
	 * @brief Set download complete messages to global settings.
	 *
	 * @param[in] downloadCompleteMsgs True if it is enabled.
	*/
	Q_INVOKABLE static
	void setDownloadCompleteMsgs(bool downloadCompleteMsgs);

	/*!
	 * @brief Get download only new messages from global settings.
	 *
	 * @return True if it is enabled.
	*/
	Q_INVOKABLE static
	bool downloadOnlyNewMsgs(void);

	/*!
	 * @brief Set download only new messages to global settings.
	 *
	 * @param[in] downloadOnlyNewMsgs True if it is enabled.
	*/
	Q_INVOKABLE static
	void setDownloadOnlyNewMsgs(bool downloadOnlyNewMsgs);

	/*!
	 * @brief Get database location from global settings.
	 *
	 * @return Database location string.
	*/
	Q_INVOKABLE static
	QString dbPath(void);

	/*!
	 * @brief Set new database location to global settings.
	 *
	 * @param[in] dbLocation New database location string.
	*/
	Q_INVOKABLE static
	void setDbPath(const QString &dbLocation);

	/*!
	 * @brief Get font size value from global settings.
	 *
	 * @return Font size value.
	*/
	Q_INVOKABLE static
	int fontSize(void);

	/*!
	 * @brief Set font size to global settings.
	 *
	 * @param[in] fontSize Font size value.
	 */
	Q_INVOKABLE static
	void setFontSize(int fontSize);

	/*!
	 * @brief Get font string from global settings.
	 *
	 * @return Font string.
	*/
	Q_INVOKABLE static
	QString font(void);

	/*!
	 * @brief Set font to global settings.
	 *
	 * @param[in] font Font identifier string.
	 */
	Q_INVOKABLE static
	void setFont(const QString &font);

	/*!
	 * @brief Get language string from global settings.
	 *
	 * @return Language string.
	*/
	Q_INVOKABLE static
	QString language(void);

	/*!
	 * @brief Set language to global settings.
	 *
	 * @param[in] language Language identifier string.
	 */
	Q_INVOKABLE static
	void setLanguage(const QString &language);

	/*!
	 * @brief Get message life from global settings.
	 *
	 * @return Message life value.
	*/
	Q_INVOKABLE static
	int messageLifeDays(void);

	/*!
	 * @brief Set messages life value to global settings.
	 *
	 * @param[in] messageLifeDays Messages life value.
	*/
	Q_INVOKABLE static
	void setMessageLifeDays(int messageLifeDays);

	/*!
	 * @brief Get zfo database size from global settings.
	 *
	 * @return Zfo db size value.
	*/
	Q_INVOKABLE static
	int zfoDbSizeMBs(void);

	/*!
	 * @brief Set zfo database size to global settings.
	 *
	 * @param[in] zfoDbSizeMBs Zfo database size value.
	*/
	Q_INVOKABLE static
	void setZfoDbSizeMBs(int zfoDbSizeMBs);

	/*!
	 * @brief Show set default database location button.
	 *
	 * @return True if show button.
	 */
	Q_INVOKABLE static
	bool showDefaultButton(void);

	/*!
	 * @brief Return current PIN value.
	 */
	Q_INVOKABLE static
	QString pinValue(void);

	/*!
	 * @brief Save PIN settings from QML.
	 *
	 * @param[in] pinValue PIN string.
	 */
	Q_INVOKABLE static
	void updatePinSettings(const QString &pinValue);

	/*!
	 * @brief Test if PIN is configured.
	 *
	 * @return True if PIN is configured.
	 */
	Q_INVOKABLE static
	bool pinConfigured(void);

	/*!
	 * @brief Verify PIN entered by user.
	 *
	 * @param[in] pinValue PIN string.
	 * @return True if PIN is valid.
	 */
	Q_INVOKABLE static
	bool pinValid(const QString &pinValue);

	/*!
	 * @brief Verify PIN entered by user and emit signal with result.
	 *
	 * @param[in] pinValue PIN string.
	 */
	Q_INVOKABLE
	void verifyPin(const QString &pinValue);

	/*!
	 * @brief Returns true if explicit clipboard operations should be used.
	 */
	Q_INVOKABLE static
	bool useExplicitClipboardOperations(void);

	/*!
	 * @brief Returns new database location string to QML.
	 *
	 * @param[in] currentLocation Current databases location.
	 * @param[in] setDefaultLocation Default databases location.
	 */
	Q_INVOKABLE static
	QString changeDbPath(const QString &currentLocation,
	    bool setDefaultLocation);

	/*!
	 * @brief Get inactivity timeout length.
	 *
	 * @return Inactivity interval in seconds.
	 */
	Q_INVOKABLE static
	int inactivityInterval(void);

	/*!
	 * @brief Set inactivity timeout length.
	 *
	 * @param[in] secs Inactivity interval length in seconds.
	 */
	Q_INVOKABLE static
	void setInactivityInterval(int secs);

	/*!
	 * @brief Last update string.
	 *
	 * @return String containing either date or time if date is today.
	 */
	static
	QString lastUpdateStr(void);

	/*!
	 * @brief Set last update time to current time.
	 */
	Q_INVOKABLE static
	void setLastUpdateToNow(void);

	/*!
	 * @brief Get records management url string.
	 *
	 * @return Records management url string.
	 */
	Q_INVOKABLE static
	QString rmUrl(void);

	/*!
	 * @brief Set records management url string.
	 *
	 * @param[in] rmUrl records management url string.
	 */
	Q_INVOKABLE static
	void setRmUrl(const QString &rmUrl);

	/*!
	 * @brief Get records management token string.
	 *
	 * @return Records management token string.
	 */
	Q_INVOKABLE static
	QString rmToken(void);

	/*!
	 * @brief Set records management token string.
	 *
	 * @param[in] rmToken records management token string.
	 */
	Q_INVOKABLE static
	void setRmToken(const QString &rmToken);

	/*!
	 * @brief Gathers all settings and stores them into configuration file.
	 *
	 * @param[in] accountModel Account model to take data from.
	 */
	Q_INVOKABLE static
	void saveAllSettings(const AccountListModel *accountModel);

	/*!
	 * @brief Return debug verbosity level.
	 */
	Q_INVOKABLE static
	int debugVerbosityLevel(void);

	/*!
	 * @brief Set debug verbosity level.
	 *
	 * @param[in] dVL log verbosity level.
	 */
	Q_INVOKABLE static
	void setDebugVerbosityLevel(int dVL);

	/*!
	 * @brief Return log verbosity level.
	 */
	Q_INVOKABLE static
	int logVerbosityLevel(void);

	/*!
	 * @brief Set log verbosity level.
	 *
	 * @param[in] lVL log verbosity level.
	 */
	Q_INVOKABLE static
	void setLogVerbosityLevel(int lVL);

	/*!
	 * @brief Check whether the application has the clean start parameter set.
	 *
	 * @return True if the value is set.
	 */
	bool cleanAppStart(void) const;

	/*!
	 * @brief Set whether the application has been freshly started.
	 *
	 * @param[in] val Boolean value.
	 */
	void setCleanAppStart(bool val);

	/*!
	 * @brief Get synchronise accounts after start from global settings.
	 *
	 * @return True if it is enabled.
	*/
	Q_INVOKABLE static
	bool syncAfterCleanAppStart(void);

	/*!
	 * @brief Set synchronise accounts after start to global settings.
	 *
	 * @param[in] syncAfterAppStart True if it is enabled.
	*/
	Q_INVOKABLE static
	void setSyncAfterCleanAppStart(bool syncAfterAppStart);

	/*!
	 * @brief Get amount of days to check the password expiration.
	 *
	 * @return Amount of days to check the password expiration.
	*/
	Q_INVOKABLE static
	int pwdExpirationDays(void);

	/*!
	 * @brief Set amount of days to check the password expiration.
	 *
	 * @param[in] days Amount of days to check the password expiration.
	*/
	Q_INVOKABLE static
	void setPwdExpirationDays(int days);

	/*!
	 * @brief Get amount of days to show message deletion notification.
	 *
	 * @return Amount of days to show message deletion notification.
	*/
	Q_INVOKABLE static
	int msgDeletionNotifyAheadDays(void);

	/*!
	 * @brief Get banner visibility remaining value.
	 *
	 * @return Number of remaining banner showing.
	*/
	Q_INVOKABLE static
	int bannerVisibilityCount(void);

	/*!
	 * @brief Set banner visibility remaining value.
	 *
	 * @param[in] cnt Number of remaining banner showing.
	*/
	Q_INVOKABLE static
	void setBannerVisibilityCount(int cnt);

signals:
	/*!
	 * @brief Send sync all accounts after startup to QML.
	 */
	void runSyncAfterCleanAppStartSig(void);

	/*!
	 * @brief Send get password expiration info after startup to QML.
	 */
	void runGetPasswordExpirationInfoSig(void);

	/*!
	 * @brief Send PIN verification result to QML.
	 *
	 * @param[in] success PIN comparison notification to QML.
	 */
	void sendPinReply(bool success);

	/*!
	 * @brief Set new status bar text and active busy indicator to QML.
	 *
	 * @param[in] txt Text message for status bar.
	 * @param[in] busy True means the status bar busy indicator is active
	 *                   and shown, false = disabled and hidden
	 */
	void statusBarTextChanged(QString txt, bool busy);

private:
	bool m_cleanAppStart; /*!< True if application has been started as it is. No ZFO viewing intended. */
};
