/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QApplication>
#include <QByteArray>
#include <QDateTime>
#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QQmlEngine> /* qmlRegisterType */
#include <QSettings>
#include <QStorageInfo>
#include <QStringBuilder>

#include "src/backup.h"
#include "src/datovka_shared/io/prefs_db.h"
#include "src/datovka_shared/io/records_management_db.h"
#include "src/datovka_shared/localisation/localisation.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/worker/pool.h"
#include "src/dialogues/dialogues.h"
#include "src/global.h"
#include "src/io/filesystem.h"
#include "src/messages.h"
#include "src/models/backup_selection_model.h"
#include "src/settings.h"
#include "src/setwrapper.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/settings/prefs_specific.h"
#include "src/sqlite/account_db.h"
#include "src/sqlite/dbs.h"
#include "src/sqlite/file_db_container.h"
#include "src/sqlite/message_db_container.h"
#include "src/sqlite/zfo_db.h"

#ifdef Q_COMPILER_RVALUE_REFS
#  define macroStdMove(x) ::std::move(x)
#else /* Q_COMPILER_RVALUE_REFS */
#  define macroStdMove(x) (x)
#endif /* Q_COMPILER_RVALUE_REFS */

#define JSON_FILE_SIZE_BYTES 1024
#define APP_VARIANT "mobile"

#define KILOBYTE Q_INT64_C(1024)
#define MEGABYTE Q_INT64_C(1048576)
#define GIGABYTE Q_INT64_C(1073741824)
#define TERABYTE Q_INT64_C(1099511627776)

#ifdef Q_OS_IOS
static
void exportFilesiOS(const QList<IosHelper::FileICloud> &iCloudFiles)
{
	if (Dialogues::YES == Dialogues::message(Dialogues::QUESTION,
	        BackupRestoreData::tr("Backup finished"),
	        BackupRestoreData::tr("Data were exported into the application sandbox. Now you can transfer the exported files to iCloud or use the iTunes application to transfer the backed up files to your Mac or PC manually."),
	        BackupRestoreData::tr("Do you want to upload the files to the application iCloud container?"),
	        Dialogues::YES | Dialogues::NO, Dialogues::NO)) {
		IosHelper::storeFilesToCloud(iCloudFiles);
	} else {
		Dialogues::message(Dialogues::INFORMATION,
		    BackupRestoreData::tr("Backup finished"),
		    BackupRestoreData::tr("Exported backup files are available in the application sandbox. Use the iTunes application in your computer to transfer the exported files to your Mac or PC manually. All exported files will automatically be deleted when you start this application again."),
		    BackupRestoreData::tr("Preserve the backup in a safe place as it contains private data."));
	}
}
#endif

/*!
 * @brief Compute approximate size string including units.
 *
 * @param[in] size Size in bytes.
 * @return String with unit.
 */
static
QString sizeString(qint64 size)
{
	qint64 divider = 1;
	QString unit("B"); /* Bytes. */

	if (Q_UNLIKELY(size < 0)) {
		return BackupRestoreData::tr("unknown");
	} else if (size > TERABYTE) {
		divider = TERABYTE;
		unit = QStringLiteral("TB");
	} else if (size > GIGABYTE) {
		divider = GIGABYTE;
		unit = QStringLiteral("GB");
	} else if (size > MEGABYTE) {
		divider = MEGABYTE;
		unit = QStringLiteral("MB");
	} else if (size > KILOBYTE) {
		divider = KILOBYTE;
		unit = QStringLiteral("kB");
	} else {
		/* Bytes. */
	}

	qint64 whole = size / divider;
	qint64 decimal = 0;

	if (divider > 1) {
		decimal = size % divider; /* Remainder. */
		decimal = (10 * decimal) / divider; /* One decimal digit. */
	}

	return QString::number(whole) %
	    ((decimal > 0) ? (Localisation::programLocale.decimalPoint() + QString("%1").arg(decimal, 1, 10, QChar('0'))) : QString()) %
	    QStringLiteral(" ") % unit;
}

/*!
 * @brief Compute file checksum.
 *
 * @param[in] filePath File path.
 * @param[in] algorithm Algorithm to use for the checksum.
 * @return Checksum, null checksum on failure.
 */
static
Json::Backup::Checksum computeFileChecksum(const QString &filePath,
    enum Json::Backup::Checksum::Algorithm algorith = Json::Backup::Checksum::ALG_SHA512)
{
	QFile f(filePath);
	if (f.open(QFile::ReadOnly)) {
		return Json::Backup::Checksum::computeChecksum(&f, algorith);
	}
	return Json::Backup::Checksum();
}

/*!
 * @brief Copy file to application sandbox.
 *
 * @param[in] sourceDir File path.
 * @param[in] fileName File name.
 * @return True if success.
 */
static
bool restoreFile(const QString &sourceDir, const QString &fileName) {

	QString targetPath(joinDirs(GlobalSettingsQmlWrapper::dbPath(),
	    fileName));
	QString sourcePath(joinDirs(sourceDir, fileName));

	if (QFile::exists(targetPath)) {
		QFile::remove(targetPath);
	}
	QFile f(sourcePath);
	if (f.copy(targetPath)) {
		logInfoNL("File '%s' has been successfully imported.",
		    fileName.toUtf8().constData());
	} else {
		logErrorNL("Import of file '%s' failed.",
		    fileName.toUtf8().constData());
		return false;
	}

	return true;
}

/*!
 * @brief Compute file size.
 *
 * @param[in] filePath File path.
 * @return File size in bytes.
 */
static inline
qint64 getFileSizeInBytes(const QString &filePath)
{
	return QFileInfo(filePath).size();
}

/*!
 * @brief Read JSON file content.
 *
 * @param[in] filePath File path.
 * @return Backup, null value on error.
 */
static
Json::Backup backupFromJsonFile(const QString &filePath)
{
	QByteArray jsonData;
	{
		QFile jsonFile(filePath);
		if (Q_UNLIKELY(!jsonFile.open(QIODevice::ReadOnly | QIODevice::Text))) {
			return Json::Backup();
		}
		jsonData = jsonFile.readAll();
		jsonFile.close();
	}
	if (jsonData.isEmpty()) {
		return Json::Backup();
	}
	return Json::Backup::fromJson(jsonData);
}

/*!
 * @brief Fill application data into the backup entry.
 *
 * @param[in,out] bu Backup description structure.
 * @param[in]     dateTime Time information.
 */
static
void fillAppData(Json::Backup &bu, const QDateTime &dateTime)
{
	bu.setDateTime(dateTime);
	bu.setAppInfo(Json::Backup::AppInfo(
	    QString(APP_NAME), QString(APP_VARIANT), QString(VERSION)));
}

/*!
 * @brief Constructs backup-transfer directory name.
 *
 * @param[in] backupType Backup type.
 * @param[in] dateTime Time information.
 */
static
QString backupDirName(enum BackupRestoreData::BackupRestoreType backupType,
    const QDateTime &dateTime)
{
	QString backupName;

	switch (backupType) {
	case BackupRestoreData::BRT_BACKUP:
		backupName = DATOVKA_BACK_DIR_NAME;
		break;
	case BackupRestoreData::BRT_TRANSFER:
		backupName = DATOVKA_TRAN_DIR_NAME;
		break;
	default:
		Q_ASSERT(0);
		return QString();
		break;
	}

	QString dirName(backupName % QStringLiteral("_")
	    % QStringLiteral(APP_NAME) % QStringLiteral("-") % QStringLiteral("mobile")
	    % QStringLiteral("_") % dateTime.toString(Qt::ISODate));

	/*
	 * Android 11 refuses to access directories and files with a colon ':'
	 * in path.
	 * https://forum.syncthing.net/t/permission-denied-on-files-with-colon-on-android-11-permission-denied-on-syncthing-tmp-files-on-android/16096
	 * https://stackoverflow.com/questions/2679699/what-characters-allowed-in-file-names-on-android
	 */
	return dirName.replace(":", "-");
}

/*!
 * @brief Constructs full path to backup/transfer directory.
 *
 * @param[in] targetPath Path where the directory should be created.
 * @param[in] backupType Backup type.
 * @param[in] dateTime Time information.
 * @return Full path.
 */
QString backupDirPath(const QString &targetPath,
    enum BackupRestoreData::BackupRestoreType backupType,
    const QDateTime &dateTime)
{
	const QString subdir = backupDirName(backupType, dateTime);
	QDir dir(joinDirs(targetPath, subdir));
	if ((!dir.exists()) && (!dir.mkpath("."))) {
		logErrorNL("Cannot create or access directory '%s'.",
		    dir.absolutePath().toUtf8().constData());
		return targetPath;
	}

	return dir.path();
}

void BackupRestoreData::declareQML(void)
{
	qmlRegisterType<BackupRestoreData>("cz.nic.mobileDatovka.qmlInteraction", 1, 0, "BackupRestoreData");
	qRegisterMetaType<BackupRestoreData>("BackupRestoreData");
	qRegisterMetaType<BackupRestoreData::BackupRestoreType>("BackupRestoreData::BackupRestoreType");
}

BackupRestoreData::BackupRestoreData(QObject *parent)
    : QObject(parent),
    m_jsonPath(),
    m_loadedBackup()
{
}

BackupRestoreData::BackupRestoreData(const BackupRestoreData &other)
    : QObject(),
    m_jsonPath(other.m_jsonPath),
    m_loadedBackup(other.m_loadedBackup)
{
}

void BackupRestoreData::fillBackupModel(BackupRestoreSelectionModel *acntModel)
{
	debugSlotCall();

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (GlobInstcs::accountDbPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(acntModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access backup model.");
		Q_ASSERT(0);
		return;
	}

	QList<AcntId> acntIdList = GlobInstcs::acntMapPtr->keys();
	foreach (const AcntId &acntId, acntIdList) {
		acntModel->appendData(false,
		    (*GlobInstcs::acntMapPtr)[acntId].accountName(),
		    acntId, GlobInstcs::accountDbPtr->dbId(acntId.username()),
		    QString());
	}
}

void BackupRestoreData::fillRestoreModel(BackupRestoreSelectionModel *acntModel)
{
	debugSlotCall();

	if (Q_UNLIKELY(acntModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access backup model.");
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(!m_loadedBackup.isValid())) {
		logErrorNL("Cannot open or read file '%s'.",
		    m_jsonPath.toUtf8().constData());
		emit actionTextSig(
		    tr("Cannot open or read file '%1'.").arg(m_jsonPath));
		return;
	}

	if (m_loadedBackup.zfoDb().isValid()) {
		emit visibleZfoSwitchSig(true);
	}

	foreach (const Json::Backup::MessageDb &mbu, m_loadedBackup.messageDbs()) {
		acntModel->appendData(false, mbu.accountName(),
		    AcntId(mbu.username(), mbu.testing()),
		    mbu.boxId(), mbu.subdir());
	}

	emit actionTextSig(
	    tr("Select accounts from the backup JSON file which you want to restore. Data of existing accounts will be replaced by data from the backup."));
}

bool BackupRestoreData::backUpAccount(const QmlAcntId *qAcntId,
    const QString &targetPath, bool includeZfoDb)
{

	if (Q_UNLIKELY((qAcntId == Q_NULLPTR) || (!qAcntId->isValid()))) {
		Q_ASSERT(0);
		return false;
	}

	QList<AcntId> acntIdList;
	acntIdList.append(*qAcntId);
	return backUpAccounts(acntIdList, targetPath, includeZfoDb);
}

bool BackupRestoreData::backUpSelectedAccounts(
    BackupRestoreSelectionModel *acntModel, const QString &targetPath,
    bool includeZfoDb)
{
	debugSlotCall();

	if (Q_UNLIKELY(acntModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access backup model.");
		Q_ASSERT(0);
		return false;
	}

	return backUpAccounts(acntModel->selectedAccountIds(), targetPath,
	    includeZfoDb);
}

bool BackupRestoreData::canBackUpAccount(const QmlAcntId *qAcntId,
    const QString &targetPath, bool includeZfoDb, bool transfer)
{
	if (Q_UNLIKELY((qAcntId == Q_NULLPTR) || (!qAcntId->isValid()))) {
		Q_ASSERT(0);
		return false;
	}

	QList<AcntId> acntIdList;
	acntIdList.append(*qAcntId);
	return canBackUpAccounts(acntIdList, targetPath, includeZfoDb,
	    transfer);
}

bool BackupRestoreData::canBackupSelectedAccounts(
    BackupRestoreSelectionModel *acntModel, const QString &targetPath,
    bool includeZfoDb, bool transfer)
{
	if (Q_UNLIKELY(acntModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access backup model.");
		Q_ASSERT(0);
		return false;
	}

	return canBackUpAccounts(acntModel->selectedAccountIds(), targetPath,
	   includeZfoDb, transfer);
}

static
qint64 sizeMessageDbs(const QList<Json::Backup::MessageDb> &mds,
    const QString &dirPath, bool takeFromJson)
{
	qint64 sum = 0;

	foreach (const Json::Backup::MessageDb &md, mds) {
		const QString filePathPref(dirPath + QDir::separator() + md.subdir() + QDir::separator());
		foreach (const Json::Backup::File &f, md.files()) {
			if (takeFromJson) {
				if (f.size() >= 0) {
					sum += f.size();
				} else {
					logWarningNL("Missing size information for file '%s'.",
					    f.fileName().toUtf8().constData());
				}
				continue;
			}

			const QString filePath(filePathPref + f.fileName());
			QFileInfo fi(filePath);
			if (fi.isFile()) {
				sum += fi.size();
			} else {
				logErrorNL("File '%s' does not exist.\n",
				    filePath.toUtf8().constData());
			}
		}
	}

	return sum;
}

static
bool haveMessageDbFiles(const QList<Json::Backup::MessageDb> &mds,
    const QString &dirPath)
{
	bool allPresent = true;

	foreach (const Json::Backup::MessageDb &md, mds) {
		const QString filePathPref(dirPath + QDir::separator() + md.subdir() + QDir::separator());
		foreach (const Json::Backup::File &f, md.files()) {
			if (Q_UNLIKELY(!f.isValid())) {
				continue; /* Ignore invalid entries. */
			}
			const QString filePath(filePathPref + f.fileName());
			QFileInfo fi(filePath);
			if (Q_UNLIKELY(!fi.isFile())) {
				logErrorNL("File '%s' does not exist.\n",
				    filePath.toUtf8().constData());
				allPresent = false;
			}
		}
	}

	return allPresent;
}

static
qint64 sizeFile(const Json::Backup::File &f, const QString &dirPath,
    bool takeFromJson)
{
	if (Q_UNLIKELY(f.fileName().isEmpty())) {
		return 0;
	}

	if (takeFromJson) {
		if (f.size() >= 0) {
			return f.size();
		} else {
			logWarningNL("Missing size information for file '%s'.",
			    f.fileName().toUtf8().constData());
			return 0;
		}
	}

	const QString filePath(dirPath + QDir::separator() + f.fileName());
	QFileInfo fi(filePath);
	if (fi.isFile()) {
		return fi.size();
	} else {
		logErrorNL("File '%s' does not exist.\n",
		    filePath.toUtf8().constData());
		return 0;
	}
}

static
bool haveFile(const Json::Backup::File &f, const QString &dirPath)
{
	if (Q_UNLIKELY(!f.isValid())) {
		return true; /* Ignore invalid entries. */
	}

	const QString filePath(dirPath + QDir::separator() + f.fileName());
	QFileInfo fi(filePath);
	if (Q_UNLIKELY(!fi.isFile())) {
		logErrorNL("File '%s' does not exist.\n",
		    filePath.toUtf8().constData());
		return false;
	}

	return true;
}

static
qint64 sizeFileList(const QList<Json::Backup::File> &fl, const QString &dirPath,
    bool takeFromJson)
{
	qint64 sum = 0;

	foreach (const Json::Backup::File &f, fl) {
		sum += sizeFile(f, dirPath, takeFromJson);
	}

	return sum;
}

static
bool haveFileList(const QList<Json::Backup::File> &fl, const QString &dirPath)
{
	bool haveAll = true;

	foreach (const Json::Backup::File &f, fl) {
		if (Q_UNLIKELY(!haveFile(f, dirPath))) {
			haveAll = false;
		}
	}

	return haveAll;
}

static
qint64 storedFilesSize(const Json::Backup &bu, const QString &dirPath,
    bool takeFromJson)
{
	qint64 sum = 0;

	sum += sizeMessageDbs(bu.messageDbs(), dirPath, takeFromJson);
	sum += sizeFile(bu.accountDb(), dirPath, takeFromJson);
	sum += sizeFile(bu.zfoDb(), dirPath, takeFromJson);
	sum += sizeFileList(bu.files(), dirPath, takeFromJson);

	return sum;
}

static
bool haveFiles(const Json::Backup &bu, const QString &dirPath)
{
	return haveMessageDbFiles(bu.messageDbs(), dirPath) &&
	    haveFile(bu.accountDb(), dirPath) &&
	    haveFile(bu.zfoDb(), dirPath) &&
	    haveFileList(bu.files(), dirPath);
}

static
bool isValidFileForTransfer(const QFileInfo &fi)
{
	return (fi.suffix() == "conf" || fi.suffix() == "db");
}

/*!
 * @brief Compute the total sum of sizes of all files in directory.
 *
 * @param[in] dirPath Target path for backup.
 * @return Account backup data size in bytes.
 */
static
qint64 dirContentSize(const QString &dirPath)
{
	if (Q_UNLIKELY(dirPath.isEmpty())) {
		Q_ASSERT(0);
		return 0;
	}

	qint64 backupSize = 0;

	QDir dir(dirPath);
	QDirIterator iterator(dir.absolutePath(), QDirIterator::Subdirectories);
	while (iterator.hasNext()) {
		QFile file(iterator.next());
		QFileInfo fi(file.fileName());
		if (fi.isFile()) {
			backupSize += fi.size();
		}
	}

	return backupSize;
}

enum BackupRestoreData::BackupRestoreType
    BackupRestoreData::loadBackupJson(const QString &jsonPath)
{
	debugSlotCall();

	QString jsonDirPath; /* Full path to file directory. */
	{
		QFileInfo fi(jsonPath);
		if (fi.suffix() != QStringLiteral("json")) {
			logErrorNL("File '%s' is not a JSON file.",
			    jsonPath.toUtf8().constData());
			emit actionTextSig(
			    tr("File '%1' is not a JSON file.").arg(jsonPath));
			return BRT_UNKNOWN;
		}
		jsonDirPath = fi.absolutePath();
	}

	m_jsonPath = jsonPath;
	m_loadedBackup = backupFromJsonFile(m_jsonPath);
	if (Q_UNLIKELY(!m_loadedBackup.isValid())) {
		logErrorNL("Cannot open or read file '%s'.",
		    jsonPath.toUtf8().constData());
		emit actionTextSig(
		    tr("Cannot open or read file '%1'.").arg(jsonPath));
		return BRT_UNKNOWN;
	}

	qint64 storedSize = storedFilesSize(m_loadedBackup, jsonDirPath, true);

	if ((m_loadedBackup.appInfo().appName() != QStringLiteral(APP_NAME)) ||
	    (m_loadedBackup.appInfo().appVariant() != QStringLiteral(APP_VARIANT))) {
		logErrorNL("JSON file '%s' does not contain valid application information.",
		    jsonPath.toUtf8().constData());
		emit actionTextSig(
		    tr("JSON file '%1' does not contain valid application information.").arg(jsonPath));
		return BRT_UNKNOWN;
	}

	QDateTime dateTime = m_loadedBackup.dateTime();

	enum BackupRestoreType restoreType = BRT_UNKNOWN;
	switch (m_loadedBackup.type()) {
	case Json::Backup::BUT_BACKUP:
		restoreType = BRT_BACKUP;
		if (dateTime.isValid()) {
			emit bacupDateTextSig(
			    tr("Backup was taken at %1.").arg(dateTime.toString(DATETIME_QML_FORMAT)));
		}
		break;
	case Json::Backup::BUT_TRANSFER:
		restoreType = BRT_TRANSFER;
		if (dateTime.isValid()) {
			emit bacupDateTextSig(
			    tr("Transfer image was taken at %1.").arg(dateTime.toString(DATETIME_QML_FORMAT)));
		}
		emit canRestoreFromTransferSig(
		    canRestoreFromTransfer(storedSize));
		break;
	default:
		logErrorNL("%s", "Unknown backup type.");
		emit actionTextSig(
		    tr("Unknown backup type. Selected JSON file contains no valid backup data."));
		break;
	}

	return restoreType;
}

/*!
 * @brief Ask whether the proceed with the restoration process.
 *
 * @param[in] backupType Backup type.
 * @return True if confirm.
 */
static
bool askConfirmation(enum BackupRestoreData::BackupRestoreType backupType)
{
	QString text;
	QString detailText;

	switch (backupType) {
	case BackupRestoreData::BRT_BACKUP:
		text = BackupRestoreData::tr("The action will continue with the restoration of the selected accounts. Current data of the selected accounts will be completely rewritten.");
		detailText= BackupRestoreData::tr("Do you wish to restore the selected accounts from the backup?");
		break;
	case BackupRestoreData::BRT_TRANSFER:
		text = BackupRestoreData::tr("The action will continue with the restoration of the complete application data. Current application data will be completely rewritten.");
		detailText= BackupRestoreData::tr("Do you wish to restore the application data from the transfer?");
		break;
	default:
		Q_ASSERT(0);
		return false;
		break;
	}

	return (Dialogues::YES == Dialogues::message(Dialogues::QUESTION,
	    BackupRestoreData::tr("Proceed?"), text, detailText, Dialogues::NO | Dialogues::YES,
	    Dialogues::YES));
}

bool BackupRestoreData::restoreDataFromTransfer(void)
{
	debugSlotCall();

	QString jsonDirPath; /* Full path to file directory. */
	{
		QFileInfo fi(m_jsonPath);
		if (fi.suffix() != QStringLiteral("json")) {
			logErrorNL("File '%s' is not a JSON file.",
			    m_jsonPath.toUtf8().constData());
			emit actionTextSig(
			    tr("File '%1' is not a JSON file.").arg(m_jsonPath));
			return false;
		}
		jsonDirPath = fi.absolutePath();
	}

	if (Q_UNLIKELY(!m_loadedBackup.isValid())) {
		logErrorNL("Cannot open or read file '%s'.",
		    m_jsonPath.toUtf8().constData());
		emit actionTextSig(
		    tr("Cannot open or read file '%1'.").arg(m_jsonPath));
		return false;
	}

	if (!askConfirmation(BackupRestoreType::BRT_TRANSFER)) {
		return false;
	}

	bool success = true;

	foreach (const Json::Backup::File &file, m_loadedBackup.files()) {
		QString sourceFilePath(joinDirs(jsonDirPath, file.fileName()));
		if (file.checksum().isValid()) {
			const QString sourceFilePath(joinDirs(jsonDirPath, file.fileName()));
			if (file.checksum() != computeFileChecksum(sourceFilePath, file.checksum().algorithm())) {
				success = false;
				logErrorNL("Checksum of file '%s' does not match.",
				    file.fileName().toUtf8().constData());
				break;
			}
		} else {
			logWarningNL(
			    "Invalid checksum. Skipping checksum check on file '%s'.",
			    file.fileName().toUtf8().constData());
		}

		emit actionTextSig(tr("Importing file '%1'.").arg(file.fileName()));
		QCoreApplication::processEvents();
		success = success && restoreFile(jsonDirPath, file.fileName());
	}

	if (success) {
		logInfoNL("%s", "All data were restored successfully.");
		emit actionTextSig(tr("Restoration finished."));

#ifdef Q_OS_IOS
		if (Dialogues::YES == Dialogues::message(Dialogues::INFORMATION,
			tr("Restoration finished"),
			tr("All application data have successfully been restored. The application will restart in order to load the imported files."),
			tr("Do you want to delete the source transfer folder from the application sandbox before the restart (recommended)?"),
			Dialogues::YES | Dialogues::NO, Dialogues::YES)) {
				IosHelper::clearRestoreFolder(jsonDirPath);
		}
#else
		Dialogues::errorMessage(Dialogues::INFORMATION,
		    tr("Restoration finished"),
		    tr("All application data have successfully been restored."),
		    tr("The application will restart in order to load the imported files."));
#endif

		GlobInstcs::quitWithoutSaveSettings = true;
		QApplication::quit();

	} else {
		QDir(GlobalSettingsQmlWrapper::dbPath()).removeRecursively();
		logErrorNL("%s", "Application data restoration has been cancelled.");
		Dialogues::errorMessage(Dialogues::CRITICAL,
		    tr("Restoration error"),
		    tr("An error during data restoration has occurred."),
		    tr("Application data restoration has been cancelled."));
	}

	return success;
}

bool BackupRestoreData::restoreSelectedAccounts(
    BackupRestoreSelectionModel *acntModel, const QString &sourcePath,
    bool includeZfoDb)
{
	debugSlotCall();

	if (Q_UNLIKELY(acntModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access backup model.");
		Q_ASSERT(0);
		return false;
	}

	if (!askConfirmation(BackupRestoreType::BRT_BACKUP)) {
		return false;
	}

	bool success = true;
	QString file;
	QFileInfo fi(sourcePath);
	QString sourceDir = fi.absolutePath();
	QList<AcntId> acntIdList = acntModel->selectedAccountIds();

	/* Restore selected accounts from backup location. */
	foreach (const AcntId &acntId, acntIdList) {
		BackupRestoreSelectionModel::Entry item =
		    acntModel->accountEntry(acntId);
		success = success && restoreAccount(acntId,
		    item.accountName, item.subdir, sourceDir);
	}

	/*
	 * Restore account database from backup location
	 * if previous actions were successful.
	 */
	if (success && m_loadedBackup.accountDb().isValid()) {
		if ((GlobInstcs::accountDbPtr != Q_NULLPTR)) {
			GlobInstcs::accountDbPtr->closeDb();
		}
		emit actionTextSig(tr("Importing file '%1'.").arg(m_loadedBackup.accountDb().fileName()));
		success = success && restoreFile(sourceDir, m_loadedBackup.accountDb().fileName());
	}

	/*
	 * Restore zfo database from backup location
	 * if previous actions were successful.
	 */
	if (includeZfoDb && success && m_loadedBackup.zfoDb().isValid()) {
		if ((GlobInstcs::zfoDbPtr != Q_NULLPTR)) {
			GlobInstcs::zfoDbPtr->closeDb();
		}
		emit actionTextSig(tr("Importing file '%1'.").arg(m_loadedBackup.zfoDb().fileName()));
		QCoreApplication::processEvents();
		success = success && restoreFile(sourceDir, m_loadedBackup.zfoDb().fileName());
	}

	if (success) {
		logInfoNL("%s", "All selected accounts were successfully restored.");
		emit actionTextSig(tr("Restoration finished."));

#ifdef Q_OS_IOS
		if (Dialogues::YES == Dialogues::message(Dialogues::INFORMATION,
			tr("Restoration finished"),
			tr("All selected account data have successfully been restored. The application will restart in order to load the imported files."),
			tr("Do you want to delete the source backup folder from the application sandbox before the restart (recommended)?"),
			Dialogues::YES | Dialogues::NO, Dialogues::YES)) {
				IosHelper::clearRestoreFolder(sourceDir);
		}
#else
		Dialogues::errorMessage(Dialogues::INFORMATION,
		    tr("Restoration finished"),
		    tr("All selected account data have successfully been restored."),
		    tr("The application will restart in order to load the imported files."));

#endif

		QApplication::quit();

	} else {
		logErrorNL("%s", "Account data restoration has been cancelled.");
		Dialogues::errorMessage(Dialogues::CRITICAL,
		    tr("Restoration error"),
		    tr("An error during data restoration has occurred."),
		    tr("Application data restoration has been cancelled."));
	}

	return success;
}

bool BackupRestoreData::canRestoreSelectedAccounts(
    BackupRestoreSelectionModel *acntModel, const QString &srcJsonPath,
    bool includeZfoDb)
{
	debugSlotCall();

	if (Q_UNLIKELY(acntModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access backup model.");
		Q_ASSERT(0);
		return false;
	}

	bool canRestore = false;
	QFileInfo fi(srcJsonPath);
	QString sourceDir = fi.absolutePath();
	qint64 backupSize =
	    getFileSizeInBytes(joinDirs(sourceDir, m_loadedBackup.accountDb().fileName()));
	qint64 targetSize =
	    computeAvailableSpace(GlobalSettingsQmlWrapper::dbPath());

	QList<AcntId> acntIdList = acntModel->selectedAccountIds();

	foreach (const AcntId &acntId, acntIdList) {
		const QString subdir = acntModel->accountEntry(acntId).subdir;
		backupSize += dirContentSize(joinDirs(sourceDir, subdir));
	}

	if (includeZfoDb) {
		backupSize += getFileSizeInBytes(joinDirs(sourceDir,
		    m_loadedBackup.zfoDb().fileName()));
	}

	canRestore = (backupSize < targetSize);

	if (!canRestore && targetSize >= 0) {
		logErrorNL("%s", "There is not enough space in the selected storage.");
		emit actionTextSig(tr("There is not enough space in the selected storage."));
	}

	sendSizeInfoToQml(backupSize, targetSize);

	if (acntIdList.isEmpty()) {
		return false;
	}

	if (!haveFiles(m_loadedBackup, sourceDir)) {
		emit actionTextSig(tr("One or more files mentioned in JSON file are missing or inaccessible."));
		return false;
	}

	return canRestore;
}

void BackupRestoreData::stopWorker(bool stop)
{
	debugSlotCall();

	if (Q_UNLIKELY(GlobInstcs::workPoolPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	if (stop) {
		logInfoNL("%s", "Waiting for pending worker threads.");
		GlobInstcs::workPoolPtr->wait();
		GlobInstcs::workPoolPtr->stop();
		logInfoNL("%s", "Worker pool stopped.");
	} else {
		GlobInstcs::workPoolPtr->start();
		logInfoNL("%s", "Worker pool started.");
	}
}

/*!
 * @brief Create JSON transfer content.
 *
 * @param[in] currentDateTime Backup date time.
 * @param[in] files List of files and their checksums.
 * @return Json content.
 */
static
QByteArray createTransferJson(const QDateTime &currentDateTime,
    const QList<Json::Backup::File> &files)
{
	Json::Backup bu;
	bu.setType(Json::Backup::BUT_TRANSFER);
	fillAppData(bu, currentDateTime);

	bu.setFiles(files);
	return bu.toJson();
}

/*!
 * @brief Remove target folder and show error message box.
 *
 * @param[in] targetDir Backup target folder.
 * @return Return always false.
 */
static
bool errorBackup(const QString &targetDir)
{
	QDir(targetDir).removeRecursively();
	logErrorNL("%s", "Application data backup has been cancelled.");
	Dialogues::errorMessage(Dialogues::CRITICAL,
	    BackupRestoreData::tr("Backup error"),
	    BackupRestoreData::tr("An error occurred when backing up data to target location. Backup has been cancelled."),
	    BackupRestoreData::tr("Application data were not backed up to target location."));
	return false;
}

bool BackupRestoreData::transferCompleteDatovkaData(const QString &targetPath)
{
	debugSlotCall();

	if (Q_UNLIKELY(GlobInstcs::setPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	QCoreApplication::processEvents();

	if (!GlobInstcs::setPtr->pinConfigured()) {
		Dialogues::errorMessage(Dialogues::WARNING,
		    tr("Transfer problem"),
		    tr("The data transfer requires application data to be secured with a PIN."),
		    tr("Set a PIN in the application settings and try again."));
		return false;
	}

	QDir dir(GlobalSettingsQmlWrapper::dbPath());
	QDirIterator iterator(dir.absolutePath(), QDirIterator::Subdirectories);
	const QDateTime currentDateTime = QDateTime::currentDateTime();
	QString targetDir;

#ifdef Q_OS_IOS
	Q_UNUSED(targetPath);
	targetDir = backupDirPath(appTransferDirPath(),
	    BackupRestoreData::BRT_TRANSFER, currentDateTime);
#else
	targetDir = backupDirPath(targetPath,
	    BackupRestoreData::BRT_TRANSFER, currentDateTime);
#endif

	QList<Json::Backup::File> files;
	QList<IosHelper::FileICloud> iCloudFiles;
	QString iCloudPath = joinDirs(QStringLiteral(DATOVKA_TRAN_DIR_NAME),
	    backupDirName(BackupRestoreData::BRT_TRANSFER, currentDateTime));

	/* Save all application files. */
	while (iterator.hasNext()) {

		QFile f(iterator.next());
		QFileInfo fi(f.fileName());
		if (fi.isFile()) {

			if (!isValidFileForTransfer(fi)) {
				continue;
			}

			QString filePath(joinDirs(targetDir, fi.fileName()));
			emit actionTextSig(tr("Transferring file '%1'.").arg(fi.fileName()));
			QCoreApplication::processEvents();

			if (f.copy(filePath)) {
				logInfoNL("File '%s' has been successfully transferred.",
				    fi.fileName().toUtf8().constData());
				Json::Backup::File file(fi.fileName(), computeFileChecksum(filePath), fi.size());
				IosHelper::FileICloud iCloudFile;
				iCloudFile.srcFilePath = filePath;
				iCloudFile.destiCloudPath = iCloudPath;
				iCloudFiles.append(iCloudFile);
				if (fi.suffix() == QStringLiteral("conf")) {
					files.prepend(file);
				} else {
					files.append(file);
				}
			} else {
				logErrorNL("Transfer of file '%s' failed.",
				    fi.fileName().toUtf8().constData());
				return errorBackup(targetDir);
			}

			QCoreApplication::processEvents();
		}
	}

	/* Create and save transfer json file */
	QString jsonFilePath = writeFile(targetDir,
	    QStringLiteral("transfer_description.json"),
	    createTransferJson(currentDateTime, files), true);
	if (!jsonFilePath.isEmpty()) {
		IosHelper::FileICloud iCloudFile;
		iCloudFile.srcFilePath = jsonFilePath;
		iCloudFile.destiCloudPath = iCloudPath;
		iCloudFiles.append(iCloudFile);
	} else {
		return errorBackup(targetDir);
	}

	/* Show final notification */
	emit actionTextSig(tr("Transfer finished."));
	logInfoNL("%s", "All application data have been successfully written to transfer.");

#ifdef Q_OS_IOS
	/* Upload files tu iCloud if needed */
	exportFilesiOS(iCloudFiles);
	return true;
#endif
	Dialogues::errorMessage(Dialogues::INFORMATION,
	    tr("Transfer finished"),
	    tr("Application data were transferred successfully to target location."),
	    tr("Preserve the transfer data in a safe place as it contains personal data."));

	return true;
}

Json::Backup::MessageDb BackupRestoreData::backUpOneAccount(
    const AcntId &acntId, const QString &targetPath, const QDateTime &dateTime,
    QList<IosHelper::FileICloud> &iCloudFiles)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::prefsPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return Json::Backup::MessageDb();
	}

	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return Json::Backup::MessageDb();
	}

	const QString boxId = GlobInstcs::accountDbPtr->dbId(acntId.username());
	if (boxId.isEmpty()) {
		logErrorNL("Backup: data-box ID missing for username '%s'.",
		    acntId.username().toUtf8().constData());
		return Json::Backup::MessageDb();
	}

	Json::Backup::MessageDb mbe;

	mbe.setTesting(acntId.testing());
	mbe.setAccountName((*GlobInstcs::acntMapPtr)[acntId].accountName());
	mbe.setBoxId(boxId);
	mbe.setUsername(acntId.username());
	const QString subdir(boxId + QStringLiteral("_") + acntId.username());
	mbe.setSubdir(subdir);
	QDir dir(joinDirs(targetPath, subdir));
	if ((!dir.exists()) && (!dir.mkpath("."))) {
		logErrorNL("Cannot create or access subdirectory '%s'.",
		    dir.absolutePath().toUtf8().constData());
		return Json::Backup::MessageDb();
	}

	QString iCloudPath = joinDirs(joinDirs(QStringLiteral(DATOVKA_BACK_DIR_NAME),
	    backupDirName(BackupRestoreData::BRT_BACKUP, dateTime)),
	    subdir);

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
	    PrefsSpecific::dataOnDisk(
		*GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[acntId]));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return Json::Backup::MessageDb();
	}

	QFileInfo fi(msgDb->fileName());
	QString fileName = fi.fileName();
	emit actionTextSig(tr("Backing up file '%1'.").arg(fileName));
	QCoreApplication::processEvents();

	QList<Json::Backup::File> fileList;
	if (msgDb->copyDb(joinDirs(dir.path(), fileName))) {
		fileList.append(Json::Backup::File(fileName, Json::Backup::Checksum(), fi.size()));
		IosHelper::FileICloud iCloudFile;
		iCloudFile.srcFilePath = joinDirs(dir.path(), fileName);
		iCloudFile.destiCloudPath = iCloudPath;
		iCloudFiles.append(iCloudFile);
	} else {
		return Json::Backup::MessageDb();
	}

	FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[acntId]));
	if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
		logErrorNL("Cannot access file database for username '%s'.",
		    acntId.username().toUtf8().constData());
		return Json::Backup::MessageDb();
	}

	fi = QFileInfo(fDb->fileName());
	fileName = fi.fileName();
	emit actionTextSig(tr("Backing up file '%1'.").arg(fileName));
	QCoreApplication::processEvents();

	if (fDb->copyDb(joinDirs(dir.path(), fileName))) {
		fileList.append(Json::Backup::File(fileName, Json::Backup::Checksum(), fi.size()));
		IosHelper::FileICloud iCloudFile;
		iCloudFile.srcFilePath = joinDirs(dir.path(), fileName);
		iCloudFile.destiCloudPath = iCloudPath;
		iCloudFiles.append(iCloudFile);
	} else {
		return Json::Backup::MessageDb();
	}

	mbe.setFiles(macroStdMove(fileList));

	return mbe;
}

qint64 BackupRestoreData::getAccountBackupSizeBytes(const AcntId &acntId)
{
	qint64 size = 0;

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::prefsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return 0;
	}

	if (!acntId.isValid()) {
		return 0;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
	    PrefsSpecific::dataOnDisk(
		*GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[acntId]));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("Cannot access message database for username '%s'.",
		    acntId.username().toUtf8().constData());
		return 0;
	}
	size += msgDb->getDbSizeInBytes();

	FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[acntId]));
	if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
		logErrorNL("Cannot access file database for username '%s'.",
		    acntId.username().toUtf8().constData());
		return 0;
	}
	size += fDb->getDbSizeInBytes();

	return size;
}

qint64 BackupRestoreData::computeAvailableSpace(const QString &targetPath)
{
	const QStorageInfo info(targetPath);
	return info.bytesAvailable();
}

bool BackupRestoreData::restoreAccount(const AcntId &acntId,
    const QString &accountName, const QString &subdir, const QString &sourceDir)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::prefsPtr == Q_NULLPTR) ||
	        (GlobInstcs::setPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	bool success = true;

	if (GlobInstcs::acntMapPtr->contains(acntId)) {

		/* Close and delete both account databases */
		MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
		    PrefsSpecific::dataOnDisk(
			*GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[acntId]));
		if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
			logErrorNL("%s", "Cannot access message database.");
			return false;
		}
		if (!GlobInstcs::messageDbsPtr->deleteDb(msgDb)) {
			logErrorNL("%s", "Could not delete message database.");
			return false;
		}

		FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
		    PrefsSpecific::dataOnDisk(
			*GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[acntId]));
		if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
			logErrorNL("Cannot access file database for username '%s'.",
			    acntId.username().toUtf8().constData());
			return false;
		}
		if (!GlobInstcs::fileDbsPtr->deleteDb(fDb)) {
			logErrorNL("%s", "Could not delete file database.");
			return false;
		}
	}

	/* Copy account databases from backup location */
	QString source = joinDirs(sourceDir, subdir);
	QDirIterator iterator(source, QDirIterator::Subdirectories);
	while (iterator.hasNext()) {
		QFile f(iterator.next());
		QFileInfo fi(f.fileName());
		if (fi.isFile()) {
			emit actionTextSig(tr("Restoring file '%1'.").arg(fi.fileName()));
			success = success &&
			    restoreFile(source, fi.fileName());
		}
	}

	if (!GlobInstcs::acntMapPtr->contains(acntId) && success) {

		/* Create new account if not exists in application settings. */
		QString acntName = tr("From backup") +  " " + accountName;
		QSettings settings(Settings::settingsPath(),
		    QSettings::IniFormat);
		GlobInstcs::acntMapPtr->addCredentialsTorso(settings,
		    acntName, acntId);
		settings.sync();
		GlobInstcs::quitWithoutSaveSettings = true;
	}

	return success;
}

bool BackupRestoreData::backUpAccounts(const QList<AcntId> &acntIds,
    const QString &targetPath, bool includeZfoDb)
{
	debugFuncCall();

	if (Q_UNLIKELY(GlobInstcs::accountDbPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	QCoreApplication::processEvents();

	const QDateTime currentDateTime = QDateTime::currentDateTime();
	QString targetDir;

	Json::Backup bu;
	bu.setType(Json::Backup::BUT_BACKUP);
	fillAppData(bu, currentDateTime);

#ifdef Q_OS_IOS
	Q_UNUSED(targetPath);
	targetDir = backupDirPath(appBackupDirPath(),
	    BackupRestoreData::BRT_BACKUP, currentDateTime);
#else
	targetDir = backupDirPath(targetPath,
	    BackupRestoreData::BRT_BACKUP, currentDateTime);
#endif

	/* Use for iCloud only */
	QList<IosHelper::FileICloud> iCloudFiles;
	QString iCloudPath = joinDirs(QStringLiteral(DATOVKA_BACK_DIR_NAME),
	    backupDirName(BackupRestoreData::BRT_BACKUP, currentDateTime));

	{ /* Backup account message and file databases */
		QList<Json::Backup::MessageDb> mbeList;
		foreach (const AcntId &acntId, acntIds) {
			Json::Backup::MessageDb mbe = backUpOneAccount(acntId,
			    targetDir, currentDateTime, iCloudFiles);
			if (mbe.isValid()) {
				mbeList.append(mbe);
			} else {
				return errorBackup(targetDir);
			}
		}
		bu.setMessageDbs(macroStdMove(mbeList));
	}

	{ /* Backup account database */
		const QFileInfo fi(GlobInstcs::accountDbPtr->fileName());
		const QString fileName = fi.fileName();
		emit actionTextSig(tr("Backing up file '%1'.").arg(fileName));
		QCoreApplication::processEvents();

		if (GlobInstcs::accountDbPtr->backup(joinDirs(targetDir, fileName))) {
			bu.setAccountDb(Json::Backup::File(fileName, Json::Backup::Checksum(), fi.size()));
			IosHelper::FileICloud iCloudFile;
			iCloudFile.srcFilePath = joinDirs(targetDir, fileName);
			iCloudFile.destiCloudPath = iCloudPath;
			iCloudFiles.append(iCloudFile);
		} else {
			return errorBackup(targetDir);
		}
	}

	/* Backup zfo database */
	if (includeZfoDb) {
		if (Q_UNLIKELY(GlobInstcs::zfoDbPtr == Q_NULLPTR)) {
			return errorBackup(targetDir);
		}

		const QFileInfo fi(GlobInstcs::zfoDbPtr->fileName());
		const QString fileName = fi.fileName();
		emit actionTextSig(tr("Backing up file '%1'.").arg(fileName));
		QCoreApplication::processEvents();

		if (GlobInstcs::zfoDbPtr->backup(joinDirs(targetDir, fileName))) {
			bu.setZfoDb(Json::Backup::File(fileName, Json::Backup::Checksum(), fi.size()));
			IosHelper::FileICloud iCloudFile;
			iCloudFile.srcFilePath = joinDirs(targetDir, fileName);
			iCloudFile.destiCloudPath = iCloudPath;
			iCloudFiles.append(iCloudFile);
		} else {
			return errorBackup(targetDir);
		}
	}

	/* Create and backup json file */
	QString jsonFilePath = writeFile(targetDir,
	    QStringLiteral("backup_description.json"), bu.toJson(), true);
	if (!jsonFilePath.isEmpty()) {
		IosHelper::FileICloud iCloudFile;
		iCloudFile.srcFilePath = jsonFilePath;
		iCloudFile.destiCloudPath = iCloudPath;
		iCloudFiles.append(iCloudFile);
	} else {
		return errorBackup(targetDir);
	}

	/* Show final notification */
	emit actionTextSig(tr("Backup finished."));
	logInfoNL("%s", "All selected accounts have been successfully backed up into target location.");

#ifdef Q_OS_IOS
	/* Upload files tu iCloud if needed */
	exportFilesiOS(iCloudFiles);
	return true;
#endif
	Dialogues::errorMessage(Dialogues::INFORMATION,
	    tr("Backup finished"),
	    tr("All selected accounts have been successfully backed up into target location."),
	    tr("Preserve the backup in a safe place as it contains personal data."));

	return true;
}

bool BackupRestoreData::canBackUpAccounts(const QList<AcntId> &acntIdList,
    const QString &targetPath, bool includeZfoDb, bool transfer)
{
	debugFuncCall();

	bool canBackup = false;
	qint64 backupSize = JSON_FILE_SIZE_BYTES;
	qint64 targetSize = 0;

#ifdef Q_OS_IOS
	Q_UNUSED(targetPath);
	targetSize = computeAvailableSpace(GlobalSettingsQmlWrapper::dbPath());
#else
	targetSize = computeAvailableSpace(targetPath);
#endif

	emit actionTextSig("");

	if (transfer) {
		backupSize = dirContentSize(GlobalSettingsQmlWrapper::dbPath());
	} else {

		foreach (const AcntId &acntId, acntIdList) {
			backupSize += getAccountBackupSizeBytes(acntId);
		}

		if ((GlobInstcs::accountDbPtr != Q_NULLPTR)) {
			backupSize += GlobInstcs::accountDbPtr->fileSize();
		} else {
			logErrorNL("%s", "Cannot access account database.");
			return canBackup;
		}

		if (includeZfoDb) {
			if ((GlobInstcs::zfoDbPtr != Q_NULLPTR)) {
				backupSize += GlobInstcs::zfoDbPtr->fileSize();
			} else {
				logErrorNL("%s", "Cannot access zfo database.");
				return canBackup;
			}
		}

		if (acntIdList.isEmpty()) {
			canBackup = false;
			goto finish;
		}
	}

	canBackup = (backupSize < targetSize);

	if (!canBackup && (targetSize >= 0)) {
		logErrorNL("%s", "There is not enough space in the selected storage.");
		emit actionTextSig(tr("There is not enough space in the selected storage."));
	}

finish:
	sendSizeInfoToQml(backupSize, targetSize);

	return canBackup;
}

bool BackupRestoreData::canRestoreFromTransfer(qint64 transferTotalSizeBytes)
{
	debugFuncCall();

	bool canRestore = false;
	QFileInfo fi(m_jsonPath);

	if (m_loadedBackup.appInfo().appVersion() != QStringLiteral(VERSION)) {
		logWarningNL("%s", "Application version does not match the transfer version.");
		emit actionTextSig(tr("Application version does not match the transfer version."));
		int msgResponse = Dialogues::message(Dialogues::QUESTION,
		    tr("Version info"),
		    tr("Warning: Application version does not match the version declared in the transfer. Is is recommended to transfer data between same versions of the application to prevent incompatibility issues."),
		    tr("Do you want to continue?"),
		    Dialogues::NO | Dialogues::YES, Dialogues::YES);
		if (msgResponse == Dialogues::NO) {
			return canRestore;
		}
	}

	qint64 backupSize = 0;

#ifdef Q_OS_IOS
	backupSize = transferTotalSizeBytes;
#else
	Q_UNUSED(transferTotalSizeBytes);
	backupSize = dirContentSize(fi.absolutePath());
#endif
	qint64 targetSize = computeAvailableSpace(GlobalSettingsQmlWrapper::dbPath());

	canRestore = (backupSize < targetSize);

	if (canRestore) {
		emit actionTextSig(tr("Restore all application data from transfer JSON file. The current application data will be complete rewritten with new data from transfer backup."));
	} else {
		logErrorNL("%s", "There is not enough space in the selected storage.");
		emit actionTextSig(tr("There is not enough space in the selected storage."));
	}

	sendSizeInfoToQml(backupSize, targetSize);

	if (!haveFiles(m_loadedBackup, fi.absolutePath())) {
		emit actionTextSig(tr("One or more files mentioned in JSON file are missing or inaccessible."));
		return false;
	}

	return canRestore;
}

void BackupRestoreData::sendSizeInfoToQml(qint64 backupSize,
    qint64 targetSize)
{
	QString infoText = tr("Required space") + ": " + sizeString(backupSize)
	 + "\n" + tr("Available space") + ": " + sizeString(targetSize);
	emit sizeTextSig(infoText);
}
