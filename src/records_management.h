/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>

#include "src/datovka_shared/records_management/io/records_management_connection.h"
#include "src/messages.h"

class AcntId; /* Forward declaration. */
class UploadHierarchyListModel; /* Forward declaration. */

class RecordsManagement : public QObject {
	Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 */
	explicit RecordsManagement(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Calls service info and displays results.
	 *
	 * @param[in] urlStr Records management url string.
	 * @param[in] tokenStr Records management token string.
	 * @return True if success.
	 */
	Q_INVOKABLE
	bool callServiceInfo(const QString &urlStr, const QString &tokenStr);

	/*!
	 * @brief Download upload hierarchy and set model.
	 *
	 * @param[in] hierarchyModel Model for hierarchy update.
	 */
	Q_INVOKABLE
	void callUploadHierarchy(UploadHierarchyListModel *hierarchyModel);

	/*!
	 * @brief Obtain information about stored messages from records managnt.
	 *
	 * @param[in] urlStr Records management url string.
	 * @param[in] tokenStr Records management token string.
	 */
	Q_INVOKABLE
	void getStoredMsgInfoFromRecordsManagement(const QString &urlStr,
	    const QString &tokenSt);

	/*!
	 * @brief Test if records management is set, active and valid.
	 *
	 * @return True if records management is set, active and valid.
	 */
	Q_INVOKABLE
	bool isValidRecordsManagement(void);

	/*!
	 * @brief Loads service information from storage.
	 */
	Q_INVOKABLE
	void loadStoredServiceInfo(void);

	/*!
	 * @brief Upload message into records management service.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] dmId Message identifier.
	 * @param[in] messageType Message orientation.
	 * @param[in] hierarchyModel Model for hierarchy selection.
	 * @return True when data have been updated, false else.
	 */
	Q_INVOKABLE
	bool uploadMessage(const QmlAcntId *qAcntId, const QString &dmId,
	    enum Messages::MessageType messageType,
	    UploadHierarchyListModel *hierarchyModel);

	/*!
	 * @brief Update record management settings.
	 *
	 * @param[in] newUrlStr New records management url string.
	 * @param[in] oldUrlStr Records management url string from settings.
	 * @param[in] srName Service name.
	 * @param[in] srToken Service token.
	 * @return True when data have been updated, false else.
	 */
	Q_INVOKABLE
	bool updateServiceInfo(const QString &newUrlStr,
	    const QString &oldUrlStr, const QString &srName,
	    const QString &srToken);

signals:

	/*!
	 * @brief Send service info to QML.
	 *
	 * @param[in] srName Service name.
	 * @param[in] srToken Service token.
	 */
	void serviceInfo(QString srName, QString srToken);

	/*!
	 * @brief Set new statusbar text and active busy indicator to QML.
	 *
	 * @param[in] txt Text message for statusbar.
	 * @param[in] busy True means the statusbar busy indicator is
	 *            active and shown, false = disabled and hidden.
	 * @param[in] isVisible True means the statusbar is visible,
	 *            false = hidden.
	 */
	void statusBarTextChanged(QString txt, bool busy, bool isVisible);

private slots:

	/*!
	 * @brief Do some actions when records management sync has been finished.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] accNumber Account number in process.
	 * @param[in] accTotal Overall number of account for sync.
	 */
	void rmSyncFinished(const AcntId &acntId, int accNumber,
	    int accTotal);

private:

	/*!
	 * @brief Upload file into records management service.
	 *
	 * @param[in] dmId Message identifier.
	 * @param[in] msgFileName Message file name.
	 * @param[in] msgData Message data.
	 * @param[in] uploadIds List of records management location ids.
	 * @return True when data have been updated, false else.
	 */
	Q_INVOKABLE
	bool uploadFile(qint64 dmId, const QString &msgFileName,
	    const QByteArray &msgData, const QStringList &uploadIds);

	RecMgmt::Connection m_rmc; /*!< Connection to records management service. */
};
