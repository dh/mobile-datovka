/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */


#include <assert.h>
#include <openssl/bio.h>
#include <openssl/cms.h>
#include <openssl/err.h>
#include <string.h>

#include "src/crypto/crypto.h"

/* Extract data from CMS (successor of PKCS#7)
 * The CMS' signature is is not verified.
 * @cms is input block with CMS structure
 * @cms_length is @cms block length in bytes
 * @data are automatically reallocated bit stream with data found in @cms.
 * You must free them.
 * @data_length is length of @data in bytes
 */
int extract_cms_data(const void *cms, const size_t cms_length,
    void **data, size_t *data_length)
{
	int retval = -1;
	BIO *bio = NULL;
	CMS_ContentInfo *cms_ci = NULL;
	const ASN1_OBJECT *asn1_obj;
	ASN1_OCTET_STRING **pos;
	int nid;
//	unsigned long err;
//	char *err_str;

	if ((NULL == cms) || (0 == cms_length) ||
	     (NULL == data) || (NULL == data_length)) {
		return -1;
	}

	zfree(*data);
	*data_length = 0;

	bio = BIO_new_mem_buf((void *) cms, cms_length);
	if (NULL == bio) {
//		while (0 != (err = ERR_get_error())) {
//			err_str = ERR_error_string(err, NULL);
//		}
		retval = -2;
		goto fail;
	}

	cms_ci = d2i_CMS_bio(bio, NULL);
	if (NULL == cms_ci) {
//		while (0 != (err = ERR_get_error())) {
//			err_str = ERR_error_string(err, NULL);
//		}
		retval = -3;
		goto fail;
	}

	BIO_free(bio);
	bio = NULL;

	asn1_obj = CMS_get0_type(cms_ci);
	nid = OBJ_obj2nid(asn1_obj);
	switch (nid) {
		case NID_pkcs7_data:
		case NID_id_smime_ct_compressedData:
		case NID_id_smime_ct_authData:
		case NID_pkcs7_enveloped:
		case NID_pkcs7_encrypted:
		case NID_pkcs7_digest:
			assert(0);
			retval = -4;
			goto fail;
			break;
		case NID_pkcs7_signed:
			break;
		default:
			assert(0);
			retval = -4;
			goto fail;
			break;
	}

	pos = CMS_get0_content(cms_ci);
	if ((NULL == pos) || (NULL == *pos)) {
		assert(0);
		retval = -5;
		goto fail;
	}

	*data = malloc((*pos)->length);
	if (NULL == *data) {
		retval = -6;
		goto fail;
	}
	*data_length = (*pos)->length;
	memcpy(*data, (*pos)->data, (*pos)->length);

	CMS_ContentInfo_free(cms_ci);
	cms_ci = NULL;

	return 0;

fail:
	if (NULL != bio) {
		BIO_free(bio);
	}
	if (NULL != cms_ci) {
		CMS_ContentInfo_free(cms_ci);
	}
	return retval;
}
