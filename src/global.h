/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

/*
 * Forward class declaration.
 * These classes must be declared before the following namespace.
 */
class MessageProcessingEmitter;
class WorkerPool;

class Settings;
class RecordsManagementSettings;
class Prefs;

class AccountDb;
class MsgDbContainer;
class FileDbContainer;
class ZfoDb;
class RecordsManagementDb;
class PrefsDb;

class Sessions;

class AccountsMap;

class ImageProvider;

class IosHelper;

/*!
 * @brief The namespace holds pointers to all globally accessible structures.
 */
namespace GlobInstcs {

	extern
	class MessageProcessingEmitter *msgProcEmitterPtr; /*!< Status message emitter. */
	extern
	class WorkerPool *workPoolPtr; /*!< Worker pool. */

	extern
	class Settings *setPtr; /*!< Settings. */
	extern
	class RecordsManagementSettings *recMgmtSetPtr; /*!< Records management settings. */
	extern
	class Prefs *prefsPtr; /*!< Preference values held in database. */

	extern
	class AccountDb *accountDbPtr; /*!< Account database. */
	extern
	class MsgDbContainer *messageDbsPtr; /*!< Message database container. */
	extern
	class FileDbContainer *fileDbsPtr; /*!< File database container. */
	extern
	class ZfoDb *zfoDbPtr; /*!< ZFO files database. */
	extern
	class RecordsManagementDb *recMgmtDbPtr; /*!< Records management database. */
	extern
	class PrefsDb *prefsDbPtr; /*!< Preferences database. */

	extern
	class Sessions *isdsSessionsPtr; /*!< ISDS session container. */

	extern
	class AccountsMap *acntMapPtr; /*!< Account container. */

	extern
	class ImageProvider *imgProvPtr; /*!< Image provider. */

	extern
	class IosHelper *iOSHelperPtr; /*!< iOS helper. */

	extern
	bool quitWithoutSaveSettings; /*!< Do not save app settings after accounts restoration. */
}
