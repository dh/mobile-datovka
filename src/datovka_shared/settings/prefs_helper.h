/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

namespace PrefsHelper {

	/*!
	 * @brief Constructs account identifier as it is used inside preferences.
	 *
	 * @param[in] username Username.
	 * @param[in] testing Pass true for a testing account.
	 * @return Constructed account identifier string.
	 */
	QString accountIdentifier(const QString &username, bool testing);

	/*!
	 * @brief Obtain account identifier data.
	 *
	 * @param[in]  accountId Account identifier string.
	 * @patam[out] username Obtained username.
	 * @param[out] testing Information whether this is an testing account.
	 * @return True on success, false on any error.
	 */
	bool accountIdentifierData(const QString &accountId, QString &username,
	    bool &testing);

}
