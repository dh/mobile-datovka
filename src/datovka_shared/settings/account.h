/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QScopedPointer>
#include <QString>

class QSettings; /* Forward declaration. */

/*!
 * @brief Defines labels used in credentials.
 */
namespace CredNames {
	extern const QString regularCreds;
	extern const QString shadowCreds;

	extern const QString acntName;
	extern const QString userName;
	extern const QString lMethod;
	extern const QString pwd;
	extern const QString pwdAlg;
	extern const QString pwdSalt;
	extern const QString pwdIv;
	extern const QString pwdCode;
	extern const QString mepToken;
	extern const QString mepTokenAlg;
	extern const QString mepTokenSalt;
	extern const QString mepTokenIv;
	extern const QString mepTokenCode;
	extern const QString testAcnt;
	extern const QString rememberPwd;
	extern const QString dbDir; /* Unused in mobile application. */
	extern const QString syncWithAll;
	extern const QString p12File;
}

class AcntSettingsPrivate;
/*!
 * @brief Holds account settings.
 */
class AcntSettings {
	Q_DECLARE_PRIVATE(AcntSettings)

public:
	/*!
	 * @brief Login method identifier.
	 */
	enum LoginMethod {
		LIM_UNKNOWN, /*!< Unknown method. */
		LIM_UNAME_PWD, /*!< Username and password. */
		LIM_UNAME_CRT, /*!< Username and certificate. */
		LIM_UNAME_PWD_CRT, /*!< Username, password and certificate. */
		LIM_UNAME_PWD_HOTP, /*!< Username, password and HOTP. */
		LIM_UNAME_PWD_TOTP, /*!< Username, password and TOTP (SMS). */
		LIM_UNAME_MEP /*!< Username and MEP (mobile key app). */
	};

	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	AcntSettings(void);
	AcntSettings(const AcntSettings &other);
#ifdef Q_COMPILER_RVALUE_REFS
	AcntSettings(AcntSettings &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
	~AcntSettings(void);

	AcntSettings &operator=(const AcntSettings &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
	AcntSettings &operator=(AcntSettings &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

	bool operator==(const AcntSettings &other) const;
	bool operator!=(const AcntSettings &other) const;

	friend void swap(AcntSettings &first, AcntSettings &second) Q_DECL_NOTHROW;

	bool isNull(void) const;

	void clear(void);

	bool isValid(void) const;
	const QString &accountName(void) const;
	void setAccountName(const QString &an);
#ifdef Q_COMPILER_RVALUE_REFS
	void setAccountName(QString &&an);
#endif /* Q_COMPILER_RVALUE_REFS */
	const QString &userName(void) const;
	void setUserName(const QString &un);
#ifdef Q_COMPILER_RVALUE_REFS
	void setUserName(QString &&un);
#endif /* Q_COMPILER_RVALUE_REFS */
	enum LoginMethod loginMethod(void) const;
	void setLoginMethod(enum LoginMethod method);
	const QString &password(void) const;
	void setPassword(const QString &pwd);
#ifdef Q_COMPILER_RVALUE_REFS
	void setPassword(QString &&pwd);
#endif /* Q_COMPILER_RVALUE_REFS */
	const QString &pwdAlg(void) const;
	void setPwdAlg(const QString &pwdAlg);
#ifdef Q_COMPILER_RVALUE_REFS
	void setPwdAlg(QString &&pwdAlg);
#endif /* Q_COMPILER_RVALUE_REFS */
	const QByteArray &pwdSalt(void) const;
	void setPwdSalt(const QByteArray &pwdSalt);
#ifdef Q_COMPILER_RVALUE_REFS
	void setPwdSalt(QByteArray &&pwdSalt);
#endif /* Q_COMPILER_RVALUE_REFS */
	const QByteArray &pwdIv(void) const;
	void setPwdIv(const QByteArray &pwdIv);
#ifdef Q_COMPILER_RVALUE_REFS
	void setPwdIv(QByteArray &&pwdIv);
#endif /* Q_COMPILER_RVALUE_REFS */
	const QByteArray &pwdCode(void) const;
	void setPwdCode(const QByteArray &pwdCode);
#ifdef Q_COMPILER_RVALUE_REFS
	void setPwdCode(QByteArray &&pwdCode);
#endif /* Q_COMPILER_RVALUE_REFS */
	const QString &mepToken(void) const;
	void setMepToken(const QString &mepToken);
#ifdef Q_COMPILER_RVALUE_REFS
	void setMepToken(QString &&mepToken);
#endif /* Q_COMPILER_RVALUE_REFS */
	bool isTestAccount(void) const;
	void setTestAccount(bool isTesting);
	bool rememberPwd(void) const;
	void setRememberPwd(bool remember);
	const QString &dbDir(void) const;
	void setDbDir(const QString &path, const QString &confDir);
#ifdef Q_COMPILER_RVALUE_REFS
	void setDbDir(QString &&path, const QString &confDir);
#endif /* Q_COMPILER_RVALUE_REFS */
	bool syncWithAll(void) const;
	void setSyncWithAll(bool sync);
	const QString &p12File(void) const;
	void setP12File(const QString &p12);
#ifdef Q_COMPILER_RVALUE_REFS
	void setP12File(QString &&p12);
#endif /* Q_COMPILER_RVALUE_REFS */

	/*!
	 * @brief Used to decrypt the password.
	 *
	 * @param[in] oldPin PIN value used to decrypt old passwords.
	 */
	void decryptPassword(const QString &oldPin);

	/*!
	 * @brief Load content from settings group.
	 *
	 * @note Content is not erased before new settings is loaded.
	 *
	 * @param[in] confDir Configuration directory path.
	 * @param[in] settings Settings structure to load data from.
	 * @param[in] group Name of group to work with.
	 */
	void loadFromSettings(const QString &confDir,
	    const QSettings &settings, const QString &group);

	/*!
	 * @brief Save content to settings.
	 *
	 * @param[in]  pinVal PIN value to be used for password encryption.
	 * @param[in]  confDir Configuration directory path.
	 * @param[out] settings Settings structure to write/append data into.
	 * @param[in]  group Name of group to write to, group is create only
	 *                   when non-empty string supplied.
	 */
	void saveToSettings(const QString &pinVal, const QString &confDir,
	    QSettings &settings, const QString &group) const;

protected:
	QScopedPointer<AcntSettingsPrivate> d_ptr; // ::std::unique_ptr ?
	/* Allow subclasses to initialise with their own specific Private. */
	AcntSettings(AcntSettingsPrivate *d);

private:
	/* Allow parent code to instantiate *Private subclass. */
	virtual
	bool ensurePrivate(void);
};

void swap(AcntSettings &first, AcntSettings &second) Q_DECL_NOTHROW;
