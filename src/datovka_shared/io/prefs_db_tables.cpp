/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QMap>
#include <QPair>
#include <QString>
#include <QVector>

#include "src/datovka_shared/io/db_tables.h"
#include "src/datovka_shared/io/prefs_db_tables.h"

namespace PrfsTbl {
	const QString tabName("preferences");

	const QVector< QPair<QString, enum EntryType> > knownAttrs = {
	{"name", DB_TEXT}, /* NOT NULL */
	{"type", DB_INTEGER}, /* NOT NULL */
	{"value", DB_TEXT},
	/*
	 * PRIMARY KEY (name)
	 */
	};

	const QMap<QString, QString> colConstraints = {
	    {"name", "NOT NULL"},
	    {"type", "NOT NULL"}
	};

	const QString &tblConstraint(
	    ",\n"
	    "        PRIMARY KEY (name)"
	);

	const QMap<QString, SQLiteTbl::AttrProp> attrProps = {
	{"name", {DB_TEXT, SQLiteTbls::tr("Preference Name")}},
	{"type", {DB_INTEGER, SQLiteTbls::tr("Type")}},
	{"value", {DB_TEXT, SQLiteTbls::tr("Value")}}
	};
} /* namespace PrfsTbl */
SQLiteTbl prfsTbl(PrfsTbl::tabName, PrfsTbl::knownAttrs, PrfsTbl::attrProps,
    PrfsTbl::colConstraints, PrfsTbl::tblConstraint);
