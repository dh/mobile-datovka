/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <utility> /* ::std::move */

#include "src/datovka_shared/isds/box_interface.h"
#include "src/datovka_shared/isds/box_interface2.h"
#include "src/datovka_shared/isds/internal_conversion.h"

/* Null objects - for convenience. */
static const Isds::AddressExt2 nullAddressExt2;
static const Isds::BirthInfo nullBirthInfo;
static const Isds::PersonName2 nullPersonName2;
static const QDate nullDate;
static const QList< QPair<int, int> > nullMatchList;
static const QString nullString;

/*!
 * @brief PIMPL AddressExt2 class.
 */
class Isds::AddressExt2Private {
	//Q_DISABLE_COPY(AddressExt2Private)
public:
	AddressExt2Private(void)
	    : m_adCode(), m_adCity(), m_adDistrict(), m_adStreet(),
	    m_adNumberInStreet(), m_adNumberInMunicipality(), m_adZipCode(),
	    m_adState()
	{ }

	AddressExt2Private &operator=(const AddressExt2Private &other) Q_DECL_NOTHROW
	{
		m_adCode = other.m_adCode;
		m_adCity = other.m_adCity;
		m_adDistrict = other.m_adDistrict;
		m_adStreet = other.m_adStreet;
		m_adNumberInStreet = other.m_adNumberInStreet;
		m_adNumberInMunicipality = other.m_adNumberInMunicipality;
		m_adZipCode = other.m_adZipCode;
		m_adState = other.m_adState;

		return *this;
	}

	bool operator==(const AddressExt2Private &other) const
	{
		return (m_adCode == other.m_adCode) &&
		    (m_adCity == other.m_adCity) &&
		    (m_adDistrict == other.m_adDistrict) &&
		    (m_adStreet == other.m_adStreet) &&
		    (m_adNumberInStreet == other.m_adNumberInStreet) &&
		    (m_adNumberInMunicipality == other.m_adNumberInMunicipality) &&
		    (m_adZipCode == other.m_adZipCode) &&
		    (m_adState == other.m_adState);
	}

	QString m_adCode;
	QString m_adCity;
	QString m_adDistrict;
	QString m_adStreet;
	QString m_adNumberInStreet;
	QString m_adNumberInMunicipality;
	QString m_adZipCode;
	QString m_adState;
};

Isds::AddressExt2::AddressExt2(void)
    : d_ptr(Q_NULLPTR)
{
}

Isds::AddressExt2::AddressExt2(const AddressExt2 &other)
    : d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) AddressExt2Private) : Q_NULLPTR)
{
	Q_D(AddressExt2);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::AddressExt2::AddressExt2(AddressExt2 &&other) Q_DECL_NOEXCEPT
    : d_ptr(other.d_ptr.take()) //d_ptr(::std::move(other.d_ptr))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Isds::AddressExt2::~AddressExt2(void)
{
}

/*!
 * @brief Ensures private address presence.
 *
 * @note Returns if private address could not be allocated.
 */
#define ensureAddressExt2Private(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			AddressExt2Private *p = new (::std::nothrow) AddressExt2Private; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Isds::AddressExt2 &Isds::AddressExt2::operator=(const AddressExt2 &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureAddressExt2Private(*this);
	Q_D(AddressExt2);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::AddressExt2 &Isds::AddressExt2::operator=(AddressExt2 &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Isds::AddressExt2::operator==(const AddressExt2 &other) const
{
	Q_D(const AddressExt2);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Isds::AddressExt2::operator!=(const AddressExt2 &other) const
{
	return !operator==(other);
}

bool Isds::AddressExt2::isNull(void) const
{
	Q_D(const AddressExt2);
	return d == Q_NULLPTR;
}

const QString &Isds::AddressExt2::code(void) const
{
	Q_D(const AddressExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_adCode;
}

void Isds::AddressExt2::setCode(const QString &co)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adCode = co;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::AddressExt2::setCode(QString &&co)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adCode = ::std::move(co);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::AddressExt2::city(void) const
{
	Q_D(const AddressExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_adCity;
}

void Isds::AddressExt2::setCity(const QString &c)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adCity = c;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::AddressExt2::setCity(QString &&c)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adCity = ::std::move(c);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::AddressExt2::district(void) const
{
	Q_D(const AddressExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_adDistrict;
}

void Isds::AddressExt2::setDistrict(const QString &di)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adDistrict = di;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::AddressExt2::setDistrict(QString &&di)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adDistrict = ::std::move(di);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::AddressExt2::street(void) const
{
	Q_D(const AddressExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_adStreet;
}

void Isds::AddressExt2::setStreet(const QString &s)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adStreet = s;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::AddressExt2::setStreet(QString &&s)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adStreet = ::std::move(s);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::AddressExt2::numberInStreet(void) const
{
	Q_D(const AddressExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_adNumberInStreet;
}

void Isds::AddressExt2::setNumberInStreet(const QString &nis)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adNumberInStreet = nis;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::AddressExt2::setNumberInStreet(QString &&nis)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adNumberInStreet = ::std::move(nis);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::AddressExt2::numberInMunicipality(void) const
{
	Q_D(const AddressExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_adNumberInMunicipality;
}

void Isds::AddressExt2::setNumberInMunicipality(const QString &nim)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adNumberInMunicipality = nim;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::AddressExt2::setNumberInMunicipality(QString &&nim)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adNumberInMunicipality = ::std::move(nim);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::AddressExt2::zipCode(void) const
{
	Q_D(const AddressExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_adZipCode;
}

void Isds::AddressExt2::setZipCode(const QString &zc)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adZipCode = zc;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::AddressExt2::setZipCode(QString &&zc)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adZipCode = ::std::move(zc);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::AddressExt2::state(void) const
{
	Q_D(const AddressExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_adState;
}

void Isds::AddressExt2::setState(const QString &s)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adState = s;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::AddressExt2::setState(QString &&s)
{
	ensureAddressExt2Private();
	Q_D(AddressExt2);
	d->m_adState = ::std::move(s);
}
#endif /* Q_COMPILER_RVALUE_REFS */

void Isds::swap(AddressExt2 &first, AddressExt2 &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

/*!
 * @brief PIMPL PersonName2 class.
 */
class Isds::PersonName2Private {
	//Q_DISABLE_COPY(PersonName2Private)
public:
	PersonName2Private(void)
	    : m_pnGivenNames(), m_pnLastName()
	{ }

	PersonName2Private &operator=(const PersonName2Private &other) Q_DECL_NOTHROW
	{
		m_pnGivenNames = other.m_pnGivenNames;
		m_pnLastName = other.m_pnLastName;

		return *this;
	}

	bool operator==(const PersonName2Private &other) const
	{
		return (m_pnGivenNames == other.m_pnGivenNames) &&
		    (m_pnLastName == other.m_pnLastName);
	}

	QString m_pnGivenNames;
	QString m_pnLastName;
};

Isds::PersonName2::PersonName2(void)
    : d_ptr(Q_NULLPTR)
{
}

Isds::PersonName2::PersonName2(const PersonName2 &other)
    : d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) PersonName2Private) : Q_NULLPTR)
{
	Q_D(PersonName2);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::PersonName2::PersonName2(PersonName2 &&other) Q_DECL_NOEXCEPT
    : d_ptr(other.d_ptr.take()) //d_ptr(::std::move(other.d_ptr))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Isds::PersonName2::~PersonName2(void)
{
}

/*!
 * @brief Ensures private person name presence.
 *
 * @note Returns if private person name could not be allocated.
 */
#define ensurePersonName2Private(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			PersonName2Private *p = new (::std::nothrow) PersonName2Private; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Isds::PersonName2 &Isds::PersonName2::operator=(const PersonName2 &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensurePersonName2Private(*this);
	Q_D(PersonName2);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::PersonName2 &Isds::PersonName2::operator=(PersonName2 &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Isds::PersonName2::operator==(const PersonName2 &other) const
{
	Q_D(const PersonName2);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Isds::PersonName2::operator!=(const PersonName2 &other) const
{
	return !operator==(other);
}

bool Isds::PersonName2::isNull(void) const
{
	Q_D(const PersonName2);
	return d == Q_NULLPTR;
}

const QString &Isds::PersonName2::givenNames(void) const
{
	Q_D(const PersonName2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_pnGivenNames;
}

void Isds::PersonName2::setGivenNames(const QString &gn)
{
	ensurePersonName2Private();
	Q_D(PersonName2);
	d->m_pnGivenNames = gn;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::PersonName2::setGivenNames(QString &&gn)
{
	ensurePersonName2Private();
	Q_D(PersonName2);
	d->m_pnGivenNames = ::std::move(gn);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::PersonName2::lastName(void) const
{
	Q_D(const PersonName2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_pnLastName;
}

void Isds::PersonName2::setLastName(const QString &ln)
{
	ensurePersonName2Private();
	Q_D(PersonName2);
	d->m_pnLastName = ln;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::PersonName2::setLastName(QString &&ln)
{
	ensurePersonName2Private();
	Q_D(PersonName2);
	d->m_pnLastName = ::std::move(ln);
}
#endif /* Q_COMPILER_RVALUE_REFS */

void Isds::swap(PersonName2 &first, PersonName2 &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

/*!
 * @brief PIMPL DbOwnerInfoExt2 class.
 */
class Isds::DbOwnerInfoExt2Private {
	//Q_DISABLE_COPY(DbOwnerInfoExt22Private)
public:
	DbOwnerInfoExt2Private(void)
	    : m_dbID(), m_aifoIsds(Type::BOOL_NULL), m_dbType(Type::BT_NULL),
	    m_ic(), m_personName(), m_firmName(), m_birthInfo(), m_address(),
	    m_nationality(), m_dbIdOVM(), m_dbState(Type::BS_ERROR),
	    m_dbOpenAddressing(Type::BOOL_NULL), m_dbUpperID()
	{ }

	DbOwnerInfoExt2Private &operator=(const DbOwnerInfoExt2Private &other) Q_DECL_NOTHROW
	{
		m_dbID = other.m_dbID;
		m_aifoIsds = other.m_aifoIsds;
		m_dbType = other.m_dbType;
		m_ic = other.m_ic;
		m_personName = other.m_personName;
		m_firmName = other.m_firmName;
		m_birthInfo = other.m_birthInfo;
		m_address = other.m_address;
		m_nationality = other.m_nationality;;
		m_dbIdOVM = other.m_dbIdOVM;
		m_dbState = other.m_dbState;
		m_dbOpenAddressing = other.m_dbOpenAddressing;
		m_dbUpperID = other.m_dbUpperID;

		return *this;
	}

	bool operator==(const DbOwnerInfoExt2Private &other) const
	{
		return (m_dbID == other.m_dbID) &&
		    (m_aifoIsds == other.m_aifoIsds) &&
		    (m_dbType == other.m_dbType) &&
		    (m_ic == other.m_ic) &&
		    (m_personName == other.m_personName) &&
		    (m_firmName == other.m_firmName) &&
		    (m_birthInfo == other.m_birthInfo) &&
		    (m_address == other.m_address) &&
		    (m_nationality == other.m_nationality) &&
		    (m_dbIdOVM == other.m_dbIdOVM) &&
		    (m_dbState == other.m_dbState) &&
		    (m_dbOpenAddressing == other.m_dbOpenAddressing) &&
		    (m_dbUpperID == other.m_dbUpperID);
	}

	QString m_dbID;
	enum Type::NilBool m_aifoIsds;
	enum Type::DbType m_dbType;
	QString m_ic;
	PersonName2 m_personName;
	QString m_firmName;
	BirthInfo m_birthInfo;
	AddressExt2 m_address;
	QString m_nationality;
	QString m_dbIdOVM;
	enum Type::DbState m_dbState;
	enum Type::NilBool m_dbOpenAddressing;
	QString m_dbUpperID;
};

Isds::DbOwnerInfoExt2::DbOwnerInfoExt2(void)
    : d_ptr(Q_NULLPTR)
{
}

Isds::DbOwnerInfoExt2::DbOwnerInfoExt2(const DbOwnerInfoExt2 &other)
    : d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) DbOwnerInfoExt2Private) : Q_NULLPTR)
{
	Q_D(DbOwnerInfoExt2);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::DbOwnerInfoExt2::DbOwnerInfoExt2(DbOwnerInfoExt2 &&other) Q_DECL_NOEXCEPT
    : d_ptr(other.d_ptr.take()) //d_ptr(::std::move(other.d_ptr))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Isds::DbOwnerInfoExt2::~DbOwnerInfoExt2(void)
{
}

/*!
 * @brief Ensures private box owner info info presence.
 *
 * @note Returns if private box owner info could not be allocated.
 */
#define ensureDbOwnerInfoExt2Private(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			DbOwnerInfoExt2Private *p = new (::std::nothrow) DbOwnerInfoExt2Private; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Isds::DbOwnerInfoExt2 &Isds::DbOwnerInfoExt2::operator=(const DbOwnerInfoExt2 &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureDbOwnerInfoExt2Private(*this);
	Q_D(DbOwnerInfoExt2);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::DbOwnerInfoExt2 &Isds::DbOwnerInfoExt2::operator=(DbOwnerInfoExt2 &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Isds::DbOwnerInfoExt2::operator==(const DbOwnerInfoExt2 &other) const
{
	Q_D(const DbOwnerInfoExt2);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Isds::DbOwnerInfoExt2::operator!=(const DbOwnerInfoExt2 &other) const
{
	return !operator==(other);
}

bool Isds::DbOwnerInfoExt2::isNull(void) const
{
	Q_D(const DbOwnerInfoExt2);
	return d == Q_NULLPTR;
}

const QString &Isds::DbOwnerInfoExt2::dbID(void) const
{
	Q_D(const DbOwnerInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_dbID;
}

void Isds::DbOwnerInfoExt2::setDbID(const QString &bi)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_dbID = bi;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbOwnerInfoExt2::setDbID(QString &&bi)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_dbID = ::std::move(bi);
}
#endif /* Q_COMPILER_RVALUE_REFS */

enum Isds::Type::NilBool Isds::DbOwnerInfoExt2::aifoIsds(void) const
{
	Q_D(const DbOwnerInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return Type::BOOL_NULL;
	}

	return d->m_aifoIsds;
}

void Isds::DbOwnerInfoExt2::setAifoIsds(enum Type::NilBool ai)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_aifoIsds = ai;
}

enum Isds::Type::DbType Isds::DbOwnerInfoExt2::dbType(void) const
{
	Q_D(const DbOwnerInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return Type::BT_NULL;
	}

	return d->m_dbType;
}

void Isds::DbOwnerInfoExt2::setDbType(enum Type::DbType bt)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_dbType = bt;
}

const QString &Isds::DbOwnerInfoExt2::ic(void) const
{
	Q_D(const DbOwnerInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_ic;
}

void Isds::DbOwnerInfoExt2::setIc(const QString &ic)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_ic = ic;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbOwnerInfoExt2::setIc(QString &&ic)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_ic = ::std::move(ic);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const Isds::PersonName2 &Isds::DbOwnerInfoExt2::personName(void) const
{
	Q_D(const DbOwnerInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullPersonName2;
	}

	return d->m_personName;
}

void Isds::DbOwnerInfoExt2::setPersonName(const PersonName2 &pn)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_personName = pn;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbOwnerInfoExt2::setPersonName(PersonName2 &&pn)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_personName = ::std::move(pn);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DbOwnerInfoExt2::firmName(void) const
{
	Q_D(const DbOwnerInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_firmName;
}

void Isds::DbOwnerInfoExt2::setFirmName(const QString &fn)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_firmName = fn;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbOwnerInfoExt2::setFirmName(QString &&fn)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_firmName = ::std::move(fn);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const Isds::BirthInfo &Isds::DbOwnerInfoExt2::birthInfo(void) const
{
	Q_D(const DbOwnerInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullBirthInfo;
	}

	return d->m_birthInfo;
}

void Isds::DbOwnerInfoExt2::setBirthInfo(const BirthInfo &bi)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_birthInfo = bi;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbOwnerInfoExt2::setBirthInfo(BirthInfo &&bi)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_birthInfo = ::std::move(bi);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const Isds::AddressExt2 &Isds::DbOwnerInfoExt2::address(void) const
{
	Q_D(const DbOwnerInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullAddressExt2;
	}

	return d->m_address;
}

void Isds::DbOwnerInfoExt2::setAddress(const AddressExt2 &a)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_address = a;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbOwnerInfoExt2::setAddress(AddressExt2 &&a)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_address = ::std::move(a);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DbOwnerInfoExt2::nationality(void) const
{
	Q_D(const DbOwnerInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_nationality;
}

void Isds::DbOwnerInfoExt2::setNationality(const QString &n)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_nationality = n;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbOwnerInfoExt2::setNationality(QString &&n)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_nationality = ::std::move(n);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DbOwnerInfoExt2::dbIdOVM(void) const
{
	Q_D(const DbOwnerInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_dbIdOVM;
}

void Isds::DbOwnerInfoExt2::setDbIdOVM(const QString &n)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_dbIdOVM = n;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbOwnerInfoExt2::setDbIdOVM(QString &&n)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_dbIdOVM = ::std::move(n);
}
#endif /* Q_COMPILER_RVALUE_REFS */

enum Isds::Type::DbState Isds::DbOwnerInfoExt2::dbState(void) const
{
	Q_D(const DbOwnerInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return Type::BS_ERROR;
	}

	return d->m_dbState;
}

void Isds::DbOwnerInfoExt2::setDbState(enum Type::DbState bs)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_dbState = bs;
}

enum Isds::Type::NilBool Isds::DbOwnerInfoExt2::dbOpenAddressing(void) const
{
	Q_D(const DbOwnerInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return Type::BOOL_NULL;
	}

	return d->m_dbOpenAddressing;
}

void Isds::DbOwnerInfoExt2::setDbOpenAddressing(enum Type::NilBool oa)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_dbOpenAddressing = oa;
}

const QString &Isds::DbOwnerInfoExt2::dbUpperID(void) const
{
	Q_D(const DbOwnerInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_dbUpperID;
}

void Isds::DbOwnerInfoExt2::setDbUpperID(const QString &ui)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_dbUpperID = ui;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbOwnerInfoExt2::setDbUpperID(QString &&ui)
{
	ensureDbOwnerInfoExt2Private();
	Q_D(DbOwnerInfoExt2);
	d->m_dbUpperID = ::std::move(ui);
}
#endif /* Q_COMPILER_RVALUE_REFS */

void Isds::swap(DbOwnerInfoExt2 &first, DbOwnerInfoExt2 &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

/*!
 * @brief PIMPL DbUserInfoExt2 class.
 */
class Isds::DbUserInfoExt2Private {
	//Q_DISABLE_COPY(DbUserInfoExt2Private)
public:
	DbUserInfoExt2Private(void)
	    : m_aifoIsds(Type::BOOL_NULL), m_personName(), m_address(),
	    m_biDate(), m_isdsID(), m_userType(Type::UT_NULL),
	    m_userPrivils(Type::PRIVIL_NONE), m_ic(), m_firmName(),
	    m_caStreet(), m_caCity(), m_caZipCode(), m_caState()
	{ }

	DbUserInfoExt2Private &operator=(const DbUserInfoExt2Private &other) Q_DECL_NOTHROW
	{
		m_aifoIsds = other.m_aifoIsds;
		m_personName = other.m_personName;
		m_address = other.m_address;
		m_biDate = other.m_biDate;
		m_isdsID = other.m_isdsID;
		m_userType = other.m_userType;
		m_userPrivils = other.m_userPrivils;
		m_ic = other.m_ic;
		m_firmName = other.m_firmName;
		m_caStreet = other.m_caStreet;
		m_caCity = other.m_caCity;
		m_caZipCode = other.m_caZipCode;
		m_caState = other.m_caState;

		return *this;
	}

	bool operator==(const DbUserInfoExt2Private &other) const
	{
		return (m_aifoIsds == other.m_aifoIsds) &&
		    (m_personName == other.m_personName) &&
		    (m_address == other.m_address) &&
		    (m_biDate == other.m_biDate) &&
		    (m_isdsID == other.m_isdsID) &&
		    (m_userType == other.m_userType) &&
		    (m_userPrivils == other.m_userPrivils) &&
		    (m_ic == other.m_ic) &&
		    (m_firmName == other.m_firmName) &&
		    (m_caStreet == other.m_caStreet) &&
		    (m_caCity == other.m_caCity) &&
		    (m_caZipCode == other.m_caZipCode) &&
		    (m_caState == other.m_caState);
	}

	enum Type::NilBool m_aifoIsds;
	PersonName2 m_personName;
	AddressExt2 m_address;
	QDate m_biDate;
	QString m_isdsID;
	enum Type::UserType m_userType;
	Type::Privileges m_userPrivils;
	QString m_ic;
	QString m_firmName;
	QString m_caStreet;
	QString m_caCity;
	QString m_caZipCode;
	QString m_caState;
};

Isds::DbUserInfoExt2::DbUserInfoExt2(void)
    : d_ptr(Q_NULLPTR)
{
}

Isds::DbUserInfoExt2::DbUserInfoExt2(const DbUserInfoExt2 &other)
    : d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) DbUserInfoExt2Private) : Q_NULLPTR)
{
	Q_D(DbUserInfoExt2);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::DbUserInfoExt2::DbUserInfoExt2(DbUserInfoExt2 &&other) Q_DECL_NOEXCEPT
    : d_ptr(other.d_ptr.take()) //d_ptr(::std::move(other.d_ptr))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Isds::DbUserInfoExt2::~DbUserInfoExt2(void)
{
}

/*!
 * @brief Ensures private box user info info presence.
 *
 * @note Returns if private box user info could not be allocated.
 */
#define ensureDbUserInfoExt2Private(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			DbUserInfoExt2Private *p = new (::std::nothrow) DbUserInfoExt2Private; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Isds::DbUserInfoExt2 &Isds::DbUserInfoExt2::operator=(const DbUserInfoExt2 &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureDbUserInfoExt2Private(*this);
	Q_D(DbUserInfoExt2);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::DbUserInfoExt2 &Isds::DbUserInfoExt2::operator=(DbUserInfoExt2 &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Isds::DbUserInfoExt2::operator==(const DbUserInfoExt2 &other) const
{
	Q_D(const DbUserInfoExt2);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Isds::DbUserInfoExt2::operator!=(const DbUserInfoExt2 &other) const
{
	return !operator==(other);
}

bool Isds::DbUserInfoExt2::isNull(void) const
{
	Q_D(const DbUserInfoExt2);
	return d == Q_NULLPTR;
}

enum Isds::Type::NilBool Isds::DbUserInfoExt2::aifoIsds(void) const
{
	Q_D(const DbUserInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return Type::BOOL_NULL;
	}

	return d->m_aifoIsds;
}

void Isds::DbUserInfoExt2::setAifoIsds(enum Type::NilBool ai)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_aifoIsds = ai;
}

const Isds::PersonName2 &Isds::DbUserInfoExt2::personName(void) const
{
	Q_D(const DbUserInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullPersonName2;
	}

	return d->m_personName;
}

void Isds::DbUserInfoExt2::setPersonName(const PersonName2 &pn)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_personName = pn;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbUserInfoExt2::setPersonName(PersonName2 &&pn)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_personName = ::std::move(pn);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const Isds::AddressExt2 &Isds::DbUserInfoExt2::address(void) const
{
	Q_D(const DbUserInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullAddressExt2;
	}

	return d->m_address;
}

void Isds::DbUserInfoExt2::setAddress(const AddressExt2 &a)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_address = a;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbUserInfoExt2::setAddress(AddressExt2 &&a)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_address = ::std::move(a);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QDate &Isds::DbUserInfoExt2::biDate(void) const
{
	Q_D(const DbUserInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullDate;
	}

	return d->m_biDate;
}

void Isds::DbUserInfoExt2::setBiDate(const QDate &bd)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_biDate = bd;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbUserInfoExt2::setBiDate(QDate &&bd)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_biDate = ::std::move(bd);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DbUserInfoExt2::isdsID(void) const
{
	Q_D(const DbUserInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_isdsID;
}

void Isds::DbUserInfoExt2::setIsdsID(const QString &id)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_isdsID = id;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbUserInfoExt2::setIsdsID(QString &&id)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_isdsID = ::std::move(id);
}
#endif /* Q_COMPILER_RVALUE_REFS */

enum Isds::Type::UserType Isds::DbUserInfoExt2::userType(void) const
{
	Q_D(const DbUserInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return Type::UT_NULL;
	}

	return d->m_userType;
}

void Isds::DbUserInfoExt2::setUserType(enum Type::UserType ut)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_userType = ut;
}

Isds::Type::Privileges Isds::DbUserInfoExt2::userPrivils(void) const
{
	Q_D(const DbUserInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return Type::PRIVIL_NONE;
	}

	return d->m_userPrivils;
}

void Isds::DbUserInfoExt2::setUserPrivils(Type::Privileges p)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_userPrivils = p;
}

const QString &Isds::DbUserInfoExt2::ic(void) const
{
	Q_D(const DbUserInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_ic;
}

void Isds::DbUserInfoExt2::setIc(const QString &ic)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_ic = ic;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbUserInfoExt2::setIc(QString &&ic)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_ic = ::std::move(ic);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DbUserInfoExt2::firmName(void) const
{
	Q_D(const DbUserInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_firmName;
}

void Isds::DbUserInfoExt2::setFirmName(const QString &fn)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_firmName = fn;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbUserInfoExt2::setFirmName(QString &&fn)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_firmName = ::std::move(fn);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DbUserInfoExt2::caStreet(void) const
{
	Q_D(const DbUserInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_caStreet;
}

void Isds::DbUserInfoExt2::setCaStreet(const QString &cs)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_caStreet = cs;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbUserInfoExt2::setCaStreet(QString &&cs)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_caStreet = ::std::move(cs);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DbUserInfoExt2::caCity(void) const
{
	Q_D(const DbUserInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_caCity;
}

void Isds::DbUserInfoExt2::setCaCity(const QString &cc)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_caCity = cc;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbUserInfoExt2::setCaCity(QString &&cc)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_caCity = ::std::move(cc);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DbUserInfoExt2::caZipCode(void) const
{
	Q_D(const DbUserInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_caZipCode;
}

void Isds::DbUserInfoExt2::setCaZipCode(const QString &cz)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_caZipCode = cz;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbUserInfoExt2::setCaZipCode(QString &&cz)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_caZipCode = ::std::move(cz);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DbUserInfoExt2::caState(void) const
{
	Q_D(const DbUserInfoExt2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_caState;
}

void Isds::DbUserInfoExt2::setCaState(const QString &cs)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_caState = cs;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbUserInfoExt2::setCaState(QString &&cs)
{
	ensureDbUserInfoExt2Private();
	Q_D(DbUserInfoExt2);
	d->m_caState = ::std::move(cs);
}
#endif /* Q_COMPILER_RVALUE_REFS */

void Isds::swap(DbUserInfoExt2 &first, DbUserInfoExt2 &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

/*!
 * @brief PIMPL DbResult2 class.
 */
class Isds::DbResult2Private {
	//Q_DISABLE_COPY(DbResult2Private)
public:
	DbResult2Private(void)
	    : m_dbID(), m_dbType(Type::BT_NULL), m_dbName(), m_dbAddress(),
	    m_dbBiDate(), m_dbICO(), m_dbIdOVM(),
	    active(false), publicSending(false), commercialSending(false),
	    nameMatches(), addressMatches()
	{ }

	DbResult2Private &operator=(const DbResult2Private &other) Q_DECL_NOTHROW
	{
		m_dbID = other.m_dbID;
		m_dbType = other.m_dbType;
		m_dbName = other.m_dbName;
		m_dbAddress = other.m_dbAddress;
		m_dbBiDate = other.m_dbBiDate;
		m_dbICO = other.m_dbICO;
		m_dbIdOVM = other.m_dbIdOVM;
		active = other.active;
		publicSending = other.publicSending;
		commercialSending = other.commercialSending;
		nameMatches = other.nameMatches;
		addressMatches = other.addressMatches;

		return *this;
	}

	bool operator==(const DbResult2Private &other) const
	{
		return (m_dbID == other.m_dbID) &&
		    (m_dbType == other.m_dbType) &&
		    (m_dbName == other.m_dbName) &&
		    (m_dbAddress == other.m_dbAddress) &&
		    (m_dbBiDate == other.m_dbBiDate) &&
		    (m_dbICO == other.m_dbICO) &&
		    (m_dbIdOVM == other.m_dbIdOVM) &&
		    (active == other.active) &&
		    (publicSending == other.publicSending) &&
		    (commercialSending == other.commercialSending) &&
		    (nameMatches == other.nameMatches) &&
		    (addressMatches == other.addressMatches);
	}

	QString m_dbID;
	enum Type::DbType m_dbType;
	QString m_dbName;
	QString m_dbAddress;
	QDate m_dbBiDate;
	QString m_dbICO;
	QString m_dbIdOVM;
	/*
	 * Following entries are derived from libisds at it does not provide
	 * interface for dbSendOptions.
	 */
	bool active; /* Box can receive messages. */
	bool publicSending; /* Seeker box can send ordinary messages into this box. */
	bool commercialSending; /* Seeker box can send commercial messages into this box. */

	QList< QPair<int, int> > nameMatches;
	QList< QPair<int, int> > addressMatches;
};

Isds::DbResult2::DbResult2(void)
    : d_ptr(Q_NULLPTR)
{
}

Isds::DbResult2::DbResult2(const DbResult2 &other)
    : d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) DbResult2Private) : Q_NULLPTR)
{
	Q_D(DbResult2);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::DbResult2::DbResult2(DbResult2 &&other) Q_DECL_NOEXCEPT
    : d_ptr(other.d_ptr.take()) //d_ptr(::std::move(other.d_ptr))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Isds::DbResult2::~DbResult2(void)
{
}

/*!
 * @brief Ensures private full-text search result presence.
 *
 * @note Returns if private full-text search result could not be allocated.
 */
#define ensureDbResult2Private(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			DbResult2Private *p = new (::std::nothrow) DbResult2Private; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Isds::DbResult2 &Isds::DbResult2::operator=(
    const DbResult2 &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureDbResult2Private(*this);
	Q_D(DbResult2);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::DbResult2 &Isds::DbResult2::operator=(DbResult2 &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Isds::DbResult2::operator==(const DbResult2 &other) const
{
	Q_D(const DbResult2);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Isds::DbResult2::operator!=(const DbResult2 &other) const
{
	return !operator==(other);
}

bool Isds::DbResult2::isNull(void) const
{
	Q_D(const DbResult2);
	return d == Q_NULLPTR;
}

const QString &Isds::DbResult2::dbID(void) const
{
	Q_D(const DbResult2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_dbID;
}

void Isds::DbResult2::setDbID(const QString &id)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->m_dbID = id;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbResult2::setDbID(QString &&id)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->m_dbID = ::std::move(id);
}
#endif /* Q_COMPILER_RVALUE_REFS */

enum Isds::Type::DbType Isds::DbResult2::dbType(void) const
{
	Q_D(const DbResult2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return Type::BT_NULL;
	}
	return d->m_dbType;
}

void Isds::DbResult2::setDbType(enum Type::DbType bt)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->m_dbType = bt;
}

const QString &Isds::DbResult2::dbName(void) const
{
	Q_D(const DbResult2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_dbName;
}

void Isds::DbResult2::setDbName(const QString &n)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->m_dbName = n;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbResult2::setDbName(QString &&n)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->m_dbName = ::std::move(n);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DbResult2::dbAddress(void) const
{
	Q_D(const DbResult2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_dbAddress;
}

void Isds::DbResult2::setDbAddress(const QString &a)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->m_dbAddress = a;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbResult2::setDbAddress(QString &&a)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->m_dbAddress = ::std::move(a);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QDate &Isds::DbResult2::dbBiDate(void) const
{
	Q_D(const DbResult2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullDate;
	}
	return d->m_dbBiDate;
}

void Isds::DbResult2::setDbBiDate(const QDate &bd)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->m_dbBiDate = bd;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbResult2::setDbBiDate(QDate &&bd)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->m_dbBiDate = ::std::move(bd);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DbResult2::ic(void) const
{
	Q_D(const DbResult2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_dbICO;
}

void Isds::DbResult2::setIc(const QString &ic)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->m_dbICO = ic;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbResult2::setIc(QString &&ic)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->m_dbICO = ::std::move(ic);
}
#endif /* Q_COMPILER_RVALUE_REFS */


const QString &Isds::DbResult2::dbIdOVM(void) const
{
	Q_D(const DbResult2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_dbIdOVM;
}

void Isds::DbResult2::setDbIdOVM(const QString &io)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->m_dbIdOVM = io;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbResult2::setDbIdOVM(QString &&io)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->m_dbIdOVM = ::std::move(io);
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Isds::DbResult2::active(void) const
{
	Q_D(const DbResult2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return false;
	}
	return d->active;
}

void Isds::DbResult2::setActive(bool a)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->active = a;
}

bool Isds::DbResult2::publicSending(void) const
{
	Q_D(const DbResult2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return false;
	}
	return d->publicSending;
}

void Isds::DbResult2::setPublicSending(bool ps)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->publicSending = ps;
}

bool Isds::DbResult2::commercialSending(void) const
{
	Q_D(const DbResult2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return false;
	}
	return d->commercialSending;
}

void Isds::DbResult2::setCommercialSending(bool cs)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->commercialSending = cs;
}

const QList< QPair<int, int> > &Isds::DbResult2::nameMatches(void) const
{
	Q_D(const DbResult2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullMatchList;
	}
	return d->nameMatches;
}

void Isds::DbResult2::setNameMatches(const QList< QPair<int, int> > &nm)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->nameMatches = nm;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbResult2::setNameMatches(QList< QPair<int, int> > &&nm)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->nameMatches = ::std::move(nm);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QList< QPair<int, int> > &Isds::DbResult2::addressMatches(void) const
{
	Q_D(const DbResult2);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullMatchList;
	}
	return d->addressMatches;
}

void Isds::DbResult2::setAddressMatches(const QList< QPair<int, int> > &am)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->addressMatches = am;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DbResult2::setAddressMatches(QList< QPair<int, int> > &&am)
{
	ensureDbResult2Private();
	Q_D(DbResult2);
	d->addressMatches = ::std::move(am);
}
#endif /* Q_COMPILER_RVALUE_REFS */

void Isds::swap(DbResult2 &first, DbResult2 &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}
