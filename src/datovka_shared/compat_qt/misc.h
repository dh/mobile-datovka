/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication>
#include <QtGlobal> /* Q_FALLTHROUGH */

#if (QT_VERSION < QT_VERSION_CHECK(5, 8, 0))
#  define Q_FALLTHROUGH()
#endif /* < Qt-5.8.0 */

#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
#  define _Qt_endl Qt::endl
#else /* < Qt-5.14.0 */
/* Defined in <QTextStream> but without namespace. */
#  define _Qt_endl endl
#endif /* >= Qt-5.14.0 */

#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
#  define _Qt_KeepEmptyParts Qt::KeepEmptyParts
#  define _Qt_SkipEmptyParts Qt::SkipEmptyParts
#else /* < Qt-5.14.0 */
#  define _Qt_KeepEmptyParts QString::KeepEmptyParts
#  define _Qt_SkipEmptyParts QString::SkipEmptyParts
#endif /* >= Qt-5.14.0 */

#define Q_DECLARE_NAMESPACE_TR(context) \
	inline QString tr(const char *sourceText, const char *disambiguation = Q_NULLPTR, int n = -1) \
	{ return QCoreApplication::translate(#context, sourceText, disambiguation, n); }
