/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QtGlobal> /* qsizetype */

namespace Compat {

	/*!
	 * @brief Initialise random generator.
	 */
	void seedRand(void);

	/*!
	 * @brief Generate random number.
	 *
	 * @patam[in] upperBound Upper interval bound, must be positive.
	 * @return Integer in range [0, \a upperBound).
	 */
	int randBounded(int upperBound);

	/*!
	 * @brief Generates random data and stores the in supplied buffer.
	 *
	 * @param[out] buffer Buffer to write data to.
	 * @param[in]  size Buffer size.
	 */
	void randFillRange(char *buffer, int size);

}
