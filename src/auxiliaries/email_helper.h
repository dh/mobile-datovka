/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#include <QString>
#include <QStringList>

/*!
 * @brief Add attachment into email.
 *
 * @param[in,out] message String to append data to.
 * @param[in] attachName Attachment name.
 * @param[in] base64 Base64-encoded attachment.
 * @param[in] boundary Boundary to be used.
 */
void addAttachmentToEmailMessage(QString &message, const QString &attachName,
    const QByteArray &base64, const QString &boundary);

/*!
 * @brief Creates email header and message body.
 *
 * @param[in] to Recipient mail address.
 * @param[in] body Email body.
 * @param[in] subj Subject text.
 * @param[in] boundary Boundary to be used.
 * @return Email header string.
 */
QString createEmailMessage(const QString &to, const QString &body,
    const QString &subject, const QString &boundary);

/*!
 * @brief Adds last line into email.
 *
 * @param[in,out] message String to append data to.
 * @param[in] boundary Boundary to be used.
 */
void finishEmailMessage(QString &message, const QString &boundary);

/*!
 * @brief Generates boundary string for email body parts.
 *
 * @return Boundary string.
 */
QString generateBoundaryString(void);

/*!
 * @brief Creates email body text.
 *
 * @param[in] dmId Message ID.
 * @param[in] from Sender name.
 * @param[in] to Recipient name.
 * @param[in] delivered Delivery time.
 * @return Email body string.
 */
QString generateEmailBodyText(qint64 dmId, const QString &from,
    const QString &to, const QString &delivered);

/*!
 * @brief Send email.
 *
 * @param[in] emailMessage Eml email content.
 * @param[in] fileList File list for email.
 * @param[in] to Recipient mail addreess.
 * @param[in] subject Email subject.
 * @param[in] body Email body.
 * @param[in] msgId Message ID.
 */
void sendEmail(const QString &emailMessage, const QStringList &fileList,
    const QString &to, const QString &subject, const QString &body,
    const QString &msgId);
