/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDateTime>
#include <QDesktopServices>
#include <QMimeDatabase>
#include <QMimeType>
#include <QObject>
#include <QUrl>

#if defined (Q_OS_ANDROID)
#include "android/src/android_io.h"
#endif
#include "ios/src/url_opener.h"
#include "src/auxiliaries/email_helper.h"
#include "src/io/filesystem.h"

void addAttachmentToEmailMessage(QString &message, const QString &attachName,
    const QByteArray &base64, const QString &boundary)
{
	const QString newLine("\n"); /* "\r\n" ? */

	QMimeDatabase mimeDb;

	QMimeType mimeType(
	    mimeDb.mimeTypeForData(QByteArray::fromBase64(base64)));

	message += newLine;
	message += "--" + boundary + newLine;
	message += "Content-Type: " + mimeType.name() + "; charset=UTF-8;"
	    + newLine + " name=\"" + attachName +  "\"" + newLine;
	message += "Content-Transfer-Encoding: base64" + newLine;
	message += "Content-Disposition: attachment;" + newLine +
	    " filename=\"" + attachName + "\"" + newLine;

	for (int i = 0; i < base64.size(); ++i) {
		if ((i % 60) == 0) {
			message += newLine;
		}
		message += base64.at(i);
	}
	message += newLine;
}

QString createEmailMessage(const QString &to, const QString &body,
    const QString &subj, const QString &boundary)
{
	const QString newLine("\n"); /* "\r\n" ? */

	/* Rudimentary header. */
	QString message("Subject: " + subj + newLine);
	if (!to.isEmpty()) {
		message += "To: " + to + newLine;
	}
	message += "MIME-Version: 1.0" + newLine;
	message += "Content-Type: multipart/mixed;" + newLine +
	    " boundary=\"" + boundary + "\"" + newLine;

	/* Body. */
	message += newLine;
	message += "--" + boundary + newLine;
	message += "Content-Type: text/plain; charset=UTF-8" + newLine;
	message += "Content-Transfer-Encoding: 8bit" + newLine;

	message += newLine;
	message += "-- " + newLine; /* Must contain the space. */
	message += body + newLine;
	return message;
}

void finishEmailMessage(QString &message, const QString &boundary)
{
	const QString newLine("\n"); /* "\r\n" ? */
	message += newLine + "--" + boundary + "--" + newLine;
}

QString generateBoundaryString(void)
{
	return "-----123456789asdfghj_" +
	    QDateTime::currentDateTimeUtc().toString("dd.MM.yyyy-HH:mm:ss.zzz");
}

QString generateEmailBodyText(qint64 dmId, const QString &from,
    const QString &to, const QString &delivered)
{
	QString body(QObject::tr("ID") + ": ");
	body += QString::number(dmId) + "\n";
	body += QObject::tr("FROM") + ": ";
	body += from + "\n";
	body += QObject::tr("TO") + ": ";
	body += to  + "\n";
	body += QObject::tr("DELIVERED") + ": ";
	body += delivered + "\n\n---\n";
	body += QObject::tr("This email has been generated with Datovka "
	    "application based on a data message (%1) delivered "
	    "to databox.").arg(dmId);
	body += "\n";
	return body;
}

void sendEmail(const QString &emailMessage, const QStringList &fileList,
   const QString &to, const QString &subject, const QString &body,
   const QString &msgId)
{
	Q_UNUSED(to);
	Q_UNUSED(subject);
	Q_UNUSED(body);
	Q_UNUSED(emailMessage);
	Q_UNUSED(msgId);
	Q_UNUSED(fileList);

#if defined Q_OS_IOS

	UrlOpener::createEmail(body, to, subject, fileList);

#elif defined Q_OS_ANDROID

	AndroidIO::createEmail(subject, to, body, fileList);

	// Old send message method for android.
	// It creates email without attachments.
	//QDesktopServices::openUrl(QUrl("mailto:" + to + "?"
	//    + "subject=" + subject + "&body=" + body));

#else

	QString tmpEmailFile = writeFile(appEmailDirPath(msgId),
	    msgId + "_mail.eml", emailMessage.toUtf8());
	QDesktopServices::openUrl(QUrl::fromLocalFile(tmpEmailFile));

#endif
}
