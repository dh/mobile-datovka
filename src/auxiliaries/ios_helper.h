/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>
#include <QString>
#include <QStringList>
#include <QUrl>

/*!
 * @brief Provides QT interface for some iOS objective-C based methods.
 */
class IosHelper : public QObject {
	Q_OBJECT

public:

	/* Defines import action for iOS File Picker */
	enum IosImportAction {
		IMPORT_NONE = 0,
		IMPORT_SEND,
		IMPORT_ZFOS,
		IMPORT_ZFO_DIR,
		IMPORT_CERT,
		IMPORT_JSON
	};
	Q_ENUM(IosImportAction)

	/* Holds some file info for uploading to iCloud */
	class FileICloud {
		public:
		QString srcFilePath;
		QString destiCloudPath;
	};

	/*!
	 * @brief Declare various properties to the QML system.
	 */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit IosHelper(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Store files to iCloud.
	 *
	 * @param[in] iCloudFiles List of file paths to be saved into iCloud.
	 */
	static
	void storeFilesToCloud(const QList<FileICloud> &iCloudFiles);

	/*!
	 * @brief Retrun short local send file path (without full sandbox path).
	 *
	 * @param[in] sandBoxFilePath Send file path.
	 * @return Short path to app sandbox where send file was stored.
	 */
	Q_INVOKABLE static
	QString getShortSendFilePath(const QString &sandBoxFilePath);

	/*!
	 * @brief Clear restore folder.
	 */
	Q_INVOKABLE static
	void clearRestoreFolder(const QString &folder);

	/*!
	 * @brief Clear backup and transfer folders.
	 */
	Q_INVOKABLE static
	void clearBackupTransferDirs(void);

	/*!
	 * @brief Clear send and tmp folders.
	 */
	Q_INVOKABLE static
	void clearSendAndTmpDirs(void);

	/*!
	 * @brief Create and open document picker controller for import.
	 *
	 * @param[in] action Import action.
	 * @param[in] allowedUtis Allowed iOS mime extensions (can be empty).
	 */
	Q_INVOKABLE
	void openDocumentPickerControllerForImport(enum IosImportAction action,
	    const QStringList &allowedUtis);

	/*!
	 * @brief Is activated when files have been selected with iOS file picker.
	 *
	 * @param[in] selectedFileUrls Path file list for QML.
	 */
	void importFilesToAppInbox(const QList<QUrl> &selectedFileUrls);

	/*!
	 * @brief Store files to device local storage.
	 *
	 * @param[in] srcFilePaths List of file paths to be saved into storage.
	 */
	static
	void storeFilesToDeviceStorage(const QStringList &srcFilePaths);

	/*!
	 * @brief iOS check for QML.
	 *
	 * @return True if iOS.
	 */
	Q_INVOKABLE static
	bool isIos(void);

	/*!
	 * @brief Get certificate file location.
	 *
	 * @return Full path to certificate directory.
	 */
	Q_INVOKABLE static
	QString getCertFileLocation(void);

signals:
	/*!
	 * @brief Is activated when a file has been chosen with document picker.
	 *
	 * @param[in] sendFilePaths Path file list for QML.
	 */
	void sendFilesSelectedSig(QStringList sendFilePaths);

	/*!
	 * @brief Is activated when a file has been chosen with document picker.
	 *
	 * @param[in] zfoFilePaths Path file list for QML.
	 */
	void zfoFilesSelectedSig(QStringList zfoFilePaths);

	/*!
	 * @brief Is activated when a file has been chosen with document picker.
	 *
	 * @param[in] certFilePaths Path file list for QML.
	 */
	void certFilesSelectedSig(QStringList certFilePaths);

	/*!
	 * @brief Is activated when a file has been chosen with document picker.
	 *
	 * @param[in] jsonFilePaths Path file list for QML.
	 */
	void jsonFilesSelectedSig(QStringList jsonFilePaths);

private:
	/*!
	 * @brief Move file from app temporary inbox to target path.
	 *
	 * @param[in] sourceFileUrl Source file url from inbox.
	 * @param[in] targePath Target file path.
	 * @return Full path where file was moved.
	 */
	QString moveFileToTargetPath(const QUrl &sourceFileUrl,
	    const QString &targePath);

	enum IosImportAction m_importAction; /*!< Holds import action type. */
};

/* Declare IosImportAction to QML. */
Q_DECLARE_METATYPE(IosHelper::IosImportAction)
