import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.iOsHelper 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {

    id: page1
    signal handlerLoader(string pageFile)
    property bool iOS: false

    Component.onCompleted: {
        iOS = iOSHelper.isIos()
        if (accountName !== "") {
            accountNameItem.textLine.text = accountName
        } else if (!iOS) {
            /* Force focus has wrong virtual keyboard behavior on the iOS. */
            accountNameItem.textLine.forceActiveFocus()
        }
        for (var i = 0; i < accountTypeRadioGroup.buttons.length; ++i) {
            if (accountTypeRadioGroup.buttons[i].testing === acntId.testing) {
                accountTypeRadioGroup.buttons[i].checked = true
            }
        }
    }

    function canNextStep() {
        if (accountNameItem.textLine.displayText === "") {
            errorText.visible = true
            accountNameItem.textLine.focus = true
            return
        }
        accountName = accountNameItem.textLine.displayText
        acntId.testing = accountTypeRadioGroup.checkedButton.testing
        accountTypeName = accountTypeRadioGroup.checkedButton.text
        handlerLoader("CreateAccountPage2.qml")
    }

    PageHeader {
        id: headerBar
        title: qsTr("Create Account: 1/4")
        onBackClicked: {
            handlerLoader("CreateAccountPage0.qml")
        }
        AccessibleOverlaidImageButton {
            id: actionButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            image.sourceSize.height: imageActionDimension
            image.source: "qrc:/ui/forward.svg"
            accessibleName: qsTr("Next step")
            onClicked: canNextStep()
        }
    }

    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight
        height: parent.height * 0.8
        Pane {
            id: flickContent
            anchors.fill: parent
            background: Rectangle {
                color: datovkaPalette.base
            }
            ButtonGroup {
                id: accountTypeRadioGroup
                onClicked: canNextStep()
            }
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                AccessibleText {
                    id: errorText
                    visible: false
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    color: "red"
                    text: qsTr("The account title must be filled in.")
                }
                TextLineItem {
                    id: accountNameItem
                    textLineTitle: "1. " + qsTr("Account Title")
                    placeholderText: qsTr("Enter custom account name.")
                    label.font.bold: true
                    isPassword: false
                }
                AccessibleText {
                    width: parent.width
                    color: datovkaPalette.text
                    wrapMode: Text.Wrap
                    text: qsTr("The account title is a user-specified name used for the identification of the account in the application (e.g. 'My Personal Data Box', 'Firm Box', etc.). The chosen name serves only for your convenience. The entry must be filled in.")
                }
                Text {
                    text: " "
                }
                AccessibleText {
                    color: datovkaPalette.text
                    font.bold: true
                    text: "2. " + qsTr("Account Type")
                }
                RadioButton {
                    ButtonGroup.group: accountTypeRadioGroup
                    property bool testing: false
                    text: qsTr("regular account")
                }
                RadioButton {
                    ButtonGroup.group: accountTypeRadioGroup
                    property bool testing: true
                    text: qsTr("test account")
                }
                AccessibleText {
                    width: parent.width
                    color: datovkaPalette.text
                    wrapMode: Text.Wrap
                    textFormat: TextEdit.RichText
                    onLinkActivated: Qt.openUrlExternally(link)
                    text: qsTr("The regular account option is used to access the production (official) ISDS environment (<a href=\"https://www.mojedatovaschranka.cz\">www.mojedatovaschranka.cz</a>). Test accounts are used to access the ISDS testing environment (<a href=\"https://www.czebox.cz\">www.czebox.cz</a>).")
                }
            } // Column
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
}
