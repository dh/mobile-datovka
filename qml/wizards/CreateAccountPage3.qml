import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.iOsHelper 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {

    id: page3
    signal handlerLoader(string pageFile)
    property bool iOS: false

    Component.onCompleted: {
        iOS = iOSHelper.isIos()
        if (loginMethod === AcntData.LIM_UNAME_PWD_CRT) {
            certificateLabel.visible = true
            certPathButtonId.visible = certificateLabel.visible
            certInfoText.visible = certificateLabel.visible
            certPathLabel.text = getFileNameFromPath(certFilePath)
            certPathLabel.visible = (certPathLabel.text !== "")
        } else if (loginMethod === AcntData.LIM_UNAME_MEP) {
            mepCodeItem.visible = true
            mepInfoText.visible = mepCodeItem.visible
            pwdItem.visible = !mepCodeItem.visible
            pwdLabel.visible = pwdItem.visible
            pwdInfoText.visible = pwdItem.visible
            mepCodeItem.textLine.text = mepCode
            pwdStr = ""
        }
        if (acntId.username !== "") {
            userNameItem.textLine.text = acntId.username
        } else if (!iOS) {
            /* Force focus has wrong virtual keyboard behavior on the iOS. */
            userNameItem.textLine.forceActiveFocus()
        }
        pwdItem.pwdTextLine.text = pwdStr
    }

    function setErrorLabel(errTxt) {
            errorText.text = errTxt
            errorText.color = "red"
            errorText.visible = true
    }

    function canNextStep() {
         if (Qt.inputMethod.visible) {
            Qt.inputMethod.commit()
            Qt.inputMethod.hide()
        }
        if (userNameItem.textLine.displayText === "") {
            setErrorLabel(qsTr("The username must be filled in."))
            userNameItem.textLine.focus = true
            return
        }
        if (loginMethod === AcntData.LIM_UNAME_MEP) {
            if (mepCodeItem.textLine.displayText === "") {
                setErrorLabel(qsTr("The communication code must be filled in."))
                mepCodeItem.textLine.focus = true
                return
             } else {
                mepCode = mepCodeItem.textLine.displayText
             }
        } else if (pwdItem.pwdTextLine.text === "") {
            setErrorLabel(qsTr("The password must be filled in."))
            pwdItem.pwdTextLine.focus = true
            return
        } else {
            pwdStr = pwdItem.pwdTextLine.text
        }
        if (loginMethod === AcntData.LIM_UNAME_PWD_CRT) {
            if (certFilePath === "") {
                setErrorLabel(qsTr("The certificate file must be specified."))
                return
             }
        }
        acntId.username = userNameItem.textLine.displayText
        handlerLoader("CreateAccountPage4.qml")
    }

    FileDialogue {
        id: fileDialogue
        multiSelect: false
        onFinished: {
            var listLength = pathListModel.count
            // set last selected certificate path
            if (listLength > 0) {
                if (pathListModel.get(listLength-1).path === "") {
                    if (certPathLabel.text === "") {
                        certPathLabel.visible = false
                        certFilePath = ""
                    }
                } else {
                    certFilePath = pathListModel.get(listLength-1).path
                    certPathLabel.text = getFileNameFromPath(certFilePath)
                    certPathLabel.visible = true
                }
            }
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Create Account: 3/4")
        onBackClicked: {
            handlerLoader("CreateAccountPage2.qml")
        }
        AccessibleOverlaidImageButton {
            id: actionButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            image.sourceSize.height: imageActionDimension
            image.source: "qrc:/ui/forward.svg"
            accessibleName: qsTr("Next step")
            onClicked: canNextStep()
        }
    }

    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight
        height: parent.height * 0.8
        Pane {
            id: flickContent
            anchors.fill: parent
            background: Rectangle {
                color: datovkaPalette.base
            }
            ButtonGroup {
                id: loginMethodRadioGroup
            }
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                Text {
                    color: datovkaPalette.dark
                    width: parent.width
                    text: qsTr("Account title") + ": <b>" + accountName + "</b>"
                }
                Text {
                    color: datovkaPalette.dark
                    width: parent.width
                    text: qsTr("Account type") + ": <b>" + accountTypeName + "</b>"
                }
                Text {
                    color: datovkaPalette.dark
                    width: parent.width
                    text: qsTr("Login method") + ": <b>" + loginMethodName + "</b>"
                }
                Rectangle {
                    height: 1
                    width: parent.width
                    color: datovkaPalette.mid
                }
                AccessibleText {
                    id: errorText
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    color: datovkaPalette.text
                    wrapMode: Text.Wrap
                    text: qsTr("Fill in your login credentials. All entries must be filled in.")
                }
                TextLineItem {
                    id: userNameItem
                    textLineTitle: "4. " + qsTr("Username")
                    placeholderText: qsTr("Enter the login name.")
                    label.font.bold: true
                    inputMethodHints: Qt.ImhLowercaseOnly | Qt.ImhPreferLowercase | Qt.ImhNoPredictiveText
                    isPassword: false
                    textLine.onTextChanged: {
                        if (!isLowerCaseWithoutNonPrintableChars(userNameItem.textLine.displayText)) {
                            userNameItem.textLine.color = "red"
                            setErrorLabel(qsTr("Warning: The username should contain only combinations of lower-case letters and digits."))
                        } else {
                            userNameItem.textLine.color = datovkaPalette.text
                            errorText.color = userNameItem.textLine.color
                            errorText.text = qsTr("Fill in your login credentials. All entries must be filled in.")
                        }
                    }
                }
                AccessibleText {
                    width: parent.width
                    color: datovkaPalette.text
                    wrapMode: Text.Wrap
                    text: qsTr("The username must consist of at least 6 characters without spaces (only combinations of lower-case letters and digits are allowed). Notification: The username is not a data-box ID.")
                }
                Text {
                    text: " "
                }
                AccessibleText {
                    id: pwdLabel
                    color: datovkaPalette.text
                    text: "5. " + qsTr("Password")
                    font.bold: true
                }
                TimedPasswordLine {
                    id: pwdItem
                    anchors.left: parent.left
                    anchors.right: parent.right
                    placeholderText: qsTr("Enter the password.")
                    pwdEyeIconVisible: true
                    hideAfterMs: 0
                }
                AccessibleText {
                    id: pwdInfoText
                    width: parent.width
                    color: datovkaPalette.text
                    wrapMode: Text.Wrap
                    text: qsTr("The password must be valid and non-expired. To check whether you've entered the password correctly you may use the icon in the field on the right. Note: You must fist change the password using the ISDS web portal if it is your very first attempt to log into the data box.")
                }
                TextLineItem {
                    id: mepCodeItem
                    visible: false
                    textLineTitle: "5. " + qsTr("Communication Code")
                    placeholderText: qsTr("Enter the communication code.")
                    label.font.bold: true
                    isPassword: false
                }
                AccessibleText {
                    id: mepInfoText
                    visible: false
                    width: parent.width
                    color: datovkaPalette.text
                    textFormat: TextEdit.RichText
                    wrapMode: Text.Wrap
                    onLinkActivated: Qt.openUrlExternally(link)
                    text: qsTr("The <a href=\"%1\">communication code</a> is a string which can be generated in the ISDS web portal. You have to have the Mobile Key application installed. The Mobile Key application needs to be paired with the corresponding data-box account on the ISDS web portal.").arg("https://secure.nic.cz/files/datove_schranky/redirect/mobile-manual-mep.html")
                }
                Text {
                    text: " "
                }
                AccessibleText {
                    id: certificateLabel
                    visible: false
                    color: datovkaPalette.text
                    font.bold: true
                    text: "6. " + qsTr("Certificate File")
                }
                AccessibleText {
                    id: certPathLabel
                    visible: false
                    width: parent.width
                    color: datovkaPalette.text
                    wrapMode: Text.WrapAnywhere
                    text: ""
                    Connections {
                       target: iOSHelper
                        function onCertFilesSelectedSig(certFilePaths) {
                            if (certFilePaths.length > 0) {
                                certFilePath = certFilePaths[0]
                                certPathLabel.text = getFileNameFromPath(certFilePath)
                                certPathLabel.visible = true
                                fileDialogue.close()
                            }
                        }
                    }
                }
                AccessibleButton {
                    id: certPathButtonId
                    visible: false
                    text: qsTr("Choose file")
                    height: inputItemHeight
                    font.pointSize: defaultTextFont.font.pointSize
                    onClicked: {
                        if (iOS) {
                            iOSHelper.openDocumentPickerControllerForImport(IosImportAction.IMPORT_CERT, [])
                            fileDialogue.raise("Select a certificate file", ["*.pem","*.p12","*.pfx"], true, iOSHelper.getCertFileLocation())
                        } else {
                            fileDialogue.raise("Select a certificate file", ["*.pem","*.p12","*.pfx"], true, "")
                        }
                    }
                }
                AccessibleText {
                    id: certInfoText
                    visible: false
                    width: parent.width
                    color: datovkaPalette.text
                    wrapMode: Text.Wrap
                    text: qsTr("The certificate file is needed for authentication purposes. The supplied file must contain a certificate and its corresponding private key. Only PEM and PFX file formats are accepted.")
                }
            } // Column
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
}
