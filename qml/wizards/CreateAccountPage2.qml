import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1

Item {

    id: page2
    signal handlerLoader(string pageFile)

    Component.onCompleted: {
        for (var i = 0; i < loginMethodRadioGroup.buttons.length; ++i) {
            if (loginMethodRadioGroup.buttons[i].loginMethod === loginMethod) {
                loginMethodRadioGroup.buttons[i].checked = true
            }
        }
    }

    function canNextStep() {
        loginMethod = loginMethodRadioGroup.checkedButton.loginMethod
        loginMethodName = loginMethodRadioGroup.checkedButton.text
        handlerLoader("CreateAccountPage3.qml")
    }

    PageHeader {
        id: headerBar
        title: qsTr("Create Account: 2/4")
        onBackClicked: {
            handlerLoader("CreateAccountPage1.qml")
        }
        AccessibleOverlaidImageButton {
            id: actionButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            image.sourceSize.height: imageActionDimension
            image.source: "qrc:/ui/forward.svg"
            accessibleName: qsTr("Next step")
            onClicked: canNextStep()
        }
    }

    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight
        height: parent.height * 0.8
        Pane {
            id: flickContent
            anchors.fill: parent
            background: Rectangle {
                color: datovkaPalette.base
            }
            ButtonGroup {
                id: loginMethodRadioGroup
                onClicked: canNextStep()
            }
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                Text {
                    color: datovkaPalette.dark
                    width: parent.width
                    text: qsTr("Account title") + ": <b>" + accountName + "</b>"
                }
                Text {
                    color: datovkaPalette.dark
                    width: parent.width
                    text: qsTr("Account type") + ": <b>" + accountTypeName + "</b>"
                }
                Rectangle {
                    height: 1
                    width: parent.width
                    color: datovkaPalette.mid
                }
                AccessibleText {
                    color: datovkaPalette.text
                    font.bold: true
                    text: "3. " + qsTr("Login Method")
                }
                RadioButton {
                    ButtonGroup.group: loginMethodRadioGroup
                    property int loginMethod: AcntData.LIM_UNAME_PWD
                    text: qsTr("username + password")
                }
                RadioButton {
                    ButtonGroup.group: loginMethodRadioGroup
                    property int loginMethod: AcntData.LIM_UNAME_MEP
                    text: qsTr("username + Mobile Key")
                }
                RadioButton {
                    ButtonGroup.group: loginMethodRadioGroup
                    property int loginMethod: AcntData.LIM_UNAME_PWD_TOTP
                    text: qsTr("username + password + SMS")
                }
                RadioButton {
                    ButtonGroup.group: loginMethodRadioGroup
                    property int loginMethod: AcntData.LIM_UNAME_PWD_HOTP
                    text: qsTr("username + password + security code")
                }
                RadioButton {
                    ButtonGroup.group: loginMethodRadioGroup
                    property int loginMethod: AcntData.LIM_UNAME_PWD_CRT
                    text: qsTr("username + password + certificate")
                }
                AccessibleText {
                    id: loginMethodText
                    width: parent.width
                    color: datovkaPalette.text
                    wrapMode: Text.Wrap
                    text: qsTr("Select the login method which you use to access the data box in the %1 ISDS environment.").arg((acntId.testing) ? qsTr("testing") : qsTr("production"))
                }
            } // Column
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
}
