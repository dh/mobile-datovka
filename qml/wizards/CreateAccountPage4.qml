import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {

    id: page4
    signal handlerLoader(string pageFile)
    property string stepNumber: "6. "
    property string loginInfoText: ""

    Component.onCompleted: {
        if (loginMethod === AcntData.LIM_UNAME_PWD_CRT) {
            crtFilePath.visible = true
            stepNumber = "7. "
            loginInfoText = qsTr("During the log-in procedure you will be asked to enter a private key password.")
        } else if (loginMethod === AcntData.LIM_UNAME_MEP) {
            rememberPassword.visible = false
            loginInfoText = qsTr("During the log-in procedure you will be asked to confirm a notification in the Mobile Key application.")
        } else if (loginMethod === AcntData.LIM_UNAME_PWD_TOTP) {
            loginInfoText = qsTr("During the log-in procedure you will be asked to enter a code which you should obtain via an SMS.")
        } else if (loginMethod === AcntData.LIM_UNAME_PWD_HOTP) {
            loginInfoText = qsTr("During the log-in procedure you will be asked to enter a security code.")
        }
    }

    function createAccount() {
        if (accounts.createAccount(accountModel, loginMethod,
            accountName, acntId.username, pwdStr, mepCode,
            acntId.testing, rememberPassword.checked,
            useLS.checked, useSyncWithAll.checked,
            certFilePath)) {
                isds.doIsdsAction("addNewAccount", acntId)
         }
    }

    AcntId {
        id: actionAcntId
    }

    Connections {
        target: isds
        function onDownloadAccountInfoFinishedSig(userName, testing) {
            settings.saveAllSettings(accountModel)
            pageView.pop(StackView.Immediate)
        }
        function onRunGetAccountInfoSig(userName, testing) {
             actionAcntId.username = userName
             actionAcntId.testing = testing
             settings.saveAllSettings(accountModel)
             isds.getAccountInfo(actionAcntId)
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Create Account: 4/4")
        onBackClicked: {
            handlerLoader("CreateAccountPage3.qml")
        }
        AccessibleOverlaidImageButton {
            id: actionButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            image.sourceSize.height: imageActionDimension
            image.source: "qrc:/ui/checkbox-marked-circle.svg"
            accessibleName: qsTr("Create account")
            onClicked: createAccount()
        }
    }

    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight
        height: parent.height * 0.8
        Pane {
            id: flickContent
            anchors.fill: parent
            background: Rectangle {
                color: datovkaPalette.base
            }
            ButtonGroup {
                id: loginMethodRadioGroup
            }
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                Text {
                    color: datovkaPalette.dark
                    width: parent.width
                    text: qsTr("Account title") + ": <b>" + accountName + "</b>"
                }
                Text {
                    color: datovkaPalette.dark
                    width: parent.width
                    text: qsTr("Account type") + ": <b>" + accountTypeName + "</b>"
                }
                Text {
                    color: datovkaPalette.dark
                    width: parent.width
                    text: qsTr("Login method") + ": <b>" + loginMethodName + "</b>"
                }
                Text {
                    color: datovkaPalette.dark
                    width: parent.width
                    text: qsTr("Username") + ": <b>" + acntId.username + "</b>"
                }
                Text {
                    id: crtFilePath
                    visible: false
                    color: datovkaPalette.dark
                    width: parent.width
                    text: qsTr("Certificate") + ": <b>" + getFileNameFromPath(certFilePath) + "</b>"
                }
                Rectangle {
                    height: 1
                    width: parent.width
                    color: datovkaPalette.mid
                }
                AccessibleText {
                    color: datovkaPalette.text
                    font.bold: true
                    text: stepNumber + qsTr("Local Account Settings")
                }
                AccessibleSwitch {
                    id: rememberPassword
                    text: qsTr("Remember password")
                    font.pointSize: defaultTextFont.font.pointSize
                    checked: true
                }
                AccessibleSwitch {
                    id: useLS
                    font.pointSize: defaultTextFont.font.pointSize
                    text: qsTr("Use local storage (database)")
                    checked: true
                }
                AccessibleText {
                    color: datovkaPalette.text
                    width: parent.width
                    text: (useLS.checked ?
                           qsTr("Copies of messages and attachments will be locally stored. No active internet connection is needed to access locally stored data.") :
                           qsTr("Copies of messages and attachments will be stored only temporarily in memory. These data will be lost on application exit."))
                    wrapMode: Text.Wrap
                }
                AccessibleSwitch {
                    id: useSyncWithAll
                    font.pointSize: defaultTextFont.font.pointSize
                    text: qsTr("Synchronise together with all accounts")
                    checked: true
                }
                AccessibleText {
                    color: datovkaPalette.text
                    width: parent.width
                    text: useSyncWithAll.checked ?
                        qsTr("The account will be included into the synchronisation process of all accounts.") :
                        qsTr("The account won't be included into the synchronisation process of all accounts.")
                    wrapMode: Text.Wrap
                }
                Rectangle {
                    height: 1
                    width: parent.width
                    color: datovkaPalette.mid
                }
                AccessibleButton {
                    text: qsTr("Create Account")
                    height: inputItemHeight
                    font.pointSize: defaultTextFont.font.pointSize
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: createAccount()
                }
                AccessibleText {
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    color: datovkaPalette.text
                    wrapMode: Text.Wrap
                    font.bold: true
                    text: qsTr("All done. The account will be created only when the application successfully logs into the data box using the supplied login credentials.") + " " + loginInfoText
                }
            } // Column
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
}
