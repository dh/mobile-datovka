/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0

/*
 * Date input dialogue.
 */
Dialog {
    id: root

    property var fieldIndex: null

    signal finished(int index, date selectedDate)

    function isValidDate(d) {
        return d instanceof Date && !isNaN(d);
    }

    function openCalendarDialogue(index, selDate, title) {
        root.title = title
        root.fieldIndex = index
        var locale = Qt.locale()
        var today = new Date()
        var filledDate = Date.fromLocaleDateString(locale, selDate, "yyyy-MM-dd")
        if (isValidDate(today)) {
            calendar.maximumDate = today
        }
        calendar.selectedDate = (isValidDate(filledDate)) ? filledDate : today
        root.open()
    }

    // center dialogue
    x: parent.width / 2 - width / 2
    y: parent.height / 2 - height / 2

    width: parent.width - 4*defaultMargin

    focus: true
    modal: true
    title: qsTr("Calendar dialog")
    standardButtons: Dialog.Ok | Dialog.Cancel

    contentItem: Calendar {
        id: calendar
    }

    onAccepted: finished(fieldIndex, calendar.selectedDate)
}
