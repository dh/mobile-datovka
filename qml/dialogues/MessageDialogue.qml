/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.2
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.dialogues 1.0

/*
 * All objectName properties are used to navigate through the structure from
 * within C++ code.
 */

Item {
    id: root
    objectName: "root"

    /* Expose children. */
    property alias dialogue: dialogue

    signal dialogueClosed(int button, int customButton)

    Dialog {
        id: dialogue
        objectName: "dialogue"

        property int icon: Dialogues.NO_ICON
        property alias content: content
        property int dlgButtons: Dialogues.NO_BUTTON

        //visible: true /* Set from C++. */
        title: ""

        /* Modality uses QtQuick.Dialogs 1.2. */
        modality: Qt.ApplicationModal
        /*
         * TODO -- To use Dialog as defined in QtQuick.Controls 2 then you have
         * to remove line with 'import QtQuick.Dialogs 1.2'. Also you must use
         * 'modal' instead of 'modality'.
         * Also you must use QtQuick 2.8 and QtQuick.Controls 2.1 to use the
         * DialogButtonBox container.
         */
        //modal: true

        standardButtons: Dialog.NoButton

        Column {
            id: content

            property alias messageText: messageText
            property alias infoMessageText: infoMessageText

            anchors.fill: parent

            AccessibleText { /* Currently there is only a text information. */
                id: importanceText
                objectName: "importanceText"

                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width

                text: (dialogue.icon == Dialogues.NO_ICON) ? "" :
                      (dialogue.icon == Dialogues.QUESTION ) ? qsTr("Question") + ":" :
                      (dialogue.icon == Dialogues.INFORMATION ) ? qsTr("Information") + ":" :
                      (dialogue.icon == Dialogues.WARNING ) ? qsTr("Warning") + ":" :
                      (dialogue.icon == Dialogues.CRITICAL ) ? qsTr("Critical") + ":" : ""

                //color: datovkaPalette.mid /* TODO */
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap

                visible: dialogue.icon != Dialogues.NO_ICON
            }
            AccessibleText {
                id: messageText
                objectName: "messageText"

                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width

                text: ""

                //color: datovkaPalette.mid /* TODO */
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
            }
            AccessibleText {
                id: infoMessageText
                objectName: "infoMessageText"

                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width

                text: ""

                //color: datovkaPalette.mid /* TODO */
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
            }
        }

//        footer: DialogButtonBox {
//        }

        onDlgButtonsChanged: {
            var buttons = Dialog.NoButton
            buttons |= (dlgButtons & Dialogues.OK) ? Dialog.Ok : Dialog.NoButton
            buttons |= (dlgButtons & Dialogues.CANCEL) ? Dialog.Cancel : Dialog.NoButton
            buttons |= (dlgButtons & Dialogues.YES) ? Dialog.Yes : Dialog.NoButton
            buttons |= (dlgButtons & Dialogues.NO) ? Dialog.No : Dialog.NoButton
            console.log("New buttons  " + dlgButtons + " " + buttons)
            dialogue.standardButtons = buttons
        }

        onVisibleChanged: {
            if (!visible) {
                root.dialogueClosed(-1, -1)
            }
        }
    }
}
