/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.2
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.dialogues 1.0

/*
 * All objectName properties are used to navigate through the structure from
 * within C++ code.
 */

Item {
    id: root
    objectName: "root"

    /* Expose children. */
    property alias dialogue: dialogue

    signal dialogueClosed()

    Dialog {
        id: dialogue
        objectName: "dialogue"

        property alias content: content

        property bool explicitPasteMenu: false
        property int dlgEchoMode: Dialogues.EM_NORMAL

        //visible: true /* Set from C++. */
        title: ""

        /* Modality uses QtQuick.Dialogs 1.2. */
        modality: Qt.ApplicationModal
        /*
         * TODO -- To use Dialog as defined in QtQuick.Controls 2 then you have
         * to remove line with 'import QtQuick.Dialogs 1.2'. Also you must use
         * 'modal' instead of 'modality'.
         * Also you must use QtQuick 2.8 and QtQuick.Controls 2.1 to use the
         * DialogButtonBox container.
         */
        //modal: true

        /*
         * https://forum.qt.io/topic/39465/solved-custom-qtquick-messagedialog/9
         * says that there is a bug in Dialog implementation on iOS.
         * There should be a workaround by setting the buttons just before
         * displaying the dialogue.
         */
        //standardButtons: StandardButton.Ok | StandardButton.Cancel

        Column {
            id: content

            property alias messageText: messageText
            property alias textInput: textInput

            anchors.fill: parent

            AccessibleText {
                id: messageText
                objectName: "messageText"

                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width

                text: ""

                //color: datovkaPalette.mid /* TODO */
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
            }
            AccessibleTextField {
                id: textInput
                objectName: "textInput"

                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width * 0.75
                text: ""
                placeholderText: ""

                focus: true

                echoMode: (dialogue.dlgEchoMode == Dialogues.EM_NORMAL) ? TextInput.Normal :
                          (dialogue.dlgEchoMode == Dialogues.EM_PWD) ? TextInput.Password :
                          (dialogue.dlgEchoMode == Dialogues.NOECHO) ? TextInput.NoEcho :
                          (dialogue.dlgEchoMode == Dialogues.EM_PWD_ECHOONEDIT) ? TextInput.PasswordEchoOnEdit : TextInput.Normal
                passwordMaskDelay: 500 // milliseconds

                InputLineMenu {
                    id: textInputMenu
                    inputTextControl: textInput
                    isPassword: true
                }

                onPressAndHold: {
                    if (dialogue.explicitPasteMenu) {
                        textInputMenu.implicitWidth = content.computeMenuWidth(textInputMenu)
                        textInputMenu.open()
                    }
                }
            }

            /* TODO -- Defined also in main.qml . */
            Text {
                id: dummyText
                text: ""
                visible: false
                wrapMode: Text.NoWrap
                elide: Text.ElideNone
            }
            function computeMenuWidth(menu) {
                var w = 0.0
                for (var i = 0; i < menu.contentData.length; i++) {
                    dummyText.text = menu.contentData[i].text + "www"
                    if (w < dummyText.width) {
                        w = dummyText.width
                    }
                }
                dummyText.text = ""
                return Math.round(w)
            }
        }

        onVisibleChanged: {
            if (!visible) {
                root.dialogueClosed()
            }
        }
    }
}
