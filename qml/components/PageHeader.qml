/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import cz.nic.mobileDatovka 1.0

/*
 * Page header component.
 */
Rectangle {
    id: root

    /* These properties must be set by caller. */
    property string title: ""
    property bool isFirstPage: false

    signal backClicked()

    anchors.top: parent.top
    width: parent.width
    height: baseHeaderHeight
    color: pageHeaderBgColor
    z: 1
    AccessibleOverlaidImageButton {
        id: backIcon
        visible: !isFirstPage
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: defaultMargin
        image.sourceSize.height: imageNavigateDimension
        image.source: (isiOS) ? "qrc:/ui/back.svg" : "qrc:/ui/android-arrow-back.svg"
    }
    AccessibleText {
        id: backText
        visible: (isiOS && !isFirstPage)
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: backIcon.right
        color: actionIconColor
        text: qsTr("Back")
    }
    MouseArea {
        function handleClick() {
            if (typeof(statusBar) != "undefined") {
                statusBar.visible = false
            }
            backClicked()
        }
        visible: !isFirstPage
        anchors.left: backIcon.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: backText.right
        Accessible.role: Accessible.Button
        Accessible.name: backText.text
        Accessible.onPressAction: {
            handleClick()
        }
        onClicked: {
            handleClick()
        }
    }
    AccessibleText {
        anchors.centerIn: parent
        font.bold: true
        color: pageHeaderTextColor
        text: title
    }
    Rectangle {
        anchors.bottom: parent.bottom
        height: 1
        width: parent.width
        color: datovkaPalette.dark
    }
}
