/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import cz.nic.mobileDatovka 1.0

/*
 * Accessible menu component.
 *
 * An object containing functions to be called and a model in specified form
 * must be provided.
 */
ScrollableListView {
    id: root
    delegateHeight: baseHeaderHeight

    /* These properties must be set by caller. */
    property var funcArr: null /* https://stackoverflow.com/a/24376300 */

    /*
     * The property funcArr must hold an object (associative array).
     * The keys must be strings, values must be functions.
     * e.g.:
        property var funcs: {
            "funcName": function callFuncName {
                console.log("Calling function funcName.")
            }
        }

     * The model entries must contain these properties.
     *   image - (string containing path of image resource)
     *           image to be shown in the menu before the text
     *   showEntry - (boolean value)
     *               true means that the entire menu entry is being shown
     *   showNext - (boolean value)
     *              true means that an further menu is being indicated
     *   name - (string with action description)
     *          action
     *   funcName - (string holding one of function array keys)
     *              string identifying function to be called
     * e.g.:
        ListModel {
            id: listModel
            ListElement {
                image: "qrc:/ui/settings.svg"
                showEntry: true
                showNext: false
                name: qsTr("Perform some action")
                funcName: "funcName"
            }
        }
     */

    /*
     * Return the first index of the element having a specified property of
     * specified value.
     *
     * Returns -1 if nothing found.
     */
    function get_model_index(model, property, value) {
        for (var i = 0; i < model.count; ++i) {
            if (model.get(i)[property] === value) {
                return i;
            }
        }
        /* Not found. */
        return -1;
    }

    /* https://stackoverflow.com/a/7356528 */
    function isFunction(functionToCheck) {
        var getType = {};
        return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
    }

    Component {
        id: menuDelegate

        Rectangle {

            property string imgSource: image
            visible: showEntry
            color: datovkaPalette.base
            height: showEntry ? root.delegateHeight : 0 /* Collapse when not visible. */
            width: parent.width
            MenuImage {
                id: menuImage
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: defaultMargin
                image.source: imgSource
            }
            Text {
                id: menuText
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: menuImage.right
                anchors.leftMargin: defaultMargin * 2
                anchors.right: parent.right
                color: datovkaPalette.text
                text: name
            }
            NextOverlaidImage {
                visible: showNext
            }
            MouseArea {
                function handleClick() {
                    /* https://stackoverflow.com/a/1098955 */
                    if (funcName in funcArr) {
                        if (isFunction(funcArr[funcName])) {
                            /* Call function. */
                            funcArr[funcName]()
                        } else {
                            console.log("'" + funcName + "' is not a function.")
                        }
                    } else {
                        console.log("Unknown function '" + funcName + "'.")
                    }
                }

                anchors.fill: parent

                Accessible.role: Accessible.Button
                Accessible.name: name
                Accessible.onScrollDownAction: {
                    root.scrollDown()
                }
                Accessible.onScrollUpAction: {
                    root.scrollUp()
                }
                Accessible.onPressAction: {
                    handleClick()
                }
                onClicked: {
                    handleClick()
                }
            }
            Rectangle {
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                height: 1
                width: (isiOS && root.count-1 !== index) ? menuText.width : parent.width
                color: (root.count-1 === index) ? datovkaPalette.dark : datovkaPalette.mid
            }
        }
    }

    delegate: menuDelegate
}
