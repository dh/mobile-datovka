/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1

Rectangle {
    id: root
    visible: false
    anchors.left: parent.left
    anchors.bottom: parent.bottom
    anchors.right: parent.right
    height: parent.height * 0.3
    color: datovkaPalette.text
    Rectangle {
        anchors.top: parent.top
        anchors.right: parent.right
        color: datovkaPalette.base
        width: imageActionDimension
        height: imageActionDimension
        z: 1
        AccessibleOverlaidImageButton {
            id: closeButton
            anchors.centerIn: parent
            image.sourceSize.height: imageActionDimension
            image.source: "qrc:/ui/remove.svg"
            accessibleName: qsTr("Close log bar")
            onClicked: {
                root.visible = false
            }
        }
    } // Rectangle
    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        contentY: contentHeight-height
        clip: true
        Pane {
            id: flickContent
            anchors.fill: parent
            background: parent
            TextEdit {
                id: logBarText
                color: datovkaPalette.base
                anchors.fill: parent
                readOnly: true
                wrapMode: Text.WordWrap
                font.pointSize: textFontSizeSmall
                text: log.loadLogContent("")
                Connections {
                    target: log
                    function onAppendNewLogMessage(newLog) {
                        logBarText.text += newLog
                    }
                }
            } // TextEdit
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
}
