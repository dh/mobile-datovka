/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1

Rectangle {
    id: root
    z: 1
    visible: false
    property int statusBarTimer: 5000
    anchors.left: parent.left
    anchors.bottom: parent.bottom
    anchors.right: parent.right
    height: baseHeaderHeight
    color: datovkaPalette.text

    Column {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.margins: defaultMargin
        spacing: formItemVerticalSpacing
        AccessibleText {
            id: statusBarText
            color: datovkaPalette.base
            anchors.horizontalCenter: parent.horizontalCenter
            text: mainWindow.width + " x "+ mainWindow.height
            Connections {
                target: isds
                function onStatusBarTextChanged(txt, busy, isVisible) {
                    root.visible = isVisible
                    statusBarText.text = txt
                    timer.running = !busy
                    progressBar.visible = busy
                }
                function onStatusBarTextMepChanged(txt) {
                    root.visible = true
                    statusBarText.text = txt
                    timer.running = false
                    progressBar.visible = false
                }
            }
            Connections {
                target: files
                function onStatusBarTextChanged(txt, busy) {
                    root.visible = true
                    statusBarText.text = txt
                    timer.running = !busy
                    progressBar.visible = busy
                }
            }
            Connections {
                target: settings
                function onStatusBarTextChanged(txt, busy) {
                    root.visible = true
                    statusBarText.text = txt
                    timer.running = !busy
                    progressBar.visible = busy
                }
            }
            Connections {
                target: recordsManagement
                function onStatusBarTextChanged(txt, busy, isVisible) {
                    root.visible = isVisible
                    statusBarText.text = txt
                    timer.running = !busy
                    progressBar.visible = busy
                }
            }
        }
        ProgressBar {
            id: progressBar
            anchors.horizontalCenter: parent.horizontalCenter
            indeterminate: true
        }
    }
    Timer {
        id: timer
        interval: statusBarTimer
        running: false
        repeat: false
        onTriggered: {
            // reset timer and clear status text
            timer.running = false
            root.visible = false
            statusBarText.text = ""
        }
    }
}
