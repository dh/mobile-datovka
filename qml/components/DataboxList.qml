/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.modelEntries 1.0

ScrollableListView {
    id: root
    delegateHeight: listItemHeight

    /* These properties must be set by caller. */
    property bool canDetailBoxes: false // enables viewing of box details
    property bool canSelectBoxes: false // enables selecting of entries
    property bool canDeselectBoxes: false // enables deselecting of entries
    property bool canRemoveBoxes: false // enables removing of entries
    property bool localContactFormat: false // false - format of contacts obtained from ISDS server
                                            // true - contacts gathered from local storage

    /* This signal should be captured to implement model interaction. */
    signal boxDetail(string boxId)
    signal boxSelect(string boxId)
    signal boxDeselect(string boxId)
    signal boxRemove(string boxId)

    delegate: Rectangle {
        id: dbItem
        height: root.delegateHeight
        width: root.width
        color: rDbSelected ? selectedItemBgColor : datovkaPalette.base
        ColumnLayout {
            id: databoxColumn
            anchors.fill: parent
            anchors.margins: defaultMargin
            anchors.verticalCenter: parent.verticalCenter
            spacing: defaultMargin
            Text {
                id: boxOwnerName
                Layout.preferredWidth: parent.width - nextNavElement.width
                elide: Text.ElideRight
                text: rDbName
                color: textHighlightColor
                font.bold: true
            }
            Text {
                id: boxOwnerAddress
                Layout.preferredWidth: parent.width - nextNavElement.width
                elide: Text.ElideRight
                color: datovkaPalette.text
                text: rDbAddress
            }
            Text {
                id: boxInfo
                font.pointSize: textFontSizeSmall
                color: imageDarkerColor
                text: if (rDbIc != "" && rDbType != "") {
                        "ID: " + rDbID + "   (" + rDbType + ")   IČO: " + rDbIc + "   " + rDbSendOption
                    } else if (rDbIc == "" && rDbType != "") {
                        "ID: " + rDbID + "   (" + rDbType + ")" + "   " + rDbSendOption
                    } else if (rDbIc != "" && rDbType == "") {
                        "ID: " + rDbID + "   IČO: " + rDbIc + "   " + rDbSendOption
                    } else  {
                        "ID: " + rDbID + "   " + rDbSendOption
                    }
            }
        } // ColumnLayout
        MouseArea {
            /*
             * Returns specified value:
             * -1 - none ov below
             *  0 - show box detail
             *  1 - deselect box
             *  2 - select box
             */
            function actionOnClick() {
                var ret = -1;
                if (canDetailBoxes && !(canSelectBoxes || canDeselectBoxes)) {
                    ret = 0;
                } else if (!canDetailBoxes && (canSelectBoxes || canDeselectBoxes)) {
                    if (rDbSelected && canDeselectBoxes) {
                        ret = 1;
                    } else if (!rDbSelected && canSelectBoxes) {
                        ret = 2;
                    }
                }
                return ret;
            }

            /* Construct string to be presented to accessibility interface. */
            function accessibleText() {
                var aText = "";

                switch (actionOnClick()) {
                case 0:
                    aText += qsTr("View details about data box '%1' (identifier '%2').").arg(rDbName).arg(rDbID);
                    break;
                case 1:
                    aText += qsTr("Deselect data box '%1' (identifier '%2').").arg(rDbName).arg(rDbID);
                    break;
                case 2:
                    aText += qsTr("Select data box '%1' (identifier '%2').").arg(rDbName).arg(rDbID);
                    break;
                default:
                    break;
                }

                return aText;
            }

            function handleClick() {
                switch (actionOnClick()) {
                case 0:
                    root.boxDetail(rDbID)
                    break;
                case 1:
                    root.boxDeselect(rDbID)
                    break;
                case 2:
                    root.boxSelect(rDbID)
                    break;
                default:
                    break;
                }
            }

            anchors.fill: parent

            Accessible.role: Accessible.Button
            Accessible.name: accessibleText()
            Accessible.onScrollDownAction: {
                root.scrollDown()
            }
            Accessible.onScrollUpAction: {
                root.scrollUp()
            }
            Accessible.onPressAction: {
                handleClick()
            }
            onClicked: {
                handleClick()
            }
        }
        NextOverlaidImage {
            id: nextNavElement
            image.source: {
                // set icon based on the databox list operation
                if (canDetailBoxes && !(canSelectBoxes || canDeselectBoxes) && !canRemoveBoxes) {
                    // we can view data box detail only
                    "qrc:/ui/next.svg"
                } else if (!canDetailBoxes && (canSelectBoxes || canDeselectBoxes) && !canRemoveBoxes) {
                    if (rDbSelected && canDeselectBoxes) {
                        "qrc:/ui/subtract.svg"
                    } else if (!rDbSelected && canSelectBoxes) {
                        "qrc:/ui/add.svg"
                    }
                } else if (!canDetailBoxes && !(canSelectBoxes || canDeselectBoxes) && canRemoveBoxes) {
                    // we can remove data box from send message recipient list
                    "qrc:/ui/remove.svg"
                }
            }
            MouseArea {
                // visible only if canDetailBoxes
                visible: (!canDetailBoxes && !(canSelectBoxes || canDeselectBoxes) && canRemoveBoxes)

                /*
                 * Returns true if can delete entries.
                 */
                function canRemoveEntries() {
                    return !canDetailBoxes && !(canSelectBoxes || canDeselectBoxes) && canRemoveBoxes;
                }

                /* Construct string to be presented to accessibility interface. */
                function accessibleText() {
                    var aText = "";

                    if (canRemoveEntries()) {
                        aText += qsTr("Remove data box '%1' (identifier '%2').").arg(rDbName).arg(rDbID);
                    }

                    return aText;
                }

                function handleClick() {
                    /* We must allow removal of existing recipients from model. */
                    if (canRemoveEntries()) {
                        root.boxRemove(rDbID)
                    }
                }

                anchors.fill: parent

                Accessible.role: Accessible.Button
                Accessible.name: accessibleText()
                Accessible.onPressAction: {
                    handleClick()
                }
                onClicked: {
                    handleClick()
                }
            }
        } // Rectangle
        Rectangle {
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            height: 1
            width: (isiOS && root.count-1 !== index) ? parent.width - defaultMargin : parent.width
            color: (root.count-1 === index) ? datovkaPalette.dark : datovkaPalette.mid
        }
    } // Rectangle
    ScrollIndicator.vertical: ScrollIndicator {}
} // ListView
