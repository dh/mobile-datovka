/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import cz.nic.mobileDatovka 1.0

/*
 * TextLineItem component.
 */
Item {
    id: root

    property alias pwdTextLine: textField

    /* These properties must be set by caller. */
    property string placeholderText: qsTr("Enter the password")
    property int horizontalAlignment: TextInput.AlignLeft
    property bool pwdEyeIconVisible: true
    property var checkBeforeShowing: null /* Pass this callback to eye icon. */

    property int hideAfterMs: 0 /* Negative and zero values are ignored. */

    width: parent.width
    height: textField.height

    function hide() {
        return showPwdEyeIcon.hide()
    }

    function show() {
        return showPwdEyeIcon.show()
    }

    AccessibleTextField {
        id: textField
        width: showPwdEyeIcon.visible ? parent.width - showPwdEyeIcon.width - formItemVerticalSpacing : parent.width
        height: inputItemHeight
        font.pointSize: defaultTextFont.font.pointSize
        echoMode: showPwdEyeIcon.showPwd ? TextInput.Normal : TextInput.Password
        passwordMaskDelay: 500 // milliseconds
        inputMethodHints: Qt.ImhNone
        placeholderText: root.placeholderText
        horizontalAlignment: root.horizontalAlignment
        InputLineMenu {
            id: textFieldMenu
            inputTextControl: textField
            isPassword: true
        }
        onPressAndHold: {
            if (settings.useExplicitClipboardOperations()) {
                textFieldMenu.implicitWidth = computeMenuWidth(textFieldMenu)
                textFieldMenu.open()
            }
        }
    }
    ShowPasswordOverlaidImage {
        id: showPwdEyeIcon
        visible: pwdEyeIconVisible && (textField.text.length > 0)
        anchors.left: textField.right
        anchors.leftMargin: formItemVerticalSpacing
        checkBeforeShowing: root.checkBeforeShowing

        onShown: {
            if (hideAfterMs > 0) {
                /* Staring timer. */
                timer.running = true
            }
        }
    }

    Timer {
        id: timer
        interval: hideAfterMs
        running: false
        repeat: false
        onTriggered: {
            showPwdEyeIcon.hide()
        }
    }
}
