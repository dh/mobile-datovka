/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2
import cz.nic.mobileDatovka 1.0

/*
 * Accessible combo box component.
 */
ComboBox {
    id: root

    /* These properties must be set by caller. */
    property string accessibleDescription: ""

    /*
     * The model entries must contain these properties.
     *   label - (string with item description)
     *           item description
     *   key - (string holding one of the keys)
     *           unique string identifying an item
     * e.g.:
        ListModel {
            id: listModel
            ListElement {
                label: qsTr("Some selection")
                key: "some"
            }
        }
     */

    currentIndex: 0
    textRole: "label"

    height: inputItemHeight
    font.pointSize: defaultTextFont.font.pointSize

    /*
     * Return current key value.
     */
    function currentKey() {
        return root.model.get(root.currentIndex).key
    }

    /*
     * Selects entry with given key.
     */
    function selectCurrentKey(key) {
        for (var i = 0; i < root.model.count; ++i) {
            //root.model.get(i)["key"]
            if (root.model.get(i).key === key) {
                root.currentIndex = i;
                return;
            }
        }
        /* Not found. */
        console.log("No entry found for key '" + key + "'.");
    }

    delegate: ItemDelegate {
        width: root.width
        height: inputItemHeight
        contentItem: AccessibleTextButton {
            text: label
            color: datovkaPalette.text
            font.pointSize: defaultTextFont.font.pointSize
            elide: Text.ElideRight
            verticalAlignment: Text.AlignVCenter
            onClicked: {
                root.currentIndex = index
                root.popup.close()
            }
        }
        highlighted: root.highlightedIndex == index
    }

    Accessible.role: Accessible.ComboBox
    Accessible.description: accessibleDescription
    Accessible.name: root.currentText
    Accessible.onPressAction: {
        root.popup.open()
    }
}
