/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2

/*
 * Accessible spin box component.
 */
SpinBox {
    id: root

    /* These properties must be set by caller. */
    property string accessibleDescription: ""

    height: inputItemHeight
    font.pointSize: defaultTextFont.font.pointSize

    /*
     * Return spin box value.
     */
    function val() {
        return root.value;
    }

    /*
     * Set spin box value.
     */
    function setVal(v) {
        if (v < root.from) {
            root.value = root.from
        } else if (v > root.to) {
            root.value = root.to
        } else {
            root.value = v
        }
    }

    MouseArea {
        anchors.fill: root.down.indicator

        function handleClick() {
            root.decrease()
            root.valueModified()
        }

        Accessible.role: Accessible.Button
        Accessible.description: root.accessibleDescription
        //Accessible.focusable: true
        Accessible.name: qsTr("Decrease value '%1'.").arg(root.val())
        Accessible.onPressAction: {
            handleClick()
        }
        onClicked: {
            handleClick()
        }
    }
    MouseArea {
        anchors.top: root.top
        anchors.bottom: root.bottom
        anchors.left: (root.down.indicator.x < root.up.indicator.x) ? root.down.indicator.right : root.up.indicator.right
        anchors.right: (root.down.indicator.x < root.up.indicator.x) ? root.up.indicator.left : root.up.indicator.left

        Accessible.role: root.editable ? Accessible.EditableText : Accessible.StaticText
        Accessible.description: root.accessibleDescription
        Accessible.editable: root.editable
        Accessible.focusable: true
        Accessible.multiLine: false
        Accessible.name: root.val()
        Accessible.readOnly: !root.editable
    }
    MouseArea {
        anchors.fill: root.up.indicator

        function handleClick() {
            root.increase()
            root.valueModified()
        }

        Accessible.role: Accessible.Button
        Accessible.description: root.accessibleDescription
        //Accessible.focusable: true
        Accessible.name: qsTr("Increase value '%1'.").arg(root.val())
        Accessible.onPressAction: {
            handleClick()
        }
        onClicked: {
            handleClick()
        }
    }
}
