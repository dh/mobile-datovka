/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2
import cz.nic.mobileDatovka 1.0

AccessibleSpinBox {
    id: root

    /* Must be a non-decreasing list ending with infinity. */
    property var items: [1, qsTr("max")]
    property int dfltIdx: 0

    from: 0
    to: items.length - 1 /* Last is infinite. */
    //stepSize: 1

    textFromValue: function(value) {
        return items[value];
    }

    valueFromText: function(text) {
        for (var i = 0; i < items.length; ++i) {
            if (items[i].toLowerCase().indexOf(text.toLowerCase()) === 0) {
                return i
            }
        }
        return sb.value
    }

    function val() {
        if (value < to) { /* Last is infinite. */
            return items[value]
        } else {
            return 0 /* Infinity is treated as zero. */
        }
    }

    function setVal(v) {
        if (v <= 0) { /* infinite */
            value = to
        } else if (v < items[0]) { /* less than minimal */
            value = from
        } else if (v > items[to - 1]) { /* more than maximal */
            value = to
        } else {
            for (var i = 0; i < (items.length - 1); ++i) {
                if (v == items[i]) {
                    value = i
                    return
                } else if (v < items[i]) {
                    value = i - 1
                    return
                }
            }
            value = from
        }
    }

    MouseArea {
        anchors.fill: root.down.indicator

        function handleClick() {
            root.decrease()
            root.valueModified()
        }

        Accessible.role: Accessible.Button
        Accessible.description: root.accessibleDescription
        //Accessible.focusable: true
        Accessible.name: qsTr("Decrease value '%1'.").arg(root.textFromValue(root.value))
        Accessible.onPressAction: {
            handleClick()
        }
        onClicked: {
            handleClick()
        }
    }
    MouseArea {
        anchors.top: root.top
        anchors.bottom: root.bottom
        anchors.left: (root.down.indicator.x < root.up.indicator.x) ? root.down.indicator.right : root.up.indicator.right
        anchors.right: (root.down.indicator.x < root.up.indicator.x) ? root.up.indicator.left : root.up.indicator.left

        Accessible.role: root.editable ? Accessible.EditableText : Accessible.StaticText
        Accessible.description: root.accessibleDescription
        Accessible.editable: root.editable
        Accessible.focusable: true
        Accessible.multiLine: false
        Accessible.name: root.textFromValue(root.value)
        Accessible.readOnly: !root.editable
    }
    MouseArea {
        anchors.fill: root.up.indicator

        function handleClick() {
            root.increase()
            root.valueModified()
        }

        Accessible.role: Accessible.Button
        Accessible.description: root.accessibleDescription
        //Accessible.focusable: true
        Accessible.name: qsTr("Increase value '%1'.").arg(root.textFromValue(root.value))
        Accessible.onPressAction: {
            handleClick()
        }
        onClicked: {
            handleClick()
        }
    }
}
