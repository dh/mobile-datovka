/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7

/*
 * List view that can be scrolled programmatically.
 */
ListView {
    id: root

    clip: true

    /* These properties must be set by caller. */
    property real delegateHeight: 1.0

    /*
     * Scroll list down.
     */
    function scrollDown() {
        var numViewed = Math.ceil(root.height / (2 * delegateHeight))
        if (numViewed < 1) {
            numViewed = 1
        }
        var indexAtTop = root.indexAt(1, 1 + root.contentY)
        indexAtTop = (indexAtTop >= 0) ? (indexAtTop + numViewed) : 0
        root.positionViewAtIndex(indexAtTop, ListView.Beginning)
        root.returnToBounds()
    }

    /*
     * Scroll list up.
     */
    function scrollUp() {
        var numViewed = Math.ceil(root.height / (2 * delegateHeight))
        if (numViewed < 1) {
            numViewed = 1
        }
        var indexAtBottom = root.indexAt(1, root.height - 2 + root.contentY)
        indexAtBottom = (indexAtBottom >= 0) ? (indexAtBottom - numViewed) : 0
        root.positionViewAtIndex(indexAtBottom, ListView.End)
        root.returnToBounds()
    }
}
