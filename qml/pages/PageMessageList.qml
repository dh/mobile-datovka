/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

/*
 * Roles are defined in MessageListModel::roleNames() and are accessed directly
 * via their names.
 */
Item {
    id: msgListPage

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null
    property int msgType

    Component.onCompleted: {
        messages.fillMessageList(messageModel, acntId, msgType)
        if (messageModel.rowCount() === 0) {
            emptyList.visible = true
            /* Don't use visible property as it hides the element. */
            menuButton.visible = false
            searchButton.visible = false
        }

        proxyMessageModel.setSourceModel(messageModel)

        /* Don't use message settings for sent messages */
        if (msgType == MessageType.TYPE_SENT) {
            menuButton.visible = false
            newMessageButton.visible = true
        }
    }

    Component.onDestruction: {
        accounts.updateOneAccountCounters(accountModel, acntId)
    }

    MessageListModel {
        id: messageModel

        Component.onCompleted: {
        }
    }

    ListSortFilterProxyModel {
        id: proxyMessageModel

        Component.onCompleted: {
            setFilterRoles([MessageListModel.ROLE_MSG_ID,
                MessageListModel.ROLE_FROM, MessageListModel.ROLE_TO,
                MessageListModel.ROLE_ANNOTATION])
        }
    }

    PageHeader {
        id: headerBar
        title: (msgType == MessageType.TYPE_RECEIVED) ? qsTr("Received messages") : qsTr("Sent messages");
        onBackClicked: {
            pageView.pop()
        }
        Row {
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            spacing: defaultMargin
            AccessibleOverlaidImageButton {
                id: searchButton
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/magnify.svg"
                accessibleName: qsTr("Filter messages.")
                onClicked: {
                    messageList.anchors.top = filterBar.bottom
                    filterBar.visible = true
                    filterBar.filterField.forceActiveFocus()
                    Qt.inputMethod.show()
                }
            }
            AccessibleOverlaidImageButton {
                id: newMessageButton
                visible: false
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/pencil-box-outline.svg"
                accessibleName: qsTr("Create and send message.")
                onClicked: {
                    statusBar.visible = false
                    pageView.push(pageSendMessage, {
                        "pageView": pageView,
                        "acntName" : acntName,
                        "acntId": acntId,
                        "action": "new"
                    }, StackView.Immediate)
                }
            }
            AccessibleOverlaidImageButton {
                id: menuButton
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/menu.svg"
                accessibleName: qsTr("Show menu of available operations.")
                onClicked: {
                    statusBar.visible = false
                    Qt.inputMethod.hide()
                    pageView.push(pageMenuMessageList, {
                            "pageView": pageView,
                            "acntName": acntName,
                            "acntId": acntId,
                            "msgType": msgType,
                            "messageModel": messageModel
                        }, StackView.Immediate)
                }
            }
        }
    }
    FilterBar {
        id: filterBar
        anchors.top: headerBar.bottom
        isAnyItem: (messageList.count > 0)
        onTextChanged: {
            proxyMessageModel.setFilterRegExpStr(text)
        }
        onClearClicked: {
            filterBar.visible = false
            messageList.anchors.top = headerBar.bottom
            Qt.inputMethod.hide()
        }
    }
    MessageList {
        id: messageList
        anchors.top: headerBar.bottom
        anchors.bottom: parent.bottom
        visible: true
        width: parent.width
        model: proxyMessageModel

        onCountChanged: {
            if (messageList.count === 0) {
                emptyList.visible = true
                menuButton.enabled = false
            } else {
                emptyList.visible = false
                menuButton.enabled = true
            }
        }
        onMsgClicked: {
            statusBar.visible = false
            Qt.inputMethod.hide()
            messages.markMessageAsLocallyRead(messageModel, acntId, msgId, true)
            pageView.push(pageMessageDetail, {
                    "pageView": pageView,
                    "fromLocalDb": true,
                    "acntName": acntName,
                    "acntId": acntId,
                    "msgDaysToDeletion": msgDaysToDeletion,
                    "msgType": msgType,
                    "msgId": msgId,
                    "messageModel": messageModel
                })
        }
        onMsgPressAndHold: {
            statusBar.visible = false
            Qt.inputMethod.hide()
            /* Use message settings for received message only */
            if (msgType == MessageType.TYPE_RECEIVED) {
                pageView.push(pageMenuMessage, {
                        "pageView": pageView,
                        "acntName": acntName,
                        "acntId": acntId,
                        "msgType": msgType,
                        "msgId": msgId,
                        "canDeleteMsg": canDelete,
                        "messageModel": messageModel
                    }, StackView.Immediate)
            }
        }

        property bool downloadStart: false
        onMovementEnded: {
            if (downloadStart) {
                downloadStart = false
                if (msgType == MessageType.TYPE_RECEIVED) {
                    isds.doIsdsAction("syncSingleAccountReceived", acntId)
                } else if (msgType == MessageType.TYPE_SENT) {
                    isds.doIsdsAction("syncSingleAccountSent", acntId)
                }
            }
        }
        onDragEnded: {
            downloadStart = contentY < -120
        }
        AcntId {
            id: actionAcntId
        }
        Connections {
            target: isds
            function onDownloadMessageListFinishedSig(isMsgReceived, userName, testing) {
                actionAcntId.username = userName
                actionAcntId.testing = testing
                accounts.updateOneAccountCounters(accountModel, actionAcntId)
                if (isMsgReceived) {
                    settings.setLastUpdateToNow()
                    settings.saveAllSettings(accountModel)
                }
                messages.fillMessageList(messageModel, acntId, msgType)
            }
            function onRunSyncSingleAccountReceivedSig(userName, testing) {
                actionAcntId.username = userName
                actionAcntId.testing = testing
                isds.syncSingleAccountReceived(actionAcntId)
            }
            function onRunSyncSingleAccountSentSig(userName, testing) {
                actionAcntId.username = userName
                actionAcntId.testing = testing
                isds.syncSingleAccountSent(actionAcntId)
            }
        }
    }
    AccessibleText {
        id: emptyList
        visible: false
        anchors.top: headerBar.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargin
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.Wrap
        text: (filterBar.filterField.text.length === 0) ?
            qsTr("No messages or have not been downloaded yet.") :
            qsTr("No message found that matches filter text '%1'.").arg(filterBar.filterField.text)
    }
}
