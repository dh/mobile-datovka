/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {
    id: acntDetailPage

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null

    function setAccountDetailInfo(acntId) {
        accountDetailText.text = accounts.fillAccountInfo(acntId)
        emptyList.visible = (accountDetailText.text === "")
        flickable.visible = !(accountDetailText.text === "")
    }

    Component.onCompleted: {
        setAccountDetailInfo(acntId)
    }

    PageHeader {
        id: headerBar
        title: qsTr("Account info")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        AccessibleOverlaidImageButton {
            id: actionButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            image.sourceSize.height: imageActionDimension
            image.source: "qrc:/ui/sync.svg"
            accessibleName: qsTr("Refresh information about the account.")
            onClicked: {
                isds.doIsdsAction("getAccountInfo", acntId)
            }
        }
    }

    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            background: Rectangle {
                color: datovkaPalette.base
            }
            AccessibleText {
                id: accountDetailText
                width: parent.width
                textFormat: TextEdit.RichText
                wrapMode: Text.WordWrap
            }
        }
        ScrollIndicator.vertical: ScrollIndicator {}
    }

    AccessibleText {
        id: emptyList
        visible: true
        anchors.top: headerBar.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargin
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.Wrap
        text: qsTr("Account info has not been downloaded yet.")
    }

    AcntId {
        id: actionAcntId
    }
    Connections {
        target: isds
        function onDownloadAccountInfoFinishedSig(userName, testing) {
            actionAcntId.username = userName
            actionAcntId.testing = testing
            setAccountDetailInfo(actionAcntId)
        }
    }
}
