/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

/*
 * Roles are defined in DataboxListModel::roleNames() and are accessed directly
 * via their names.
 */

Item {
    id: pageDataboxSearch

    /* These properties must be set by caller. */
    property var pageView
    property AcntId acntId: null
    // recipBoxModel (if not null) holds recipient list of send message page
    property var recipBoxModel: null

    /* These properties remember choice of ComboBoxes */
    property string searchType: "GENERAL"
    property string searchScope: "ALL"

    /* These properties remember origin search settings during pagination */
    property int page: 0
    property string searchTextTmp
    property string searchTypeTmp
    property string searchScopeTmp

    Component.onCompleted: {
         searchPhraseText.forceActiveFocus()
         proxyDataboxModel.setSourceModel(foundBoxModel)
    }

    DataboxListModel {
        id: foundBoxModel
        Component.onCompleted: {
        }
    }
    ListSortFilterProxyModel {
        id: proxyDataboxModel
        Component.onCompleted: {
            setFilterRoles([DataboxListModel.ROLE_DB_NAME,
                DataboxListModel.ROLE_DB_ADDRESS, DataboxListModel.ROLE_DB_IC,
                DataboxListModel.ROLE_DB_ID])
        }
    }
    PageHeader {
        id: headerBar
        title: qsTr("Find databox")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        Row {
            anchors.verticalCenter: parent.verticalCenter
            spacing: defaultMargin
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            AccessibleOverlaidImageButton {
                id: filterButton
                visible: false
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/magnify.svg"
                accessibleName: qsTr("Filter data boxes.")
                onClicked: {
                    databoxList.anchors.top = filterBar.bottom
                    filterBar.visible = true
                    filterBar.filterField.forceActiveFocus()
                    Qt.inputMethod.show()
                }
            }
            AccessibleOverlaidImageButton {
                id: actionButton
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/account-search.svg"
                accessibleName: qsTr("Search data boxes.")
                onClicked: {
                    searchPhraseText.focus = false
                    searchPanel.doSearchRequest()
                }
            }
        }
    } // PageHeader
    Pane {
        id: searchPanel
        anchors.top: headerBar.bottom
        width: parent.width
        function doSearchRequest() {
            isds.doIsdsAction("findDataboxFulltext", acntId)
        }
        Column {
            anchors.right: parent.right
            anchors.left: parent.left
            spacing: formItemVerticalSpacing
            AccessibleComboBox {
                id: searchScopeComboBox
                width: parent.width
                accessibleDescription: qsTr("Select type of sought data box")
                model: ListModel {
                    id: searchScopeComboBoxModel
                    ListElement { label: qsTr("All databoxes"); key: "ALL" }
                    ListElement { label: qsTr("OVM"); key: "OVM" }
                    ListElement { label: qsTr("PO"); key: "PO" }
                    ListElement { label: qsTr("PFO"); key: "PFO" }
                    ListElement { label: qsTr("FO"); key: "FO" }
                }
                onCurrentIndexChanged: searchScope = currentKey()
            }
            AccessibleComboBox {
                id: searchTypeComboBox
                width: parent.width
                accessibleDescription: qsTr("Select entries to search in")
                model: ListModel {
                    id: searchTypeComboBoxModel
                    ListElement { label: qsTr("All fields"); key: "GENERAL" }
                    ListElement { label: qsTr("Address"); key: "ADDRESS" }
                    ListElement { label: qsTr("IČO"); key: "ICO" }
                    ListElement { label: qsTr("Databox ID"); key: "DBID" }
                }
                onCurrentIndexChanged: searchType = currentKey()
            }
            AccessibleTextField {
                id: searchPhraseText
                placeholderText: qsTr("Enter sought phrase")
                focus: true
                width: parent.width
                font.pointSize: defaultTextFont.font.pointSize
                height: inputItemHeight
                onAccepted: searchPanel.doSearchRequest()
                InputLineMenu {
                    id: searchPhraseTextMenu
                    inputTextControl: searchPhraseText
                    isPassword: false
                }
                onPressAndHold: {
                    if (settings.useExplicitClipboardOperations()) {
                        searchPhraseTextMenu.implicitWidth = computeMenuWidth(searchPhraseTextMenu)
                        searchPhraseTextMenu.open()
                    }
                }
            }
            Item {
                id: searchResults
                height: inputItemHeight
                width: parent.width
                visible: false
                Item {
                    id: searchResultPrevious
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    width: parent.width * 0.07
                    height: parent.height
                    OverlaidImage {
                        id: previousImage
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.rightMargin: defaultMargin
                        image.sourceSize.height: imageNavigateDimension
                        image.source: "qrc:/ui/back.svg"
                    }
                    MouseArea {
                        /* TODO -- Make this a separate component (image button?). */
                        function handleClick() {
                            if (page > 0) {
                                page = page-1
                            }
                            isds.findDataboxFulltext(acntId, foundBoxModel, searchTextTmp, searchTypeTmp, searchScopeTmp, page)
                            if (recipBoxModel != null) {
                                foundBoxModel.selectEntries(recipBoxModel.boxIds(), true)
                            }
                        }

                        anchors.fill: parent

                        Accessible.role: Accessible.Button
                        Accessible.name: qsTr("Previous")
                        Accessible.onPressAction: {
                            handleClick()
                        }
                        onClicked: {
                            handleClick()
                        }
                    }
                }
                AccessibleText {
                    id: searchResultText
                    anchors.centerIn: parent
                    text: ""
                }
                Item {
                    id: searchResultNext
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    height: parent.height
                    width: parent.width * 0.07
                    OverlaidImage {
                        id: nextImage
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        anchors.rightMargin: defaultMargin
                        image.sourceSize.height: imageNavigateDimension
                        image.source: "qrc:/ui/next.svg"
                    }
                    MouseArea {
                        /* TODO -- Make this a separate component (image button?). */
                        function handleClick() {
                            page = page+1
                            isds.findDataboxFulltext(acntId, foundBoxModel, searchTextTmp, searchTypeTmp, searchScopeTmp, page)
                            if (recipBoxModel != null) {
                                foundBoxModel.selectEntries(recipBoxModel.boxIds(), true)
                            }
                        }

                        anchors.fill: parent

                        Accessible.role: Accessible.Button
                        Accessible.name: qsTr("Next")
                        Accessible.onPressAction: {
                            handleClick()
                        }
                        onClicked: {
                            handleClick()
                        }
                    }
                } // Rectangle
            } // Item
        } // Column
    } // Pane
    FilterBar {
        id: filterBar
        anchors.top: searchPanel.bottom
        isAnyItem: (databoxList.count > 0)
        onTextChanged: {
            proxyDataboxModel.setFilterRegExpStr(text)
        }
        onClearClicked: {
            filterBar.visible = false
            databoxList.anchors.top = searchPanel.bottom
            Qt.inputMethod.hide()
        }
    }
    DataboxList {
        id: databoxList
        anchors.top: searchPanel.bottom
        anchors.bottom: parent.bottom
        visible: true
        width: parent.width
        model: proxyDataboxModel
        canDetailBoxes: recipBoxModel == null
        canSelectBoxes: recipBoxModel != null
        canDeselectBoxes: recipBoxModel != null
        onBoxSelect: {
            var boxEntry = foundBoxModel.entry(boxId)
            if (recipBoxModel != null) {
                foundBoxModel.selectEntry(boxEntry.dbID, true)
                recipBoxModel.addEntry(boxEntry)
            }
        }
        onBoxDeselect: {
            if (recipBoxModel != null) {
                foundBoxModel.selectEntry(boxId, false)
                recipBoxModel.removeEntry(boxId)
            }
        }
        onBoxDetail: {
            statusBar.visible = false
            var boxEntry = foundBoxModel.entry(boxId)
            if (recipBoxModel == null) {
                Qt.inputMethod.hide()
                pageView.push(pageDataboxDetail, {
                    "pageView": pageView,
                    "acntId": acntId,
                    "dbID": boxEntry.dbID,
                    "dbType": boxEntry.dbType
                }, StackView.Immediate)
            }
        }
    } // DataboxList
    AcntId {
        id: actionAcntId
    }
    Connections {
        target: isds
        function onRunFindDataboxFulltextSig(userName, testing) {
            searchResults.visible = false
            searchResultPrevious.enabled = false
            searchResultNext.enabled = false
            searchResultText.text = ""
            page = 0
            searchTextTmp = searchPhraseText.text
            searchTypeTmp = searchType
            searchScopeTmp = searchScope
            actionAcntId.username = userName
            actionAcntId.testing = testing
            isds.findDataboxFulltext(actionAcntId, foundBoxModel, searchPhraseText.text, searchType, searchScope, page)
            if (foundBoxModel.rowCount() <= 0) {
                filterButton.visible = false
            } else {
                filterButton.visible = true
            }
            if (recipBoxModel != null) {
                foundBoxModel.selectEntries(recipBoxModel.boxIds(), true)
            }
        }
    }
    Connections {
        target: isds
        function onSendSearchResultsSig(totalCount, currentCount, position, lastPage) {
            searchResults.visible = true
            searchResultNext.visible = (totalCount > 0) && (totalCount > currentCount)
            searchResultPrevious.visible = (totalCount > 0) && (totalCount > currentCount)
            searchResultNext.enabled = !lastPage
            searchResultPrevious.enabled = (position > 0)
            if ((totalCount > 0) && (totalCount > currentCount)) {
                searchResultText.text = qsTr("Shown") + ": " + (position + 1) + "-" + (position + currentCount) + " " + qsTr("from") + " " + totalCount
            } else {
                searchResultText.text = qsTr("Found") + ": " + totalCount
            }
        }
    }
} // Item
