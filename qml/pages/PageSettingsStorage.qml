/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0

Item {
    id: pageSettingsStorage

    /* These properties must be set by caller. */
    property var pageView

    property int currentZfoSize: 0
    /* Possibility to change db location is implicit disabled now for all OS */
    property bool showChangeDbLocation: false

    Component.onCompleted: {
        dbPathLabel.visible = showChangeDbLocation
        dbPathText.visible = showChangeDbLocation
        dbPathButton.visible = showChangeDbLocation
        dbResetPathButton.visible = (showChangeDbLocation && settings.showDefaultButton())
        messageLifeSpinBox.setVal(settings.messageLifeDays())
        attachLifeSpinBox.setVal(settings.attachmentLifeDays())
        currentZfoSize = settings.zfoDbSizeMBs()
        zfoDbSizeSpinBox.value = currentZfoSize
        dbPathText.text = settings.dbPath()
    }

    Component.onDestruction: {
        settings.saveAllSettings(accountModel)
        if (currentZfoSize > zfoDbSizeSpinBox.value) {
            zfo.reduceZfoDbSize(currentZfoSize-zfoDbSizeSpinBox.value);
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Storage settings")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
    }
    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                AccessibleText {
                    id: messageLifeLabel
                    text: qsTr("Number of days to keep messages")
                }
                AccessibleSpinBoxZeroMax {
                    /* Holds value in days. */
                    id: messageLifeSpinBox

                    /* Must be a non-decreasing list ending with infinity. */
                    items: [90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 160, 165, 170, 175, 180, qsTr("don't delete")]
                    dfltIdx: 19

                    accessibleDescription: qsTr("Select the number of days how long downloaded messages are kept in local storage after download.")

                    onValueModified: settings.setMessageLifeDays(messageLifeSpinBox.val())
                }
                AccessibleText {
                    color: datovkaPalette.mid
                    width: parent.width
                    text: if (messageLifeSpinBox.val() === 0) {
                              qsTr("Messages won't automatically be deleted from the local storage.")
                        } else {
                              qsTr("Messages will be locally stored for a period of %1 days since their acceptance time. By default they are not deleted.").arg(messageLifeSpinBox.val())
                        }
                    wrapMode: Text.Wrap
                }
                AccessibleText {
                    id: attachLifeLabel
                    text: qsTr("Number of days to keep attachments")
                }
                AccessibleSpinBoxZeroMax {
                    /* Holds value in days. */
                    id: attachLifeSpinBox

                    /* Must be a non-decreasing list ending with infinity. */
                    items: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, qsTr("like messages")]
                    dfltIdx: 17

                    accessibleDescription: qsTr("Select the number of days how long message content is kept in local storage after download.")

                    onValueModified: settings.setAttachmentLifeDays(attachLifeSpinBox.val())
                }
                AccessibleText {
                    color: datovkaPalette.mid
                    width: parent.width
                    text: if (attachLifeSpinBox.val() === 0) {
                              qsTr("Attachments won't be deleted from the local storage as long as the corresponding messages won't be deleted.")
                        } else {
                              qsTr("Attachments will be locally stored, but no longer than corresponding messages, for a period of %1 days since their download time. Default value corresponds with message settings.").arg(attachLifeSpinBox.val())
                        }
                    wrapMode: Text.Wrap
                }
                AccessibleText {
                    id: zfoLifeLabel
                    text: qsTr("ZFO storage size limit in MB")
                }
                AccessibleSpinBox {
                    /* Holds value in MBs. */
                    id: zfoDbSizeSpinBox

                    from: 0
                    to: 1000
                    stepSize: 100

                    accessibleDescription: qsTr("Specify the maximal amount of memory for preserving recently downloaded messages.")

                    onValueModified: settings.setZfoDbSizeMBs(zfoDbSizeSpinBox.val())
                }
                AccessibleText {
                    color: datovkaPalette.mid
                    width: parent.width
                    text: if (zfoDbSizeSpinBox.val() === 0) {
                              qsTr("Message ZFO data won't be automatically stored.")
                        } else {
                              qsTr("Maximum size of stored message ZFO data is set to %1 MB. Default is %2 MB.").arg(zfoDbSizeSpinBox.val()).arg(200)
                        }
                    wrapMode: Text.Wrap
                }
                AccessibleText {
                    id: dbPathLabel
                    text: qsTr("Databases location")
                }
                AccessibleText {
                    id: dbPathText
                    color: datovkaPalette.mid
                    width: parent.width
                    wrapMode: Text.Wrap
                    text: ""
                }
                AccessibleButton {
                    id: dbPathButton
                    font.pointSize: defaultTextFont.font.pointSize
                    height: inputItemHeight
                    text: qsTr("Change location")
                    onClicked: {
                        var newLocation = settings.changeDbPath(dbPathText.text.toString(), false)
                        if (newLocation !== "") {
                            if (newLocation !== dbPathText.text) {
                                if (messages.moveOrCreateNewDbsToNewLocation(newLocation)) {
                                    dbPathText.text = newLocation;
                                    dbResetPathButton.visible = true
                                    settings.setDbPath(newLocation)
                                }
                            }
                        }
                    }
                }
                AccessibleButton {
                    id: dbResetPathButton
                    text: qsTr("Set default")
                    font.pointSize: defaultTextFont.font.pointSize
                    height: inputItemHeight
                    onClicked: {
                        var newLocation = settings.changeDbPath(dbPathText.text.toString(), true)
                        if (newLocation !== "") {
                            if (newLocation !== dbPathText.text) {
                                if (messages.moveOrCreateNewDbsToNewLocation(newLocation)) {
                                    dbPathText.text = newLocation;
                                    dbResetPathButton.visible = false
                                    settings.setDbPath(newLocation)
                                }
                            }
                        }
                    }
                }
                AccessibleText {
                    id: vacuumLabel
                    text: qsTr("Clean up all databases")
                }
                AccessibleButton {
                    id: vacuumButton
                    font.pointSize: defaultTextFont.font.pointSize
                    height: inputItemHeight
                    text: qsTr("Clean now")
                    onClicked: {
                       files.vacuumFileDbs()
                    }
                }
                AccessibleText {
                    id: vacuumText
                    color: datovkaPalette.mid
                    width: parent.width
                    wrapMode: Text.Wrap
                    text: qsTr("The action performs a clean-up in local databases in order to optimise their speed and size. Note: It may take a while to complete this action.")
                }
            } // Column layout
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
} // Item
