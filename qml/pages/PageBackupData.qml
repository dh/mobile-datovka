/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQml 2.2
import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0
import cz.nic.mobileDatovka.qmlInteraction 1.0

Item {
    id: mainLayout

    /* These properties must be set by caller. */
    property var pageView
    property bool transfer: false
    property AcntId acntId: null

    property string backupTargetPath: ""
    property bool iOS: false

    function showBusyIndicator(visible) {
        busyElement.visible = visible
        headerBar.enabled = !visible
        backupPath.enabled = !visible
    }

    function canBackup() {
        backUpButton.visible = (acntId === null ) ? backup.canBackupSelectedAccounts(backupSelectionModel, backupTargetPath, includeZfoDb.checked, transfer) : backup.canBackUpAccount(acntId, backupTargetPath, includeZfoDb.checked, transfer)
        if (backUpButton.visible) {
            actionInfoText.color = datovkaPalette.text
        } else {
            actionInfoText.color = "red"
        }
    }

    Component.onCompleted: {
        iOS = iOSHelper.isIos()
        backup.stopWorker(true)
        backupAccountList.visible = false
        backupPathLabel.visible = false

        if (iOS) {
            backupPathLabel.visible = true
            backupPathLabel.text = qsTr("Target") + ": " + qsTr("Application sandbox")
        }

        if (transfer) {
            includeZfoDb.visible = false
            includeAllAccounts.visible = false
            headerBar.title = qsTr("Transfer application data")
            infoText.text = qsTr("The action allows to transfer complete application data to another device. Preserve the files in a safe place as it contains login and private data. Transferred data require to be secured with a PIN. Set PIN in the application settings if it not set.")
            if (iOS) {
                infoText.text = infoText.text + " " + qsTr("Data will be stored into the application sandbox and, subsequently, you can upload them into the iCloud or you can use the iTunes application in order to copy them to your Mac or PC.")
            } else {
                infoText.text = infoText.text + " " + qsTr("Specify the location where you want to store the data.")
            }
        } else {
            headerBar.title = (acntId === null ) ? qsTr("Back up accounts") : qsTr("Back up account") + ": " + acntId.username
            infoText.text = qsTr("The action allows to back up selected accounts. Preserve the backup in a safe place as it contains private data.")
            if (iOS) {
                infoText.text = infoText.text + " " + qsTr("Data will be stored into the application sandbox and, subsequently, you can upload them into the iCloud or you can use the iTunes application in order to copy them to your Mac or PC.")
            } else {
                infoText.text = infoText.text + " " + qsTr("Specify the location where you want to store the data.")
            }
            includeZfoDb.visible = true
            includeAllAccounts.visible = (acntId === null )
            if (acntId === null ) {
                backupAccountList.visible = true
                backup.fillBackupModel(backupSelectionModel)
            }
        }
        canBackup()
    }

    Component.onDestruction: {
        backup.stopWorker(false)
    }

    BackupRestoreData {
        id: backup
    }

    BackupRestoreSelectionModel {
        id: backupSelectionModel
        Component.onCompleted: {
        }
        onDataChanged: {
            includeAllAccounts.checked = (backupSelectionModel.rowCount() > 0) && (backupSelectionModel.numSelected() == backupSelectionModel.rowCount())
        }
    }

    FileDialogue {
        id: fileDialogue
        onFinished: {
            var listLength = pathListModel.count
            if (listLength > 0) {
                if (pathListModel.get(listLength-1).path === "") {
                    if (backupTargetPath === "") {
                        backupPathLabel.visible = false
                    }
                } else {
                    backupPathLabel.visible = true
                    backupTargetPath = pathListModel.get(listLength-1).path
                    canBackup()
                }
            }
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Back up accounts")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        Row {
            anchors.verticalCenter: parent.verticalCenter
            spacing: defaultMargin
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            AccessibleOverlaidImageButton {
                id: backUpButton
                visible: false
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/checkbox-marked-circle.svg"
                accessibleName: qsTr("Proceed with operation")
                onClicked: {
                    var success = false
                    showBusyIndicator(true)
                    if (transfer) {
                        success = backup.transferCompleteDatovkaData(backupTargetPath)
                    } else if (acntId !== null) {
                        success = backup.backUpAccount(acntId, backupTargetPath, includeZfoDb.checked)
                    } else {
                        success = backup.backUpSelectedAccounts(backupSelectionModel, backupTargetPath, includeZfoDb.checked)
                    }
                    showBusyIndicator(false)
                    //if (success) {
                    //    pageView.pop(StackView.Immediate)
                    //}
                }
            }
        }
    }
    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                width: parent.width
                spacing: defaultMargin
                AccessibleText {
                    id: infoText
                    anchors.topMargin: defaultMargin
                    color: datovkaPalette.mid
                    wrapMode: Text.Wrap
                    horizontalAlignment: Text.AlignHCenter
                    width: parent.width
                    text: qsTr("Specify the location where you want to back up your data. Preserve the backup in a safe place as it contains private data.")
                }
                AccessibleSwitch {
                    id: includeZfoDb
                    text: qsTr("Back up ZFO data")
                    font.pointSize: defaultTextFont.font.pointSize
                    checked: false
                    onClicked: canBackup()
                }
                AccessibleButton {
                    id: backupPath
                    visible: !iOS
                    text: qsTr("Choose location")
                    height: inputItemHeight
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pointSize: defaultTextFont.font.pointSize
                    onClicked: {
                        fileDialogue.raise("Select folder", ["*.*"], false, "")
                    }
                }
                Text {
                    id: backupPathLabel
                    color: datovkaPalette.text
                    width: parent.width
                    wrapMode: Text.Wrap
                    text: qsTr("Target") + ": " + backupTargetPath
                }
                AccessibleText {
                    id: sizeInfoText
                    width: parent.width
                }
                AccessibleText {
                    id: actionInfoText
                    width: parent.width
                    wrapMode: Text.Wrap
                    font.bold: true
                }
                AccessibleSwitch {
                    id: includeAllAccounts
                    visible: false
                    text: qsTr("Select all accounts")
                    font.pointSize: defaultTextFont.font.pointSize
                    checked: false
                    onClicked: {
                        backupSelectionModel.setAllSelected(includeAllAccounts.checked)
                        canBackup()
                    }
                }
                ScrollableListView {
                    id: backupAccountList
                    visible: false
                    delegateHeight: baseHeaderHeight
                    width: parent.width
                    height: 300
                    model: backupSelectionModel
                    delegate: Rectangle {
                        id: uploadItem
                        color: (!rSelected) ? datovkaPalette.base : normalAccountBgColor
                        height: backupAccountList.delegateHeight
                        width: parent.width
                        Rectangle {
                            visible: (0 === index)
                            anchors.top: parent.top
                            anchors.right: parent.right
                            height: 1
                            width: parent.width
                            color: datovkaPalette.dark
                        }
                        Text {
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: parent.left
                            anchors.leftMargin: defaultMargin
                            width: parent.width - defaultMargin
                            elide: Text.ElideRight
                            color: datovkaPalette.text
                            text: rAcntName + "\n" + rUserName + "     boxID: " + rBoxId
                        }
                        MouseArea {
                            function handleClick() {
                                backupSelectionModel.setSelected(index, !rSelected)
                                canBackup()
                            }
                            anchors.fill: parent
                            Accessible.role: Accessible.Button
                            Accessible.name: (!rSelected) ? qsTr("Select %1.").arg(rAcntName) : qsTr("Deselect %1.").arg(rAcntName)
                            Accessible.onScrollDownAction: backupAccountList.scrollDown()
                            Accessible.onScrollUpAction: backupAccountList.scrollUp()
                            Accessible.onPressAction: {
                                handleClick()
                            }
                            onClicked: {
                                handleClick()
                            }
                        }
                        Rectangle {
                            anchors.bottom: parent.bottom
                            anchors.right: parent.right
                            height: 1
                            width: (isiOS && backupAccountList.count-1 !== index) ? parent.width - defaultMargin : parent.width
                            color: (backupAccountList.count-1 === index) ? datovkaPalette.dark : datovkaPalette.mid
                        }
                    }
                }
            }
            Connections {
                target: backup
                function onActionTextSig(actionInfo) {
                    actionInfoText.text = actionInfo
                }
            }
            Connections {
                target: backup
                function onSizeTextSig(sizeInfo) {
                    sizeInfoText.text = sizeInfo
                }
            }
        }
    }
    Rectangle {
        id: busyElement
        visible: false
        anchors.fill: parent
        color: datovkaPalette.text
        opacity: 0.2
        z: 2
        BusyIndicator {
            anchors.centerIn: parent
            running: true
        }
    }
}
