/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {
    id: srvcListPage

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null

    property string gsInternIdSelected

    Component.onCompleted: {
        gov.loadServicesToModel(acntId, govServiceModel)
        proxyGovServiceModel.setSourceModel(govServiceModel)
    }

    GovServiceListModel {
        id: govServiceModel
    }

    ListSortFilterProxyModel {
        id: proxyGovServiceModel

        Component.onCompleted: {
            setFilterRoles([GovServiceListModel.ROLE_GOV_SRVC_FULL_NAME,
                GovServiceListModel.ROLE_GOV_SRVC_INST_NAME,
                GovServiceListModel.ROLE_GOV_SRVC_BOXID])
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("E-Gov Services");
        onBackClicked: {
            pageView.pop()
        }
        Row {
            anchors.verticalCenter: parent.verticalCenter
            spacing: defaultMargin
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            AccessibleOverlaidImageButton {
                id: searchButton
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/magnify.svg"
                accessibleName: qsTr("Set filter")
                onClicked: {
                    govServiceList.anchors.top = filterBar.bottom
                    filterBar.visible = true
                    filterBar.filterField.forceActiveFocus()
                    Qt.inputMethod.show()
                }
            }
        }
    }
    FilterBar {
        id: filterBar
        anchors.top: headerBar.bottom
        isAnyItem: (govServiceList.count > 0)
        onTextChanged: {
            proxyGovServiceModel.setFilterRegExpStr(text)
        }
        onClearClicked: {
            filterBar.visible = false
            govServiceList.anchors.top = headerBar.bottom
            Qt.inputMethod.hide()
        }
    }
    GovServiceList {
        id: govServiceList
        anchors.top: headerBar.bottom
        anchors.bottom: parent.bottom
        visible: true
        width: parent.width
        model: proxyGovServiceModel
        onCountChanged: {
            emptyList.visible = (govServiceList.count === 0)
        }
        onGovServiceClicked: {
            pageView.push(pageGovService, {
                "pageView": pageView,
                "acntName" : acntName,
                "acntId": acntId,
                "gsInternId": gsInternId,
                "gsFullName": gsFullName,
                "gsInstName": gsInstName
            }, StackView.Immediate)
        }
    }
    AccessibleText {
        id: emptyList
        visible: false
        anchors.top: headerBar.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargin
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.Wrap
        text: (filterBar.filterField.text.length === 0) ?
            qsTr("No available e-government service.") :
            qsTr("No e-government service found that matches filter text '%1'.").arg(filterBar.filterField.text)
    }
}
