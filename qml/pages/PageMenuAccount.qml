/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {
    id: mainLayout

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null
    property var loginMethod

    Component.onCompleted: {
        var index
        loginMethod = isds.getAccountLoginMethod(acntId)
        if (loginMethod === AcntData.LIM_UNAME_MEP) {
            /* Hide change password for MEP account. */
            index = accountMenuList.get_model_index(accountMenuListModel, "funcName", "changePwd");
            if (index >= 0) {
                accountMenuListModel.setProperty(index, "showEntry", false)
            }
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Account properties")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
    }

    /* Object (associative array) holding functions. */
    property var funcs: {
        "accSett": function callAccSett() {
            pageView.replace(pageSettingsAccount, {
                "pageView": pageView,
                "acntName": acntName,
                "acntId": acntId
            }, StackView.Immediate)
        },
        "viewAcntInfo": function callViewAcntInfo() {
            pageView.replace(pageAccountDetail, {
                "pageView": pageView,
                "acntName": acntName,
                "acntId": acntId
            }, StackView.Immediate)
        },
        "createMsg": function callCreateMsg() {
            pageView.replace(pageSendMessage, {
                "pageView": pageView,
                "acntName" : acntName,
                "acntId": acntId,
                "action": "new"
            }, StackView.Immediate)
        },
        "govServiceList": function callGovServiceList() {
            pageView.replace(pageGovServiceList, {
                "pageView": pageView,
                "acntName" : acntName,
                "acntId": acntId,
            }, StackView.Immediate)
        },
        "importMsg": function callImportMsg() {
            pageView.replace(pageImportMessage, {
                "pageView": pageView,
                "acntName" : acntName,
                "acntId": acntId
            }, StackView.Immediate)
        },
        "findBox": function callFindBox() {
            pageView.replace(pageDataboxSearch, {
                "pageView": pageView,
                "acntId": acntId
            }, StackView.Immediate)
        },
        "backupData": function callBackupData() {
            pageView.replace(pageBackupData, {
                "pageView": pageView,
                "acntId": acntId
            }, StackView.Immediate)
        },
        "cleanDb": function callCleanDb() {
            files.deleteFileDb(acntId)
        },
        "changePwd": function callChangePwd() {
            pageView.replace(pageChangePassword, {
                "pageView": pageView,
                "acntName": acntName,
                "acntId": acntId
            }, StackView.Immediate)
        },
        "delAcnt": function callDelAcnt() {
            if (accounts.removeAccount(accountModel, acntId, true)) {
                settings.saveAllSettings(accountModel)
                pageView.pop(StackView.Immediate)
            }
        }
    }

    ListModel {
        id: accountMenuListModel
        ListElement {
            image: "qrc:/ui/settings.svg"
            showEntry: true
            showNext: true
            name: qsTr("Account settings")
            funcName: "accSett"
        }
        ListElement {
            image: "qrc:/ui/account-box.svg"
            showEntry: true
            showNext: true
            name: qsTr("View account info")
            funcName: "viewAcntInfo"
        }
        ListElement {
            image: "qrc:/ui/pencil-box-outline.svg"
            showEntry: true
            showNext: true
            name: qsTr("Create message")
            funcName: "createMsg"
        }
        ListElement {
            image: "qrc:/ui/send-gov-msg.svg"
            showEntry: true
            showNext: true
            name: qsTr("Send e-gov request")
            funcName: "govServiceList"
        }
        ListElement {
            image: "qrc:/ui/file-import.svg"
            showEntry: true
            showNext: true
            name: qsTr("Import message")
            funcName: "importMsg"
        }
        ListElement {
            image: "qrc:/ui/account-search.svg"
            showEntry: true
            showNext: true
            name: qsTr("Find databox")
            funcName: "findBox"
        }
        ListElement {
            image: "qrc:/ui/database.svg"
            showEntry: true
            showNext: true
            name: qsTr("Back up downloaded data")
            funcName: "backupData"
        }
        ListElement {
            image: "qrc:/ui/database.svg"
            showEntry: true
            showNext: false
            name: qsTr("Clean up the file database")
            funcName: "cleanDb"
        }
        ListElement {
            image: "qrc:/ui/account-key.svg"
            showEntry: true
            showNext: false
            name: qsTr("Change password")
            funcName: "changePwd"
        }
        ListElement {
            image: "qrc:/ui/account-remove.svg"
            showEntry: true
            showNext: false
            name: qsTr("Delete account")
            funcName: "delAcnt"
        }
    }

    AccessibleMenu {
        id: accountMenuList
        anchors.top: headerBar.bottom
        anchors.bottom: parent.bottom
        width: parent.width
        funcArr: funcs
        model: accountMenuListModel
    }
}
