/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.iOsHelper 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {
    id: mainLayout

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null

    property bool iOS: false

    /* Clear import info and import results */
    function clearInfo() {
        infoText.text = ""
        progressText.text = ""
    }

    Component.onCompleted: {
        iOS = iOSHelper.isIos()
        clearInfo()
    }

    Component.onDestruction: {
        if (iOS) {
            iOSHelper.clearSendAndTmpDirs()
        }
    }

    /* File dialog for choosing of files/directories from the storage. */
    FileDialogue {
        id: fileDialogue
        multiSelect: true
        onFinished: {
            var files = []
            for (var j = 0; j < pathListModel.count; ++j) {
                files.push(pathListModel.get(j).path)
            }
            clearInfo()
            infoText.text = isds.importZfoMessages(acntId, files, verifyMessage.checked)
            pathListModel.clear()
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("ZFO message import")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
    }
    /* Object (associative array) holding functions. */
    property var funcs: {
        "zfoFile": function callZfoFile() {
            clearInfo()
            if (iOS) {
                iOSHelper.openDocumentPickerControllerForImport(IosImportAction.IMPORT_ZFOS, ["cz.nic.mobile-datovka.zfo"])
            } else {
                fileDialogue.raise(qsTr("Select ZFO files"), ["*.zfo"], true, "")
            }
        },
        "zfoDir": function callZfoDir() {
            clearInfo()
            if (iOS) {
                /* Import ZFOs from selected directory on iOS is not used now because the UIDocumentPickerViewController still has not support for this. */
                iOSHelper.openDocumentPickerControllerForImport(IosImportAction.IMPORT_ZFO_DIR, ["public.directory","public.folder"])
            } else {
                fileDialogue.raise(qsTr("Select import directory"), ["*.*"], false, "")
            }
        }
    }

    /* Import ZFO list model for iOS only. Import ZFOs from selected directory on iOS is not allowed now. */
    ListModel {
        id: importMenuListModeliOS
        ListElement {
            image: "qrc:/ui/datovka-file-zfo.svg"
            showEntry: true
            showNext: true
            name: qsTr("Import selected ZFO files")
            funcName: "zfoFile"
        }
    }

    /* Import ZFO list model for other platforms. */
    ListModel {
        id: importMenuListModel
        ListElement {
            image: "qrc:/ui/datovka-file-zfo.svg"
            showEntry: true
            showNext: true
            name: qsTr("Import selected ZFO files")
            funcName: "zfoFile"
        }
        ListElement {
            image: "qrc:/ui/datovka-folder-open.svg"
            showEntry: true
            showNext: true
            name: qsTr("Import ZFO files from directory")
            funcName: "zfoDir"
        }
    }

    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                AccessibleText {
                    color: datovkaPalette.mid
                    wrapMode: Text.Wrap
                    horizontalAlignment: Text.AlignHCenter
                    width: parent.width
                    text: qsTr("Here you can import messages from ZFO files into the local database.")
                }
                AccessibleSwitch {
                    id: verifyMessage
                    text: qsTr("Verify messages on the ISDS server")
                    font.pointSize: defaultTextFont.font.pointSize
                    checked: false
                    onClicked: {
                        clearInfo()
                        if (acntId === null) {
                            return
                        }
                        if (verifyMessage.checked && !isds.isLogged(acntId)) {
                            loginButton.visible = true;
                            loginText.visible = loginButton.visible
                        } else {
                            loginButton.visible = false;
                            loginText.visible = loginButton.visible
                        }
                    }
                }
                AccessibleText {
                    color: datovkaPalette.mid
                    wrapMode: Text.Wrap
                    width: parent.width
                    text: (verifyMessage.checked) ?
                        qsTr("Imported messages are going to be sent to the ISDS server where they are going to be checked. Messages failing this test won't be imported. We don't recommend using this option when you are using a mobile or slow internet connection.") :
                        qsTr("Imported messages won't be validated on the ISDS server.")
                }
                AccessibleText {
                    id: loginText
                    visible: false
                    color: datovkaPalette.mid
                    wrapMode: Text.Wrap
                    width: parent.width
                    text: qsTr("Account must be logged in to the data box to be able to verify messages.")
                }
                AccessibleButton {
                    id: loginButton
                    visible: false
                    height: inputItemHeight
                    font.pointSize: defaultTextFont.font.pointSize
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Log in now")
                    onClicked: isds.doIsdsAction("importZfoMessage", acntId)
                }
                AccessibleMenu {
                    id: importMenuList
                    height: 2 * delegateHeight
                    width: parent.width
                    funcArr: funcs
                    model: (iOS) ? importMenuListModeliOS : importMenuListModel
                }
                Text {
                    text: " "
                }
                AccessibleText {
                    id: infoText
                    wrapMode: Text.Wrap
                    horizontalAlignment: Text.AlignHCenter
                    width: parent.width
                    font.bold: true
                    text: ""
                }
                AccessibleText {
                    id: progressText
                    wrapMode: Text.Wrap
                    width: parent.width
                    font.pointSize: textFontSizeSmall
                    textFormat: TextEdit.RichText
                    text: ""
                }
            } // Column layout
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
    Connections {
        target: isds
        function onRunImportZfoMessageSig(userName, testing) {
            loginButton.visible = false;
            loginText.text = qsTr("Account is logged in to ISDS.") + " " + qsTr("Select ZFO files or a directory.")
            clearInfo()
        }
        function onZfoImportFinishedSig(txt) {
            infoText.text = txt
            accounts.loadModelCounters(accountModel)
        }
        function onZfoImportProgressSig(txt) {
            progressText.text = progressText.text + "<br/>" + txt
        }
    }
    Connections {
        target: iOSHelper
        function onZfoFilesSelectedSig(zfoFilePaths) {
            clearInfo()
            infoText.text = isds.importZfoMessages(acntId, zfoFilePaths, verifyMessage.checked)
        }
    }
}
