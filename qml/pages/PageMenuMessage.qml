/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {
    id: mainLayout

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null
    property int msgType
    property string msgId
    property bool canDeleteMsg
    property var messageModel: null

    Component.onCompleted: {
        var index = messageMenuList.get_model_index(messageMenuListModel, "funcName", "delMsg");
        if (index >= 0) {
            messageMenuListModel.setProperty(index, "showEntry", canDeleteMsg)
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Message") + ": " + msgId
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
    }

    /* Object (associative array) holding functions. */
    property var funcs: {
        "downlAttch": function callDownlAttch() {
            isds.doIsdsAction("downloadMessage", acntId)
            pageView.pop(StackView.Immediate)
        },
        "markRead": function callMarkRead() {
            messages.markMessageAsLocallyRead(messageModel, acntId, msgId, true)
            pageView.pop(StackView.Immediate)
        },
        "markUnread": function callMarkUnread() {
            messages.markMessageAsLocallyRead(messageModel, acntId, msgId, false)
            pageView.pop(StackView.Immediate)
        },
        "delMsg": function callDelMsg() {
            messages.deleteMessageFromDbs(accountModel, messageModel, acntId, msgId)
            pageView.pop(StackView.Immediate)
        }
    }

    ListModel {
        id: messageMenuListModel
        ListElement {
            image: "qrc:/ui/datovka-email-download.svg"
            showEntry: true
            showNext: false
            name: qsTr("Download attachments")
            funcName: "downlAttch"
        }
        ListElement {
            image: "qrc:/ui/delete.svg"
            showEntry: true
            showNext: false
            name: qsTr("Delete message")
            funcName: "delMsg"
        }
        ListElement {
            image: "qrc:/ui/email-open-outline.svg"
            showEntry: true
            showNext: false
            name: qsTr("Mark as read")
            funcName: "markRead"
        }
        ListElement {
            image: "qrc:/ui/email-outline.svg"
            showEntry: true
            showNext: false
            name: qsTr("Mark as unread")
            funcName: "markUnread"
        }
    }

    AccessibleMenu {
        id: messageMenuList
        anchors.top: headerBar.bottom
        anchors.bottom: parent.bottom
        width: parent.width
        funcArr: funcs
        model: messageMenuListModel
    }

    AcntId {
        id: actionAcntId
    }
    Connections {
        target: isds
        function onRunDownloadMessageSig(userName, testing) {
            actionAcntId.username = userName
            actionAcntId.testing = testing
            isds.downloadMessage(acntId, msgType, msgId)
        }
    }
}
