/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0

Item {
    id: mainLayout

    /* These properties must be set by caller. */
    property var pageView

    Component.onCompleted: {
        var index
        if (accountModel.rowCount() <= 0) {
            index = settingsMenuList.get_model_index(settingsMenuListModel, "funcName", "searchMsg");
            if (index >= 0) {
                settingsMenuListModel.setProperty(index, "showEntry", false)
            }
            index = settingsMenuList.get_model_index(settingsMenuListModel, "funcName", "importMsg");
            if (index >= 0) {
                settingsMenuListModel.setProperty(index, "showEntry", false)
            }
            index = settingsMenuList.get_model_index(settingsMenuListModel, "funcName", "settRecMan");
            if (index >= 0) {
                settingsMenuListModel.setProperty(index, "showEntry", false)
            }
            index = settingsMenuList.get_model_index(settingsMenuListModel, "funcName", "transferData");
            if (index >= 0) {
                settingsMenuListModel.setProperty(index, "showEntry", false)
            }
            index = settingsMenuList.get_model_index(settingsMenuListModel, "funcName", "backupAccounts");
            if (index >= 0) {
                settingsMenuListModel.setProperty(index, "showEntry", false)
            }
        }
        if (recordsManagement.isValidRecordsManagement()) {
            index = settingsMenuList.get_model_index(settingsMenuListModel, "funcName", "updateRecManData");
            if (index >= 0) {
                settingsMenuListModel.setProperty(index, "showEntry", true)
            }
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Settings")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
    }

    /* Object (associative array) holding functions. */
    property var funcs: {
        "addAcnt": function callAddAcnt() {
            pageView.replace(pageAccountWizard, {"pageView": pageView}, StackView.Immediate)
        },
        "searchMsg": function callSearchMsg() {
            pageView.replace(pageMessageSearch, {"pageView": pageView}, StackView.Immediate)
        },
        "importMsg": function callImportMsg() {
            pageView.replace(pageImportMessage, {
                "pageView": pageView,
                "acntName" : "",
                "acntId": null
            }, StackView.Immediate)
        },
        "updateRecManData": function callUpdateRecManData() {
            recordsManagement.getStoredMsgInfoFromRecordsManagement(settings.rmUrl(), settings.rmToken())
            pageView.pop(StackView.Immediate)
        },
        "settRecMan": function callSettRecMan() {
            pageView.replace(pageSettingsRecordsManagement, {"pageView": pageView}, StackView.Immediate)
        },
        "settGeneral": function callSettGeneral() {
            pageView.replace(pageSettingsGeneral, {"pageView": pageView}, StackView.Immediate)
        },
        "settSync": function callSettSync() {
            pageView.replace(pageSettingsSync, {"pageView": pageView}, StackView.Immediate)
        },
        "settStorage": function callSettStorage() {
            pageView.replace(pageSettingsStorage, {"pageView": pageView}, StackView.Immediate)
        },
        "settSecPin": function callSettSecPin() {
            pageView.replace(pageSettingsPin, {"pageView": pageView}, StackView.Immediate)
        },
        "transferData": function callTransferData() {
            pageView.replace(pageBackupData, {"pageView": pageView, "transfer": true}, StackView.Immediate)
        },
        "backupAccounts": function callBackupData() {
            pageView.replace(pageBackupData, {"pageView": pageView}, StackView.Immediate)
        },
        "restoreData": function callRestoreData() {
            pageView.replace(pageRestoreData, {"pageView": pageView}, StackView.Immediate)
        },
        "logViewer": function callLogViewer() {
            pageView.replace(pageLog, {"pageView": pageView}, StackView.Immediate)
        },
        "showLogPanel": function callShowLogPanel() {
            logBar.visible = true
            pageView.pop(StackView.Immediate)
        },
        "userGuide": function callUserGuide() {
            Qt.openUrlExternally("https://secure.nic.cz/files/datove_schranky/redirect/mobile-manual.html")
            pageView.pop(StackView.Immediate)
        },
        "aboutDatovka": function callAboutDatovka() {
            pageView.replace(pageAboutApp, {"pageView": pageView}, StackView.Immediate)
        }
    }

    ListModel {
        id: settingsMenuListModel
        ListElement {
            image: "qrc:/ui/account-plus.svg"
            showEntry: true
            showNext: true
            name: qsTr("Add account")
            funcName: "addAcnt"
        }
        ListElement {
            image: "qrc:/ui/magnify.svg"
            showEntry: true
            showNext: true
            name: qsTr("Search message")
            funcName: "searchMsg"
        }
        ListElement {
            image: "qrc:/ui/file-import.svg"
            showEntry: true
            showNext: true
            name: qsTr("Import message")
            funcName: "importMsg"
        }
        ListElement {
            image: "qrc:/ui/briefcase.svg"
            showEntry: false
            showNext: false
            name: qsTr("Update list of uploaded files")
            funcName: "updateRecManData"
        }
        ListElement {
            image: "qrc:/ui/briefcase.svg"
            showEntry: true
            showNext: true
            name: qsTr("Records Management")
            funcName: "settRecMan"
        }
        ListElement {
            image: "qrc:/ui/settings.svg"
            showEntry: true
            showNext: true
            name: qsTr("General")
            funcName: "settGeneral"
        }
        ListElement {
            image: "qrc:/ui/sync-all.svg"
            showEntry: true
            showNext: true
            name: qsTr("Synchronization")
            funcName: "settSync"
        }
        ListElement {
            image: "qrc:/ui/database.svg"
            showEntry: true
            showNext: true
            name: qsTr("Storage")
            funcName: "settStorage"
        }
        ListElement {
            image: "qrc:/ui/key-variant.svg"
            showEntry: true
            showNext: true
            name: qsTr("Security and PIN")
            funcName: "settSecPin"
        }
        ListElement {
            image: "qrc:/ui/mobile.svg"
            showEntry: true
            showNext: true
            name: qsTr("Transfer all application data")
            funcName: "transferData"
        }
        ListElement {
            image: "qrc:/ui/database.svg"
            showEntry: true
            showNext: true
            name: qsTr("Back up downloaded data")
            funcName: "backupAccounts"
        }
        ListElement {
            image: "qrc:/ui/database.svg"
            showEntry: true
            showNext: true
            name: qsTr("Restore data")
            funcName: "restoreData"
        }
        ListElement {
            image: "qrc:/ui/format-list-bulleted.svg"
            showEntry: true
            showNext: true
            name: qsTr("Log Viewer")
            funcName: "logViewer"
        }
        ListElement {
            image: "qrc:/ui/book-open.svg"
            showEntry: true
            showNext: false
            name: qsTr("Show log panel")
            funcName: "showLogPanel"
        }
        ListElement {
            image: "qrc:/ui/home.svg"
            showEntry: true
            showNext: false
            name: qsTr("User Guide")
            funcName: "userGuide"
        }
        ListElement {
            image: "qrc:/ui/information.svg"
            showEntry: true
            showNext: true
            name: qsTr("About Datovka")
            funcName: "aboutDatovka"
        }
    }

    AccessibleMenu {
        id: settingsMenuList
        anchors.top: headerBar.bottom
        anchors.bottom: parent.bottom
        width: parent.width
        funcArr: funcs
        model: settingsMenuListModel
    }
}
