/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.files 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.msgInfo 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {
    id: msgDetailPage

    /* These properties must be set by caller. */
    property var pageView
    property bool fromLocalDb: false
    property var rawZfoContent: null /* Raw ZFO file content. */
    property string acntName: ""
    property AcntId acntId: null
    property int msgDaysToDeletion
    property int msgType: MessageType.TYPE_RECEIVED
    property string msgId: ""
    property var messageModel: null

    /* These properties are set from within the page. */
    property string zfoId /*Prevents infinite loop that would be caused by setting msgId from within the page. */
    property string msgAnnotation
    property string msgDescrHtml /* Message HTML description. */
    property string emailBody
    property int zfoType: MsgInfo.TYPE_UNKNOWN

    function setTopButtonVisibility() {
        /* More than two visible icons causes problems on iOS devices. */
        msgEmailButton.visible = !fromLocalDb && (attachmentList.count > 0)
        attachmentSaveButon.visible = !fromLocalDb && (attachmentList.count > 0)
        msgDownloadButton.visible = fromLocalDb && (attachmentList.count === 0)
        attachmentMenuButon.visible = fromLocalDb && (attachmentList.count > 0)
    }

    function textColorRed(txt) {
        return "<div style=\"color:red\">" + txt + "</div>"
    }

    function showMsgDeletionInfo(msgDaysToDeletion) {
        var text = ""
        if (msgDaysToDeletion <= settings.msgDeletionNotifyAheadDays() && msgDaysToDeletion >0) {
            /*
             * actDTType 0 == Long term storage is not active.
             * -1 means DTInfo is unknown.
             */
            if (accounts.actDTType(acntId) <= 0) {
                text = qsTr("The message will be deleted from ISDS in %n day(s).", "", msgDaysToDeletion)
            } else {
                if (accounts.isDTCapacityFull(acntId)) {
                    text = qsTr("The message will be deleted from ISDS in %n day(s) because the long term storage is full.", "", msgDaysToDeletion)
                } else {
                    text = qsTr("The message will be moved to the long term storage in %n day(s) if the storage is not full. The message will be deleted from the ISDS server if the storage is full.", "", msgDaysToDeletion)
                }
            }
        } else if (msgDaysToDeletion === 0) {
            text = qsTr("The message has already been deleted from the ISDS.")
        } else {
            return
        }
        if (text !== "") {
            msgDescrHtml = textColorRed(text) + msgDescrHtml
        }
    }

    function fillContentFromDb() {
        if (!fromLocalDb && (rawZfoContent != null)) {
            /* This method also sets the attachment model. */
            var msgInfo = files.zfoData(attachmentModel, rawZfoContent)
            zfoId = msgInfo.idStr
            msgAnnotation = msgInfo.annotation
            msgDescrHtml = msgInfo.descrHtml
            emailBody = msgInfo.emailBody
            zfoType = msgInfo.type
            if (zfoType == MsgInfo.TYPE_UNKNOWN) {
                errorNotification.text = qsTr("File doesn't contain a valid message nor does it contain valid delivery information or the file is corrupt.")
                errorNotification.visible = true
                flickable.visible = false
            }
            attachmentArea.visible = zfoType == MsgInfo.TYPE_MESSAGE
            emptyList.visible = zfoType == MsgInfo.TYPE_MESSAGE
            rawZfoContent = null /* There is no need to remember this. */
        } else if (fromLocalDb && (acntId.username.length != 0) && (msgId.length != 0)) {
            zfoId = msgId
            msgDescrHtml = messages.getMessageDetail(acntId, zfoId)
            attachmentModel.setFromDb(acntId, zfoId)
            zfoType = MsgInfo.TYPE_MESSAGE
            showMsgDeletionInfo(msgDaysToDeletion)
        }
        emptyList.visible = (attachmentList.count === 0)
        setTopButtonVisibility()
    }

    onRawZfoContentChanged: {
        fillContentFromDb()
    }

    onAcntIdChanged: {
        fillContentFromDb()
    }

    onMsgIdChanged: {
        fillContentFromDb()
    }

    FileListModel {
        id: attachmentModel
        Component.onCompleted: {
        }
    }

    Connections {
        target: isds
        function onDownloadMessageFinishedSig(msgId) {
            fillContentFromDb()
            messages.overrideDownloaded(messageModel, msgId, true);
        }
    }

    Connections {
        target: messages
        function onUpdateMessageDetail(msgHtmlInfo) {
            msgDescrHtml = msgHtmlInfo
        }
    }

    PageHeader {
        id: headerBar
        title: {
            var str = "";
            if (fromLocalDb && (msgType == MessageType.TYPE_RECEIVED)) {
               str += qsTr("Received") + ": " + zfoId
            } else if (fromLocalDb && (msgType == MessageType.TYPE_SENT)) {
               str += qsTr("Sent") + ": " + zfoId
            } else if (zfoType == MsgInfo.TYPE_MESSAGE) {
               str += qsTr("ZFO")  + ": " + zfoId
            } else if (zfoType == MsgInfo.TYPE_DELIVERY_INFO) {
               str += qsTr("ZFO")  + ": " + zfoId
            } else if (zfoType != MsgInfo.TYPE_UNKNOWN) {
                str += qsTr("Message ID") + ": " + zfoId
            } else {
               str += qsTr("Unknown ZFO content")
            }
            return str
        }
        onBackClicked: pageView.pop()
        Row {
            anchors.verticalCenter: parent.verticalCenter
            spacing: defaultMargin
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            AccessibleOverlaidImageButton {
                id: msgEmailButton
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/reply.svg"
                accessibleName: qsTr("Email attachments")
                onClicked: {
                    files.sendAttachmentEmailZfo(attachmentModel, zfoId, msgAnnotation, emailBody)
                }
            }
            AccessibleOverlaidImageButton {
                id: attachmentSaveButon
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/save-to-disk.svg"
                accessibleName: qsTr("Save attachments.")
                onClicked: {
                    if (!fromLocalDb) {
                        files.saveAttachmentsToDiskZfo(attachmentModel, zfoId)
                    } else {
                        files.saveMsgFilesToDisk(acntId, zfoId, MsgAttachFlag.MSG_ATTACHS)
                    }
                }
            }
            AccessibleOverlaidImageButton {
                id: msgDownloadButton
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/datovka-email-download.svg"
                accessibleName: qsTr("Download message")
                onClicked: {
                    if (fromLocalDb) {
                        isds.doIsdsAction("downloadMessage", acntId)
                    }
                }
            }
            AccessibleOverlaidImageButton {
                id: attachmentMenuButon
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/menu.svg"
                accessibleName: qsTr("Show menu of available operations")
                onClicked: {
                    statusBar.visible = false
                    if (fromLocalDb) {
                        pageView.push(pageMenuMessageDetail, {
                                "pageView": pageView,
                                "acntName": acntName,
                                "acntId": acntId,
                                "msgType": msgType,
                                "msgId": zfoId,
                                "messageModel": messageModel,
                                "attachmentModel" : attachmentModel
                            }, StackView.Immediate)
                    }
                }
            }
        }
    } // PageHeader
    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        contentHeight: flickContent.implicitHeight
        height: (zfoType == MsgInfo.TYPE_MESSAGE) ? parent.height * 0.4 : parent.height - baseHeaderHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            background: Rectangle {
                color: datovkaPalette.base
            }
            AccessibleText {
                width: parent.width
                textFormat: TextEdit.RichText
                wrapMode: Text.WordWrap
                text: msgDescrHtml
            }
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
    Rectangle {
        id: attachmentArea
        anchors.top: flickable.bottom
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        color: datovkaPalette.window
        Rectangle {
            anchors.top: parent.top
            height: 1
            width: parent.width
            color: datovkaPalette.dark
        }
        Item {
            id: attachmentLabel
            width: parent.width
            height: baseHeaderHeight
            AccessibleText {
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: defaultMargin * 2
                textFormat: TextEdit.RichText
                text: "<h3>" + qsTr("Attachments") + "</h3>"
                font.bold: true
            }
            Rectangle {
                anchors.bottom: parent.bottom
                height: 1
                width: parent.width
                color: datovkaPalette.dark
            }
        }
        ScrollableListView {
            id: attachmentList

            delegateHeight: baseHeaderHeight * 1.1

            anchors.top: attachmentLabel.bottom
            anchors.bottom: parent.bottom
            visible: true
            width: parent.width
            model: attachmentModel
            onCountChanged: {
                emptyList.visible = (attachmentList.count === 0)
                setTopButtonVisibility()
            }
            delegate: Rectangle {
                id: attachmentItem
                width: attachmentList.width
                height: attachmentList.delegateHeight
                color: datovkaPalette.base
                Image {
                    id: imageAttachment
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.leftMargin: defaultMargin
                    sourceSize.height: imageActionDimension * 1.4
                    source: files.getAttachmentFileIcon(rFileName)
                }
                ColumnLayout {
                    id: databoxColumn
                    anchors.left: imageAttachment.right
                    anchors.leftMargin: defaultMargin
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: defaultMargin
                    Text {
                        id: fileNameId
                        Layout.preferredWidth: parent.width - nextNavElement.width
                        elide: Text.ElideRight
                        text: rFileName
                        color: datovkaPalette.text
                        font.bold: true
                    }
                    Text {
                        id: fileSizeId
                        Layout.preferredWidth: parent.width - nextNavElement.width
                        text: rFileSizeStr
                        color: imageDarkerColor
                        font.pointSize: textFontSizeSmall
                    }
                }
                NextOverlaidImage {
                    id: nextNavElement
                }
                MouseArea {
                    function handleClick() {
                        locker.ignoreNextSuspension()
                        if (files.isZfoFile(rFileName)) {
                            console.log("Attachment is ZFO: " + rFileName)
                            var fileContent = null
                            if (!fromLocalDb) {
                                fileContent = rBinaryContent
                            } else {
                                fileContent = files.getFileRawContentFromDb(acntId, rFileId)
                            }
                            pageView.push(pageMessageDetail, {
                                    "pageView": pageView,
                                    "fromLocalDb": false,
                                    "rawZfoContent": fileContent
                                })
                        } else {
                            if (!fromLocalDb) {
                                files.openAttachment(rFileName, rBinaryContent)
                            } else {
                                files.openAttachmentFromDb(acntId, rFileId)
                            }
                        }
                    }

                    anchors.fill: parent

                    Accessible.role: Accessible.Button
                    Accessible.name: qsTr("Open attachment '%1'.").arg(rFileName)
                    Accessible.onScrollDownAction: attachmentList.scrollDown()
                    Accessible.onScrollUpAction: attachmentList.scrollUp()
                    Accessible.onPressAction: {
                        handleClick()
                    }
                    onClicked: {
                        handleClick()
                    }
                }
                Rectangle {
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    height: 1
                    width: (isiOS && attachmentList.count-1 !== index) ? fileNameId.width + nextNavElement.width : parent.width
                    color: (attachmentList.count-1 === index) ? datovkaPalette.dark : datovkaPalette.mid
                }
            }
            property bool downloadStart: false
            //onContentYChanged: // whenever position changes
            //onVerticalVelocityChanged:
            //onFlickEnded: // similar to onDragEnded but shorter distance
            onMovementEnded: {
                if (downloadStart) {
                    downloadStart = false
                    if (fromLocalDb) {
                        isds.doIsdsAction("downloadMessage", acntId)
                    }
                }
            }
            onDragEnded: {
                downloadStart = contentY < -120
            }
            ScrollIndicator.vertical: ScrollIndicator {}
        } // Listview
        AccessibleTextButton {
            id: emptyList
            visible: false
            color: (fromLocalDb && isiOS) ? actionIconColor : datovkaPalette.text
            anchors.fill: parent
            anchors.margins: defaultMargin
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap
            text: fromLocalDb ? qsTr("Attachments have not been downloaded yet.\nClick the icon or this text for their download.") : qsTr("No attachments present.")
            onClicked: {
                if (fromLocalDb) {
                    isds.doIsdsAction("downloadMessage", acntId)
                }
            }
        } // Text
    } // Rectangle
    AccessibleText {
        id: errorNotification
        visible: false
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WordWrap
        text: ""
    } // Text
    AcntId {
        id: actionAcntId
    }
    Connections {
        target: isds
        function onRunDownloadMessageSig(userName, testing) {
            if (fromLocalDb) {
                actionAcntId.username = userName
                actionAcntId.testing = testing
                isds.downloadMessage(actionAcntId, msgType, zfoId)
            }
        }
    }
}
