/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {
    id: mainLayout

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null
    property int msgType
    property var messageModel: null

    PageHeader {
        id: headerBar
        title: qsTr("Message operations")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
    }

    /* Object (associative array) holding functions. */
    property var funcs: {
        "markRead": function callMarkRead() {
            messages.markMessagesAsLocallyRead(messageModel, acntId, msgType, true)
            pageView.pop(StackView.Immediate)
        },
        "markUnread": function callMarkUnread() {
            messages.markMessagesAsLocallyRead(messageModel, acntId, msgType, false)
            pageView.pop(StackView.Immediate)
        }
    }

    ListModel {
        id: messagesMenuListModel
        ListElement {
            image: "qrc:/ui/email-open-outline.svg"
            showEntry: true
            showNext: false
            name: qsTr("Mark messages as read")
            funcName: "markRead"
        }
        ListElement {
            image: "qrc:/ui/email-outline.svg"
            showEntry: true
            showNext: false
            name: qsTr("Mark messages as unread")
            funcName: "markUnread"
        }
    }

    AccessibleMenu {
        id: messagesMenuList
        anchors.top: headerBar.bottom
        anchors.bottom: parent.bottom
        width: parent.width
        funcArr: funcs
        model: messagesMenuListModel
    }
}
