/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {
    id: pageChangePassword

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null
    property var loginMethod

    function changePwdSettings() {
            loginMethod = isds.getAccountLoginMethod(acntId)
            errLineText.visible = false
            actionButton.enabled = true
            loginButton.visible = false;
            if (loginMethod === AcntData.LIM_UNAME_PWD_TOTP) {
                topLineText.text = qsTr("Account is logged in to ISDS.") + " " + qsTr("Enter current password, twice a new password and SMS code.")
                otpCode.placeholderText =  qsTr("Enter SMS code")
                otpCode.visible = true
                sendSmsButton.visible = true
                sendSmsButton.enabled = true
            } else if (loginMethod === AcntData.LIM_UNAME_PWD_HOTP) {
                topLineText.text = qsTr("Account is logged in to ISDS.") + " " + qsTr("Enter current password, twice a new password and security code.")
                otpCode.placeholderText =  qsTr("Enter security code")
                otpCode.visible = true
                sendSmsButton.visible = false
            } else {
                topLineText.text = qsTr("Account is logged in to ISDS.") + " " + qsTr("Enter current password and twice a new password.")
                otpCode.visible = false
                sendSmsButton.visible = false
            }
    }

    function showHidePasswords(showPwd) {
        oldPwd.echoMode = showPwd ? TextInput.Normal : TextInput.Password
        newPwd.echoMode = oldPwd.echoMode
        newPwdAgain.echoMode = oldPwd.echoMode
    }

    function showHidePwdButton() {
        pwdButton.visible = (oldPwd.text.length > 0 || newPwd.text.length > 0 || newPwdAgain.text.length > 0)
    }

    Component.onCompleted: {
        if (!isds.isLogged(acntId)) {
            actionButton.enabled = false
            loginButton.visible = true;
            topLineText.text = qsTr("Account must be logged in to the data box to be able to change the password.")
        } else {
            changePwdSettings()
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Change password") + ": " + acntId.username
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        Row {
            anchors.verticalCenter: parent.verticalCenter
            spacing: defaultMargin
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            AccessibleOverlaidImageButton {
                id: pwdButton
                property bool showPwd: false
                visible: false
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: showPwd ? "qrc:/ui/eye-pwd-hide.svg" : "qrc:/ui/eye-pwd-show.svg"
                accessibleName: showPwd ?  qsTr("Hide passwords") : qsTr("Show passwords")
                onClicked: {
                    showPwd = !showPwd
                    showHidePasswords(showPwd)
                }
            }
            AccessibleOverlaidImageButton {
                id: actionButton
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/checkbox-marked-circle.svg"
                accessibleName: qsTr("Accept changes")
                onClicked: {
                    if (oldPwd.text === "" || newPwd.text === "") {
                        errLineText.text = qsTr("Old and new password must both be filled in.")
                        errLineText.visible = true
                        return
                    }
                    if (newPwd.text !== newPwdAgain.text) {
                        errLineText.text = qsTr("The new password fields do not match.")
                        errLineText.visible = true
                        return
                    }
                    if (!isds.isCorrectPassword(newPwd.text.toString())) {
                        errLineText.text = qsTr("Wrong password format. The new password must contain at least 8 characters including at least 1 number and at least 1 upper-case letter.")
                        errLineText.visible = true
                        return
                    }
                    if (loginMethod === AcntData.LIM_UNAME_PWD_TOTP || loginMethod === AcntData.LIM_UNAME_PWD_HOTP)  {
                        if (otpCode.text === "") {
                            errLineText.text = qsTr("Security/SMS code must be filled in.")
                            errLineText.visible = true
                            return
                        }
                     }
                     errLineText.visible = false
                    if (isds.changePassword(acntId, oldPwd.text.toString(), newPwd.text.toString(), otpCode.text.toString())) {
                        settings.saveAllSettings(accountModel)
                        pageView.pop(StackView.Immediate)
                    } else {
                        errLineText.text = qsTr("Password change failed.")
                        errLineText.visible = true
                    }
                }
            }
        }
    } // PageHeader
    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                AccessibleText {
                    id: topLineText
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: datovkaPalette.mid
                    width: parent.width
                    text: qsTr("Enter the current password and a new password.")
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.Wrap
                }
                AccessibleButton {
                    id: loginButton
                    visible: false
                    height: inputItemHeight
                    font.pointSize: defaultTextFont.font.pointSize
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Log in now")
                    onClicked: isds.doIsdsAction("changePassword", acntId)
                }
                Column {
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width / 2
                    spacing: formItemVerticalSpacing
                    AccessibleTextField {
                        id: oldPwd
                        anchors.left: parent.left
                        anchors.right: parent.right
                        height: inputItemHeight
                        font.pointSize: defaultTextFont.font.pointSize
                        echoMode: TextInput.Password
                        passwordMaskDelay: 500 // milliseconds
                        inputMethodHints: Qt.ImhNone
                        placeholderText: qsTr("Current password")
                        horizontalAlignment: TextInput.AlignHCenter
                        onTextChanged: showHidePwdButton()
                        InputLineMenu {
                            id: oldPwdMenu
                            inputTextControl: oldPwd
                            isPassword: true
                        }
                        onPressAndHold: {
                            if (settings.useExplicitClipboardOperations()) {
                                oldPwdMenu.implicitWidth = computeMenuWidth(oldPwdMenu)
                                oldPwdMenu.open()
                            }
                        }
                    }
                    AccessibleTextField {
                        id: newPwd
                        anchors.left: parent.left
                        anchors.right: parent.right
                        height: inputItemHeight
                        font.pointSize: defaultTextFont.font.pointSize
                        echoMode: TextInput.Password
                        passwordMaskDelay: 500 // milliseconds
                        inputMethodHints: Qt.ImhNone
                        placeholderText: qsTr("New password")
                        horizontalAlignment: TextInput.AlignHCenter
                        onTextChanged: showHidePwdButton()
                        InputLineMenu {
                            id: newPwdMenu
                            inputTextControl: newPwd
                            isPassword: true
                        }
                        onPressAndHold: {
                            if (settings.useExplicitClipboardOperations()) {
                                newPwdMenu.implicitWidth = computeMenuWidth(newPwdMenu)
                                newPwdMenu.open()
                            }
                        }
                    }
                    AccessibleTextField {
                        id: newPwdAgain
                        anchors.left: parent.left
                        anchors.right: parent.right
                        height: inputItemHeight
                        font.pointSize: defaultTextFont.font.pointSize
                        echoMode: TextInput.Password
                        passwordMaskDelay: 500 // milliseconds
                        inputMethodHints: Qt.ImhNone
                        placeholderText: qsTr("Confirm the new password")
                        horizontalAlignment: TextInput.AlignHCenter
                        onTextChanged: showHidePwdButton()
                        InputLineMenu {
                            id: newPwdAgainMenu
                            inputTextControl: newPwdAgain
                            isPassword: true
                        }
                        onPressAndHold: {
                            if (settings.useExplicitClipboardOperations()) {
                                newPwdAgainMenu.implicitWidth = computeMenuWidth(newPwdAgainMenu)
                                newPwdAgainMenu.open()
                            }
                        }
                    }
                    AccessibleButton {
                        id: sendSmsButton
                        visible: false
                        enabled: false
                        height: inputItemHeight
                        font.pointSize: defaultTextFont.font.pointSize
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: qsTr("Send SMS code")
                        onClicked: {
                            if (oldPwd.text === "") {
                                errLineText.text = qsTr("Enter the current password to be able to send the SMS code.")
                                errLineText.visible = true
                                sendSmsButton.enabled = true
                                sendSmsButton.visible = true
                            } else {
                                if (isds.sendSMS(acntId, oldPwd.text.toString())) {
                                    errLineText.text = qsTr("SMS code was sent...")
                                    errLineText.visible = true
                                    sendSmsButton.enabled = false
                                    sendSmsButton.visible = false
                                } else {
                                    errLineText.text = qsTr("SMS code cannot be sent. Entered wrong current password or ISDS server is out of order.")
                                    errLineText.visible = true
                                    sendSmsButton.enabled = true
                                    sendSmsButton.visible = true
                                }
                            }
                        }
                    }
                    AccessibleTextField {
                        id: otpCode
                        visible: false
                        anchors.left: parent.left
                        anchors.right: parent.right
                        height: inputItemHeight
                        font.pointSize: defaultTextFont.font.pointSize
                        echoMode: TextInput.Normal
                        passwordMaskDelay: 500 // milliseconds
                        inputMethodHints: Qt.ImhPreferNumbers
                        placeholderText: qsTr("Enter OTP code")
                        horizontalAlignment: TextInput.AlignHCenter
                        text: ""
                    }
                }
                AccessibleText {
                    id: errLineText
                    visible: false
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.bold: true
                    width: parent.width
                    text: ""
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.Wrap
                }
                AccessibleText {
                    id: bottomLineText
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: datovkaPalette.mid
                    width: parent.width
                    text: qsTr("This will change the password on the ISDS server. You won't be able to log in to ISDS without the new password. If your current password has already expired then you must log into the ISDS web portal to change it.")
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.Wrap
                }
                AccessibleText {
                    id: bottomLineText2
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: datovkaPalette.mid
                    width: parent.width
                    text: qsTr("If you just want to update the password because it has been changed elsewhere then go to the ") + qsTr("Account settings")+ "."
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.Wrap
                }
                AccessibleText {
                    id: bottomLineText3
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: datovkaPalette.mid
                    width: parent.width
                    text: qsTr("Keep a copy of the login credentials and store it on a safe place which only an authorised person has access to.")
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.Wrap
                }
            } // Column layout
            Connections {
                target: isds
                function onRunChangePasswordSig() {
                    changePwdSettings()
                }
            }
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
} // Item
