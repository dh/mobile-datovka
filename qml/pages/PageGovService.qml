/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {
    id: pageGovService

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null
    property string gsInternId
    property string gsFullName
    property string gsInstName

    Component.onCompleted: {
        gov.loadFormToModel(acntId, gsInternId, govFormModel)
        actionButton.enabled = govFormModel.haveAllMandatory()
        userMandatoryText.visible = govFormModel.containsMandatoryUser()
        fromDataBoxText.visible = govFormModel.containsBoxOwnerData()
    }

    GovFormListModel {
        id: govFormModel

        onDataChanged: {
            actionButton.enabled = govFormModel.haveAllMandatory()
            userMandatoryText.visible = govFormModel.containsMandatoryUser()
            fromDataBoxText.visible = govFormModel.containsBoxOwnerData()
        }

        onValidityNotification: {
            validityNotification.text = message
            actionButton.enabled = govFormModel.haveAllMandatory()
        }
    }

    /* Makes date selection easier. */
    CalendarDialogue {
        id: calendarDialogue

        onFinished: {
             var locale = Qt.locale()
             var value = selectedDate.toLocaleDateString(locale, "yyyy-MM-dd")
             govFormModel.setProperty(index, "gsVal", value)
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Service:") + " " + gsInternId
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        AccessibleOverlaidImageButton {
            id: actionButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            image.sourceSize.height: imageActionDimension
            image.source: "qrc:/ui/send-msg.svg"
            accessibleName: qsTr("Send request")
            onClicked: {
                if (govFormModel.haveAllValid()) {
                    isds.doIsdsAction("sendGovMessage", acntId)
                }
            }
        }
    }
    Pane {
        id: formPane
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        Column {
            anchors.right: parent.right
            anchors.left: parent.left
            spacing: formItemVerticalSpacing
            AccessibleText {
                font.bold: false
                width: parent.width
                wrapMode: Text.Wrap
                text: qsTr("Account:") + " " + acntName
            }
            AccessibleText {
                font.bold: false
                width: parent.width
                wrapMode: Text.Wrap
                text: qsTr("Request:") + " " + gsFullName
            }
            AccessibleText {
                font.bold: false
                width: parent.width
                wrapMode: Text.Wrap
                text: gsInstName
            }
            Text {
                font.bold: false
                text: " "
            }
            AccessibleText {
                id: validityNotification
                font.bold: false
                color: "#ffff0000" // Red
                width: parent.width
                wrapMode: Text.Wrap
                text: ""
                visible: validityNotification.text !== ""
            }
            AccessibleText {
                id: userMandatoryText
                font.bold: false
                width: parent.width
                wrapMode: Text.Wrap
                text: "(*) " + qsTr("Required information")
            }
            AccessibleText {
                id: fromDataBoxText
                font.bold: false
                width: parent.width
                wrapMode: Text.Wrap
                text: "(**) " + qsTr("Acquired from data box")
            }
        }
    }
    AccessibleText {
        id: emptyList
        visible: false
        anchors.centerIn: parent
        wrapMode: Text.Wrap
        text: qsTr("No user data needed.")
    }
    GovFormList {
        id: formList
        anchors.top: formPane.bottom
        anchors.bottom: parent.bottom
        visible: true
        width: parent.width
        model: govFormModel
        onCountChanged: {
            emptyList.visible = (formList.count === 0)
        }
        onCalendarClicked: {
            calendarDialogue.openCalendarDialogue(index, dateVal, qsTr("Select date") + " " + dateDescr)
        }
    }

    AcntId {
        id: actionAcntId
    }
    Connections {
        target: isds
        function onRunSendGovMessageSig(userName, testing) {
            actionButton.enabled = false
            actionAcntId.username = userName
            actionAcntId.testing = testing
            if (gov.sendGovRequest(actionAcntId, gsInternId, govFormModel)) {
                pageView.pop(null, StackView.Immediate) // back to account list
            } else {
                actionButton.enabled = govFormModel.haveAllMandatory()
            }
        }
    }
} // Item
